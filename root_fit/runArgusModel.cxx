void runArgusModel(){
	using namespace RooFit;

	RooRealVar mes("mes", "m_{ES} (GeV)",5.20,5.30);

	RooRealVar sigmean("sigmean"," B^{#pm} mass", 5.28, 5.20, 5.30);
	RooRealVar sigwidth("sigwidth", "B^{#pm} width", 0.0027, 0.001, 1.);

	RooGaussian signalModel("signal","signal PDF", mes, sigmean, sigwidth);

	RooRealVar argpar("argpar", "argus shape parameter", -20.0, -100., -1.);
	RooArgusBG background("background", "Argus PDF", mes, RooConst(5.291), argpar);

	RooRealVar nsig("nsig", "#signal events", 200, 0., 10000);
	RooRealVar nbkg("nbkg", "#background events", 800, 0., 10000);
	RooAddPdf model("model", "g+a", {signalModel, background}, {nsig, nbkg});

	std::unique_ptr<RooDataSet> data{model.generate(mes, 2000)};

	model.fitTo(*data);

	RooPlot *mesframe =mes.frame();

	data->plotOn(mesframe);
	model.plotOn(mesframe);
	model.plotOn(mesframe, Components(background), LineStyle(ELineStyle::kDashed));
	model.plotOn(mesframe, Components(signalModel), LineStyle(ELineStyle::kDashed), LineColor(kGreen));
	TCanvas *c1 = new TCanvas("c1","",800,600);
	c1->cd();
	mesframe->Draw();
	c1->SaveAs("rootfit.png");
}
