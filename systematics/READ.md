-----------------------------------------/* F1 */--------------------------------------------------

before determining the Smirnov transformation.
ratio of the tight-ID efficiency with and without precuts on f1,determined using MC simulaiton.



-----------------------------------------/* non Closure	test	 */--------------------------------------------------
it's used to quantify the impat of the preservation of electron shower-shape correlations - as well as the impact of different kinematic distributions of electrons from Z decays and prompt photons



After applying Smirnov transforamtion

it's quantified by the efficiency difference between MC photon and pseudo-photons |ε_MCphoton-ε_MC,eletron,transf.|


-----------------------------------------/* background */--------------------------------------------------
template-based




-----------------------------------------/* EEconversion */--------------------------------------------------
up-down showershape

before applying Smirnov tranformation

If true converted photons is reconstructed as unconverted photons and vice versa, then this will lead to a mis-modelling of the Smirnov transformation computed from simulaiton.

In order to estimate this effect, we vary in a conservative way the truth conversion-fraction in the simulation by increasing or decreasing it by 10%.

The transformations resulting from the variation of the simulated samples are then applied on the electrons from MC and the difference in efficiency is computed.


Absolute difference in photon ID efficiency between the up and down variation fo the truth conversion by a factor of 10%.



-----------------------------------------/* Fragmentation */--------------------------------------------------
up-down 50% weight of fragmentation on MC, then redo Smirnov


fragmentation photons, which originate from the fragmentation of hish-pt partons.

fragmentation photons, as a result of the differing production kinematics, tend to be less isolated than direct photons, and their showre shape distributions are usually broader than those of direct


photons, and their shower shape distributions are usually broader than those of direct photons, as well as of (prompt) electrons . As a consequence, any mis-modelling of the fragmentaion fraction

in the photon MC sample used in the derivation of the Smirnov transformaitons could introduce a bias in the transformation itself and thus a bias in the measured efficiency.


The extent of this effect is studied by varying the fragmentation fraction in the MC sample up and down by 50%. The down-variation is achieved by decreasing the weight of fragmentation photons from

the MC sample by 50%. The up-variation is performed by increasing the weight of the fragmentation photons by 50%.


Absolute difference of photon ID efficiencies from varyign the fraction of fragmentation photons up and down by 50% in the MC sample.



-----------------------------------------/* statMC  */--------------------------------------------------
give mc a weight

bootstrap method

In the limit of large statistical uncertainties in the MC shower shape, particularly in the tails of the distributions. The larger the MC sample is, the smoother the shower-shape distributions are and 

the closer the resulting SMirnoc transformations are to their "true" shape.


For each shower shape distribution, a set of toys are created. These shower-shpe distributions are created based on the original distribution from the MC sample by assigning to each electron and photon

a weight drawn from a Possion distribution of mean 1. A Smirnov transformation is extracted from each of these toy variable distributions, The larger the MC sample, the smaller the influence of this

reweighting onthe resulting shower-shape.





-----------------------------------------/* Fudge factor */--------------------------------------------------

before deriving the Smirnov Transformation

In order to mitigate the differences between MC and data shower-shape distribution, correction factors for MC samples have been derived.

The correction of MC simulation is not only applied in order to compute the MC photon identification efficiencies and the scale factors,

it's also applied in the process of deriving Smirnov transformations.

Unfudged MC leads to a distribution wichi does not mathch the original phootn shower shape and therefore, to a bad estimate od the photon ID efficiency.

Therefore it's necessary to apply fudge factors on electron shower shapes before deriving the Smirnov transformations.

Since it's known that the photon shower-shape distributions from MC simulaitons hould be corrected in order to approximate the data distributions ,

fudge factors are also applied on photons before extracting the transformations.


Shower shape variables are divided into four groups.


