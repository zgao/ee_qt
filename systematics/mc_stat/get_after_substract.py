import os 
eta_names_f0 = ["0 < |eta| < 0.6","0.6 < |eta| < 0.8","0.8 < |eta| < 1.15","1.15 < |eta| < 1.37","1.52 < |eta| < 1.81","1.81 < |eta| < 2.01","2.01 < |eta| < 2.37"]
def list_txt_files(directory):
    txt_files = [f for f in os.listdir(directory) if f.endswith('txt')]
    return txt_files

directory_path1 = 'con/70_110/truth/'
directory_path2 = 'con/80_100/truth/'
txt_files1 = list_txt_files(directory_path1)
txt_files2 = list_txt_files(directory_path2)
#  print(txt_files1)
#  print(txt_files2)

entries_f1 = [[[[0 for k in range(2)] for i in range(12) ] for j in range(7)] for l in range(len(txt_files1))]
for i in range(len(txt_files1)):
    
    with open(directory_path1+txt_files1[i],"r") as f1:
        for j,line in enumerate(f1):
            line =line.strip()
            lines = line.split()
            #  print(i,line)

            if j%13==0:
                continue
            entries_f1[i][int(j/13)][j%13-1][0] = float(lines[0])
            entries_f1[i][int(j/13)][j%13-1][1] = float(lines[1])
    #  print(entries_f1[0])
    #  print(entries_f1[6])
    #  exit()

entries_f2 = [[[[0 for k in range(2)] for i in range(12) ] for j in range(7)] for l in range(len(txt_files2))]
for i in range(len(txt_files2)):
    
    with open(directory_path2+txt_files2[i],"r") as f2:
        for j,line in enumerate(f2):
            line =line.strip()
            lines = line.split()
            #  print(i,line)

            if j%13==0:
                continue
            entries_f2[i][int(j/13)][j%13-1][0] = float(lines[0])
            entries_f2[i][int(j/13)][j%13-1][1] = float(lines[1])

for k in range(len(txt_files1)):
    with open('con/final/truth/'+txt_files1[k],'w') as f3:
        for i in range(7):
            f3.write(f"{eta_names_f0[i]}\n")
            for j in range(12):
                f3.write(f"{entries_f2[k][i][j][0]-abs(entries_f1[k][i][j][0]-entries_f2[k][i][j][0])}\t\t\t{entries_f2[k][i][j][1]-abs(entries_f1[k][i][j][1]-entries_f2[k][i][j][1])}\n")
                #  print(entries_f2[k][i][j][0]-abs(entries_f1[k][i][j][0]-entries_f2[k][i][j][0]), entries_f2[k][i][j][1]-abs(entries_f1[k][i][j][1]-entries_f2[k][i][j][1]))
                #  print(entries_f2[k][i][j][0],entries_f1[k][i][j][0],entries_f2[k][i][j][1],entries_f1[k][i][j][1])
                #  print(txt_files1[k])
            #  break
        #  break

    f3.close()
    print(f"con/final/truth/{txt_files1[k]} is created")

#------
directory_path1 = 'unc/70_110/truth/'
directory_path2 = 'unc/80_100/truth/'
txt_files1 = list_txt_files(directory_path1)
txt_files2 = list_txt_files(directory_path2)
#  print(txt_files1)
#  print(txt_files2)

entries_f1 = [[[[0 for k in range(2)] for i in range(12) ] for j in range(7)] for l in range(len(txt_files1))]
for i in range(len(txt_files1)):
    
    with open(directory_path1+txt_files1[i],"r") as f1:
        for j,line in enumerate(f1):
            line =line.strip()
            lines = line.split()
            #  print(i,line)

            if j%13==0:
                continue
            entries_f1[i][int(j/13)][j%13-1][0] = float(lines[0])
            entries_f1[i][int(j/13)][j%13-1][1] = float(lines[1])
    #  print(entries_f1[0])
    #  print(entries_f1[6])
    #  exit()

entries_f2 = [[[[0 for k in range(2)] for i in range(12) ] for j in range(7)] for l in range(len(txt_files2))]
for i in range(len(txt_files2)):
    
    with open(directory_path2+txt_files2[i],"r") as f2:
        for j,line in enumerate(f2):
            line =line.strip()
            lines = line.split()
            #  print(i,line)

            if j%13==0:
                continue
            entries_f2[i][int(j/13)][j%13-1][0] = float(lines[0])
            entries_f2[i][int(j/13)][j%13-1][1] = float(lines[1])

for k in range(len(txt_files1)):
    with open('unc/final/truth/'+txt_files1[k],'w') as f3:
        for i in range(7):
            f3.write(f"{eta_names_f0[i]}\n")
            for j in range(12):
                f3.write(f"{entries_f2[k][i][j][0]-abs(entries_f1[k][i][j][0]-entries_f2[k][i][j][0])}\t\t\t{entries_f2[k][i][j][1]-abs(entries_f1[k][i][j][1]-entries_f2[k][i][j][1])}\n")
                #  print(entries_f2[k][i][j][0]-abs(entries_f1[k][i][j][0]-entries_f2[k][i][j][0]), entries_f2[k][i][j][1]-abs(entries_f1[k][i][j][1]-entries_f2[k][i][j][1]))
                #  print(entries_f2[k][i][j][0],entries_f1[k][i][j][0],entries_f2[k][i][j][1],entries_f1[k][i][j][1])
                #  print(txt_files1[k])
            #  break
        #  break

    f3.close()
    print(f"unc/final/truth/{txt_files1[k]} is created")
