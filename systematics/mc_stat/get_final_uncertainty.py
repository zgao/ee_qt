import os
import math
eta_names_f0 = ["0 < |eta| < 0.6","0.6 < |eta| < 0.8","0.8 < |eta| < 1.15","1.15 < |eta| < 1.37","1.52 < |eta| < 1.81","1.81 < |eta| < 2.01","2.01 < |eta| < 2.37"]
def list_txt_files(directory):
	txt_files = [f for f in os.listdir(directory) if f.endswith('txt')]
	return txt_files

directory_path1 ='con/final/truth/'
directory_path2 ='unc/final/truth/'

#  directory_path3 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/nominal/con_get_con.txt'
#  directory_path4 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/nominal/unc_get_unc.txt'
#  directory_path3 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/nominal_subtract_con.txt'
#  directory_path4 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/nominal_subtract_unc.txt'
#  directory_path3 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/truth_pt_subtract_con.txt'
#  directory_path4 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/truth_pt_subtract_unc.txt'

txt_files1 = list_txt_files(directory_path1)
txt_files2 = list_txt_files(directory_path2)
entries_f1 = [[[[0 for k in range(2)] for i in range(12) ] for j in range(7)] for l in range(len(txt_files1))]
entries_f2 = [[[[0 for k in range(2)] for i in range(12) ] for j in range(7)] for l in range(len(txt_files2))]
#  entries_f3 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]
#  entries_f4 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]

for i in range(len(txt_files1)): 
	with open(directory_path1+txt_files1[i],"r") as f1:
		for j,line in enumerate(f1):
			line =line.strip()
			lines = line.split()

			if j%13==0:
				continue
			entries_f1[i][int(j/13)][j%13-1][0] = float(lines[0])
			entries_f1[i][int(j/13)][j%13-1][1] = float(lines[1])

for i in range(len(txt_files2)): 
	with open(directory_path2+txt_files2[i],"r") as f2:
		for j,line in enumerate(f2):
			line =line.strip()
			lines = line.split()

			if j%13==0:
				continue
			entries_f2[i][int(j/13)][j%13-1][0] = float(lines[0])
			entries_f2[i][int(j/13)][j%13-1][1] = float(lines[1])


#  with open(directory_path3,"r") as f3:
#      for j,line in enumerate(f3):
#          line =line.strip()
#          lines = line.split()
#
#          if j%13==0:
#              continue
#          entries_f3[int(j/13)][j%13-1][0] = float(lines[0])
#          entries_f3[int(j/13)][j%13-1][1] = float(lines[1])
#
#  with open(directory_path4,"r") as f4:
#      for j,line in enumerate(f4):
#          line =line.strip()
#          lines = line.split()
#
#          if j%13==0:
#              continue
#          entries_f4[int(j/13)][j%13-1][0] = float(lines[0])
#          entries_f4[int(j/13)][j%13-1][1] = float(lines[1])

final_unc =[[0 for i in range(12) ] for j in range(7)]
final_con =[[0 for i in range(12) ] for j in range(7)]

con_value = [[0 for i in range(12)] for j in range(7)]
unc_value = [[0 for i in range(12)] for j in range(7)]
for i in range(len(txt_files1)):
	for j in range(7):
		for k in range(12):
			#  con_value[j][k] += (entries_f1[i][j][k][0]/entries_f1[i][j][k][1] - entries_f3[j][k][0]/entries_f3[j][k][1])**2 / len(txt_files1)
			#  unc_value[j][k] += (entries_f2[i][j][k][0]/entries_f2[i][j][k][1] - entries_f4[j][k][0]/entries_f4[j][k][1])**2 / len(txt_files1)
			con_value[j][k] += (entries_f1[i][j][k][0]/entries_f1[i][j][k][1]) / len(txt_files1)
			unc_value[j][k] += (entries_f2[i][j][k][0]/entries_f2[i][j][k][1]) / len(txt_files1)

print(len(txt_files1))
con_values = [[0 for i in range(12)] for j in range(7)]
unc_values = [[0 for i in range(12)] for j in range(7)]
for i in range(len(txt_files1)):
	for j in range(7):
		for k in range(12):
			con_values[j][k] += (entries_f1[i][j][k][0]/entries_f1[i][j][k][1] - con_value[j][k])**2 / len(txt_files1)
			unc_values[j][k] += (entries_f2[i][j][k][0]/entries_f2[i][j][k][1] - unc_value[j][k])**2 / len(txt_files1)



with open('./truth/new_conversion/converted_final_uncertainty.txt','w') as f5:
	for i in range(7):
		f5.write(f"{eta_names_f0[i]}\n")
		for j in range(12):
			f5.write(f"{math.sqrt(con_values[i][j])}\n")


with open('./truth/new_conversion/unconverted_final_uncertainty.txt','w') as f6:
	for i in range(7):
		f6.write(f"{eta_names_f0[i]}\n")
		for j in range(12):
			f6.write(f"{math.sqrt(unc_values[i][j])}\n")
