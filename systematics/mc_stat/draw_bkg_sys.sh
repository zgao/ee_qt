#!/bin/bash
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/75_80_con.txt -c converted -pdf bkg_systematic_uncertainty/75_80_con.pdf -root bkg_systematic_uncertainty/75_80_con.root
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/75_80_unc.txt -c unconverted -pdf bkg_systematic_uncertainty/75_80_unc.pdf -root bkg_systematic_uncertainty/75_80_unc.root


# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/frag_down/80_100_con_frag_down_uncertainty.txt -c converted -pdf bkg_systematic_uncertainty/frag_down/80_100_con_frag_down.pdf -root bkg_systematic_uncertainty/frag_down/80_100_con_frag_down.root
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/frag_down/80_100_unc_frag_down_uncertainty.txt -c unconverted -pdf bkg_systematic_uncertainty/frag_down/80_100_unc_frag_down.pdf -root bkg_systematic_uncertainty/frag_down/80_100_unc_frag_down.root
#
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/frag_up/80_100_con_frag_up_uncertainty.txt  -c converted -pdf bkg_systematic_uncertainty/frag_up/80_100_con_frag_up.pdf -root bkg_systematic_uncertainty/frag_up/80_100_con_frag_up.root
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/frag_up/80_100_unc_frag_up_uncertainty.txt  -c unconverted -pdf bkg_systematic_uncertainty/frag_up/80_100_unc_frag_up.pdf -root bkg_systematic_uncertainty/frag_up/80_100_unc_frag_up.root

# python3 draw_bkg_sys.py -in ./converted_final_uncertainty.txt	 -c converted	-pdf converted.pdf -root converted.root
# python3 draw_bkg_sys.py -in ./unconverted_final_uncertainty.txt  -c unconverted -pdf unconverted.pdf -root unconverted.root

python3 draw_bkg_sys.py -in ./truth/new_conversion/converted_final_uncertainty.txt -c converted	-pdf ./truth/new_conversion/converted.pdf -root ./truth/new_conversion/converted.root
python3 draw_bkg_sys.py -in ./truth/new_conversion/unconverted_final_uncertainty.txt  -c unconverted -pdf ./truth/new_conversion/unconverted.pdf -root ./truth/new_conversion/unconverted.root
