import ROOT
import re
import argparse
import sys
from collections import defaultdict
import re

from FudgeFactor import FudgeFactor
import writeFudgeFactors

# List of observables - note that not all necessarily will require corrections.
# For example, the tracking variables d0/d0significance are not corrected. To
# see the full list of current corrected variables, see the
# observables_to_correct list below
observables = [ 'f3', 'f1', 'rhad', 'weta2', 'rphi', 'reta', 'wstot', 'eratio',
	'trackd0pvunbiased', 'charged0', 'd0significance', 'charged0significance',
	#  'DeltaPoverP', 'TRT_PID', 'deltaeta1', 'deltaphiRescaled','fracs1']#,'delta_E']
	#  'DeltaPoverP', 'TRT_PID', 'deltaeta1', 'deltaphiRescaled','delta_E']
	'DeltaPoverP', 'TRT_PID', 'deltaeta1', 'deltaphiRescaled','w_eta1']

observables_to_correct = [ 'f3', 'f1', 'rhad', 'weta2', 'rphi', 'reta', 'wstot',
	'TRT_PID', 'deltaeta1', 'deltaphiRescaled','fracs1']#,'delta_E']
	#  'TRT_PID', 'deltaeta1', 'deltaphiRescaled','delta_E']
	#  'TRT_PID', 'deltaeta1', 'deltaphiRescaled','w_eta1']

# Et Bins used in PDFs to be corrected, in GeV.
#  et_binning = [15, 20, 30, 40, 50]
et_binning = [0,4, 20, 30, 40, 50, 60, 80, 100, 150, 200]
# Abseta binning used
abseta_binning = [0.00, 0.60, 0.80, 1.15, 1.37, 1.52,
	1.81, 2.01, 2.37, 2.47]

# Location of PDFs to be used in corrections. The MC PDFs are convolved with a gaussian which is then fit such that
# the resulting convolution matches with the data PDFs.
#  mc_rundir = "/eos/user/j/jpuddefo/MC21_OnlineTunes/MC20Corrs/MC21_Offline_Bulk/FudgingValidation/MCPDFs/SmoothedPDFsOutput.root"
#  mc_rundir = "/afs/cern.ch/user/z/zgao/eos/con1_fudge/Smooth_MC.root"
#  mc_rundir = "/afs/cern.ch/user/z/zgao/eos/con1_fudge/Smooth_MC_8_9.root"
mc_rundir = "/afs/cern.ch/user/z/zgao/eos/fudge_factor/v_02/Smooth_MC_v02.root"
#  data_rundir = "/eos/user/j/jpuddefo/MC21_OnlineTunes/MC20Corrs/MC21_Offline_Bulk/FudgingValidation/SmoothedPDFsOutput.root"
#  data_rundir = "/afs/cern.ch/user/z/zgao/eos/con1_fudge/SmoothedPDFsOutput.root"
#  data_rundir = "/afs/cern.ch/user/z/zgao/eos/con1_fudge/Smoothed_data_8_9.root"
#  data_rundir = "/afs/cern.ch/user/z/zgao/eos/fudge_factor/v_02/Smooth_data_v02.root"
data_rundir = "/afs/cern.ch/user/z/zgao/eos/fudge_factor/v_01/Smooth_data_v01.root"

mc_pdfs = ROOT.TFile.Open(mc_rundir, "READ")
data_pdfs = ROOT.TFile.Open(data_rundir, "READ")

def getStartingValues(obs):

	defaultMeanAndWidth = ([0, -2, 2],[1, 0, 2])

	# Starting values for the mean/width of the gaussian. These have been
	# found by trial and error on what makes the best fit. These may need
	# changing when new datasets are used.
	meanAndWidthVals = {
		"el_rhad": ([0, -0.5, 0.5], [0.025, 0, 0.05]),
		"el_f3": ([0.005, -0.05, 0.05], [0.01, 0, 0.02]),
		"el_weta2": ([0, -0.1, 0.1], [0.05, 0, 0.1]),
		"el_rphi": ([0, -0.1, 0.1], [0.05, 0, 0.1]),
		"el_reta": ([0.005, -0.05, 0.05], [0.007, 0, 0.015]),
		"el_wstot": ([0, -0.5, 0.5], [0.0, 0, 1]),
		"el_eratio": ([0, -0.25, 0.25], [0.5, 0, 1]),
		"el_f1": ([0, -1, 1], [0.05, 0, 0.1]),
		"el_deltaeta1": ([0.00005, 0, 0.0001], [0.0003, 0.0, 0.001]),
		"el_TRT_PID": ([0, -1, 1], [0.06, 0, 0.1]),
		"fracs1": ([0, -1, 1], [0.001, 0, 0.05]),
		"delta_E": ([0, -1, 1], [0.001, 0, 0.004]),
		"w_eta1": ([0, -1, 1], [0.001, 0, 0.05])
	}

	if obs in meanAndWidthVals.keys():
		return meanAndWidthVals[obs]
	else:
		return defaultMeanAndWidth

def getPDFsFromFiles():
	"""
	Get the data/MC PDF for each observable, in each Et and eta bin. "Special" bins
	are removed, and the MC PDFs are scaled to the data PDF.
	"""

	for obs in observables:
		print("Making FFs for observable {obs}".format(obs=obs))
		for low_et_edge, up_et_edge in zip(et_binning[:-1], et_binning[1:]):
			for low_abseta_edge, up_abseta_edge in zip(abseta_binning[:-1], abseta_binning[1:]):

				try:

					#  MCPDF = mc_pdfs.Get('el_{obs}/sig/el_{obs}_sig_smoothed_hist_from_KDE_et{et:1.0f}eta{eta:1.2f}'.format(
					#  MCPDF = mc_pdfs.Get('{obs}/sig/{obs}_sig_smoothed_hist_from_KDE_et{et:1.0f}eta{eta:1.2f}'.format(
					#  MCPDF = mc_pdfs.Get('el_{obs}/sig/el_{obs}_sig_smoothed_hist_from_KDE_et{et:1.0f}eta{eta:1.2f}'.format(
					MCPDF = mc_pdfs.Get('{obs}/sig/{obs}_sig_smoothed_hist_from_KDE_et{et:1.0f}eta{eta:1.2f}'.format(
								obs=obs, et=low_et_edge, eta=low_abseta_edge))

					#  DataPDF = data_pdfs.Get('el_{obs}/sig/el_{obs}_sig_et{et:1.0f}eta{eta:1.2f}'.format(
					DataPDF = data_pdfs.Get('{obs}/sig/{obs}_sig_smoothed_hist_from_KDE_et{et:1.0f}eta{eta:1.2f}'.format(
								obs=obs, et=low_et_edge, eta=low_abseta_edge))

					print(MCPDF.GetName())

					#TODO: add pear correction for delta_eta1

					# Normalise PDFs to unity
					MCPDF.Scale(DataPDF.Integral()/MCPDF.Integral())

				# Some observables don't have PDFs for endcap, so skip when this happens
				except:
					print("Failed to get the following PDFs, Skipping FF calculation:" +
						"{obs}, {et}, {eta}".format(obs=obs, et=low_et_edge, eta=low_abseta_edge))
					continue


				# Remove Start and End bins from MC PDF (Smoothing adds under/overflow)
				#MCPDF.SetBinContent(1, MCPDF.GetBinContent(2))
				#MCPDF.SetBinContent(MCPDF.GetNBinsX(), MC.GetBinContent(MCPDF.GetNBinsX()-1))

				# For Special Bins, use average of adjacent bins. Special Bins are where
				# a default value is added in cases where an actual value is not valid.
				# E.g undefined TRT results will go in the TRT=0 bin

				def setSpecialBinToAverageOfAdjacent(specialBin):

					avgBinContentMC = (MCPDF.GetBinContent(specialBin-1)+
						MCPDF.GetBinContent(specialBin+1))/2.0

					avgBinContentData = (MCPDF.GetBinContent(specialBin-1)+
						MCPDF.GetBinContent(specialBin+1))/2.0

					MCPDF.SetBinContent(specialBin, avgBinContentMC)
					DataPDF.SetBinContent(specialBin, avgBinContentData)

				if (obs in ["f3", "f1", "TRT_PID", "DeltaPoverP"]):
					setSpecialBinToAverageOfAdjacent(MCPDF.FindBin(0))
				if (obs in ["reta", "rphi"]):
					setSpecialBinToAverageOfAdjacent(MCPDF.FindBin(1))

				# yield PDFs

				yield obs, low_et_edge, low_abseta_edge, DataPDF, MCPDF


def convertToRooFit():
	"""
	For each data/MC PDF, create the necessary RooFit variables, i.e the
	mean, width and observable for the Gaussian, as well as a DataHist for the
	data PDF, and a HistPDF for the MC PDF. The function then yields a class
	which stores all these as class variables
	"""

	for obs, et, eta, DataPDF, MCPDF in getPDFsFromFiles():

		histName = DataPDF.GetName()

		x = ROOT.RooRealVar(histName+"_var", histName+"_var",
			MCPDF.GetXaxis().GetXmin(), MCPDF.GetXaxis().GetXmax())

		x.setBins(MCPDF.GetNbinsX(), "b")


		# get starting Mean and Width values
		meanVals, widthVals = getStartingValues(obs)

		# RooFit variables for Gaussian


		mean = ROOT.RooRealVar(histName+"_mean", histName+"_mean", *meanVals)

		width = ROOT.RooRealVar(histName+"_width", histName+"_width", *widthVals)

		gaussian = ROOT.RooGaussian(histName+"_gauss", histName+"_gauss",
			x, mean, width)

		# RooFit Variables from MC and Data PDFs

		MC_dataHist = ROOT.RooDataHist(MCPDF.GetName()+"_tmp", MCPDF.GetName()+"_tmp", ROOT.RooArgList(x),
				ROOT.RooFit.Import(MCPDF))

		MC_RooVar = ROOT.RooHistPdf(MCPDF.GetName()+"_rooPDF", MCPDF.GetName(),
			ROOT.RooArgSet(x), MC_dataHist)

		Data_RooVar = ROOT.RooDataHist(DataPDF.GetName()+"_rooDataHist",
			DataPDF.GetName(), ROOT.RooArgList(x), ROOT.RooFit.Import(DataPDF))

		newFF = FudgeFactor(mean, width, x, gaussian, DataPDF, MCPDF,
			MC_RooVar, Data_RooVar, obs, et, eta)


		yield newFF


def getFFs():
	"""
	Create a convPDF of the gaussian and the MC PDF, and fit this to the data PDF
	to yield the mean/width of the gaussian used in the corrections.
	"""
	for ff in convertToRooFit():

		convPDF = ROOT.RooFFTConvPdf(ff.mcPdf.GetName()+"conv", "MCHist (X) Gaussian",
			ff.observable, ff.rooVarMC, ff.gaussian)

		#  combined_range = ROOT.RooFit.Cut("(x >=0.0 && x <= 0.1) &&(x >= 0.6 && x <= 1.0)")
		#  print("	 ***_***  ",dir(ff))
		ff.observable.setRange("left",0.,0.1)
		ff.observable.setRange("right",0.7,1.)

		#  print(ff.low_et_edge ==0., ff.low_et_edge)
		#  exit()

		if ff.low_abseta_edge==0.:
			ff.observable.setRange("tail",0.4,1.)
		elif ff.low_abseta_edge ==0.6:
			ff.observable.setRange("tail",0.5,1.)
		elif ff.low_abseta_edge ==0.8:
			ff.observable.setRange("tail",0.4,1.)
		elif ff.low_abseta_edge ==1.15:
			ff.observable.setRange("tail",0.4,1.)
		elif ff.low_abseta_edge ==1.52:
			ff.observable.setRange("tail",0.4,1.)
		elif ff.low_abseta_edge ==1.81:
			ff.observable.setRange("tail",0.4,1.)
		elif ff.low_abseta_edge ==2.01:
			ff.observable.setRange("tail",0.4,1.)



		convPDF.fitTo(ff.rooVarData, ROOT.RooFit.Minimizer("Minuit2"),
			#  ROOT.RooFit.Range("full"))
			#  ROOT.RooFit.Range("0.,100."))
			ROOT.RooFit.Range(0.5,0.8))
			#  ROOT.RooFit.Range(0.05,0.7))
			#  ROOT.RooFit.Range(0.,0.1))
			#  ROOT.RooFit.Range(0.6,1.))
			#  ROOT.RooFit.Range(0.1,1.0))
			#  ROOT.RooFit.Range(0.01,1.0))
			#  ROOT.RooFit.Range(0.1,0.7))
			#  ROOT.RooFit.Range("tail"))
			#  ROOT.RooFit.Range(0.0,0.1),ROOT.RooFit.Range(0.7,1.0))
			#  combined_range)
			#  ROOT.RooFit.Range("left,right"))
			
		#  print("----------------",ROOT.RooFit.Range("full"))
		#  exit()
			#  ROOT.RooFit.Range(0.05,1))

		ff.convPDF = convPDF

		yield ff


def writePlotToFile(root_file, ff):
	"""
	For each data/MC/Conv PDF, plot all of these in a single graph, to be used
	to check that the conv PDF matches the data PDF.
	"""
	# plot options
	dataHistMarkerSize = 1.3
	mcHistLineColor = ROOT.kBlue
	convPDFLineColor = ROOT.kRed
	ATLASLabelType = "Internal"
	sqrtEnergy = "#sqrt{{s}} = 13 TeV"
	processType = "#font[42]{Z #rightarrow ee}" if ff.low_et_edge >= 15.0 else \
		"#font[42]{J#psi #rightarrow ee}"


	if root_file.GetDirectory(ff.variable):
		root_file.cd(ff.variable)
	else:
		root_file.mkdir(ff.variable)
		root_file.cd(ff.variable)

	canvas = ROOT.TCanvas(ff.dataPdf.GetName())

	xframe = ff.observable.frame()

	ff.rooVarData.plotOn(xframe, ROOT.RooFit.Name("DataHist"),
		ROOT.RooFit.MarkerSize(dataHistMarkerSize))

	ff.rooVarMC.plotOn(xframe, ROOT.RooFit.Name("MCHist"),
		ROOT.RooFit.LineColor(mcHistLineColor))

	ff.convPDF.plotOn(xframe, ROOT.RooFit.Name("ConvPDF"),
		ROOT.RooFit.LineColor(convPDFLineColor))

	# Drawing options for canvas
	xframe.Draw()
	xframe.SetTitle(" ")
	xframe.SetMaximum(xframe.GetMaximum()*1.43)
	xframe.SetXTitle(ff.variable)

	energyLabel = ROOT.TLatex(0.2, 0.82, sqrtEnergy)
	energyLabel.SetNDC()
	energyLabel.Draw()

	etLabel = ROOT.TLatex(0.2, 0.76, str(ff.low_et_edge) + " #leq E_{T} #leq " +
		str(et_binning[et_binning.index(ff.low_et_edge) + 1]) + " GeV")

	etLabel.SetNDC()
	etLabel.Draw()

	etaLabel = ROOT.TLatex(0.2, 0.7, str(ff.low_abseta_edge) + " #leq |#eta| #leq " +
		str(abseta_binning[abseta_binning.index(ff.low_abseta_edge) + 1]))

	etaLabel.SetNDC()
	etaLabel.Draw()

	legend = ROOT.TLegend(0.5, 0.67, 0.9, 0.92, "", "brNDC")
	legend.SetBorderSize(0)
	legend.SetFillColor(0)
	legend.SetFillStyle(0)
	legend.AddEntry("DataHist", processType+" Data", "P")
	legend.AddEntry("MCHist", processType + " MC", "F")
	legend.AddEntry("ConvPDF", processType + " MC Corrected", "L")
	legend.Draw()


	canvas.Write()
def writeFFsToConfFile(conf_file, ff):
	"""
	Write all the gaussian mean/widths for each et/eta bin and for each observable
	to a conf file. This is then used in a later function to create the individual
	configs for each variable to be used in the EGammaVariableCorrectionTool
	"""
	conf_file.write("{var}_et{et}_eta{eta}: ".format(var=ff.variable,
		et=ff.low_et_edge, eta=ff.low_abseta_edge))
	conf_file.write("{:.5E} {:.5E} ".format(ff.shift.getValV(), ff.width.getValV()))
	conf_file.write("          Error {:.5f}  {:.5f} \n".format(ff.shift.getError(), ff.width.getError()))



def createVariableCorrectionToolConfigs(conf_file):
	"""
	Uses the conf file containing all gaussian corrections to write the
	individual config files for each variable. Also produces the
	'FudgeFactorCorrectionToolConfig.conf'. This file contains the filepaths for
	each of the individual configs, and is what is given to the
	EGammaVariableCorrectionTool.
	"""

	# Create dicts to store shifts and widths of gaussian for each variable and et, eta
	varMeans = defaultdict(lambda: defaultdict(list))
	varWidths = defaultdict(lambda: defaultdict(list))

	et_binning_for_configs = et_binning[:-1]
	eta_binning_for_configs = abseta_binning[:-1]

	# Open conf file with all shifts/widths and read out, separating
	# into each variable
	searchString = re.compile("(.*)_et(.*)_eta(.*)")
	with open(conf_file) as file:
		lines = file.readlines()

	for line in lines:
		x = line.split()
		searchResult = searchString.search(x[0])
		if searchResult:

			varMeans[searchResult.group(1)][searchResult.group(2)].append(x[1])
			varWidths[searchResult.group(1)][searchResult.group(2)].append(x[2])

	# for each variable, create a proper config that
	# can be used in the EGammaVariableCorrectionTool. There are some
	# variables for which configs are not created, and others where the endcap/
	# crack region are not used which requires extra work to get the correct config

	for var in varMeans.keys():
		if var in ['DeltaPoverP', 'trackd0pvunbiased', 'charged0', 'eratio']:
			pass

		elif var in ['f3']:
			for ptBin in varMeans[var]:
				varMeans[var][ptBin].append('0')

			for ptBin in varWidths[var]:
				varWidths[var][ptBin].append('0')

			writeFudgeFactors.createConfFileForVariable(var, varMeans, varWidths,
				et_binning_for_configs, eta_binning_for_configs)

		# For the TRT, add 0s for last two eta bins
		elif var in ['TRT_PID']:
			for ptBin in varMeans[var]:
				varMeans[var][ptBin].extend(["0","0"])

			for ptBin in varWidths[var]:
				varWidths[var][ptBin].extend(["0","0"])

			writeFudgeFactors.createConfFileForVariable(var, varMeans, varWidths,
				et_binning_for_configs, eta_binning_for_configs)

		# For weta2, dont correct 1.37-1.52 bin
		elif var in ['weta2']:

			for ptBin in varMeans[var]:
				varMeans[var][ptBin][4] = 0

			for ptBin in varWidths[var]:
				varWidths[var][ptBin][4] = 0


			writeFudgeFactors.createConfFileForVariable(var, varMeans, varWidths,
				et_binning_for_configs, eta_binning_for_configs)

		else:
			writeFudgeFactors.createConfFileForVariable(var, varMeans, varWidths,
				et_binning_for_configs, eta_binning_for_configs)

	writeFudgeFactors.createToolConfig(observables_to_correct)


def formatFFs():

	# File to save plots of Data, MC and Corr MC
	outFile = ROOT.TFile.Open("outFile_FF.root", "RECREATE")

	confFile = open("FudgeFactors.conf", "w")

	for ff in getFFs():
		writePlotToFile(outFile, ff)
		writeFFsToConfFile(confFile, ff)

	outFile.Close()
	confFile.close()

	createVariableCorrectionToolConfigs("FudgeFactors.conf")


if __name__ == "__main__":
	formatFFs()
