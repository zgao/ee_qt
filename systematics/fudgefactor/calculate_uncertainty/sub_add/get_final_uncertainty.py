import os
import math
eta_names_f0 = ["0 < |eta| < 0.6","0.6 < |eta| < 0.8","0.8 < |eta| < 1.15","1.15 < |eta| < 1.37","1.52 < |eta| < 1.81","1.81 < |eta| < 2.01","2.01 < |eta| < 2.37"]
def list_txt_files(directory):
    txt_files = [f for f in os.listdir(directory) if f.endswith('txt')]
    return txt_files

#  directory_path3 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/nominal/con_get_con.txt'
#  directory_path4 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/nominal/unc_get_unc.txt'
directory_path1 ='./truth/80_100_sub_add_con.txt'
directory_path2 ='./truth/80_100_sub_add_unc.txt'

directory_path3 ='./truth/70_110_sub_add_con.txt'
directory_path4 ='./truth/70_110_sub_add_unc.txt'

#  txt_files1 = list_txt_files(directory_path1)
#  txt_files2 = list_txt_files(directory_path2)
entries_f1 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]
entries_f2 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]
entries_f3 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]
entries_f4 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]

with open(directory_path1,"r") as f1:
    for j,line in enumerate(f1):
        line =line.strip()
        lines = line.split()

        if j%13==0:
            continue
        entries_f1[int(j/13)][j%13-1][0] = float(lines[0])
        entries_f1[int(j/13)][j%13-1][1] = float(lines[1])

with open(directory_path2,"r") as f2:
    for j,line in enumerate(f2):
        line =line.strip()
        lines = line.split()

        if j%13==0:
            continue
        entries_f2[int(j/13)][j%13-1][0] = float(lines[0])
        entries_f2[int(j/13)][j%13-1][1] = float(lines[1])


with open(directory_path3,"r") as f3:
    for j,line in enumerate(f3):
        line =line.strip()
        lines = line.split()

        if j%13==0:
            continue
        entries_f3[int(j/13)][j%13-1][0] = float(lines[0])
        entries_f3[int(j/13)][j%13-1][1] = float(lines[1])

with open(directory_path4,"r") as f4:
    for j,line in enumerate(f4):
        line =line.strip()
        lines = line.split()

        if j%13==0:
            continue
        entries_f4[int(j/13)][j%13-1][0] = float(lines[0])
        entries_f4[int(j/13)][j%13-1][1] = float(lines[1])

final_unc =[[0 for i in range(12) ] for j in range(7)]
final_con =[[0 for i in range(12) ] for j in range(7)]

con_value = [[0 for i in range(12)] for j in range(7)]
unc_value = [[0 for i in range(12)] for j in range(7)]

with open("sub_add_subtract_con.txt","w") as f_con:
    for i in range(7):
        f_con.write(f"{eta_names_f0[i]}\n")
        for j in range(12):
            f_con.write(f"{entries_f1[i][j][0]-abs(entries_f1[i][j][0]-entries_f3[i][j][0])}\t\t\t{entries_f1[i][j][1]-abs(entries_f1[i][j][1]-entries_f3[i][j][1])}\n")

with open("sub_add_subtract_unc.txt","w") as f_unc:
    for i in range(7):
        f_unc.write(f"{eta_names_f0[i]}\n")
        for j in range(12):
            f_unc.write(f"{entries_f2[i][j][0]-abs(entries_f2[i][j][0]-entries_f4[i][j][0])}\t\t\t{entries_f2[i][j][1]-abs(entries_f2[i][j][1]-entries_f4[i][j][1])}\n")

for i in range(7):
    for j in range(12):
        con_value[i][j] += abs( (entries_f1[i][j][0]-abs(entries_f1[i][j][0]-entries_f3[i][j][0]))/(entries_f1[i][j][1]-abs(entries_f1[i][j][1]-entries_f3[i][j][1])) - entries_f1[i][j][0]/entries_f1[i][j][1])
        unc_value[i][j] += abs( (entries_f2[i][j][0]-abs(entries_f2[i][j][0]-entries_f4[i][j][0]))/(entries_f2[i][j][1]-abs(entries_f2[i][j][1]-entries_f4[i][j][1])) - entries_f2[i][j][0]/entries_f2[i][j][1])

#  with open('./conversion_down_converted_final_uncertainty_for_background.txt','w') as f5:
#      for i in range(7):
#          f5.write(f"{eta_names_f0[i]}\n")
#          for j in range(12):
#              f5.write(f"{con_value[i][j]}\n")
#
#
#  with open('./conversion_down_unconverted_final_uncertainty_for_background.txt','w') as f6:
#      for i in range(7):
#          f6.write(f"{eta_names_f0[i]}\n")
#          for j in range(12):
#              f6.write(f"{unc_value[i][j]}\n")
