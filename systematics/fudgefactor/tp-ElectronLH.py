#! /usr/bin/env python
import ROOT

import argparse
from os.path import exists
from glob import glob
import re
from contextlib import contextmanager
import os

from ElectronLikelihoodID import KDEValues

ROOT.PyConfig.IgnoreCommandLineOptions = True # don't pollute argparse help!



# Map between new names and old names of LH variables
translate_vars = {
	'r_had1' : 'el_rhad1',
	'r_had'  : 'el_rhad',
	'f3'	 : 'el_f3',
	'w_eta2' : 'el_weta2',
	'r_phi'  : 'el_rphi',
	'r_eta'  : 'el_reta',
	'w_stot' : 'el_wstot',
	'e_ratio': 'el_eratio',
	'f1'	 : 'el_f1',
	'bare_d0': 'el_trackd0pvunbiased',
	'charge_times_bare_d0' : 'el_charged0',
	'absd0sig' : 'el_d0significance',
	'charge_times_d0sig' : 'el_charged0significance',
	'track_dp_over_p' : 'el_DeltaPoverP',
	'e_probability_ht' : 'el_eProbabilityHT',
	'transformed_e_probability_ht' : 'el_TRT_PID',
	'delta_eta1' : 'el_deltaeta1',
	'rescaled_cluster_track_dphi' : 'el_deltaphiRescaled',
	'e_over_p' : 'el_EOverP',
	'e_probability_ht_rnn' : 'el_eProbabilityHT_RNN',
	'transformed_e_probability_ht_rnn' : 'el_TRT_PID_RNN',
	'blayer_hits_n' : 'el_numberOfInnerMostPixelLayerHits',
	'pixel_hits_n' : 'el_numberOfPixelHits',
	'silicon_hits_n' : 'el_nSi', # not sure, wasn't on makePDFs? Elo: this varible isn't in the root file for the pdfs at the end, I can't remember
	'fracs1' : 'fracs1',
	'delta_E': 'delta_E'

  # exactly how it's used, I need to check.
}

def parse_args():
	"""Parses and returns command line arguments.

	Calling this function has all the side effects of creating an
	``argparse.ArgumentParser``, e.g. creating usage instructions and a help
	menu.

	Returns:
		``argparse.Namespace``: the command line arguments
	"""
	parser = argparse.ArgumentParser(
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('-v', '--verbose', help='Print extra info.',
		default=False, action='store_true')
	# Subparsers
	subparsers = parser.add_subparsers(title='actions',
		dest='action')
	parser_translate = subparsers.add_parser('translate',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser_translate.add_argument('expression')
	parser_smooth = subparsers.add_parser('smooth',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser_smooth.add_argument('files', nargs='*', help='List of all files to' \
		'include in the smoothing. Can be from difference run directorys - ' \
		'e.g normally include one translated_flattened.root file from a Zee, Jpsi' \
		' and jf17 run directory. Make sure to include full path for each file!')
	parser_tune = subparsers.add_parser('tune',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser_tune.add_argument('expression')
	parser_tune.add_argument('--pileup_obs', default='nvtx')
	parser_tune.add_argument('--trig', action='store_true', default=False)

	return parser.parse_args()

@contextmanager
def cd(newdir):
	""" Defines a context to change to one directory and, after the context
	ends, back to the previous directory.

	Arguments:
		newdir (``str``): folder in which the actions will be made

	Example:
		.. code-block:: python

			# in directory 'foo/'
			with cd('bar/'):
				do_stuff_in_bar()
			# back in 'foo/'

	Using a context provides some safety from failure.
	"""
	prevdir = os.getcwd()
	os.chdir(os.path.expanduser(newdir))
	try:
		yield
	finally:
		os.chdir(prevdir)

def translate_hists(th1_list):
	for th1 in th1_list:
		th1_title = th1.GetTitle()
		m = re.match(r'(.*)-for-pt(.*)to([0-9]*[.]?[0-9]+)_.*eta(.*)to(.*)', th1_title)
		var_name = m.group(1)
		pt_low = float(m.group(2))
		pt_high = float(m.group(3))
		abseta_low = float(m.group(4))
		abseta_high = float(m.group(5))

		# Either get translated name, or keep original name if no translation
		# exists in the dictionary
		translated_var = translate_vars.get(var_name, var_name)


		# Translate the full th1 title based on format define in MakePDFs.cxx
		translated_title = '{var}_et{pt:.0f}eta{eta:3.2f}'.format(
			var=translated_var, pt=float(pt_low), eta=abseta_low)

		print('Translating {old_title} to {new_title}'.format(
			old_title=th1_title, new_title=translated_title))
		th1.SetNameTitle(translated_title, translated_title)
	return th1_list


def translate(args):
	"""

	Create root files where the TH1s from a flattened.root file are "translated"
	into the naming format used in the old T&P. A new root file is made for each
	folder in the flattened.root  - i.e for each Tag-Probe pair, for each systematic
	and for each specific probe. The naming convention for the resulting files is
	"<tppair>_<systematic>_<probe_object>.root"
	"""
	ROOT.TH1.AddDirectory(ROOT.kFALSE)

	# find run directories
	rundirs = glob(args.expression)
	if not rundirs:
		print('No run directories found corresponding to expression "{0}".'
			.format(args.expression))
	# Go through each run directory
	for rundir in rundirs:
		print('In run directory "{0}"...'.format(rundir))
		with cd(rundir):
			if os.path.exists('flattened.root'):
				flat_file = ROOT.TFile.Open('flattened.root')
				for key in flat_file.GetListOfKeys():
					if key.GetClassName() == 'TDirectoryFile':
						flat_file.cd(key.GetName())
						for systematic in ROOT.gDirectory.GetListOfKeys():
							flat_file.cd(key.GetName() + "/" + systematic.GetName())
							for probe_name in ROOT.gDirectory.GetListOfKeys():

								# File path to cd into to get histograms
								filepath = "{key}/{systematic}/{probe_name}".format(
									key=key.GetName(), systematic=systematic.GetName(),
									probe_name=probe_name.GetName())

								# Make a new root file for each variant of probes
								filename = "{key}_{systematic}_{probe_name}".format(
									key=key.GetName(), systematic=systematic.GetName(),
									probe_name=probe_name.GetName())

								flat_file.cd(filepath)

								print("Translating hists in folder: {filepath}".format(
									filepath=filepath))

								th1_list = ROOT.TList()
								for histogram in ROOT.gDirectory.GetListOfKeys():
									th1_list.append(ROOT.gDirectory.Get(
										histogram.GetTitle()))

								translated_list = translate_hists(th1_list)
								translated_file = ROOT.TFile(filename +
									'_translated_flattened.root', 'RECREATE')

								translated_list.Write()
								translated_file.Close()


def getScaleFactor(href,h) :
	Denom = float(h.Integral()*href.GetXaxis().GetBinWidth(1))
	return 1 if Denom == 0 else href.Integral()*h.GetXaxis().GetBinWidth(1)/Denom


def hadd_and_kde_smoothing(args):
	""" First, adds all signal root files from J/Psi and Z together, and all
	background root files together. Then applies KDE smoothing on each

	Provide a list of files to include in the smoothing process."""

	filepath_sig = 'merged-hist-sig.root'
	filepath_bkg = 'merged-hist-bkg.root'

	filelist_sig = []
	filelist_bkg = []

	# For each root file, get the rundir, find if is signal or bkg
	# then place rootfile in appropiate list
	for root_file in args.files:
		print(root_file)
		rundir = os.path.dirname(root_file)
		filename = os.path.basename(root_file)
		print(rundir)
		for file_types_sig in ['*EGAM1*.root', '*EGAM2*.root']:
			if glob(rundir +'/event_framework_run/' + file_types_sig):
				filelist_sig.append(root_file)
		for fileTypes_bkg in ['*EGAM7*.root'] : # need to check works in general
			if glob(rundir+'/event_framework_run/'+fileTypes_bkg):
				filelist_bkg.append(root_file)

	# Hadd Signal

	print('Hadding: ' + filepath_sig + ' ' + ' '.join(filelist_sig))
	os.system('hadd -f ' + filepath_sig + ' ' + ' '.join(filelist_sig))
	#  signal_file = ROOT.TFile(filepath_sig, 'READ')




	#  signal_file = ROOT.TFile('/afs/cern.ch/user/z/zgao/eos/con1_fudge/user.zgao.tp-PDFs_MC20_Zee_not_minus_1-361106-EGAM1-e3601_s3681_r13145_p4940_20230721_053539_hist/tp_tight_tag_nominal_probe_electron_translated_flattened.root', 'READ')
	signal_file = ROOT.TFile('/afs/cern.ch/user/z/zgao/eos/con1_fudge/user.zgao.tp-PDFs_data2018_Zee-data18_13TeV-EGAM1-grp18_v02_p4926_20230715_110235_hist/tp_tight_tag_nominal_probe_electron_translated_flattened.root', 'READ')

	# Hadd bkg
	print('Hadding: ' + filepath_bkg + ' ' + ' '.join(filelist_bkg))
	os.system('hadd -f ' + filepath_bkg + ' ' + ' '.join(filelist_bkg))
	bkg_file = ROOT.TFile(filepath_bkg, 'READ')

	print('Doing KDE Smoothing for Signal and Background...')

	# Regex to compare to graph names
	searchString = re.compile('(.*)_et(\d+)eta(\d\.\d+)')

	# Variables list just to create folders
	#  LHVariablesList = ['el_rhad1', 'el_rhad', 'el_f3', 'el_weta2', 'el_rphi',
	#	   'el_reta', 'el_wstot', 'el_eratio', 'el_f1', 'el_trackd0pvunbiased',
	#	   'el_charged0', 'el_d0significance', 'el_charged0significance',
	#	   'el_DeltaPoverP', 'el_eProbabilityHT', 'el_TRT_PID', 'el_deltaeta1',
	#	   'el_deltaphiRescaled', 'el_EOverP', 'el_eProbabilityHT_RNN',
	#	   'el_TRT_PID_RNN', 'el_numberOfInnerMostPixelLayerHits',
	#	   'el_numberOfPixelHits', 'el_nSi']

	LHVariablesList = ['delta_E','fracs1']


	# Variables that have values to remove before KDE and re-add after
	SpecialVariables = ['el_eratio', 'el_rphi', 'el_reta', 'el_f1', 'el_f3',
		'el_DeltaPoverP', 'el_TRTHighTHitsRatio', 'el_eProbabilityHT', 'el_eProbabilityHT_RNN'
		'el_TRT_PID', 'el_TRT_PID_RNN']

	# KDE Output file
	pdf_out_file = ROOT.TFile('SmoothedPDFsOutput.root', 'RECREATE')

	# Make output file structure
	for variable in LHVariablesList:
		ROOT.gDirectory.mkdir(variable)
		pdf_out_file.cd(variable)
		ROOT.gDirectory.mkdir('sig')
		ROOT.gDirectory.mkdir('bkg')
		pdf_out_file.cd()

	# go through each histogram in merged root files
	# for signal and background:
	for file_type in ['sig', 'bkg']:
		if file_type == 'sig':
			getFineFactors = KDEValues.getKDEFineFactorSignal
			hadded_file = signal_file
		else:
			getFineFactors = KDEValues.getKDEFineFactorBackground
			hadded_file = bkg_file

		for rawHistoKey in hadded_file.GetListOfKeys():
			searchResult = searchString.search(rawHistoKey.GetName())
			var = searchResult.group(1)
			et = searchResult.group(2)
			eta = searchResult.group(3)

			if args.verbose:
				print("Smoothing: ", var, et, eta)

			# skip variables not used (e.g r_had1, numberInnerMost...??)
			
			if var not in ['fracs1','delta_E']:continue
			if var not in KDEValues.KDEFineFactorSig:
				continue

			fine_factor = getFineFactors(var, et, eta)


			# Dont use last two eta bins for TRT variables
			if var in ['el_TRT_PID', 'el_TRTHighTHitsRatio', 'el_eProbabilityHT',
				'el_TRT_PID_RNN', 'el_eProbabilityHT_RNN']:
				if eta == '2.01' or eta == '2.37':
					continue
			# Don't use last eta bin for f3
			if var == 'el_f3':
				if eta == '2.37':
					continue

			# Get unsmoothed histogram
			rawHisto = rawHistoKey.ReadObj()

			#################################################################
			# Have to temporarily remove Special bins for some variables that
			# we dont want to smooth
			#################################################################


			spBinContent = 0.0
			spBinLowContent = 0.0
			spBinHighContent = 0.0

			if var in ['el_f1', 'el_f3', 'el_DeltaPoverP',
				'el_TRTHighTHitsRatio', 'el_TRT_PID', 'el_TRT_PID_RNN']:
				spBin = rawHisto.FindBin(0)
				spBinContent = rawHisto.GetBinContent(spBin)

				# Set special bins to average of adjacent bins rather than 0 for
				# consistent smoothing
				avgBinContent = (rawHisto.GetBinContent(spBin-1) +
					rawHisto.GetBinContent(spBin+1))/2.0

				rawHisto.SetBinContent(spBin, avgBinContent)

			if var in ['el_rphi','el_reta']:
				spBin = rawHisto.FindBin(1)
				spBinContent = rawHisto.GetBinContent(spBin)

				# Set special bins to average of adjacent bins rather than 0 for
				# consistent smoothing
				avgBinContent = (rawHisto.GetBinContent(spBin-1) +
					rawHisto.GetBinContent(spBin+1))/2.0

				rawHisto.SetBinContent(spBin, avgBinContent)

			if var in ['el_eratio']:
				spBinLow = rawHisto.FindBin(0)
				spBinHigh = rawHisto.FindBin(1)
				spBinLowContent = rawHisto.GetBinContent(spBinLow)
				spBinHighContent = rawHisto.GetBinContent(spBinHigh)

				# Set special bins to average of adjacent bins rather than 0 for
				# consistent smoothing
				spBinLowAvg = (rawHisto.GetBinContent(spBinLow-1) +
					rawHisto.GetBinContent(spBinLow+1))/2.0

				spBinHighAvg = (rawHisto.GetBinContent(spBinHigh-1) +
					rawHisto.GetBinContent(spBinHigh+1))/2.0

				rawHisto.SetBinContent(spBinLow, spBinLowAvg)
				rawHisto.SetBinContent(spBinHigh, spBinHighAvg)

			if var in ['el_eProbabilityHT', 'el_eProbabilityHT_RNN']:
				spBin = rawHisto.FindBin(0.5)
				spBinContent = rawHisto.GetBinContent(spBin)

				# Set special bins to average of adjacent bins rather than 0 for
				# consistent smoothing
				avgBinContent = (rawHisto.GetBinContent(spBin-1) +
					rawHisto.GetBinContent(spBin+1))/2.0

				rawHisto.SetBinContent(spBin, avgBinContent)

			# Do KDE smoothing
			if rawHisto.GetEntries() > 10:
				try:
					TMVAPDF = ROOT.TMVA.PDF('temp_'+var+file_type, rawHisto,
						ROOT.TMVA.KDEKernel.kGauss, ROOT.TMVA.KDEKernel.kAdaptiveKDE,
						ROOT.TMVA.KDEKernel.kSampleMirror, fine_factor, 0)
					TMVAPDF.ValidatePDF(0)
					KDEHisto = TMVAPDF.GetPDFHist()
				except Exception as e:
					error_info = {
						'exception' : repr(e),
						'sb_type' : file_type,
						'var' : var,
						'et' : et,
						'eta' : eta,
					}
					print('KDE smoothing failed for {sb_type} variable {var} in bin (et={et}, eta={eta}) '
						  'with the exception shown below. Will use *unsmoothed* histogram instead. '
						  'Exception was:\n{exception}'.format(**error_info))
					KDEHisto = rawHisto

			else:
				KDEHisto = rawHisto

			rawHisto.SetName('{var}_{Type}_et{et}eta{eta}'.format(var=var,
				Type=file_type, et=et, eta=eta))

			KDEHistoName = '{var}_{Type}_smoothed_hist_from_KDE_et{et}eta{eta}'.format(var=var,
				Type=file_type, et=et, eta=eta)

			KDEHisto.SetNameTitle(KDEHistoName, KDEHistoName)
			KDEHisto.Rebin(int(10000/rawHisto.GetNbinsX()))
			SF = getScaleFactor(rawHisto, KDEHisto)
			KDEHisto.Scale(SF)

			#####################################################
			## Re-add the special bins -
			## have to refind the bin as bins have changed in smoothed
			######################################################

			if var in ['el_f1', 'el_f3', 'el_DeltaPoverP',
				'el_TRTHighTHitsRatio', 'el_TRT_PID', 'el_TRT_PID_RNN']:
				spBinRaw = rawHisto.FindBin(0)
				spBinKDE = KDEHisto.FindBin(0)
				rawHisto.SetBinContent(spBinRaw, spBinContent)
				KDEHisto.SetBinContent(spBinKDE, spBinContent)

			if var in ['el_rphi','el_reta']:
				spBinRaw = rawHisto.FindBin(1)
				spBinKDE = KDEHisto.FindBin(1)
				rawHisto.SetBinContent(spBinRaw, spBinContent)
				KDEHisto.SetBinContent(spBinKDE, spBinContent)

			if var in ['el_eratio']:
				spBinLowRaw = rawHisto.FindBin(0)
				spBinHighRaw = rawHisto.FindBin(1)
				spBinLowKDE = KDEHisto.FindBin(0)
				spBinHighKDE = KDEHisto.FindBin(1)
				rawHisto.SetBinContent(spBinLowRaw, spBinLowContent)
				rawHisto.SetBinContent(spBinHighRaw, spBinHighContent)
				KDEHisto.SetBinContent(spBinLowKDE, spBinLowContent)
				KDEHisto.SetBinContent(spBinHighKDE, spBinHighContent)

			if var in ['el_eProbabilityHT', 'el_eProbabilityHT_RNN']:
				spBinRaw = rawHisto.FindBin(0.5)
				spBinKDE = KDEHisto.FindBin(0.5)
				rawHisto.SetBinContent(spBinRaw, spBinContent)
				KDEHisto.SetBinContent(spBinKDE, spBinContent)

			# Move underflow/overflow bins to first and last histogram bin
			underflow = rawHisto.GetBinContent(0)
			overflow = rawHisto.GetBinContent(rawHisto.GetNbinsX()+1)
			rawHisto.SetBinContent(0,0) # empty underflow
			rawHisto.SetBinContent(rawHisto.GetNbinsX()+1,0) # empty overflow

			rawHisto.AddBinContent(1, underflow)
			rawHisto.AddBinContent(rawHisto.GetNbinsX(), overflow)

			KDEHisto.AddBinContent(1, underflow)
			KDEHisto.AddBinContent(KDEHisto.GetNbinsX(), overflow)

			# Navigate to correct folder and write histogram to file
			pdf_out_file.cd(var+'/'+file_type)
			rawHisto.Write()
			KDEHisto.Write()
			pdf_out_file.cd()

	print("KDE smoothing finished")
	pdf_out_file.Close()
	signal_file.Close()
	bkg_file.Close()

def make_hists(thn, args):
	"""
	Takes in a 4d histogram and returns 2d histograms of the last two
	variables in per each bin of the first two variables
	"""
	th2_list = ROOT.TList()
	th1_list = ROOT.TList()
	print(thn.GetTitle())

	axis_vars = thn.GetTitle().split('-')
	d0, d1, d2, d3 = axis_vars

	# For each pt, eta bin, set the range to a single bin then flatten to a 2d
	# hist of discrimant (x-axis) vs nvtx (y-axis)
	for d0_bin in range(1, thn.GetAxis(0).GetNbins()+1):
		d0_low_edge = thn.GetAxis(0).GetBinLowEdge(d0_bin)
		d0_bin_id = 'et{low:.0f}'.format(low=d0_low_edge)

		for d1_bin in range(1, thn.GetAxis(1).GetNbins()+1):
			d1_low_edge = thn.GetAxis(1).GetBinLowEdge(d1_bin)
			d1_bin_id = 'eta{low:3.2f}'.format(low=d1_low_edge)

			# Set range of bin to specific d0, d1 bins
			thn.GetAxis(0).SetRange(d0_bin, d0_bin)
			thn.GetAxis(1).SetRange(d1_bin, d1_bin)
			th2_id = 'h_2D_el-{}Discriminant_{d0}{d1}'.format(args.pileup_obs,
				d0=d0_bin_id, d1=d1_bin_id)
			th1_id = 'h_el_LHDiscriminant_{d0}{d1}'.format(
				d0=d0_bin_id, d1=d1_bin_id)


			# Project onto d2,d3
			if args.verbose:
				print("Creating histogram: {hist}".format(hist=th2_id))
			th2 = thn.Projection(3, 2)
			th2.SetNameTitle(th2_id, th2_id)
			th2_list.append(th2)
			th1 = thn.Projection(2)
			th1.SetNameTitle(th1_id, th1_id)
			th1_list.append(th1)
	return th2_list, th1_list

def make_tuning_hists(args):
	"""
	For each run directory that matches the expression given in the
	command-line argument, converts the 4d histograms assumed to be present
	in the histograms.root file into 2d histograms of LHDiscriminant vs <PileupObs>
	and 1d histograms inclusive of <Pileupobs>. Both 2d and 1d histograms are produced
	for each pt/eta bin that is used for tuning and are placed in a root file.

	In each run directory, this process is done for each tag_probe_pair, for
	each systematic and for each object that histograms are produced for. For
	example, for a config with one tag-probe pair plotting the preselected and
	normal probe for the nominal systematic will produce two root files.
	"""

	ROOT.TH2.AddDirectory(ROOT.kFALSE)

	# find run directories
	rundirs = glob(args.expression)
	if not rundirs:
		print('No run directories found corresponding to expression "{0}".'
			.format(args.expression))
	# go through each run directory
	for rundir in rundirs:
		print('In run directory "{0}"...'.format(rundir))
		with cd(rundir):

			if os.path.exists('histograms.root'):
				hist_file = ROOT.TFile.Open('histograms.root')
				for key in hist_file.GetListOfKeys():
					if key.GetClassName() == 'TDirectoryFile':
						hist_file.cd(key.GetName())
						for systematic in ROOT.gDirectory.GetListOfKeys():
							hist_file.cd(key.GetName()+"/" + systematic.GetName())
							for probe_name in ROOT.gDirectory.GetListOfKeys():

								# File path to cd into to get histograms
								filepath = "{key}/{systematic}/{probe_name}".format(
									key=key.GetName(), systematic=systematic.GetName(),
									probe_name=probe_name.GetName())

								# File name for new root file
								filename = "{key}_{systematic}_{probe_name}".format(
									key=key.GetName(), systematic=systematic.GetName(),
									probe_name=probe_name.GetName())

								hist_file.cd(filepath+'/reco')
								thn_histogram = ROOT.THnSparseD()

								for histogram in ROOT.gDirectory.GetListOfKeys():
									if histogram.GetClassName() == 'THnSparseT<TArrayD>':
										thn_histogram = ROOT.gDirectory.Get(
											histogram.GetName())

								# make the file and the directory structure
								tuning_file = ROOT.TFile(filename+'_tuningHists.root',
									'RECREATE')
								# need cutflow histograms in the tuning part
								h_cutflow = hist_file.Get("Cutflow")
								h_cutflow.Write()

								trig_name = '_trig_EF' if args.trig else ''

								main_folder = "LH_Alg{trig}_PassTrackQuality".format(
									trig=trig_name)

								two_d_folder = main_folder + \
									"_h_2D_el_{}Discriminant_et_eta".format(args.pileup_obs)

								one_d_folder = main_folder + \
									"_h_el_LHDiscriminant_et_eta"

								ROOT.gDirectory.mkdir(main_folder)
								tuning_file.cd(main_folder)

								ROOT.gDirectory.mkdir(two_d_folder)
								ROOT.gDirectory.cd(two_d_folder)

								# get the TH2s and write to file
								th2_list, th1_list = make_hists(thn_histogram, args)
								print('Writing 2d hists to file...')
								th2_list.Write()

								ROOT.gDirectory.cd("..")
								ROOT.gDirectory.mkdir(one_d_folder)
								ROOT.gDirectory.cd(one_d_folder)

								print('Writing 1d hists to file...')
								th1_list.Write()

								tuning_file.Close()

def main():
	"""Performs the action requested via cmd-line arguments."""
	args = parse_args()

	if args.action == 'translate':
		translate(args)
	if args.action == 'smooth':
		hadd_and_kde_smoothing(args)
	if args.action == 'tune':
		make_tuning_hists(args)


if __name__ == '__main__':
	main()
