# bins stored as stings to make regex searching for bin easier.
# Dont need to try to convert string to float which is error prone.
KDEPtBins = ['4','7','10','15','20','30','40','50','250']
KDEEtaBins = ['0.00','0.60','0.80','1.15','1.37','1.52','1.81','2.01','2.37']

def getKDEFineFactorSignal(variable, pt, eta):
    if isinstance(KDEFineFactorSig[variable], list):
        return KDEFineFactorSig[variable][KDEPtBins.index(pt)][KDEEtaBins.index(eta)]
    else:
        return KDEFineFactorSig[variable]

def getKDEFineFactorBackground(variable, pt, eta):
    if isinstance(KDEFineFactorBkg[variable], list):
        return KDEFineFactorBkg[variable][KDEPtBins.index(pt)][KDEEtaBins.index(eta)]
    else:
        return KDEFineFactorBkg[variable]


KDEFineFactorSig = dict()
KDEFineFactorBkg = dict()

KDEFineFactorSig = {'el_deltaeta1':   1.0
                        ,'el_f0':         1.5
                        ,'el_f1':         0.9
                        ,'el_f3':         2.
                        ,'el_fside':      1.
                        ,'fracs1':      1.
						,'delta_E':     0.2
						,'w_eta1':		1.
                        ,'el_deltaphi2':  1.5
                        ,'el_reta':       2.
                        ,'el_rhad':       2.
                        ,'el_rhad1':      2.
                        ,'el_rphi':       1.0
                        ,'el_weta2':     15.
                        ,'el_wstot':      0.5
                        ,'el_ws3':        2.
                        ,'el_d0Sig':      0.3
                        ,'el_d0significance':      0.3
                        ,'el_TRTHighTHitsRatio': 2.
                        ,'el_TRTHighTOutliersRatio': 2.
                        ,'el_trackd0':    0.2
                        ,'el_trackd0_physics':    0.2
                        ,'el_trackd0pvunbiased':    0.2
                        ,'el_deltaEmax2': 0.2
                        ,'el_eratio': 0.2
                        ,'el_ptcone20pt': 2.
                        ,'el_EoverP':     0.15
                        ,'el_PassBL':     1.
                        ,'el_deltaphiRescaled':1.
                        ,'el_DeltaPoverP': 1.
                        ,'el_MVAResponse':2.
                        ,'el_cl_phi'     :2.
                        ,'el_eProbabilityHT':1
                        ,'el_eProbabilityHT_RNN':1
                        ,'el_TRT_PID':2
                        ,'el_TRT_PID_RNN':2
                        ,'el_TRT_PID_significance':1
                        ,'el_charged0':1.2
                        ,'el_deltaDeltaPhiFirstAndLM':1.3
                        ,'el_deltaCurvOverErrCurv':0.3
                        }

KDEFineFactorSig['el_weta2'] =     [[15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0], #4
                                    [15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0], #7
                                    [15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0], #10
                                    [15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0], #15
                                    [15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0], #20
                                    [15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0], #30
                                    [15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0], #40
                                    [15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0], #50
                                    [15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0]] #250
                                    #  [15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0], #40
                                    #  [15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0], #40
                                    #  [15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0], #40
                                    #  [15.0, 15.0, 15.0, 15.0, 2.0, 15.0, 15.0, 15.0, 15.0]] #40

KDEFineFactorSig['el_eratio'] =     [[0.3, 0.2, 0.2,  0.2,  0.2,  0.2,  0.4, 1.0, 0.2], #4
                                     [0.3, 0.2, 0.2,  0.2,  0.2,  0.2,  0.4, 1.0, 0.2], #7
                                     [0.3, 0.2, 0.2,  0.2,  0.2,  0.2,  0.4, 1.0, 0.2], #10
                                     [0.3, 0.3, 0.3,  0.3,  0.3,  0.3,  0.4, 1.0, 0.2], #15
                                     [0.2, 0.2, 0.3,  0.3,  0.3,  0.2,  1.0, 1.0, 0.2], #20
                                     [0.2, 0.2, 0.25, 0.3,  0.3,  0.25, 1.0, 1.0, 0.2], #30
                                     [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 0.2], #40
                                     [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 0.2], #50
                                     [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 0.2]] #250
                                     #  [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 0.2], #40
                                     #  [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 0.2], #40
                                     #  [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 0.2], #40
                                     #  [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 0.2]] #40

KDEFineFactorSig['el_deltaEmax2'] = [[0.3, 0.2, 0.2,  0.2,  0.2,  0.2,  0.4, 1.0, 1.0], #4
                                     [0.3, 0.2, 0.2,  0.2,  0.2,  0.2,  0.4, 1.0, 1.0], #7
                                     [0.3, 0.2, 0.2,  0.2,  0.2,  0.2,  0.4, 1.0, 1.0], #10
                                     [0.3, 0.3, 0.3,  0.3,  0.3,  0.3,  0.4, 1.0, 1.0], #15
                                     [0.2, 0.2, 0.3,  0.3,  0.3,  0.2,  1.0, 1.0, 1.0], #20
                                     [0.2, 0.2, 0.25, 0.3,  0.3,  0.25, 1.0, 1.0, 1.0], #30
                                     [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 1.0], #40
                                     [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 1.0], #50
                                     [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 1.0]] #250
                                     #  [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 1.0], #40
                                     #  [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 1.0], #40
                                     #  [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 1.0], #40
                                     #  [0.2, 0.2, 0.25, 0.25, 0.25, 0.2,  1.0, 1.0, 1.0]] #40

KDEFineFactorBkg = {'el_deltaeta1':   1.0 #0.5
                        ,'el_f0':         1.5
                        ,'el_f1':         0.9
                        ,'el_f3':         2.
                        ,'el_fside':      1.
                        ,'el_deltaphi2':  1.0
                        ,'el_reta':       1.0 # was 0.5
                        ,'el_rhad':       1.2 #0.275 # 0.1 #1.2
                        ,'el_rhad1':      1.2
                        ,'el_rphi':       1.3 # 0.8
                        ,'el_wstot':      0.5
                        ,'el_ws3':        2.
                        ,'el_d0Sig':      0.3
                        ,'el_d0significance':      0.3
                        ,'el_TRTHighTHitsRatio': 2.
                        ,'el_TRTHighTOutliersRatio': 2.
                        ,'el_trackd0':    0.1
                        ,'el_trackd0_physics':    0.1
                        ,'el_trackd0pvunbiased':    1.0 #0.1
                        ,'el_ptcone20pt': 1.0
                        ,'el_EoverP':     0.1
                        ,'el_PassBL':     1.
                        ,'el_deltaphiRescaled':2. #1.
                        ,'el_DeltaPoverP': 2.
                        ,'el_MVAResponse':2.
                        ,'el_cl_phi'     :2.
                        ,'el_eProbabilityHT':1
                        ,'el_eProbabilityHT_RNN':1
                        ,'el_TRT_PID':2
                        ,'el_TRT_PID_RNN':2
                        ,'el_TRT_PID_significance':1
                        ,'el_charged0':1.2
                        ,'el_deltaDeltaPhiFirstAndLM':1.3
                        ,'el_deltaCurvOverErrCurv':0.3
                        }

KDEFineFactorBkg['el_weta2'] = [[8.0, 8.0, 8.0, 8.0, 6.0, 8.0, 8.0, 8.0, 8.0], #4  +2
                                             [8.0, 8.0, 8.0, 8.0, 6.0, 8.0, 8.0, 8.0, 8.0], #7
                                             [8.0, 8.0, 8.0, 8.0, 6.0, 8.0, 8.0, 8.0, 8.0], #10
                                             [4.0, 4.0, 4.0, 4.0, 2.0, 4.0, 4.0, 4.0, 4.0], #15
                                             [4.0, 4.0, 4.0, 4.0, 2.0, 4.0, 4.0, 4.0, 4.0], #20
                                             [4.0, 4.0, 4.0, 4.0, 2.0, 4.0, 4.0, 4.0, 4.0], #30
                                             [4.0, 4.0, 4.0, 4.0, 2.0, 4.0, 4.0, 4.0, 4.0]] #40

KDEFineFactorBkg['el_deltaEmax2'] = [[0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 1.0, 1.0], #4
                                             [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 1.0, 1.0], #7
                                             [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 1.0, 1.0], #10
                                             [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 1.0, 1.0], #15
                                             [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.6, 1.0, 1.0], #20
                                             [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.6, 1.0, 1.0], #30
                                             [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 1.0, 1.0, 1.0]] #40

KDEFineFactorBkg['el_eratio'] =     [[1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 4.0, 0.2], #4
                                             [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 4.0, 0.2], #7
                                             [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 4.0, 0.2], #10
                                             [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 1.0, 0.2], #15
                                             [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.6, 1.0, 0.2], #20
                                             [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.6, 1.0, 0.2], #30
                                             [0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 1.0, 1.0, 0.2]] #40
