import os
import math
eta_names_f0 = ["0 < |eta| < 0.6","0.6 < |eta| < 0.8","0.8 < |eta| < 1.15","1.15 < |eta| < 1.37","1.52 < |eta| < 1.81","1.81 < |eta| < 2.01","2.01 < |eta| < 2.37"]
def list_txt_files(directory):
	txt_files = [f for f in os.listdir(directory) if f.endswith('txt')]
	return txt_files

#  directory_path1 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/nominal_subtract_con.txt'
#  directory_path1 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/truth_pt_subtract_con.txt'
directory_path1 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/truth_pt_subtract_con.txt'
#  directory_path1 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/nominal/con_get_con.txt'
#  directory_path1 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataconvIDEF_all_final.txt'
#  directory_path2 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/nominal_subtract_unc.txt'
#  directory_path2 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/truth_pt_subtract_unc.txt'
directory_path2 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/truth_pt_subtract_unc.txt'
#  directory_path2 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/nominal/unc_get_unc.txt'
#  directory_path2 ='/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataUnconvIDEF_all_final.txt'


directory = "/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/four_groups/substract_bkg_txts/new_conversion_match/"
files = os.listdir(directory)

files_con =[file for file in files if file.endswith("con.txt")]
files_unc =[file for file in files if file.endswith("unc.txt")]

entries_f1 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]
entries_f2 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]
entries_f3 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]
entries_f7 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]
con_values = [[0 for i in range(12)] for j in range(7)]
unc_values = [[0 for i in range(12)] for j in range(7)]

with open(directory_path1,"r") as f1:
    for j,line in enumerate(f1):
        line =line.strip()
        lines = line.split()

        if j%13==0:
            continue
        entries_f1[int(j/13)][j%13-1][0] = float(lines[0])
        entries_f1[int(j/13)][j%13-1][1] = float(lines[1])

with open(directory_path2,"r") as f2:
    for j,line in enumerate(f2):
        line =line.strip()
        lines = line.split()

        if j%13==0:
            continue
        entries_f2[int(j/13)][j%13-1][0] = float(lines[0])
        entries_f2[int(j/13)][j%13-1][1] = float(lines[1])
 
for file in files_con:
    with open(f"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/four_groups/substract_bkg_txts/new_conversion_match/{file}","r") as f3:
        for j,line in enumerate(f3):
            line =line.strip()
            lines = line.split()

            if j%13==0:
                continue
            entries_f3[int(j/13)][j%13-1][0] = float(lines[0])
            entries_f3[int(j/13)][j%13-1][1] = float(lines[1])
    final_con =[[0 for i in range(12) ] for j in range(7)]

    con_value = [[0 for i in range(12)] for j in range(7)]

    con_value_add_add = [[0 for i in range(12)] for j in range(7)]

    for j in range(7):
        for k in range(12):
            con_value[j][k] = entries_f1[j][k][0]/entries_f1[j][k][1]
            con_value_add_add[j][k] = entries_f3[j][k][0]/entries_f3[j][k][1]


    for j in range(7):
        for k in range(12):
            con_values[j][k] += (con_value[j][k] - con_value_add_add[j][k])**2

for file in files_unc:
    with open(f"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/four_groups/substract_bkg_txts/new_conversion_match/{file}","r") as f7:
        for j,line in enumerate(f7):
            line =line.strip()
            lines = line.split()

            if j%13==0:
                continue
            entries_f7[int(j/13)][j%13-1][0] = float(lines[0])
            entries_f7[int(j/13)][j%13-1][1] = float(lines[1])
    final_unc =[[0 for i in range(12) ] for j in range(7)]
    unc_value = [[0 for i in range(12)] for j in range(7)]

    unc_value_add_add = [[0 for i in range(12)] for j in range(7)]

    for j in range(7):
        for k in range(12):
            unc_value[j][k] = entries_f2[j][k][0]/entries_f2[j][k][1]
            unc_value_add_add[j][k] = entries_f7[j][k][0]/entries_f7[j][k][1]


    for j in range(7):
        for k in range(12):
            unc_values[j][k] += (unc_value[j][k] - unc_value_add_add[j][k])**2

with open('./converted_final_uncertainty.txt','w') as f5:
	for i in range(7):
		f5.write(f"{eta_names_f0[i]}\n")
		for j in range(12):
			f5.write(f"{math.sqrt(con_values[i][j])}\n")


with open('./unconverted_final_uncertainty.txt','w') as f6:
	for i in range(7):
		f6.write(f"{eta_names_f0[i]}\n")
		for j in range(12):
			f6.write(f"{math.sqrt(unc_values[i][j])}\n")
