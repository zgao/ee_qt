import ROOT
import math
import sys
import argparse

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()


ROOT.gStyle.SetOptStat(0)


parser = argparse.ArgumentParser(description='')
parser.add_argument('-conpdf', '--conpdf', dest="conpdf_file", required=True, help='con pdf file')
parser.add_argument('-uncpdf', '--uncpdf', dest="uncpdf_file", required=True, help='unc pdf file')
#  parser.add_argument('-conroot', '--conroot', dest="conroot_file", required=True, help='con root file')
#  parser.add_argument('-uncroot', '--uncroot', dest="uncroot_file", required=True, help='unc root file')
args = parser.parse_args()

file_stat_con = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/data/con/final_new/converted_err.root","READ")
file_stat_unc = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/data/unc/final_new/unconverted_err.root","READ")

file_bkg_con =ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/bkg_systematic_uncertainty/75_80_con.root","READ")
file_bkg_unc =ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/bkg_systematic_uncertainty/75_80_unc.root","READ")

canvas=ROOT.TCanvas("canvas","",800,600)
canvas.Print(args.conpdf_file+"[")
canvas2=ROOT.TCanvas("canvas2","",800,600)
canvas2.Print(args.uncpdf_file+"[")
seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]
#  out_con = ROOT.TFile(args.conroot_file,"RECREATE")
#  out_unc = ROOT.TFile(args.uncroot_file,"RECREATE")

for i in range(7):
	h_stat_con = file_stat_con.Get(f"histogram2;{int(i+1)}")
	h_stat_unc = file_stat_unc.Get(f"histogram2;{int(i+1)}")

	h_bkg_con = file_bkg_con.Get(f"histogram;{int(i+1)}")
	h_bkg_unc = file_bkg_unc.Get(f"histogram;{int(i+1)}")

	canvas.cd()
	h_stat_con.Draw()
	h_stat_con.GetYaxis().SetRangeUser(0,0.02)
	h_bkg_con.Draw("same")

	h_stat_con.SetLineColor(ROOT.kBlue)
	h_bkg_con.SetLineColor(ROOT.kRed)
	legend = ROOT.TLegend(0.6,0.5,0.9,0.7)																											 
	legend.AddEntry(h_stat_con,"stat","l")
	legend.AddEntry(h_bkg_con," background ","l")
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	legend.Draw("same")

	text1 = ROOT.TPaveText(150,0.014,200,0.017)
	text1.AddText("converted")
	text1.AddText(seven_eta_cut_name[i])
	text1.SetFillStyle(0)
	text1.SetBorderSize(0)
	text1.SetTextSize(0.05)
	text1.Draw("same")

	canvas.Print(args.conpdf_file)

	canvas2.cd()
	h_stat_unc.Draw()
	h_stat_unc.GetYaxis().SetRangeUser(0,0.02)
	h_bkg_unc.Draw("same")
	h_stat_unc.SetLineColor(ROOT.kBlue)
	h_bkg_unc.SetLineColor(ROOT.kRed)
	legend = ROOT.TLegend(0.6,0.5,0.9,0.7)																											 
	legend.AddEntry(h_stat_unc,"stat","l")
	legend.AddEntry(h_bkg_unc,"background","l")
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	legend.Draw("same")

	text1 = ROOT.TPaveText(150,0.014,200,0.017)
	text1.AddText("unconverted")
	text1.AddText(seven_eta_cut_name[i])
	text1.SetFillStyle(0)
	text1.SetBorderSize(0)
	text1.SetTextSize(0.05)
	text1.Draw("same")
	canvas2.Print(args.uncpdf_file)

canvas.Print(args.conpdf_file+"]")
canvas2.Print(args.uncpdf_file+"]")
