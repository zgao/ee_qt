import ROOT

# Create a workspace
workspace = ROOT.RooWorkspace("workspace")

# Create a RooRealVar for invariant mass
mee = ROOT.RooRealVar("mee", "Invariant Mass", 60, 120, "GeV")

# Load your data and MC distributions
data_hist = ROOT.TH1F("data_hist", "Data Invariant Mass", 60, 60, 120)
mc_signal_hist = ROOT.TH1F("mc_signal_hist", "MC Signal Invariant Mass", 60, 60, 120)
background_template_hist = ROOT.TH1F("background_template_hist", "Background Template", 60, 60, 120)

# Convert histograms to RooDataHist
data_rdh = ROOT.RooDataHist("data_rdh", "Data RooDataHist", ROOT.RooArgList(mee), data_hist)
mc_signal_rdh = ROOT.RooDataHist("mc_signal_rdh", "MC Signal RooDataHist", ROOT.RooArgList(mee), mc_signal_hist)

# Create background template RooHistPdf
background_template_pdf = ROOT.RooHistPdf("background_template_pdf", "Background Template RooHistPdf",
		                                          ROOT.RooArgSet(mee), background_template_hist)

# Define a normalization factor for the template fit
background_norm = ROOT.RooRealVar("background_norm", "Background Normalization", 0, 1000)

# Create the combined model for fitting
combined_pdf = ROOT.RooAddPdf("combined_pdf", "Combined PDF", ROOT.RooArgList(background_template_pdf),
		                               ROOT.RooArgList(background_norm))

# Perform the template fit
fit_result = combined_pdf.fitTo(data_rdh, ROOT.RooFit.Range(60, 120))

# Get the fitted background normalization
fitted_background_norm = background_norm.getVal()

# Create the estimated signal distribution by subtracting the fitted background from data
estimated_signal_hist = data_hist.Clone("estimated_signal_hist")
estimated_signal_hist.Add(background_template_hist, -fitted_background_norm)

# Plot the original data, fitted template, and estimated signal
canvas = ROOT.TCanvas("canvas", "Template Fit")
frame = mee.frame()
data_rdh.plotOn(frame)
combined_pdf.plotOn(frame)
estimated_signal_rdh = ROOT.RooDataHist("estimated_signal_rdh", "Estimated Signal RooDataHist",
		                                        ROOT.RooArgList(mee), estimated_signal_hist)
estimated_signal_rdh.plotOn(frame, ROOT.RooFit.LineColor(ROOT.kRed))
frame.Draw()

# Print the fitted background normalization
print("Fitted Background Normalization:", fitted_background_norm)

#  canvas.Draw()
canvas.SaveAs("123.pdf")

