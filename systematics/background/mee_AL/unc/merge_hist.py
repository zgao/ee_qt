import ROOT

def merge_histograms(input_file_name, output_file_name, hist_name_prefix):
    # Open the input ROOT file
    input_file = ROOT.TFile(input_file_name)

    # Create a list to hold histograms to be merged
    hist_list = ROOT.TList()

    # Loop over all objects in the file
    for key in input_file.GetListOfKeys():
        obj = key.ReadObj()
        if isinstance(obj, ROOT.TH1) and obj.GetName().startswith(hist_name_prefix):
            # Add the histogram to the list
            hist_list.Add(obj)

    # Merge histograms in the list
    merged_hist = hist_list[0].Clone()
    for i in range(1, hist_list.GetEntries()):
        merged_hist.Add(hist_list[i])

    # Create an output file
    output_file = ROOT.TFile(output_file_name, "RECREATE")

    # Write the merged histogram to the output file
    merged_hist.Write()

    # Close the output file
    output_file.Close()

    print("Histograms merged successfully and written to", output_file_name)

if __name__ == "__main__":
    input_file_name = "merge.root"
    output_file_name = "output.root"
    #  hist_name_prefix = "ID_hist"  # Prefix of histograms to be merged
    hist_name_prefix = "AL_hist"  # Prefix of histograms to be merged

    merge_histograms(input_file_name, output_file_name, hist_name_prefix)

