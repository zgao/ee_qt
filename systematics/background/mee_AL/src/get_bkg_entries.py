import ROOT

left_value = 80
right_value = 100
left_left_value = 70
right_right_value = 110


#  left_value = 75
#  right_value = 105
#  left_left_value = 60
#  right_right_value = 120

#  left_value = 82
#  right_value = 98
#  left_left_value = 74
#  right_right_value = 106

def get_bkg_entries(hist):
	#  print(hist.GetNBinsX())
	bin_number_left = hist.FindBin(left_value)
	bin_number_right = hist.FindBin(right_value)

	bincontent_left = hist.GetBinContent(bin_number_left)
	bincontent_right = hist.GetBinContent(bin_number_right)

	bin_number_left_left = hist.FindBin(left_left_value)
	bin_number_right_right = hist.FindBin(right_right_value)

	bincontent_left_left = hist.GetBinContent(bin_number_left_left)
	bincontent_right_right = hist.GetBinContent(bin_number_right_right)
	
	left_entries_medium  = 0
	for i in range(bin_number_left_left+1, bin_number_left):
		left_entries_medium += hist.GetBinContent(i)

	right_entries_medium  = 0
	for i in range(bin_number_right+1, bin_number_right_right ):
		right_entries_medium += hist.GetBinContent(i)


	left_entries_left = (hist.GetBinLowEdge(bin_number_left_left +1) -left_left_value ) / hist.GetBinWidth(bin_number_left_left) * bincontent_left_left
	left_entries_right = (left_value - hist.GetBinLowEdge(bin_number_left) )/ hist.GetBinWidth(bin_number_left) * bincontent_left

	right_entries_right = (right_right_value - hist.GetBinLowEdge(bin_number_right_right ) ) / hist.GetBinWidth(bin_number_right_right) * bincontent_right_right
	right_entries_left = (hist.GetBinLowEdge(bin_number_right+1) - right_value)/ hist.GetBinWidth(bin_number_right) * bincontent_right

	total_entries_left = left_entries_medium + left_entries_left + left_entries_right
	total_entries_right = right_entries_medium + right_entries_left + right_entries_right

	return total_entries_left + total_entries_right

def get_22(hist):
	integral1 = hist.Integral(hist.FindBin(80), hist.FindBin(100))
	integral2 = hist.Integral(hist.FindBin(70), hist.FindBin(110))
	return integral2 - integral1



#  f_con_AL = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/con/merge.root","READ")
#  f_con_ID = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_ID/con/merge.root","READ")
f_con_AL = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/con/frag_down/merge_all.root","READ")
f_con_ID = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_ID/con/frag_down/merge_all.root","READ")

#  f_con_AL = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/con/frag_up/merge_all.root","READ")
#  f_con_ID = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_ID/con/frag_up/merge_all.root","READ")


#----------------------------------------------------------------------------------------------------------------------------------

#  f_unc_AL = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/unc/merge.root","READ")
#  f_unc_ID = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_ID/unc/merge.root","READ")
f_unc_AL = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/unc/frag_down/merge_all.root","READ")
f_unc_ID = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_ID/unc/frag_down/merge_all.root","READ")

#  f_unc_AL = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/unc/frag_up/merge_all.root","READ")
#  f_unc_ID = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_ID/unc/frag_up/merge_all.root","READ")

#  f_unc_AL = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/unc/test1.root","READ")
#  f_unc_ID = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_ID/unc/test1.root","READ")
with open ("background_con.txt","w")  as outfile: 
	for key in f_con_AL.GetListOfKeys():
		obj = key.ReadObj()
		AL_name = obj.GetName()
		ID_name = AL_name.replace("AL","ID")

		eta_pt_name = AL_name.replace("AL_hist_","")
		#  print(eta_pt_name)
		#  print(obj.GetName())
		#  print(ID_name)
		#  print(key)

		hist_AL = f_con_AL.Get(AL_name)
		hist_ID = f_con_ID.Get(ID_name)

		outfile.write(f"{eta_pt_name}-----AL; {get_bkg_entries(hist_AL)} ;ID; {get_bkg_entries(hist_ID)}\n")
		print("ID: ",get_22(hist_ID)," AL: ",get_22(hist_AL))

outfile.close()
print("background_con.txt has benn created!")


with open ("background_unc.txt","w")  as outfile: 
	for key in f_unc_AL.GetListOfKeys():
		obj = key.ReadObj()
		AL_name = obj.GetName()
		ID_name = AL_name.replace("AL","ID")

		eta_pt_name = AL_name.replace("AL_hist_","")
		#  print(eta_pt_name)
		#  print(obj.GetName())
		#  print(ID_name)
		#  print(key)

		hist_AL = f_unc_AL.Get(AL_name)
		hist_ID = f_unc_ID.Get(ID_name)

		outfile.write(f"{eta_pt_name}-----AL; {get_bkg_entries(hist_AL)} ;ID; {get_bkg_entries(hist_ID)}\n")
		#  print(get_22(hist_AL))
		print("ID: ",get_22(hist_ID)," AL: ",get_22(hist_AL))

outfile.close()
print("background_unc.txt has benn created!")


with open("./background_con.txt","r")as file1:
	for line in file1:
		line =line.strip()
		lines = line.split(";")
		if float(lines[-1]) > float(lines[1]):
			print("error	-----")
with open("./background_unc.txt","r")as file1:
	for line in file1:
		line =line.strip()
		lines = line.split(";")
		if float(lines[-1]) > float(lines[1]):
			print("error	-----")
