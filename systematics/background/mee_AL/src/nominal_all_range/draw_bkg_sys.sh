#!/bin/bash
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/75_80_con.txt -c converted -pdf bkg_systematic_uncertainty/75_80_con.pdf -root bkg_systematic_uncertainty/75_80_con.root
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/75_80_unc.txt -c unconverted -pdf bkg_systematic_uncertainty/75_80_unc.pdf -root bkg_systematic_uncertainty/75_80_unc.root


# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/frag_down/80_100_con_frag_down_uncertainty.txt -c converted -pdf bkg_systematic_uncertainty/frag_down/80_100_con_frag_down.pdf -root bkg_systematic_uncertainty/frag_down/80_100_con_frag_down.root
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/frag_down/80_100_unc_frag_down_uncertainty.txt -c unconverted -pdf bkg_systematic_uncertainty/frag_down/80_100_unc_frag_down.pdf -root bkg_systematic_uncertainty/frag_down/80_100_unc_frag_down.root

python3 draw_bkg_sys.py -in converted_final_uncertainty.txt -c converted -pdf converted.pdf -root converted.root
python3 draw_bkg_sys.py -in unconverted_final_uncertainty.txt -c unconverted -pdf unconverted.pdf -root unconverted.root
