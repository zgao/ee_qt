import sys

eta_names_f1 = ["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]
pt_names_f1 =  ['25<P_{t}<30','30<P_{t}<35','35<P_{t}<40','40<P_{t}<45','45<P_{t}<50','50<P_{t}<60','60<P_{t}<80','80<P_{t}<100','100<P_{t}<125','125<P_{t}<150','150<P_{t}<175','175<P_{t}<250']

eta_names_f0 = ["0 < |eta| < 0.6","0.6 < |eta| < 0.8","0.8 < |eta| < 1.15","1.15 < |eta| < 1.37","1.52 < |eta| < 1.81","1.81 < |eta| < 2.01","2.01 < |eta| < 2.37"]

source_file = sys.argv[1]
bkg_file = sys.argv[2]
out_file = sys.argv[3]

entries_f1 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]
#  with open("./75_105/background_con.txt","r") as f1:
with open(bkg_file,"r") as f1:
	for line in f1:
		line =line.strip()
		lines = line.split(";")

		for i in range(len(eta_names_f1)):
			if eta_names_f1[i] in lines[0]:
				for j in range(len(pt_names_f1)):
					if pt_names_f1[j] in lines[0]:
						entries_f1[i][j][0]= float(lines[1])
						entries_f1[i][j][1]= float(lines[3])
line_number = 0
entries_f2 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]
#  with open("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/75_105/dataconvIDEF_all_final.txt","r") as f0:
with open(source_file,"r") as f0:
	for line in f0 :
		line = line.strip()
		line = line.split()
		line_number +=1
		if (line_number-1) %13 ==0:
			continue
		#  print(line[0],line[1])
		eta_number = int((line_number -1) / 13)
		pt_number = line_number - eta_number* 13 -2
		entries_f2[eta_number][pt_number][0] = float(line[1])
		entries_f2[eta_number][pt_number][1] = float(line[0])
			

print(entries_f1[5][3])
print(entries_f2[5][3])

#  with open("75_105_con_test.txt","w") as f3:
with open(out_file,"w") as f3:
    for i in range(len(eta_names_f0)):
        #  print(eta_names_f0[i],"\n")
        f3.write(f"{eta_names_f0[i]}\n")

        for j in range(len(pt_names_f1)):
            #  print(float(entries_f2[i][j][0] - entries_f1[i][j][0]),"    ", float(entries_f2[i][j][1] - entries_f1[i][j][1]))
            f3.write(f"{float(entries_f2[i][j][0] - entries_f1[i][j][0])}\t\t\t{float(entries_f2[i][j][1] - entries_f1[i][j][1])}\n")

f3.close()

print(f"{out_file} has been created ")

#  print(sys.argv[0])
#  print(sys.argv[1])
