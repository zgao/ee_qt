import sys
eta_names_f1 = ["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]

file1 = sys.argv[1]
file2 = sys.argv[2]
file3 = sys.argv[3]

line_number = 0
entries_f0 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]
with open(file1,"r") as f0:
	for line in f0:
		line = line.strip()
		line = line.split()
		line_number +=1
		if (line_number-1) %13 ==0:
			continue
		#  print(line[0],line[1])
		eta_number = int((line_number -1) / 13)
		pt_number = line_number - eta_number* 13 -2
		entries_f0[eta_number][pt_number][0] = float(line[0])
		entries_f0[eta_number][pt_number][1] = float(line[1])

line_number = 0
entries_f1 = [[[0 for k in range(2)] for i in range(12) ] for j in range(7)]
with open(file2,"r") as f0:
	for line in f0:
		line = line.strip()
		line = line.split()
		line_number +=1
		if (line_number-1) %13 ==0:
			continue
		#  print(line[0],line[1])
		eta_number = int((line_number -1) / 13)
		pt_number = line_number - eta_number* 13 -2
		entries_f1[eta_number][pt_number][0] = float(line[0])
		entries_f1[eta_number][pt_number][1] = float(line[1])



with open(file3,"w") as f3:
	for i in range(len(eta_names_f1)):
		f3.write(f"{eta_names_f1[i]}\n")
	
		for j in range(12):
			f3.write(f"{abs(entries_f1[i][j][0] / entries_f1[i][j][1] - entries_f0[i][j][0] / entries_f0[i][j][1])}\n")
	
f3.close()
print(f"{file3} systematic uncertainty is calculated")
