#!/bin/bash
# p calculte_substract_bkg.py  /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/75_105/dataconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/75_105/background_con.txt 75_105_con.txt
# p calculte_substract_bkg.py  /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/75_105/dataUnconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/75_105/background_unc.txt 75_105_unc.txt
# #
# #
# p calculte_substract_bkg.py  /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/80_100/background_con.txt 80_100_con.txt
# p calculte_substract_bkg.py  /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataUnconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/80_100/background_unc.txt 80_100_unc.txt

# p calculte_substract_bkg.py  /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/82_98/dataconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/82_98/background_con.txt 82_98_con.txt
# p calculte_substract_bkg.py  /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/82_98/dataUnconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/82_98/background_unc.txt 82_98_unc.txt



#------------- calculte bkg systematic uncertainty -----------------


# p calculate_sys.py 75_105_con.txt 80_100_con.txt bkg_systematic_uncertainty/75_80_con.txt
# p calculate_sys.py 75_105_unc.txt 80_100_unc.txt bkg_systematic_uncertainty/75_80_unc.txt


# p calculate_sys.py 82_98_con.txt 80_100_con.txt bkg_systematic_uncertainty/82_80_con.txt
# p calculate_sys.py 82_98_unc.txt 80_100_unc.txt bkg_systematic_uncertainty/82_80_unc.txt

#----------------------------------------------------------------
# exchange means first numerator ,second denominator

# p calculte_substract_bkg_exchange.py	/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/80_100/background_con.txt 80_100_con_final.txt
# p calculte_substract_bkg_exchange.py	/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataUnconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/80_100/background_unc.txt 80_100_unc_final.txt

#-------   after revise uncertainty

# p calculte_substract_bkg_exchange.py /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/background_con.txt 80_100_con_final.txt
# p calculte_substract_bkg_exchange.py /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataUnconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/background_unc.txt 80_100_unc_final.txt

# p calculate_sys.py	/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/80_100_con_final.txt 80_100_con_background_uncertainty.txt
# p calculate_sys.py	/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataUnconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/80_100_unc_final.txt 80_100_unc_background_uncertainty.txt

# p calculte_substract_bkg_exchange.py /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/75_105/dataconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/75_105/background_con.txt 75_105_con_final.txt
# p calculte_substract_bkg_exchange.py /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/75_105/dataUnconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/75_105/background_unc.txt 75_105_unc_final.txt

# p calculate_sys.py	/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/75_105/dataconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/75_105/75_105_con_final.txt 75_105_con_background_uncertainty.txt
# p calculate_sys.py	/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/75_105/dataUnconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/75_105/75_105_unc_final.txt 75_105_unc_background_uncertainty.txt
# p calculate_sys.py /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/75_105/75_105_con_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/80_100/80_100_con_final.txt 1_2_final_revise_uncertainty_con.txt
# p calculate_sys.py /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/75_105/75_105_unc_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/80_100/80_100_unc_final.txt 1_2_final_revise_uncertainty_unc.txt

#----------------fragmentation -----
# p calculte_substract_bkg_exchange.py  /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/frag_down/dataconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_down/background_con.txt 80_100_con_frag_down.txt
# p calculte_substract_bkg_exchange.py  /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/frag_up/dataconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_up/background_con.txt 80_100_con_frag_up.txt

# p calculte_substract_bkg_exchange.py  /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/frag_down/dataUnconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_down/background_unc.txt 80_100_unc_frag_down.txt
# p calculte_substract_bkg_exchange.py  /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/frag_up/dataUnconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_up/background_unc.txt 80_100_unc_frag_up.txt


p calculate_sys.py	/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_down/80_100_con_frag_down.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_up/80_100_con_frag_up.txt 80_100_con_frag_down_up_uncertainty.txt
p calculate_sys.py	/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_down/80_100_unc_frag_down.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_up/80_100_unc_frag_up.txt 80_100_unc_frag_down_up_uncertainty.txt

# p calculate_sys.py	/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/frag_down/dataUnconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_down/80_100_unc_frag_down.txt 80_100_unc_frag_down_uncertainty.txt
# p calculate_sys.py	/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/frag_up/dataUnconvIDEF_all_final.txt /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_up/80_100_unc_frag_up.txt 80_100_unc_frag_up_uncertainty.txt
