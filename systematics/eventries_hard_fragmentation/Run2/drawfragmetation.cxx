#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TCanvas.h>
#include <TLegend.h>
using namespace std;
void drawfragme(const char* filename) {
    // Open the ROOT file
    TFile* file = TFile::Open(filename);
    if (!file || file->IsZombie()) {
        std::cerr << "Error: Unable to open file " << filename << std::endl;
        return;
    }

    // Get the PhotonCandidates tree
    TTree* tree = dynamic_cast<TTree*>(file->Get("PhotonCandidates"));
    if (!tree) {
        std::cerr << "Error: Unable to retrieve PhotonCandidates tree from file " << filename << std::endl;
        file->Close();
        return;
    }

    const Double_t binEdges[] = {25, 30, 35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250};
    const Int_t nBins = sizeof(binEdges) / sizeof(binEdges[0]) - 1;
    

    // Create histogram 1
    //TH1F* hist1 = new TH1F("hist1", "Normalized pt Distribution (All Events)", nBins, binEdges);
    TH1F* hist1 = new TH1F("hist1", "ConvPhoton pt Distribution (All Events)", 250, 0, 250);

    // Create histogram 2
    //TH1F* hist2 = new TH1F("hist2", "Frag ConvPhoton pt Distribution (MC_channel_number > 800671)", nBins, binEdges);
    TH1F* hist2 = new TH1F("hist2", "Frag ConvPhoton pt Distribution (MC_channel_number <801663)", 250, 0, 250);

    TH1F* hist3 = new TH1F("hist3", "UnconvPhoton pt Distribution (All Events)", 250, 0, 250);
    TH1F* hist4 = new TH1F("hist4", "Frag UnconvPhoton pt Distribution (MC_channel_number < 801663)", 250, 0, 250);

    TH1F* hist_hard_con = new TH1F("hist_hard_con", "hard con <800672", 250, 0, 250);
    TH1F* hist_hard_unc = new TH1F("hist_hard_unc", "hard unc <800672", 250, 0, 250);

    // Define branch variables
    Double_t pt, MCTotWeight, XsecWeight,MC_weight,Yweight,GE,SumWeights, Lumi;
	Float_t pileupWeight,kFacrot,vertexweight, truth_pt;
    Int_t MC_channel_number, convType;

    // Set branch addresses
    tree->SetBranchAddress("pt", &pt);
    tree->SetBranchAddress("MC_channel_number", &MC_channel_number);
    tree->SetBranchAddress("mcTotWeight_1", &MCTotWeight);
    tree->SetBranchAddress("MC_weight", &MC_weight);
    tree->SetBranchAddress("pileupWeight", &pileupWeight);
    tree->SetBranchAddress("Yweight", &Yweight);
    tree->SetBranchAddress("GE", &GE);
    tree->SetBranchAddress("XsecWeight", &XsecWeight);
    tree->SetBranchAddress("convType", &convType);
    tree->SetBranchAddress("SumWeights", &SumWeights);
    tree->SetBranchAddress("Lumi", &Lumi);
    tree->SetBranchAddress("kFacrot", &kFacrot);
    tree->SetBranchAddress("vertexweight", &vertexweight);
    tree->SetBranchAddress("truth_pt", &truth_pt);
    // Get the total number of events
    Long64_t nentries = tree->GetEntries();

	cout<<nentries<<endl;
    // Fill histogram 1
	Double_t frag_total = 0;
	Double_t frag_con = 0;
	Double_t frag_unc = 0;
	Double_t frag_total_weight = 0;
	Double_t frag_con_weight = 0;
	Double_t frag_unc_weight = 0;

	Double_t hard_total = 0;
	Double_t hard_con = 0;
	Double_t hard_unc = 0;
	Double_t hard_total_weight = 0;
	Double_t hard_con_weight = 0;
	Double_t hard_unc_weight = 0;

	Double_t all_total = 0;
	Double_t all_con = 0;
	Double_t all_unc = 0;
	Double_t all_total_weight = 0;
	Double_t all_con_weight = 0;
	Double_t all_unc_weight = 0;

	Double_t number[2][7][12];
	Double_t number_weight[2][7][12];

	// Double_t etas[7]={0,0.6,0.8,}

    for (Long64_t i = 0; i < nentries; ++i) {
        tree->GetEntry(i);
		if (i%1000000 ==0) cout<<i<<endl;
        // Calculate weight
        Float_t weight = MCTotWeight;
		all_total ++;
		all_total_weight +=weight;

		 std::vector<int> list_hard = {800658, 800659, 800660,800661, 800662, 800663, 800664, 800665, 800666, 800667, 800668, 800669, 800670, 800671};
		 std::vector<int> list_frag = {800672, 800673, 800674, 800675, 800676, 800677, 800678, 800679, 800680, 800681, 800682, 800683};
		
		 std::vector<int> list_truth_pt = {8, 17, 35, 50, 70, 140, 280, 500, 800, 1000, 1500, 2000, 2500, 3000};
		 if (std::find(list_hard.begin(), list_hard.end(), MC_channel_number) != list_hard.end()) {
			int series = std::distance(list_hard.begin(), std::find(list_hard.begin(), list_hard.end(), MC_channel_number));
			if (series < list_hard.size() - 1) {
				// if (truth_pt < list_truth_pt[series] || truth_pt > list_truth_pt[series + 1]) {
				if (truth_pt > list_truth_pt[series] && truth_pt < list_truth_pt[series + 1]) {
					continue; // Handle 'continue' accordingly in your context
				}
			}
			if (series == list_hard.size() - 1) {
				// if (truth_pt < list_truth_pt[series]) {
				if (truth_pt > list_truth_pt[series]) {
					continue; // Handle 'continue' accordingly in your context
				}
			}
			// std::cout << "hard: i" << i << std::endl;
		}
		if (std::find(list_frag.begin(), list_frag.end(), MC_channel_number) != list_frag.end()) {
			int series = std::distance(list_frag.begin(), std::find(list_frag.begin(), list_frag.end(), MC_channel_number));
			if (series < list_frag.size() - 1) {
				// if (truth_pt < list_truth_pt[series] || truth_pt > list_truth_pt[series + 1]) {
				if (truth_pt > list_truth_pt[series] && truth_pt < list_truth_pt[series + 1]) {
					continue; // Handle 'continue' accordingly in your context
				}
			}
			else if (series == list_frag.size() - 1) {
				// if (truth_pt < list_truth_pt[series]) {
				if (truth_pt > list_truth_pt[series]) {
					continue; // Handle 'continue' accordingly in your context
				}
			}
		}

        // Fill histogram 1
        if (convType > 0){
            hist1->Fill(pt, weight);
            // Fill histogram 2 only for events that meet the condition
			all_con ++;
			all_con_weight += weight;
            if (MC_channel_number >=800672) {
                hist2->Fill(pt, weight);
				frag_total ++;
				frag_total_weight += weight;
				frag_con ++;
				frag_con_weight +=weight;

            }
			else{
				hist_hard_con->Fill(pt,weight);
				hard_total ++;
				hard_total_weight += weight ;
				hard_con ++;
				hard_con_weight += weight ;
			}
        }
        else if (convType == 0) {
            hist3->Fill(pt, weight);
			all_unc ++;
			all_unc_weight += weight;
            if (MC_channel_number >=800672) {
                hist4->Fill(pt, weight);
				frag_total_weight += weight;
				frag_total ++;
				frag_unc ++;
				frag_unc_weight += weight ;
            }
			else{
				hist_hard_unc->Fill(pt,weight);
				hard_total_weight += weight;
				hard_total ++;
				hard_unc ++;
				hard_unc_weight += weight;
			}
        }

    }

	cout<<all_total<<" "<<all_con<<" "<<all_unc<<" "<<all_total_weight<<" "<<all_con_weight<<" "<<all_unc_weight<<" "<<endl;
	cout<<hard_total<<" "<<hard_con<<" "<<hard_unc<<" "<<hard_total_weight<<" "<<hard_con_weight<<" "<<hard_unc_weight<<" "<<endl;
	cout<<frag_total<<" "<<frag_con<<" "<<frag_unc<<" "<<frag_total_weight<<" "<<frag_con_weight<<" "<<frag_unc_weight<<" "<<endl;

    // Normalize histograms
    //hist1->Scale(1.0 / hist1->Integral());
    //hist2->Scale(1.0 / hist2->Integral());

    // Create canvas 1
    TCanvas* c1 = new TCanvas("c1", "Normalized pt Distributions", 800, 600);
	c1->cd();
    c1->SetLogy();
	

    // Draw histogram 1
    hist1->SetLineColor(kBlue);
    hist1->SetLineWidth(3);
    hist1->GetXaxis()->SetTitle("P_{T} [GeV]");
    hist1->GetYaxis()->SetTitle("Events");
    hist1->Draw();

    // Draw histogram 2, set color
    hist2->SetLineColor(kRed);
    hist2->SetLineWidth(2);
    hist2->GetXaxis()->SetTitle("P_{T} [GeV]");
    hist2->GetYaxis()->SetTitle("Events");
    hist2->Draw("SAME");

    hist_hard_con->SetLineColor(kGreen);
    hist_hard_con->SetLineWidth(2);
    hist_hard_con->GetXaxis()->SetTitle("P_{T} [GeV]");
    hist_hard_con->GetYaxis()->SetTitle("Events");
    hist_hard_con->Draw("SAME");

    // Set legend
    TLegend* legend1 = new TLegend(0.6, 0.6, 0.8, 0.8);
    legend1->AddEntry(hist1, "conv photon", "l");
    legend1->AddEntry(hist2, "frag conv photon", "l");
    legend1->AddEntry(hist_hard_con, "hard conv photon", "l");
    legend1->Draw();

    // Show canvas 1
    c1->SaveAs("frag_inclusive_con.pdf");

    // Create canvas 3
    TCanvas* c3 = new TCanvas("c3", "pt Distributions", 800, 600);
	c3->cd();
    c3->SetLogy();

    // Draw histogram 1
    hist3->SetLineColor(kBlue);
    hist3->SetLineWidth(3);
    hist3->GetXaxis()->SetTitle("P_{T} [GeV]");
    hist3->GetYaxis()->SetTitle("Events");
    hist3->Draw();

    // Draw histogram 2, set color
    hist4->SetLineColor(kRed);
    hist4->SetLineWidth(2);
    hist4->GetXaxis()->SetTitle("P_{T} [GeV]");
    hist4->GetYaxis()->SetTitle("Events");
    hist4->Draw("SAME");

    hist_hard_unc->SetLineColor(kGreen);
    hist_hard_unc->SetLineWidth(2);
    hist_hard_unc->GetXaxis()->SetTitle("P_{T} [GeV]");
    hist_hard_unc->GetYaxis()->SetTitle("Events");
    hist_hard_unc->Draw("SAME");

    // Set legend
    TLegend* legend3 = new TLegend(0.6, 0.6, 0.8, 0.8);
    legend3->AddEntry(hist3, "unconv photon", "l");
    legend3->AddEntry(hist4, "frag unconv photon", "l");
    legend3->AddEntry(hist_hard_unc, "hard unconv photon", "l");
    legend3->Draw();

    // Show canvas 1
    c3->SaveAs("frag_inclusive_unc.pdf");

    // Create canvas 2
    TCanvas* c2 = new TCanvas("c2", "Ratio of frag/photon (conv)", 800, 600);
    //c2->SetLogy();
	c2->cd();

    // Create histogram 3 to store the division result
    TH1F* hist5 = static_cast<TH1F*>(hist2->Clone("hist5"));
    hist5->Divide(hist1);
    hist5->SetTitle("Ratio of frag/photon (conv)");

    // Set canvas 2 title
    hist5->SetLineColor(kBlack);
    hist5->GetXaxis()->SetTitle("P_{T} [GeV]");
    hist5->GetYaxis()->SetTitle("Ratio");
    hist5->Draw();

    // Show canvas 2
    c2->SaveAs("frag_inclusive_ratio_con.pdf");

    TCanvas* con_hard_frag = new TCanvas("con_hard_frag", "pt Distributions", 800, 600);
	con_hard_frag->cd();
	hist_hard_con->Divide(hist2);
    hist_hard_con->SetTitle("Ratio of hard/frag (conv)");
	hist_hard_con->Draw();
	hist_hard_con->GetXaxis()->SetTitle("P_{T} [GeV]");
	hist_hard_con->GetYaxis()->SetTitle("Ratio");
	con_hard_frag->SaveAs("frag_hard_ratio_con.pdf");

    TCanvas* unc_hard_frag = new TCanvas("unc_hard_frag", "pt Distributions", 800, 600);
	unc_hard_frag->cd();
	hist_hard_unc->Divide(hist4);
    hist_hard_unc->SetTitle("Ratio of hard/frag (uncv)");
	hist_hard_unc->Draw();
	hist_hard_unc->GetXaxis()->SetTitle("P_{T} [GeV]");
	hist_hard_unc->GetYaxis()->SetTitle("Ratio");
	unc_hard_frag->SaveAs("frag_hard_ratio_unc.pdf");


    TCanvas* c4 = new TCanvas("c4", "all photon pt Distributions", 800, 600);

    TH1F* hist6 = static_cast<TH1F*>(hist4->Clone("hist6"));
    hist6->Divide(hist3);
    hist6->SetTitle("Ratio of frag/photon (unconv)");

    // Set canvas 2 title
    hist6->SetLineColor(kBlue);
    hist6->GetXaxis()->SetTitle("P_{T} [GeV]");
    hist6->GetYaxis()->SetTitle("Ratio");
    hist6->Draw();

    c4->SaveAs("frag_inclusive_ratio_unc.pdf");

    /*TCanvas* c5 = new TCanvas("c5", "frag photon pt Distributions", 800, 600);

    hist4->SetLineColor(kRed);
    hist4->SetLineWidth(2);
    hist4->Draw();
    c5->Draw();*/

    // TFile* fragphotonFile = TFile::Open("fragphoton.root", "RECREATE");
    // Write histograms to the new file with new names
    // hist1->Write("conv_photon");
    // hist2->Write("frag_conv_photon");
    // hist3->Write("unconv_photon");
    // hist4->Write("frag_unconv_photon");
	// hist_hard_con->Write(")

    // Close the new file
    // fragphotonFile->Close();

}


int drawfragmetation(){

    // drawfragme("/afs/cern.ch/user/z/zgao/eos/con1_fudge/photon_13_totWeight_fudged/merge.root");
    drawfragme("/afs/cern.ch/user/z/zgao/eos/con1_fudge/photon_13_totWeight_fudged/merge_truth_pt.root");

    return 0;

}
