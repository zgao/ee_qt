import ROOT
import os

tree = ROOT.TChain("PhotonCandidates")
#  tree = ROOT.TChain("SinglePhoton")


tree.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/new_conversion/user.zgao.mc23.801649.SinglePhoton.v10_hist.root")
tree.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/new_conversion/user*80165*.root")
tree.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/new_conversion/user.zgao.mc23.801660.SinglePhoton.v09_hist.root")


h_total_con = ROOT.TH1("total_con","total_con",250,0,250)
h_total_unc = ROOT.TH1("total_unc","total_unc",250,0,250)
h_hard_con = ROOT.TH1("hard_con","hard_con",250,0,250)
h_hard_unc = ROOT.TH1("hard_unc","hard_unc",250,0,250)
h_frag_con = ROOT.TH1("frag_con","frag_con",250,0,250)
h_frag_unc = ROOT.TH1("frag_unc","frag_unc",250,0,250)

number = tree.GetEntries()
print("totalnumber: frag ",number)
final_numer = 0
for i in range(number):
	tree.GetEntry(i)
	if i %100000==0:
		print(i)
	final_numer += tree.mcTotWeight_1

	if tree.convType >0:
		h_total_con.Fill(tree.pt,tree.mcTotWeight_1)
		h_frag_con.Fill(tree.pt,tree.mcTotWeight_1)
	if tree.convType ==0:
		h_total_unc.Fill(tree.pt,tree.mcTotWeight_1)
		h_frag_unc.Fill(tree.pt,tree.mcTotWeight_1)

print("totalnumer_weight: frag",final_numer)




tree2 = ROOT.TChain("PhotonCandidates")
#  tree2 = ROOT.TChain("SinglePhoton")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/new_conversion/user*80167*.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/new_conversion/user.zgao.mc23.801663.SinglePhoton.v10_hist.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/new_conversion/user.zgao.mc23.801664.SinglePhoton.v10_hist.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/new_conversion/user.zgao.mc23.801665.SinglePhoton.v10_hist.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/new_conversion/user.zgao.mc23.801666.SinglePhoton.v10_hist.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/new_conversion/user.zgao.mc23.801667.SinglePhoton.v10_hist.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/new_conversion/user.zgao.mc23.801668.SinglePhoton.v10_hist.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/new_conversion/user.zgao.mc23.801669.SinglePhoton.v10_hist.root")

final_number2 = 0
print("totalnumber: hard: ",tree2.GetEntries())
for j in range(tree2.GetEntries()):
#  for j in range(number):
	tree2.GetEntry(j)
	if j %100000==0:
		print(j)
	final_number2 += tree2.mcTotWeight_1

	if tree2.convType >0:
		h_total_con.Fill(tree2.pt,tree2.mcTotWeight_1)
		h_hard_con.Fill(tree2.pt,tree2.mcTotWeight_1)
	if tree2.convType ==0:
		h_total_unc.Fill(tree2.pt,tree2.mcTotWeight_1)
		h_hard_con.Fill(tree2.pt,tree2.mcTotWeight_1)
	#  final_number2 += tree2.mcTotWeight

print("totalnumber_weight: hard: ",final_number2)

c1= ROOT.TCanvas("c1","c1",800,600)
c1.cd()
c1.SetLogy()
h_total_con.Draw("h")
h_frag_con.Draw("same")
c1.SaveAs("total_frag_con_4_25.pdf")

c2= ROOT.TCanvas("c2","c2",800,600)
c2.cd()
c2.SetLogy()
h_total_unc.Draw("h")
h_frag_unc.Draw("same")
c2.SaveAs("total_frag_unc_4_25.pdf")

c3= ROOT.TCanvas("c3","c3",800,600)
c3.cd()
c3.SetLogy()
h_hard_con.Draw("h")
h_frag_con.Draw("same")
c3.SaveAs("hard_frag_con_4_25.pdf")

c4= ROOT.TCanvas("c4","c4",800,600)
c4.cd()
c4.SetLogy()
h_hard_unc.Draw("h")
h_frag_unc.Draw("same")
c4.SaveAs("hard_frag_unc_4_25.pdf")
