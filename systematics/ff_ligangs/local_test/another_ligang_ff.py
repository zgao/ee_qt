import ROOT
import iminuit
from iminuit import Minuit
import math
import argparse
import sys
from matplotlib import pyplot as plt


def has_duplicates(lst):
    seen = set()
    for value in lst:
        if value in seen:
            return True  # Duplicate found
        seen.add(value)
    return False  # No duplicates found

#  input_path = "./results.root"
#  input_path = "/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/ff_ligangs/both_pass/merge.root"
input_path = "./merge.root"
input_file = ROOT.TFile(input_path,"READ")

#  output_path = "/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/ff_ligangs/another_test.root"
output_path = "./another_test.root"
#  output_path = "/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/ff_ligangs/single_test.root"
output_file = ROOT.TFile(output_path,"RECREATE")

list_etas = ["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]
#  eta_series = int(sys.argv[1])
list_variables =['r_had1','r_had0','r_eta','r_phi','w_eta2','w_eta1','w_stot','fracs1','delta_E','e_ratio']
#  variable_series =int(sys.argv[2])

#  print(list_etas[eta_series])
#  exit()
key_list = input_file.GetListOfKeys()

for key in key_list:

    obj = key.ReadObj()

    obj_name = obj.GetName()

    #  if list_etas[eta_series] not in obj_name:
    #      continue
    #  if list_variables[variable_series] not in obj_name:
    #      continue
    if "data" not in obj_name:
        continue
    if "r_had0" not in obj_name:
    #  if "r_had1" not in obj_name:
    #  if "r_phi" not in obj_name:
    #  if "delta_E" not in obj_name:
    #  if "e_ratio" not in obj_name:
        continue
    #  if "data_hist_r_had1_1.15<|#eta|<1.37_80<P_{t}<100" not in obj_name:
        #  continue
    #  if "data_hist_delta_E_0.80<|#eta|<1.15_30<P_{t}<35" not in obj_name:
        #  continue
    #  if "data_hist_r_had1_2.01<|#eta|<2.37_40<P_{t}<45" not in obj_name:
    #  if "data_hist_r_had1_0.60<|#eta|<0.80_60<P_{t}<80" not in obj_name:
        #  continue
    #  #  if "data_hist_r_had1_0.00<|#eta|<0.60_25<P_{t}<30" not in obj_name:
        #  continue
    #  if "data_hist_fracs1_0.80<|#eta|<1.15_25<P_{t}<30" not in obj_name:
        #  continue
    #  if "e_ratio" not in obj_name: #5sigma,(0.4,3)
    #  if "data_hist_e_ratio_2.01<|#eta|<2.37_25<P_{t}<30" not in obj_name:
        #  continue
        #  continue
    
    number_failure = 0

    print(obj_name)


    hist_data = input_file.Get(obj_name)
    mc_name = obj_name.replace("data","mc")
    hist_mc = input_file.Get(mc_name)

    mc_value_mu = hist_mc.GetMean()
    mc_value_sigma = hist_mc.GetStdDev()

    data_value_mu = hist_data.GetMean()
    data_value_sigma = hist_data.GetStdDev()

    initial_value_sigma = mc_value_sigma / data_value_sigma
    initial_value_mu = initial_value_sigma * data_value_mu - mc_value_mu

    print("initial_value_mu ",initial_value_mu)
    print("initial_value_sigma ",initial_value_sigma)



    number_bins = hist_data.GetNbinsX()
    number_bins_2 = hist_mc.GetNbinsX()
    #  print('debug number of bins =', number_bins, number_bins_2)

    #  scale_parameter = hist_data.Integral()/hist_mc.Integral()
    #  print(scale_parameter)

    hist_temp_rename = obj_name.replace("data","temp")  
    hist_temp = hist_mc.Clone(hist_temp_rename)
    hist_temp.Sumw2()

    x_min = hist_temp.GetXaxis().GetXmin()
    x_max = hist_temp.GetXaxis().GetXmax()
    #  print("x_min ",x_min," x_max ",x_max)

    #  hist_temp.Reset()
    #  hist_temp.Sumw2()

    #  hist_data.Scale(1./hist_data.Integral())
    #  hist_mc.Scale(1./hist_mc.Integral())

    def cost_function(μ,σ,α):

        final_chi = 0
        chi_temp =0

        for i in range(number_bins):
            hist_temp.SetBinContent(i+1, 0)
            hist_temp.SetBinError(i+1, 0)

        for i in range(number_bins):

            x_primitive_low_edge = hist_mc.GetBinLowEdge(i+1)
            x_primitive_high_edge = hist_mc.GetBinLowEdge(i+1+1)

            x_after_low_edge = x_primitive_low_edge * σ - μ
            x_after_high_edge = x_primitive_high_edge * σ - μ

            #  if abs(x_after_low_edge - x_after_high_edge) >= abs(x_max-x_min):
                #  hist_temp.SetBinContent( i+1, 0)
                #  hist_temp.SetBinError( i+1, 0)



            bin_x_after_low_edge = hist_mc.FindBin(x_after_low_edge)
            bin_x_after_high_edge = hist_mc.FindBin(x_after_high_edge)

            #  print("σ: ",σ," μ: ",μ," bin_x_after_low_edge: ",bin_x_after_low_edge," bin_x_after_high_edge: ",bin_x_after_high_edge)
            
            if bin_x_after_low_edge ==0 and bin_x_after_high_edge==0:
                continue
            if bin_x_after_low_edge ==int(number_bins+1)  and bin_x_after_high_edge== int(number_bins+1):
                continue

            hist_mc.SetBinContent(0,0)
            hist_mc.SetBinError(0,0)
            hist_mc.SetBinContent(number_bins+1,0)
            hist_mc.SetBinError(number_bins+1,0)

            bin_width = hist_mc.GetBinWidth(1)

            if abs(bin_x_after_low_edge - bin_x_after_high_edge) == 0:
                #  print(" the same bin ")
                fraction = abs(x_after_high_edge - x_after_low_edge)/ bin_width
                hist_temp.SetBinContent( i+1, fraction * hist_mc.GetBinContent(bin_x_after_low_edge)  )
                hist_temp.SetBinError( i+1, fraction * hist_mc.GetBinError(bin_x_after_low_edge) )

                #  if i==2553:
                    #  print("fraction ",fraction," content ",hist_temp.GetBinContent(i+1))


            #  specific_bin = hist_mc.FindBin( x_after )
            #  hist_temp.Fill( x_after, hist_mc.GetBinContent(i+1))

            if abs(bin_x_after_low_edge - bin_x_after_high_edge) >= 1:
                #  print(" not the same bin ..................")
                high_edge_bin_x_after_low_edge = hist_mc.GetBinLowEdge(bin_x_after_low_edge+1)
                low_edge_bin_x_after_high_edge = hist_mc.GetBinLowEdge(bin_x_after_high_edge)

                fraction_1 = abs(high_edge_bin_x_after_low_edge - x_after_low_edge) / bin_width
                fraction_2 = abs(low_edge_bin_x_after_high_edge - x_after_high_edge) /bin_width
                #  if i==2553:
                    #  print("fraction1 ",fraction_1," content1 ",hist_temp.GetBinContent(bin_x_after_low_edge))
                    #  print("fraction2 ",fraction_2," content2 ",hist_temp.GetBinContent(bin_x_after_high_edge))
                #  print("fraction_1 ",fraction_1," fraction_2 ",fraction_2, "i is ",i)

                bincontent_temp = 0
                binerror_temp = 0
                for mediate_bin in range(bin_x_after_low_edge + 1, bin_x_after_high_edge):
                    #  print("bin_x_after_low_edge: ",bin_x_after_low_edge," bin_x_after_high_edge ",bin_x_after_high_edge," 1/σ ",1/σ)
                    bincontent_temp += hist_mc.GetBinContent(mediate_bin)
                    #  print("bincontent_temp: ",bincontent_temp," bin_x_after_low_edge + 1 ",bin_x_after_low_edge + 1," bin_x_after_high_edge: ",bin_x_after_high_edge)
                    
                    binerror_temp += hist_mc.GetBinError(mediate_bin)**2

                final_bin_content = fraction_1 * hist_mc.GetBinContent(bin_x_after_low_edge) + fraction_2 * hist_mc.GetBinContent(bin_x_after_high_edge) + bincontent_temp 
                final_bin_error = math.sqrt( (fraction_1 * hist_mc.GetBinError(bin_x_after_low_edge))**2 + (fraction_2 * hist_mc.GetBinError(bin_x_after_high_edge))**2 + ( binerror_temp))


                hist_temp.SetBinContent(i+1 , final_bin_content)
                hist_temp.SetBinError(i+1 , final_bin_error)
            #  if i == number_bins :
                #  if hist_temp.Integral() ==0 :
                    #  list_zero.append(obj.GetName())
                    #  continue


        #  if hist_temp.Integral() ==0 :
            #  print("σ: ",σ," μ: ",μ," bin_x_after_low_edge: ",bin_x_after_low_edge," bin_x_after_high_edge: ",bin_x_after_high_edge)
        #  if σ == nan:
            #  continue

        #  if hist_temp.Integral()==0:
            #  continue

        # hiist_data.Scale(1./hist_data.Integral())
        #  hist_mc.Scale(1./hist_mc.Integral())
        #  print(hist_temp.GetBinContent(2554),"------------")
        if hist_temp.Integral()>0:
            hist_temp.Scale(1./hist_temp.Integral())
        #  print(hist_temp.GetBinContent(2554),"------------0000000")

        for i in range(number_bins):
            if hist_temp.GetBinContent(i+1) <0:
                #  print(i+1, "minus ")
                hist_temp.SetBinContent(i+1,0)
            
            if hist_data.GetBinContent(i+1)==0 and hist_temp.GetBinError(i+1) ==0:
                continue
            #  print("mc content ",hist_temp.GetBinContent(i+1), "mc error ",hist_temp.GetBinError(i+1),"data content ",hist_data.GetBinContent(i+1), " data error ",math.sqrt(hist_data.GetBinContent(i+1)))

            chi_temp += (hist_data.GetBinContent(i+1) - α * hist_temp.GetBinContent(i+1))**2 / (hist_data.GetBinContent(i+1) + α**2 *  hist_temp.GetBinError(i+1)**2)


        final_chi = chi_temp

        return final_chi


    #  m = Minuit(cost_function,μ = initial_value_mu, σ = initial_value_sigma, α=hist_data.Integral())
    #  m = Minuit(cost_function,μ = 0, σ = 1, α=hist_data.Integral())

    list_initial = [(0,1),(initial_value_mu,initial_value_sigma)]
    
    #  aa=0
    for series in list_initial:
        m = Minuit(cost_function,μ = series[0], σ = series[1], α=hist_data.Integral())
        m.limits["μ"]= (-3 * mc_value_sigma, 3* mc_value_sigma)
        m.limits["σ"] =(0.1,10)
        m.limits["α"] =(0.1*hist_data.Integral(),3*hist_data.Integral())
        #  m.errors = (0.01*mc_value_sigma, 0.01, 0.01*hist_data.Integral())
        m.migrad()
        print(m.migrad())
        m.hesse()
        #  m.valid
        print(m.valid," ========================================= ")
        if m.valid == True:
            #  print('The fit succeeds with i =', i)
            break
    print("first attempt is over ")

    #  if m.valid == False:
    #      N = 20
    #      step_mu = 0.05*mc_value_sigma
    #      for i in range(N):
    #          initial_mu = (-N/2+i)*step_mu
    #          #  print("============== ",initial_values[0],initial_values[1])
    #          m = Minuit(cost_function,μ = initial_mu, σ = 1, α=hist_data.Integral())
    #          m.limits["μ"]= (-3 * mc_value_sigma, 3* mc_value_sigma)
    #          m.limits["σ"] =(0.1,10)
    #          m.limits["α"] =(0.1*hist_data.Integral(),3*hist_data.Integral())
    #          m.errors = (0.01*mc_value_sigma, 0.01, 0.01*hist_data.Integral())
    #          m.migrad()
    #          m.hesse()
    #          print(m.valid)
    #          if m.valid == True:
    #              print('The fit succeeds with i =', i)
    #              break



    print(" theoretic_value_mu ",initial_value_mu," theoretic_value_sigma ",initial_value_sigma, 'initial alpha =', hist_data.Integral())
    print("mc_value_sigma", mc_value_sigma)
    print('underflow bin 0 =', hist_data.GetBinContent(0))
    print('overflow bin nbins =', hist_data.GetBinContent(hist_data.GetNbinsX()+1))

    #  print(m.valid)
    print("μ ",m.values["μ"])
    print("σ ",m.values["σ"])
    print("α ",m.values["α"])
    print("initial_value_sigma", initial_value_sigma)
    print("initial_integral_alpha ",hist_data.Integral())
    print("*****___***** final state : ",m.valid)

    #  m.draw_mnprofile("μ")
    #  plt.show()
    


    k = 1/m.values["σ"]
    b = m.values["μ"]
    #  c = m.values["α"]

    hist_final_rename = obj_name.replace("data","final_ff_mc")
    hist_final =  hist_mc.Clone(hist_final_rename)
    hist_final.Reset()

    for i in range(hist_mc.GetNbinsX()):
        #  print("i       ",i)
        x_hist_final_low_edge  = hist_mc.GetBinLowEdge(i+1)
        x_hist_final_high_edge = hist_mc.GetBinLowEdge(i+2)

        hist_mc.SetBinContent(0,0)
        hist_mc.SetBinError(0,0)

        hist_mc.SetBinContent(hist_mc.GetNbinsX()+1,0)
        hist_mc.SetBinError(hist_mc.GetNbinsX()+1,0)


        x_after_of_low_edge = x_hist_final_low_edge/k - b
        x_after_of_high_edge = x_hist_final_high_edge/k - b

        bin_number_of_low_edge = hist_mc.FindBin(x_after_of_low_edge)
        bin_number_of_up_edge = hist_mc.FindBin(x_after_of_high_edge)
        #  print(" bin_low ",bin_number_of_low_edge," bin_high ",bin_number_of_up_edge)


        high_edge_of_low_bin = hist_mc.GetBinLowEdge(bin_number_of_low_edge+1)
        low_edge_of_low_bin = hist_mc.GetBinLowEdge(bin_number_of_low_edge)

        low_edge_of_high_bin = hist_mc.GetBinLowEdge(bin_number_of_up_edge)

        bin_width = hist_mc.GetBinWidth(1)

        #-----------------------------------------------------------------------------------------------
        if bin_number_of_low_edge > bin_number_of_up_edge:
            print("low bin > high bin")


        if (bin_number_of_up_edge - bin_number_of_low_edge) >=3:
            print("bin difference is bigger than 2")
        if (bin_number_of_up_edge - bin_number_of_low_edge) ==2:
            print("bin difference is bigger than 1 equal to 2")
        #  if (bin_number_of_up_edge - bin_number_of_low_edge) ==1:
            #  print("bin difference is equal to 1")
        #-----------------------------------------------------------------------------------------------

        if abs(bin_number_of_low_edge - bin_number_of_up_edge ) == 0:
            #  print(" the same bin ")
            fraction = abs(x_after_of_low_edge - x_after_of_high_edge)/ bin_width
            #  hist_final.SetBinContent( i+1, fraction * hist_mc.GetBinContent(bin_number_of_low_edge) /k )
            hist_final.SetBinContent( i+1, fraction * hist_mc.GetBinContent(bin_number_of_low_edge))
            #  hist_final.SetBinError( i+1, fraction * hist_mc.GetBinError(bin_number_of_low_edge)  /k )
            hist_final.SetBinError( i+1, fraction * hist_mc.GetBinError(bin_number_of_low_edge))



        if abs(bin_number_of_up_edge - bin_number_of_low_edge) >= 1:
            #  print(" not the same bin ..................")
            high_edge_bin_x_after_low_edge = hist_mc.GetBinLowEdge(bin_number_of_low_edge+1)
            low_edge_bin_x_after_high_edge = hist_mc.GetBinLowEdge(bin_number_of_up_edge)

            fraction_1 = abs(high_edge_bin_x_after_low_edge - x_after_of_low_edge) / bin_width
            fraction_2 = abs(low_edge_bin_x_after_high_edge - x_after_of_high_edge) /bin_width
            #  print("fraction_1 ",fraction_1," fraction_2 ",fraction_2, "i is ",i)

            bincontent_temp = 0
            binerror_temp = 0
            for mediate_bin in range(bin_number_of_low_edge+ 1, bin_number_of_up_edge):
                #  print("bin_number_of_low_edge: ",bin_number_of_low_edge," bin_number_of_up_edge ",bin_number_of_up_edge," 1/σ ",k)
                bincontent_temp += hist_mc.GetBinContent(mediate_bin)
                
                binerror_temp += hist_mc.GetBinError(mediate_bin)**2

            #  final_bin_content = fraction_1 * hist_mc.GetBinContent(bin_number_of_low_edge) / k+ fraction_2 * hist_mc.GetBinContent(bin_number_of_up_edge) / k + bincontent_temp / k
            final_bin_content = fraction_1 * hist_mc.GetBinContent(bin_number_of_low_edge) + fraction_2 * hist_mc.GetBinContent(bin_number_of_up_edge) + bincontent_temp
            final_bin_error = math.sqrt( (fraction_1 * hist_mc.GetBinError(bin_number_of_low_edge))**2 + (fraction_2 * hist_mc.GetBinError(bin_number_of_up_edge) )**2 + ( binerror_temp))


            hist_final.SetBinContent(i+1 , final_bin_content)
            hist_final.SetBinError(i+1 , final_bin_error)
    

    output_file.WriteTObject(hist_final)
    output_file.WriteTObject(hist_temp)
    output_file.WriteTObject(hist_mc)
    output_file.WriteTObject(hist_data)
    print(f"{obj_name} is completed")
    #  break
#  print("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/ff_ligangs/another_version.root  is created")
print(f"{output_path}  is created")
#  with open("failure_zero.txt","w") as file_zero:
    #  for line in list_zero:
        #  file_zero.write(line)

