# Open the input file in read mode and output file in write mode
import sys

name = sys.argv[1]
with open(f'{name}.txt', 'r') as infile, open(f'{name}_temp.txt', 'w') as outfile:
    # Iterate over each line in the input file
    for line in infile:
        # Check if the line contains the word "difference"
        if "difference" not in line:
            # Write the line to the output file
            outfile.write(line)

