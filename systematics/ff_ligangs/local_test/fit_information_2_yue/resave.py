import ROOT

# Open the original ROOT file
original_file = ROOT.TFile("final.root", "READ")

# Create a new ROOT file to save selected histograms
new_file = ROOT.TFile("selected_histograms.root", "RECREATE")

# Iterate over all keys in the original ROOT file
for key in original_file.GetListOfKeys():
    obj = key.ReadObj()
    # Check if the object is a TH1F histogram and contains "final_ff" in its name
    if isinstance(obj, ROOT.TH1F) and "final_ff" in obj.GetName():
        # Write the histogram to the new ROOT file
        obj.Write()

# Close the original and new ROOT files
original_file.Close()
new_file.Close()

