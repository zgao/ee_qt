import ROOT

# Open the original ROOT file
original_file = ROOT.TFile("selected_histograms.root", "READ")

# Create a new ROOT file to save the renamed histograms
new_file = ROOT.TFile("renamed_histograms.root", "RECREATE")

# Iterate over all keys in the original ROOT file
for key in original_file.GetListOfKeys():
    obj = key.ReadObj()
    # Check if the object is a TH1 histogram and if its name matches the pattern
    if isinstance(obj, ROOT.TH1) and obj.GetName().startswith("final_ff_"):
        # Remove the "final_ff_" prefix from the histogram's name
        new_name = obj.GetName().replace("final_ff_mc_", "")
        obj.SetName(new_name)
        # Write the histogram to the new ROOT file with the new name
        obj.Write()

# Close the original and new ROOT files
original_file.Close()
new_file.Close()

