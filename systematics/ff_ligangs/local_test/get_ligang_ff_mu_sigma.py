import ROOT

variables=['r_had1','r_had0','r_eta','r_phi','w_eta2','w_eta1','w_stot','fracs1','delta_E','e_ratio']

list_eta = ["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]

list_pt =  ['25<P_{t}<30','30<P_{t}<35','35<P_{t}<40','40<P_{t}<45','45<P_{t}<50','50<P_{t}<60','60<P_{t}<80','80<P_{t}<100','100<P_{t}<125','125<P_{t}<150','150<P_{t}<175','175<P_{t}<250']

list_mus = [[[0 for i in range(len(list_pt))] for j in range(len(list_eta))] for k in range(len(variables))]
list_sigmas = [[[0 for i in range(len(list_pt))] for j in range(len(list_eta))] for k in range(len(variables))]

with open("./fit_information_2_yue/e_ratio_temp.txt") as file1:
    lines = file1.readlines()

    for i,line in enumerate(lines):
        #  print(line)
        if "The_fit_is_valid" in line:
            list_name = lines[i+1].split()
            print(list_name[0])

            list_sigma = lines[i-5].split()
            print("sigma: ",list_sigma[1])
            list_mu = lines[i-6].split()
            print("mu: ",list_mu[1])

            name_split = list_name[0].split("_")
            pt_range = name_split[-2] + '_' + name_split[-1]
            eta_range = name_split[-3]
            print(pt_range)
            print(eta_range)

            name_temp = 0
            name_temp = "_".join(name_split[2:-3])
            print(name_temp)

            index_variable = variables.index(name_temp)
            #  print(variables.index(name_temp))
            index_eta = list_eta.index(eta_range)
            #  print(list_eta.index(eta_range))
            index_pt = list_pt.index(pt_range)
            #  print(list_pt.index(pt_range))

            list_mus[index_variable][index_eta][index_pt] = list_mu[1]
            list_sigmas[index_variable][index_eta][index_pt] = list_sigma[1]
    print(list_mus)
    print(list_sigmas)



            

