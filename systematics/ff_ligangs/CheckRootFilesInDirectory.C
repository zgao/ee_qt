#include <iostream>
#include <TSystem.h> // For gSystem
#include <TString.h> // For TString
#include <TSystemDirectory.h>
#include <TList.h>

bool IsRootFileIntact(const char* filename) {
    // Open the ROOT file
    TFile file(filename, "READ");

    // Check if the file is intact (not zombie)
    bool isIntact = !file.IsZombie();

    // Close the file
    file.Close();

    return isIntact;
}

void CheckRootFilesInDirectory(){
    // Create a TSystemDirectory to list files in the directory
	// const char *directoryPath ="/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/both_pass_probe/data";
	const char *directoryPath =".";
    TSystemDirectory dir(directoryPath, directoryPath);

    // Get the list of files in the directory
    TList* filesList = dir.GetListOfFiles();

    if (filesList) {
        // Iterate over the files in the directory
        for (int i = 0; i < filesList->GetSize(); ++i) {
            TString filename(filesList->At(i)->GetName());
            
            // Check if the file has a .root extension
            if (filename.EndsWith(".root")) {
                TString filePath = TString::Format("%s/%s", directoryPath, filename.Data());

                // Check if the ROOT file is intact
                if (IsRootFileIntact(filePath.Data())) {
                    std::cout << "File " << filePath << " is intact." << std::endl;
                } else {
                    std::cout << "File " << filePath << " is corrupted or cannot be opened." << std::endl;
                }
            }
        }
    }
}

// int main() {
//     // Provide the directory path
//     const char* directoryPath = "/path/to/your/directory";
//
//     // Check ROOT files in the directory
//     CheckRootFilesInDirectory(directoryPath);
//
//     return 0;
// }

