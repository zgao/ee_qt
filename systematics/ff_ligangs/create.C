void create(){
	// int binnumber =500;
	int binnumber =5000;
	TRandom3 rndgen;

	TH1D h("hist_mc", "hist_mc", /*nbins=*/binnumber, /*xmin=*/-10, /*xmax=*/10);
	// h.FillRandom("gaus", [>nsamples=<]1000000);

	TH1D h2("hist_data", "hist_data", /*nbins=*/binnumber, /*xmin=*/-10, /*xmax=*/10);
	for(int i=0;i<1000000;i++){
		h2.Fill(rndgen.Gaus(4,2));
		h.Fill(rndgen.Gaus(3,1));
	}

	TFile * file1 = new TFile("results.root","recreate");

	h.Write();
	h2.Write();
	file1->Close();

	// for(int i =0 ;i< h.GetNbinsX();i++){
	//     cout<<"BinContetn: "<<h.GetBinContent(i+1)<<endl;
	//     cout<<h.GetBinError(i+1)<<endl;
	// }
}


