import ROOT
import math
import argparse





parser = argparse.ArgumentParser(description='')
parser.add_argument('-in', '--input', dest="input_variable", required=True, help='input which variable ')
parser.add_argument('-pt', '--pt', dest="input_pt", required=True, help='input which pt ')
parser.add_argument('-eta', '--eta', dest="input_eta", required=True, help='input which eta')
parser.add_argument('-o','--output',dest='output',required=True,help='location hist saved')
args = parser.parse_args()

#  tree_data = ROOT.TChain("Electrons_All")
tree_data = ROOT.TChain("ZeeCandidates")
tree_data.Add("/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/both_pass_probe/data/user*.root")
entries_data = tree_data.GetEntries()
print(entries_data)
list_branches = ["r_had1" ,"r_had0" ,"r_eta", "r_phi", "w_eta2", "w_eta1", "w_stot", "fracs1", "delta_E","e_ratio"]
bin_numbers = [200,400,1000,1000,500,500,200,500,500,500]
low_edge = [-0.05,-0.1,0.2,0.2,0.,0.1,0,0,-500,0.]
#  high_edge = [0.05, 0.1, 1.2, 1.2, 0.02, 1.0, 6, 1, 3000, 1.0]
high_edge = [0.05, 0.1, 1.2, 1.2, 0.02, 1.0, 6, 1, 3000, 1.2]
list_pts = [ 25, 30, 35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250]
list_etas = [0.00, 0.60, 0.80, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37]

seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]
#  seven_eta_cut_name_rename=["0_00_eta_0_60","0_60_eta_0_80","0_80_eta_1_15","1_15_eta_1_37","1_37_eta_1_52","1_52_eta_1_81","1_81_eta_2_01","2_01_eta_2_37"]
seven_eta_cut_name_rename=["0_00_eta_0_60","0_60_eta_0_80","0_80_eta_1_15","1_15_eta_1_37","1_52_eta_1_81","1_81_eta_2_01","2_01_eta_2_37"]

seven_eta_cut_select_temp=["primary_cluster_be2_abseta__NOSYS[0]<0.6",
				"primary_cluster_be2_abseta__NOSYS[0]>0.6 && primary_cluster_be2_abseta__NOSYS[0]<0.80",
				"primary_cluster_be2_abseta__NOSYS[0]>0.80 && primary_cluster_be2_abseta__NOSYS[0]<1.15",																		
				"primary_cluster_be2_abseta__NOSYS[0]>1.15 && primary_cluster_be2_abseta__NOSYS[0]<1.37",
				"primary_cluster_be2_abseta__NOSYS[0]>1.52 && primary_cluster_be2_abseta__NOSYS[0]<1.81",
				"primary_cluster_be2_abseta__NOSYS[0]>1.81 && primary_cluster_be2_abseta__NOSYS[0]<2.01",
				"primary_cluster_be2_abseta__NOSYS[0]>2.01 && primary_cluster_be2_abseta__NOSYS[0]<2.37",
				]



twelve_pt_cut_name=['25<P_{t}<30','30<P_{t}<35','35<P_{t}<40','40<P_{t}<45','45<P_{t}<50','50<P_{t}<60','60<P_{t}<80','80<P_{t}<100','100<P_{t}<125','125<P_{t}<150','150<P_{t}<175','175<P_{t}<250']
twelve_pt_cut_name_rename=['25_Pt_30','30_Pt_35','35_Pt_40','40_Pt_45','45_Pt_50','50_Pt_60','60_Pt_80','80_Pt_100','100_Pt_125','125_Pt_150','150_Pt_175','175_Pt_250']
   
twelve_pt_cut_select_temp=["(pt__NOSYS[0]>25)&&(pt__NOSYS[0]<30)","(pt__NOSYS[0]>30)&&(pt__NOSYS[0]<35)","(pt__NOSYS[0]>35)&&(pt__NOSYS[0]<40)","(pt__NOSYS[0]>40)&&(pt__NOSYS[0]<45)","(pt__NOSYS[0]>45)&&(pt__NOSYS[0]<50)","(pt__NOSYS[0]>50)&&(pt__NOSYS[0]<60)","(pt__NOSYS[0]>60)&&(pt__NOSYS[0]<80)","(pt__NOSYS[0]>80)&&(pt__NOSYS[0]<100)","(pt__NOSYS[0]>100)&&(pt__NOSYS[0]<125)","(pt__NOSYS[0]>125)&&(pt__NOSYS[0]<150)","(pt__NOSYS[0]>150)&&(pt__NOSYS[0]<175)","(pt__NOSYS[0]>175)&&(pt__NOSYS[0]<250)"]
   

hists_names_data = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]																				
hists_names_mc = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]																				
hists_names_mc_ff = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]																				
hists_data = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]
hists_mc = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]
hists_mc_ff = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]

for i in range(12):
	#  if i != 11:
		#  continue
	for j in range(7):
		#  if j!=2:
			#  continue
		for k in range(10):
			#  if k!=0:
				#  continue
			#  print(i,j,k)
			hists_names_mc_ff[i][j][k]= 'mc_ff_hist' + '_' + list_branches[k] + '_' + seven_eta_cut_name[j] + '_' + twelve_pt_cut_name[i] 
			hists_names_data[i][j][k]= 'data_hist' + '_' + list_branches[k] + '_' + seven_eta_cut_name[j] + '_' + twelve_pt_cut_name[i] 
			hists_names_mc[i][j][k]= 'mc_hist' + '_' + list_branches[k] + '_' + seven_eta_cut_name[j] + '_' + twelve_pt_cut_name[i] 
			#  print(len(variables),len(seven_eta_cut_name),len(twelve_pt_cut_name))
			#  print("pass ")
			hists_data[i][j][k] = ROOT.TH1F(hists_names_data[i][j][k],hists_names_data[i][j][k],bin_numbers[k],low_edge[k],high_edge[k])
			hists_mc[i][j][k] = ROOT.TH1F(hists_names_mc[i][j][k],hists_names_mc[i][j][k],bin_numbers[k],low_edge[k],high_edge[k])
			hists_mc_ff[i][j][k] = ROOT.TH1F(hists_names_mc_ff[i][j][k],hists_names_mc_ff[i][j][k],bin_numbers[k],low_edge[k],high_edge[k])


output_file = ROOT.TFile(f"{args.output}/{list_branches[int(args.input_variable)]}_{seven_eta_cut_name_rename[int(args.input_eta)]}_{twelve_pt_cut_name_rename[int(args.input_pt)]}.root", "recreate")


E_value_data = 0
C_value_data = 0
number_data = 0
for entry in range(entries_data):

	tree_data.GetEntry(entry)

	if entry % 1000000 ==0:
		print(entry)

	if tree_data.f1<0.05:
		continue
	if not tree_data.pass__denominator==1:
		continue
	if tree_data.pt >250 or tree_data.pt<25:
		continue
	if tree_data.primary_cluster_be2_abseta>2.37:
		continue
	if tree_data.primary_cluster_be2_abseta>1.37 and tree_data.primary_cluster_be2_abseta<1.52 :
		continue

	r_had1 = tree_data.r_had1
	r_had0 = tree_data.r_had0
	r_eta = tree_data.r_eta
	r_phi = tree_data.r_phi
	w_eta2 = tree_data.w_eta2
	w_eta1 = tree_data.w_eta1
	w_stot = tree_data.w_stot
	fracs1 = tree_data.fracs1
	delta_E = tree_data.delta_E
	e_ratio = tree_data.e_ratio

	lists_variables = [r_had1, r_had0, r_eta, r_phi, w_eta2, w_eta1, w_stot, fracs1, delta_E, e_ratio]


	#  if tree_data.fracs1__NOSYS[0]<0 or tree_data.fracs1__NOSYS[0]>1. :
		#  continue
	if tree_data.pt < list_pts[int(args.input_pt)] or tree_data.pt > list_pts[int(args.input_pt)+1]:
		continue

	if int(args.input_eta) >=4 :
		if tree_data.primary_cluster_be2_abseta < list_etas[int(args.input_eta)+1] or tree_data.primary_cluster_be2_abseta > list_etas[int(args.input_eta)+2]:
			continue
	if int(args.input_eta) <4:
		if tree_data.primary_cluster_be2_abseta < list_etas[int(args.input_eta)] or tree_data.primary_cluster_be2_abseta > list_etas[int(args.input_eta)+1]:
			continue

	if lists_variables[int(args.input_variable)] < low_edge[int(args.input_variable)] or lists_variables[int(args.input_variable)] > high_edge[int(args.input_variable)]:
		continue
	#  hists_data.Fill(tree_data.fracs1__NOSYS[0])

	hists_data[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].Fill(lists_variables[int(args.input_variable)])
	

	number_data += 1
	#  E_value_data += tree_data.fracs1__NOSYS[0]
	E_value_data += lists_variables[int(args.input_variable)]
	#  print(tree_data.fracs1__NOSYS[0])

	

print("number_data ",number_data)
E_value_data_final = E_value_data/number_data
print("E_value_data_final_one_one ",E_value_data_final)


print(hists_data[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetMean())
print(hists_data[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetStdDev())
E_value_data_final = hists_data[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetMean()
C_value_data_final = hists_data[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetStdDev() * hists_data[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetStdDev()
print("E_value_data_final: ", E_value_data_final," C_value_data_final: ", C_value_data_final)
#  exit()
#  exit()
#------------------------
tree_mc = ROOT.TChain("ZeeCandidates")
tree_mc.Add("/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/both_pass_probe/no_ff/user*.root")
entries_mc = tree_mc.GetEntries()
#  entries_mc =1000000
print("entries_mc ",entries_mc)
#  entries_mc =1000000
#  entries_mc =1000000

E_value_mc = 0
C_value_mc = 0
number_mc = 0
xsec = 2000000
cross = 31.3985

f1 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/no_ff/user.zgao.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20231212_134708_hist/merge.root","READ")
sumweight = f1.Get("CutBookkeeper_601189_1_NOSYS").GetBinContent(2)
weight_temp =0
for entry in range(entries_mc):
	tree_mc.GetEntry(entry)
	if entry % 1000000 ==0:
		print(entry)
	#  if tree_mc.eta<1:
		#  continue
	if tree_mc.f1<0.05:
		continue
	if not tree_mc.pass__denominator==1:
		continue
	if tree_mc.pt >250 or tree_mc.pt<25:
		continue
	if tree_mc.primary_cluster_be2_abseta>2.37:
		continue
	if tree_mc.primary_cluster_be2_abseta>1.37 and tree_mc.primary_cluster_be2_abseta<1.52 :
		continue

	if tree_mc.pt < list_pts[int(args.input_pt)] or tree_mc.pt > list_pts[int(args.input_pt)+1]:
		continue

	if int(args.input_eta) >=4 :
		if tree_mc.primary_cluster_be2_abseta < list_etas[int(args.input_eta)+1] or tree_mc.primary_cluster_be2_abseta > list_etas[int(args.input_eta)+2]:
			continue

	if int(args.input_eta)< 4:	
		if tree_mc.primary_cluster_be2_abseta < list_etas[int(args.input_eta)] or tree_mc.primary_cluster_be2_abseta > list_etas[int(args.input_eta)+1]:
			continue
										  
	r_had1 = tree_mc.r_had1
	r_had0 = tree_mc.r_had0
	r_eta = tree_mc.r_eta
	r_phi = tree_mc.r_phi
	w_eta2 = tree_mc.w_eta2
	w_eta1 = tree_mc.w_eta1
	w_stot = tree_mc.w_stot
	fracs1 = tree_mc.fracs1
	delta_E = tree_mc.delta_E
	e_ratio = tree_mc.e_ratio
										  
	lists_variables = [r_had1, r_had0, r_eta, r_phi, w_eta2, w_eta1, w_stot, fracs1, delta_E, e_ratio]
										  
	if lists_variables[int(args.input_variable)] < low_edge[int(args.input_variable)] or lists_variables[int(args.input_variable)] > high_edge[int(args.input_variable)]:
		continue						  
										  
	weight = xsec * cross * tree_mc.MC_event_weight/sumweight
	hists_mc[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].Fill(lists_variables[int(args.input_variable)], weight)
	
	weight_temp += weight

	number_mc += 1
	E_value_mc += lists_variables[int(args.input_variable)] * weight


print("number_mc ",number_mc)
print("E_value_mc_final_one_one ",E_value_mc/weight_temp)
hists_mc[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetMean()
hists_mc[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetStdDev()

print(hists_mc[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetMean())
print(hists_mc[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetStdDev())

E_value_mc_final = hists_mc[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetMean()
C_value_mc_final = hists_mc[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetStdDev()* hists_mc[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetStdDev()
print("E_value_mc_final: ", E_value_mc_final," C_value_mc_final: ", C_value_mc_final)

sigma = math.sqrt(C_value_mc_final / C_value_data_final)
mu	  = sigma *E_value_data_final - E_value_mc_final
print(sigma)
print(mu)

#  exit()

output_file.WriteTObject(hists_data[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)])
output_file.WriteTObject(hists_mc[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)])


after_mc_mean =0
after_mc_mean_final =0

after_mc_co = 0
after_mc_co_final =0

after_mc_weight = 0
after_mc_weight_final = 0

for entry in range(entries_mc):
	tree_mc.GetEntry(entry)
	if entry % 1000000 ==0:
		print(entry)
	#  if tree_mc.eta<1:
		#  continue
	if tree_mc.f1<0.05:
	#  if tree_data.f1<0.05:
		continue
	if not tree_mc.pass__denominator==1:
		continue
	if tree_mc.pt >250 or tree_mc.pt<25:
		continue
	if tree_mc.primary_cluster_be2_abseta>2.37:
		continue
	if tree_mc.primary_cluster_be2_abseta>1.37 and tree_mc.primary_cluster_be2_abseta<1.52 :
		continue
																	
	r_had1 = tree_mc.r_had1
	r_had0 = tree_mc.r_had0
	r_eta = tree_mc.r_eta
	r_phi = tree_mc.r_phi
	w_eta2 = tree_mc.w_eta2
	w_eta1 = tree_mc.w_eta1
	w_stot = tree_mc.w_stot
	fracs1 = tree_mc.fracs1
	delta_E = tree_mc.delta_E
	e_ratio = tree_mc.e_ratio
																	
	lists_variables = [r_had1, r_had0, r_eta, r_phi, w_eta2, w_eta1, w_stot, fracs1, delta_E, e_ratio]
																	
	if lists_variables[int(args.input_variable)] < low_edge[int(args.input_variable)] or lists_variables[int(args.input_variable)] > high_edge[int(args.input_variable)]:
		continue
	if tree_mc.pt < list_pts[int(args.input_pt)] or tree_mc.pt > list_pts[int(args.input_pt)+1]:
		continue



	if int(args.input_eta) >=4 :
		if tree_mc.primary_cluster_be2_abseta < list_etas[int(args.input_eta)+1] or tree_mc.primary_cluster_be2_abseta > list_etas[int(args.input_eta)+2]:
			continue

	if int(args.input_eta)< 4:	
		if tree_mc.primary_cluster_be2_abseta < list_etas[int(args.input_eta)] or tree_mc.primary_cluster_be2_abseta > list_etas[int(args.input_eta)+1]:
			continue
	
	weight = xsec * cross * tree_mc.MC_event_weight/sumweight
	temp = (lists_variables[int(args.input_variable)] +mu)/sigma
	hists_mc_ff[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].Fill(temp,weight)

	after_mc_mean += temp* weight

	after_mc_weight += weight

after_mc_weight_final = after_mc_weight
after_mc_mean_final = after_mc_mean/after_mc_weight_final

print("after_mc_mean_final ",after_mc_mean_final)
print("after_mc_weight ",after_mc_weight)

print(hists_mc_ff[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetMean())
print(hists_mc_ff[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetStdDev())
print("after_mc_mean_final ", hists_mc_ff[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetMean())
print("after_mc_co_final ", hists_mc_ff[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].GetStdDev())






output_file.WriteTObject(hists_mc_ff[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)])
print(f"{args.output}/{list_branches[int(args.input_variable)]}_{seven_eta_cut_name_rename[int(args.input_eta)]}_{twelve_pt_cut_name_rename[int(args.input_pt)]}.root is created")

#  hists_data_scaled = hists_data[int(args.input_pt)][int(args.input_eta)][int(args.input_variable)].Clone("ta_scaled")
#  hists_data_scaled.Scale(1./hists_data_scaled.Integral())
#  output_file.WriteTObject(hists_data_scaled)

#  hists_mc_scaled = hists_mc.Clone("hists_mc_scaled")
#  hists_mc_scaled.Scale(1./hists_mc_scaled.Integral())
#  output_file.WriteTObject(hists_mc_scaled)

#  hists_mc_ff_scaled = hists_mc_ff.Clone("hists_mc_ff_scaled")
#  hists_mc_ff_scaled.Scale(1./hists_mc_ff_scaled.Integral())
#  output_file.WriteTObject(hists_mc_ff_scaled)
