import ROOT

def rename_histograms(input_file_name, output_file_name):
	# Open the input ROOT file
	input_file = ROOT.TFile(input_file_name, "READ")
	if input_file.IsZombie():
		print(f"Error: Could not open input file {input_file_name}")
		return

	# Create the output ROOT file
	output_file = ROOT.TFile(output_file_name, "RECREATE")
	if output_file.IsZombie():
		print(f"Error: Could not create output file {output_file_name}")
		input_file.Close()
		return

	# Loop over all objects in the input file
	for key in input_file.GetListOfKeys():
		obj = key.ReadObj()

		# Check if the object is a TH1F histogram
		if isinstance(obj, ROOT.TH1F):
			hist = obj

			# Get the current histogram name
			original_name = hist.GetName()
			#  if "mc_ff" not in original_name:
			if "ff_mc" not in original_name:
				continue

			# Create a new name (you can modify this logic according to your needs)
			#  new_name = "new_" + original_name
			#  new_name = original_name.replace(".","_")
			#  new_name = original_name.replace("mc_ff_","")
			#  new_name = original_name.replace("new_ff_mc_","")
			new_name = original_name.replace("final_ff_mc_","")

			# Rename the histogram
			hist.SetName(new_name)

			# Write the renamed histogram to the output file
			hist.Write()

	# Close both input and output files
	input_file.Close()
	output_file.Close()

	print(f"Histograms renamed and saved to {output_file_name}")

if __name__ == "__main__":
	# Provide the input and output file names
	#  input_file_name = "merge.root"
	#  input_file_name = "another_version.root"
	#  input_file_name = "another_test.root"
	input_file_name = "single_test.root"
	#  output_file_name = "output.root"
	#  output_file_name = "another_test_rename.root"
	output_file_name = "single_test_rename.root"

	# Call the function to rename histograms
	rename_histograms(input_file_name, output_file_name)

