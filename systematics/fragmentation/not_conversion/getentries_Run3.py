import ROOT
import os

tree = ROOT.TChain("PhotonCandidates")
#  tree = ROOT.TChain("SinglePhoton")


tree.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/user.zgao.mc23.801649.SinglePhoton.v10_hist.root")
tree.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/user*80165*.root")
tree.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/user.zgao.mc23.801660.SinglePhoton.v09_hist.root")

number = tree.GetEntries()
print("totalnumber: frag ",number)
final_numer = 0
for i in range(number):
    tree.GetEntry(i)
    if i %100000==0:
        print(i)
    final_numer += tree.mcTotWeight_1
print("totalnumer_weight: frag",final_numer)




tree2 = ROOT.TChain("PhotonCandidates")
#  tree2 = ROOT.TChain("SinglePhoton")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/user*80167*.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/user.zgao.mc23.801663.SinglePhoton.v10_hist.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/user.zgao.mc23.801664.SinglePhoton.v10_hist.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/user.zgao.mc23.801665.SinglePhoton.v10_hist.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/user.zgao.mc23.801666.SinglePhoton.v10_hist.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/user.zgao.mc23.801667.SinglePhoton.v10_hist.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/user.zgao.mc23.801668.SinglePhoton.v10_hist.root")
tree2.Add("/afs/cern.ch/user/z/zgao/eos/Run3_photon/rename/user.zgao.mc23.801669.SinglePhoton.v10_hist.root")

final_number2 = 0
print("totalnumber: hard: ",tree2.GetEntries())
for j in range(tree2.GetEntries()):
#  for j in range(number):
    tree2.GetEntry(j)
    if j %100000==0:
        print(j)
    final_number2 += tree2.mcTotWeight_1
    #  final_number2 += tree2.mcTotWeight

print("totalnumber_weight: hard: ",final_number2)
