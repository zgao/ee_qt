import ROOT
import os

#  tree = ROOT.TChain("PhotonCandidates")
tree = ROOT.TChain("SinglePhoton")
#  tree.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20d*.root")

tree.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_80065*")
tree.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_80066*")
tree.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800670_p5536_Rel22_AB22.2.97_v13.root")
tree.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800671_p5536_Rel22_AB22.2.97_v13.root")

#  tree.Add("./user.zgao.mc23.801649.SinglePhoton.v10_hist.root")
#  tree.Add("./user*80165*.root")
#  tree.Add("user.zgao.mc23.801660.SinglePhoton.v09_hist.root")

number = tree.GetEntries()
print("totalnumber: hard ",number)
final_numer = 0
#  number =10
for i in range(number):
    tree.GetEntry(i)
    if i %100000==0:
        print(i)
    final_numer += tree.mcTotWeight
print("totalnumer_weight: hard",final_numer)

#  tree2 = ROOT.TChain("PhotonCandidates")
tree2 = ROOT.TChain("SinglePhoton")
#  tree2.Add("user*80167*.root")
#  tree2.Add("user.zgao.mc23.801663.SinglePhoton.v10_hist.root")
#  tree2.Add("user.zgao.mc23.801664.SinglePhoton.v10_hist.root")
#  tree2.Add("user.zgao.mc23.801665.SinglePhoton.v10_hist.root")
#  tree2.Add("user.zgao.mc23.801666.SinglePhoton.v10_hist.root")
#  tree2.Add("user.zgao.mc23.801667.SinglePhoton.v10_hist.root")
#  tree2.Add("user.zgao.mc23.801668.SinglePhoton.v10_hist.root")
#  tree2.Add("user.zgao.mc23.801669.SinglePhoton.v10_hist.root")

#  tree2.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_80067*")
tree2.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800672_p5536_Rel22_AB22.2.97_v13.root")
tree2.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800673_p5536_Rel22_AB22.2.97_v13.root")
tree2.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800674_p5536_Rel22_AB22.2.97_v13.root")
tree2.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800675_p5536_Rel22_AB22.2.97_v13.root")
tree2.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800676_p5536_Rel22_AB22.2.97_v13.root")
tree2.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800677_p5536_Rel22_AB22.2.97_v13.root")
tree2.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800678_p5536_Rel22_AB22.2.97_v13.root")
tree2.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800679_p5536_Rel22_AB22.2.97_v13.root")
tree2.Add("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_80068")
#  tree2.Remove("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800670_p5536_Rel22_AB22.2.97_v13.root")
#  tree2.Remove("/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800671_p5536_Rel22_AB22.2.97_v13.root")
final_number2 = 0
#  number =10
print("totalnumber: frag: ",tree2.GetEntries())
for j in range(tree2.GetEntries()):
#  for j in range(number):
    tree2.GetEntry(j)
    if j %100000==0:
        print(j)
    #  final_number2 += tree2.mcTotWeight_1
    final_number2 += tree2.mcTotWeight

print("totalnumber_weight: frag: ",final_number2)
