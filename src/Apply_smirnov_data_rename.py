import math
import ROOT
from array import array
import os
import argparse

def PrintProgressBar(index , total):
	if (index % 100000 ==0 ):
	#  if (index / 1 == index ):
		print_bar = " [";
		for bar in range(20):
			current_fraction = float(bar) / 20.0
			if (float(index) / float(total) > current_fraction):
				print_bar += "/"
			else:
				print_bar += "."

		print_bar +="]"
		progress = 100. * (float(index) / float(total))
		print(f"{print_bar}{progress: .2f}%\n", end="\r", flush=True )

parser = argparse.ArgumentParser(description='')

parser.add_argument('-n', '--nominal', help=' nominal without systematic uncertainties')
parser.add_argument('-in','--input',dest='input_file', required=True, help='input file to get Smirnov')
parser.add_argument('-out','--output',dest='output_file', required=True, help='output file allplied Smirnov')
parser.add_argument('-v', '--version',dest='versions', required=True, help='version')
parser.add_argument('-c', '--converted',dest='if_converted',required=True,help='con or uncon( con or unc )')
parser.add_argument('-s', '--smirnov',dest='smirnov',required=True,help='smirnov transformation')

args = parser.parse_args()
inputfile = args.input_file
outputfile = args.output_file

data_file = ROOT.TFile(inputfile,"READ")
#  tree = data_file.Get("Electrons_All")
tree = data_file.Get("ZeeCandidates")


branch_names = [branch.GetName() for branch in tree.GetListOfBranches()]
smirnov_file = ROOT.TFile(args.smirnov,"READ")

output_file = ROOT.TFile(outputfile,"RECREATE")
output_tree = ROOT.TTree("ZeeCandidates","ZeeCandidates")

eta = array('d',[0])
phi = array ('d',[0])
pt = array ('d',[0])
primary_cluster_be2_eta = array ('d',[0])
primary_cluster_be2_abseta = array ('d',[0])
r_had0 = array ('d',[0])
r_had1 = array ('d',[0])
e_ratio = array ('d',[0])
delta_E = array ('d',[0])
w_eta2 = array ('d',[0])
w_eta1 = array ('d',[0])
r_phi = array ('d',[0])
r_eta = array ('d',[0])
w_stot = array ('d',[0])
f1 = array ('d',[0])
fracs1 = array ('d',[0])
f3 = array ('d',[0])
ptcone20 = array ('d',[0])
topoetcone20 = array ('d',[0])
#  pass__TightLLH = array ('d',[0])
#  pass__denominator = array ('d',[0])
E277 = array ('d',[0])
#  pt_cluster = array ('d',[0])
#  eta_cluster = array ('d',[0])
mee = array ('d',[0])


output_tree.Branch("eta",eta, "eta/D")
output_tree.Branch("phi",phi, "phi/D")
output_tree.Branch("pt",pt, "pt/D")
output_tree.Branch("primary_cluster_be2_eta",primary_cluster_be2_eta, "primary_cluster_be2_eta/D")
output_tree.Branch("primary_cluster_be2_abseta",primary_cluster_be2_abseta, "primary_cluster_be2_abseta/D")
output_tree.Branch("r_had0",r_had0, "r_had0/D")
output_tree.Branch("r_had1",r_had1, "r_had1/D")
output_tree.Branch("e_ratio",e_ratio, "e_ratio/D")
output_tree.Branch("delta_E",delta_E, "delta_E/D")
output_tree.Branch("w_eta2",w_eta2, "w_eta2/D")
output_tree.Branch("w_eta1",w_eta1, "w_eta1/D")
output_tree.Branch("r_phi",r_phi, "r_phi/D")
output_tree.Branch("r_eta",r_eta, "r_eta/D")
output_tree.Branch("w_stot",w_stot, "w_stot/D")
output_tree.Branch("f1",f1, "f1/D")
output_tree.Branch("fracs1",fracs1, "fracs1/D")
output_tree.Branch("f3",f3, "f3/D")
output_tree.Branch("ptcone20",ptcone20, "ptcone20/D")
output_tree.Branch("topoetcone20",topoetcone20, "topoetcone20/D")
#  output_tree.Branch("pass__TightLLH",pass__TightLLH, "pass__TightLLH/D")
#  output_tree.Branch("pass__denominator",pass__denominator, "pass__denominator/D")
output_tree.Branch("E277",E277, "E277/D")
#  output_tree.Branch("pt_cluster",pt_cluster, "pt_cluster/F")
#  output_tree.Branch("eta_cluster",eta_cluster, "eta_cluster/F")
output_tree.Branch("mee",mee, "mee/D")



list_eta = [ 0, 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37]
list_pt = [25,30,35,40,45,50,60,80,100,125,150,175,250]



print(tree.GetEntries())
num =[0 for i in range(20)]
for entry in range(tree.GetEntries()):
	tree.GetEntry(entry)
	PrintProgressBar(entry,tree.GetEntries())

	num[0]+=1

	eta_temp= tree.eta
	phi_temp = tree.phi
	pt_temp = tree.pt
	primary_cluster_be2_eta_temp = tree.primary_cluster_be2_eta
	primary_cluster_be2_abseta_temp = tree.primary_cluster_be2_abseta
	r_had0_temp = tree.r_had0
	r_had1_temp = tree.r_had1
	e_ratio_temp = tree.e_ratio
	delta_E_temp = tree.delta_E
	w_eta2_temp = tree.w_eta2
	w_eta1_temp = tree.w_eta1
	r_phi_temp = tree.r_phi
	r_eta_temp = tree.r_eta
	w_stot_temp = tree.w_stot
	f1_temp = tree.f1
	fracs1_temp = tree.fracs1
	ptcone20_temp = tree.ptcone20
	topoetcone20_temp = tree.topoetcone20
	E277_temp = tree.E277
	mee_temp = tree.mee
	e_temp = tree.e



	#  if(mzee<80 or mzee >100):
	#  if(mzee<75 or mzee >105):
	#  if(mzee<82 or mzee >98):
		#  num[1]+=1
		#  continue

	#  num[2]+=1
	num[-1]+=1
	
	iprobe =0
	times =0

	if (pt_temp <25 or pt_temp > 250):
		if times ==0:
			num[5]+=1
		if times ==1:
			num[9]+=1
		continue
	if (math.fabs(primary_cluster_be2_eta_temp) >1.37 and math.fabs(primary_cluster_be2_eta_temp) < 1.52):
		if tiems ==0:
			num[7]+=1
		if times ==1:
			num[11]+=1
		continue
	if math.fabs(primary_cluster_be2_eta_temp) >2.37:
		if tiems ==0:
			num[6]+=1
		if times ==1:
			num[10]+=1
		continue
	if (ptcone20_temp >0.05*pt_temp or topoetcone20_temp> 0.065*pt_temp):
		if times ==0:
			num[8]+=1
		if times ==1:
			num[12]+=1
		continue
	if times ==0:
		num[16]+=1
	if times ==1:
		num[17]+=1
	num[14]+=1




	if math.fabs(primary_cluster_be2_eta_temp)<0.6: 
		eta_name ="0.00<|#eta|<0.60"
	elif math.fabs(primary_cluster_be2_eta_temp)>=0.6 and math.fabs(primary_cluster_be2_eta_temp)< 0.8 :
		eta_name ="0.60<|#eta|<0.80"
	elif math.fabs(primary_cluster_be2_eta_temp)>=0.8 and math.fabs(primary_cluster_be2_eta_temp)< 1.15 :
		eta_name ="0.80<|#eta|<1.15"
	elif math.fabs(primary_cluster_be2_eta_temp)>=1.15 and math.fabs(primary_cluster_be2_eta_temp)< 1.37 :
		eta_name ="1.15<|#eta|<1.37"
	elif math.fabs(primary_cluster_be2_eta_temp)>=1.52 and math.fabs(primary_cluster_be2_eta_temp)< 1.81 :
		eta_name ="1.52<|#eta|<1.81"
	elif math.fabs(primary_cluster_be2_eta_temp)>=1.81 and math.fabs(primary_cluster_be2_eta_temp)< 2.01 :
		eta_name ="1.81<|#eta|<2.01"
	elif math.fabs(primary_cluster_be2_eta_temp)>=2.01 and math.fabs(primary_cluster_be2_eta_temp)< 2.37 :
		eta_name ="2.01<|#eta|<2.37"
	else:
		continue

	if pt_temp<30:
		pt_name ="25<P_{t}<30"
	elif pt_temp>= 30 and pt_temp<35:
		pt_name ="30<P_{t}<35"
	elif pt_temp>= 35 and pt_temp<40:
		pt_name ="35<P_{t}<40"
	elif pt_temp>= 40 and pt_temp<45:
		pt_name ="40<P_{t}<45"
	elif pt_temp>= 45 and pt_temp<50:
		pt_name ="45<P_{t}<50"
	elif pt_temp>= 50 and pt_temp<60:
		pt_name ="50<P_{t}<60"
	elif pt_temp>= 60 and pt_temp<80:
		pt_name ="60<P_{t}<80"
	elif pt_temp>= 80 and pt_temp<100:
		pt_name ="80<P_{t}<100"
	elif pt_temp>= 100 and pt_temp<125:
		pt_name ="100<P_{t}<125"
	elif pt_temp>= 125 and pt_temp<150:
		pt_name ="125<P_{t}<150"
	elif pt_temp>= 150 and pt_temp<175:
		pt_name ="150<P_{t}<175"
	elif pt_temp>= 175 and pt_temp<250:
		pt_name ="175<P_{t}<250"
	else:
		continue
	
	if times==0 :
		num[3]+=1
	if times==1:
		num[4]+=1

	eta[0] = eta_temp
	phi[0] = phi_temp
	primary_cluster_be2_eta[0] = primary_cluster_be2_eta_temp
	primary_cluster_be2_abseta[0] = primary_cluster_be2_abseta_temp
	ptcone20[0] = ptcone20_temp
	topoetcone20[0] = topoetcone20_temp
	E277[0] = E277_temp
	pt[0] = pt_temp
	mee[0] =mee_temp
	#  f3[0] = f3_temp
	f1[0] = f1_temp


	r_had0[0] = smirnov_file.Get(f"Smirnov_r_had0_{eta_name}_{pt_name}").Interpolate(r_had0_temp)
	r_had1[0] = smirnov_file.Get(f"Smirnov_r_had1_{eta_name}_{pt_name}").Interpolate(r_had1_temp)
	e_ratio[0] = smirnov_file.Get(f"Smirnov_e_ratio_{eta_name}_{pt_name}").Interpolate(e_ratio_temp)
	delta_E[0] = smirnov_file.Get(f"Smirnov_delta_E_{eta_name}_{pt_name}").Interpolate(delta_E_temp)
	w_eta2[0] = smirnov_file.Get(f"Smirnov_w_eta2_{eta_name}_{pt_name}").Interpolate(w_eta2_temp)
	w_eta1[0] = smirnov_file.Get(f"Smirnov_w_eta1_{eta_name}_{pt_name}").Interpolate(w_eta1_temp)
	r_eta[0] = smirnov_file.Get(f"Smirnov_r_eta_{eta_name}_{pt_name}").Interpolate(r_eta_temp)
	r_phi[0] = smirnov_file.Get(f"Smirnov_r_phi_{eta_name}_{pt_name}").Interpolate(r_phi_temp)
	w_stot[0] = smirnov_file.Get(f"Smirnov_w_stot_{eta_name}_{pt_name}").Interpolate(w_stot_temp)
	fracs1[0] = smirnov_file.Get(f"Smirnov_fracs1_{eta_name}_{pt_name}").Interpolate(fracs1_temp)

	output_tree.Fill()

output_file.Write()
output_file.Close()
data_file.Close()
print("it's over")
print(output_file)
for i in range(len(num)):
	print("i: ",i," ",num[i])
