import os
import ROOT

# Define the input ROOT file and histogram name

directory_path = "/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/"

all_files = os.listdir(directory_path)
#  print(all_files)
mc20e_files = [ files for files in all_files if files.startswith("Py_mc20e")]

for i in mc20e_files:
		#  print(i)

	#  input_root_file = "/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/Py_mc20e_800658_p5536_Rel22_AB22.2.97_v13.root"
	input_root_file = f"/eos/atlas/atlascerngroupdisk/perf-egamma/InclusivePhotons/mc20_gammajet_v13/{i}"	
	histogram_name = "histCutflow"

	# Open the ROOT file for reading
	root_file = ROOT.TFile(input_root_file, "READ")

	# Check if the file is open
	if not root_file.IsOpen():
		print("Failed to open ROOT file:", input_root_file)
		exit(1)

	# Get the histogram from the ROOT file
	histogram = root_file.Get(histogram_name)
	print(str(input_root_file))
	for j in range(histogram.GetNbinsX()):
		print(j+1,"  ",histogram.GetBinContent(j+1))


	# Check if the histogram exists
	#  if not histogram:
		#  print("Histogram not found:", histogram_name)
		#  root_file.Close()
		#  exit(1)

	# Create a new ROOT file for saving the histogram
	#  output_root_file = os.path.basename(input_root_file)
	#  output_root_file = "output.root"
	#  output_file = ROOT.TFile(output_root_file, "RECREATE")

	# Check if the output file is open
	#  if not output_file.IsOpen():
		#  print("Failed to create output ROOT file:", output_root_file)
		#  root_file.Close()
		#  exit(1)

	# Write the histogram to the output file
	#  histogram.Write()

	# Close the input and output files
	root_file.Close()
	#  output_file.Close()

	#  print("Histogram saved to", output_root_file)

