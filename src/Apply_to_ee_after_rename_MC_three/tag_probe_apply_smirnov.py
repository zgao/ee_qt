import math
import ROOT
from array import array
import os
import argparse

def PrintProgressBar(index , total):
	if (index % 100000 ==0 ):
	#  if (index / 1 == index ):
		print_bar = " [";
		for bar in range(20):
			current_fraction = float(bar) / 20.0
			if (float(index) / float(total) > current_fraction):
				print_bar += "/"
			else:
				print_bar += "."

		print_bar +="]"
		progress = 100. * (float(index) / float(total))
		print(f"{print_bar}{progress: .2f}%\n", end="\r", flush=True )

parser = argparse.ArgumentParser(description='')

parser.add_argument('-n', '--nominal', help=' nominal without systematic uncertainties')
parser.add_argument('-in','--input',dest='input_file', required=True, help='input file to get Smirnov')
parser.add_argument('-out','--output',dest='output_file', required=True, help='output file allplied Smirnov')
parser.add_argument('-v', '--version',dest='versions', required=True, help='version')
parser.add_argument('-c', '--converted',dest='if_converted',required=True,help='con or uncon( con or unc )')
parser.add_argument('-s', '--smirnov',dest='smirnov',required=True,help='smirnov transformation')

args = parser.parse_args()
inputfile = args.input_file
outputfile = args.output_file

#  data_file = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/con1_fudge/user.zgao.tp-example_zmass-data18_13TeV-EGAM1-grp18_v01_p5535_20230716_071833_ANALYSIS.root/user.zgao.34090425._000112.ANALYSIS.root","READ")
data_file = ROOT.TFile(inputfile,"READ")
tree = data_file.Get("ZeeCandidates")


branch_names = [branch.GetName() for branch in tree.GetListOfBranches()]
#  smirnov_file = ROOT.TFile("smirnov_transformation.root","READ")
smirnov_file = ROOT.TFile(args.smirnov,"READ")


output_file = ROOT.TFile(outputfile,"RECREATE")
output_tree = ROOT.TTree("ZeeCandidates","ZeeCandidates")



puweight = array('f',[0])
MC_event_weight = array('d',[0])
xSec = array('f',[0])
MC_channel_number = array('i',[0])


eta = array('d',[0])
phi = array ('d',[0])
pt = array ('d',[0])
primary_cluster_be2_eta = array ('d',[0])
primary_cluster_be2_abseta = array ('d',[0])
r_had0 = array ('d',[0])
r_had1 = array ('d',[0])
e_ratio = array ('d',[0])
delta_E = array ('d',[0])
w_eta2 = array ('d',[0])
w_eta1 = array ('d',[0])
r_phi = array ('d',[0])
r_eta = array ('d',[0])
w_stot = array ('d',[0])
f1 = array ('d',[0])
fracs1 = array ('d',[0])
f3 = array ('d',[0])
ptcone20 = array ('d',[0])
topoetcone20 = array ('d',[0])
#  pass__TightLLH = array ('d',[0])
pass__denominator = array ('i',[0])
#  E277 = array ('d',[0])
#  pt_cluster = array ('d',[0])
#  eta_cluster = array ('d',[0])
mee = array ('d',[0])


output_tree.Branch("puweight",puweight, "puweight/F")
output_tree.Branch("MC_event_weight",MC_event_weight, "MC_event_weight/D")
output_tree.Branch("xSec",xSec, "xSec/F")
output_tree.Branch("MC_channel_number",MC_channel_number, "MC_channel_number/I")



output_tree.Branch("eta",eta, "eta/D")
output_tree.Branch("phi",phi, "phi/D")
output_tree.Branch("pt",pt, "pt/D")
output_tree.Branch("primary_cluster_be2_eta",primary_cluster_be2_eta, "primary_cluster_be2_eta/D")
output_tree.Branch("primary_cluster_be2_abseta",primary_cluster_be2_abseta, "primary_cluster_be2_abseta/D")
output_tree.Branch("r_had0",r_had0, "r_had0/D")
output_tree.Branch("r_had1",r_had1, "r_had1/D")
output_tree.Branch("e_ratio",e_ratio, "e_ratio/D")
output_tree.Branch("delta_E",delta_E, "delta_E/D")
output_tree.Branch("w_eta2",w_eta2, "w_eta2/D")
output_tree.Branch("w_eta1",w_eta1, "w_eta1/D")
output_tree.Branch("r_phi",r_phi, "r_phi/D")
output_tree.Branch("r_eta",r_eta, "r_eta/D")
output_tree.Branch("w_stot",w_stot, "w_stot/D")
output_tree.Branch("f1",f1, "f1/D")
output_tree.Branch("fracs1",fracs1, "fracs1/D")
output_tree.Branch("f3",f3, "f3/D")
output_tree.Branch("ptcone20",ptcone20, "ptcone20/D")
output_tree.Branch("topoetcone20",topoetcone20, "topoetcone20/D")
#  output_tree.Branch("pass__TightLLH",pass__TightLLH, "pass__TightLLH/D")
output_tree.Branch("pass__denominator",pass__denominator, "pass__denominator/I")
#  output_tree.Branch("E277",E277, "E277/D")
#  output_tree.Branch("pt_cluster",pt_cluster, "pt_cluster/F")
#  output_tree.Branch("eta_cluster",eta_cluster, "eta_cluster/F")
output_tree.Branch("mee",mee, "mee/D")

puweight_temp =array('f',[0])
MC_event_weight_temp =array('d',[0])
xSec_temp =array('f',[0])
MC_channel_number_temp =array('i',[0])

eta_temp=array('d',[0])
phi_temp = array('d',[0])
pt_temp = array('d',[0])
primary_cluster_be2_eta_temp = array('d',[0])
primary_cluster_be2_abseta_temp = array('d',[0])
r_had0_temp = array('d',[0])
r_had1_temp = array('d',[0])
e_ratio_temp = array('d',[0])
delta_E_temp = array('d',[0])
w_eta2_temp = array('d',[0])
w_eta1_temp = array('d',[0])
r_phi_temp = array('d',[0])
r_eta_temp = array('d',[0])
w_stot_temp = array('d',[0])
f1_temp = array('d',[0])
fracs1_temp = array('d',[0])
f3_temp = array('d',[0])
ptcone20_temp = array('d',[0])
topoetcone20_temp = array('d',[0])
pass__TightLLH_temp = array('i',[0])
pass__denominator_temp = array('i',[0])
pass__tag_temp= array('i',[0])
#  E277_temp = array('int,[0]')
#  eta_cluster_temp = array('d',[0])
mee_temp = array('d',[0])
e_temp = array('d',[0])
actual_mu_temp = array('d',[0])
average_mu_temp = array('d',[0])



tree.SetBranchAddress("pileupWeight",puweight_temp)
tree.SetBranchAddress("MC_event_weight",MC_event_weight_temp)
tree.SetBranchAddress("Interaction_per_crossing_actual",xSec_temp)
tree.SetBranchAddress("MC_channel_number",MC_channel_number_temp)

tree.SetBranchAddress("eta",eta_temp)
tree.SetBranchAddress("phi",phi_temp)
tree.SetBranchAddress("pt",pt_temp)
tree.SetBranchAddress("primary_cluster_be2_eta",primary_cluster_be2_eta_temp)
tree.SetBranchAddress("primary_cluster_be2_abseta",primary_cluster_be2_abseta_temp)
#  tree.SetBranchAddress("r_had__NOSYS",r_had_temp)
tree.SetBranchAddress("r_had0",r_had0_temp)
tree.SetBranchAddress("r_had1",r_had1_temp)
tree.SetBranchAddress("e_ratio",e_ratio_temp)
tree.SetBranchAddress("delta_E",delta_E_temp)
tree.SetBranchAddress("w_eta2",w_eta2_temp)
tree.SetBranchAddress("w_eta1",w_eta1_temp)
tree.SetBranchAddress("r_eta",r_eta_temp)
tree.SetBranchAddress("r_phi",r_phi_temp)
tree.SetBranchAddress("w_stot",w_stot_temp)
tree.SetBranchAddress("f1",f1_temp)
tree.SetBranchAddress("fracs1",fracs1_temp)
tree.SetBranchAddress("f3",f3_temp)
tree.SetBranchAddress("ptcone20",ptcone20_temp)
tree.SetBranchAddress("topoetcone20",topoetcone20_temp)
tree.SetBranchAddress("pass__TightLLH",pass__TightLLH_temp)
tree.SetBranchAddress("pass__denominator",pass__denominator_temp)
#  tree.SetBranchAddress("E277",E277_temp)
tree.SetBranchAddress("e",e_temp)
tree.SetBranchAddress("pass__tag",pass__tag_temp)
#  tree.SetBranchAddress("actual_mu__NOSYS",actual_mu_temp)
#  tree.SetBranchAddress("average_mu__NOSYS",average_mu_temp)

list_eta = [ 0, 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37]
list_pt = [25,30,35,40,45,50,60,80,100,125,150,175,250]




print(tree.GetEntries())
num=[0 for i in range(10)]
for entry in range(tree.GetEntries()):
#  for entry in range(10000):
	tree.GetEntry(entry)
	
	PrintProgressBar(entry,tree.GetEntries())
	#  print(eta_temp)
	#  print(w_eta1_temp[0])
	#  print("123")
	#  print(eta_temp[0])
	#  exit()
	num[0]+=1
	if math.fabs(primary_cluster_be2_eta_temp[0])<0.6: 
		eta_name ="0.00<|#eta|<0.60"
	elif math.fabs(primary_cluster_be2_eta_temp[0])>0.6 and math.fabs(primary_cluster_be2_eta_temp[0])< 0.8 :
		eta_name ="0.60<|#eta|<0.80"
	elif math.fabs(primary_cluster_be2_eta_temp[0])>0.8 and math.fabs(primary_cluster_be2_eta_temp[0])< 1.15 :
		eta_name ="0.80<|#eta|<1.15"
	elif math.fabs(primary_cluster_be2_eta_temp[0])>1.15 and math.fabs(primary_cluster_be2_eta_temp[0])< 1.37 :
		eta_name ="1.15<|#eta|<1.37"
	elif math.fabs(primary_cluster_be2_eta_temp[0])>1.52 and math.fabs(primary_cluster_be2_eta_temp[0])< 1.81 :
		eta_name ="1.52<|#eta|<1.81"
	elif math.fabs(primary_cluster_be2_eta_temp[0])>1.81 and math.fabs(primary_cluster_be2_eta_temp[0])< 2.01 :
		eta_name ="1.81<|#eta|<2.01"
	elif math.fabs(primary_cluster_be2_eta_temp[0])>2.01 and math.fabs(primary_cluster_be2_eta_temp[0])< 2.37 :
		eta_name ="2.01<|#eta|<2.37"
	else:
		continue
	
	num[1]+=1
	if pt_temp[0]<30:
		pt_name ="25<P_{t}<30"
	elif pt_temp[0]> 30 and pt_temp[0]<35:
		pt_name ="30<P_{t}<35"
	elif pt_temp[0]> 35 and pt_temp[0]<40:
		pt_name ="35<P_{t}<40"
	elif pt_temp[0]> 40 and pt_temp[0]<45:
		pt_name ="40<P_{t}<45"
	elif pt_temp[0]> 45 and pt_temp[0]<50:
		pt_name ="45<P_{t}<50"
	elif pt_temp[0]> 50 and pt_temp[0]<60:
		pt_name ="50<P_{t}<60"
	elif pt_temp[0]> 60 and pt_temp[0]<80:
		pt_name ="60<P_{t}<80"
	elif pt_temp[0]> 80 and pt_temp[0]<100:
		pt_name ="80<P_{t}<100"
	elif pt_temp[0]> 100 and pt_temp[0]<125:
		pt_name ="100<P_{t}<125"
	elif pt_temp[0]> 125 and pt_temp[0]<150:
		pt_name ="125<P_{t}<150"
	elif pt_temp[0]> 150 and pt_temp[0]<175:
		pt_name ="150<P_{t}<175"
	elif pt_temp[0]> 175 and pt_temp[0]<250:
		pt_name ="175<P_{t}<250"
	else:
		continue
	num[3]+=1

	eta[0] = eta_temp[0]
	phi[0] = phi_temp[0]
	primary_cluster_be2_eta[0] = primary_cluster_be2_eta_temp[0]
	primary_cluster_be2_abseta[0] = primary_cluster_be2_abseta_temp[0]
	ptcone20[0] = ptcone20_temp[0]
	topoetcone20[0] = topoetcone20_temp[0]
	#  E277[0] = E277_temp[0]
	pt[0] = pt_temp[0]
	#  mee[0] =mzee
	f3[0] = f3_temp[0]
	f1[0] = f1_temp[0]

	puweight[0] = puweight_temp[0]
	MC_event_weight[0]=MC_event_weight_temp[0]
	xSec[0] = xSec_temp[0]
	MC_channel_number[0]= MC_channel_number_temp[0]
	pass__denominator[0] = pass__denominator_temp[0]

	#  print(f"Smirnov_r_had1_{eta_name}_{pt_name}")
	#  if r_had0_temp[0] < smirnov_file.Get(f"Smirnov_r_had0_{eta_name}_{pt_name}").GetXaxis().GetBinLowEdge(1) or r_had0_temp[0] > smirnov_file.Get(f"Smirnov_r_had0_{eta_name}_{pt_name}").GetXaxis().GetBinUpEdge(smirnov_file.Get(f"Smirnov_r_had0_{eta_name}_{pt_name}").GetNbinsX()) :
	#	   continue
	#  r_had0[0] = smirnov_file.Get(f"Smirnov_r_had0_{eta_name}_{pt_name}").Interpolate(r_had0_temp[0])
	#  if abs(r_had0_temp[0] - r_had0[0]) >0.00000001:
	#	   print(r_had0_temp[0]," ",r_had0[0])
	#
	#  if r_had1_temp[0] < smirnov_file.Get(f"Smirnov_r_had1_{eta_name}_{pt_name}").GetXaxis().GetBinLowEdge(1) or r_had1_temp[0] > smirnov_file.Get(f"Smirnov_r_had1_{eta_name}_{pt_name}").GetXaxis().GetBinUpEdge(smirnov_file.Get(f"Smirnov_r_had1_{eta_name}_{pt_name}").GetNbinsX()) :
	#	   continue
	#  r_had1[0] = smirnov_file.Get(f"Smirnov_r_had1_{eta_name}_{pt_name}").Interpolate(r_had1_temp[0])
	#
	#  if e_ratio_temp[0] < smirnov_file.Get(f"Smirnov_e_ratio_{eta_name}_{pt_name}").GetXaxis().GetBinLowEdge(1) or e_ratio_temp[0] > smirnov_file.Get(f"Smirnov_e_ratio_{eta_name}_{pt_name}").GetXaxis().GetBinUpEdge(smirnov_file.Get(f"Smirnov_e_ratio_{eta_name}_{pt_name}").GetNbinsX()) :
	#	   continue
	#  e_ratio[0] = smirnov_file.Get(f"Smirnov_e_ratio_{eta_name}_{pt_name}").Interpolate(e_ratio_temp[0])
	#  #  if pt_temp[0]> 175 and pt_temp[0]<250 and math.fabs(primary_cluster_be2_eta_temp[0])>2.01 and math.fabs(primary_cluster_be2_eta_temp[0])< 2.37 :
	#  #	  print(e_ratio_temp[0]," ",e_ratio[0]," ",e_ratio_temp[0]/e_ratio[0])
	#
	#  if delta_E_temp[0] < smirnov_file.Get(f"Smirnov_delta_E_{eta_name}_{pt_name}").GetXaxis().GetBinLowEdge(1) or delta_E_temp[0] > smirnov_file.Get(f"Smirnov_delta_E_{eta_name}_{pt_name}").GetXaxis().GetBinUpEdge(smirnov_file.Get(f"Smirnov_delta_E_{eta_name}_{pt_name}").GetNbinsX()) :
	#	   continue
	#  delta_E[0] = smirnov_file.Get(f"Smirnov_delta_E_{eta_name}_{pt_name}").Interpolate(delta_E_temp[0])
	#
	#  if w_eta2_temp[0] < smirnov_file.Get(f"Smirnov_w_eta2_{eta_name}_{pt_name}").GetXaxis().GetBinLowEdge(1) or w_eta2_temp[0] > smirnov_file.Get(f"Smirnov_w_eta2_{eta_name}_{pt_name}").GetXaxis().GetBinUpEdge(smirnov_file.Get(f"Smirnov_w_eta2_{eta_name}_{pt_name}").GetNbinsX()) :
	#	   continue
	#  w_eta2[0] = smirnov_file.Get(f"Smirnov_w_eta2_{eta_name}_{pt_name}").Interpolate(w_eta2_temp[0])
	#
	#  if w_eta1_temp[0] < smirnov_file.Get(f"Smirnov_w_eta1_{eta_name}_{pt_name}").GetXaxis().GetBinLowEdge(1) or w_eta1_temp[0] > smirnov_file.Get(f"Smirnov_w_eta1_{eta_name}_{pt_name}").GetXaxis().GetBinUpEdge(smirnov_file.Get(f"Smirnov_w_eta1_{eta_name}_{pt_name}").GetNbinsX()) :
	#	   continue
	#  w_eta1[0] = smirnov_file.Get(f"Smirnov_w_eta1_{eta_name}_{pt_name}").Interpolate(w_eta1_temp[0])
	#
	#  if r_eta_temp[0] < smirnov_file.Get(f"Smirnov_r_eta_{eta_name}_{pt_name}").GetXaxis().GetBinLowEdge(1) or r_eta_temp[0] > smirnov_file.Get(f"Smirnov_r_eta_{eta_name}_{pt_name}").GetXaxis().GetBinUpEdge(smirnov_file.Get(f"Smirnov_r_eta_{eta_name}_{pt_name}").GetNbinsX()) :
	#	   continue
	#  r_eta[0] = smirnov_file.Get(f"Smirnov_r_eta_{eta_name}_{pt_name}").Interpolate(r_eta_temp[0])
	#
	#  if r_phi_temp[0] < smirnov_file.Get(f"Smirnov_r_phi_{eta_name}_{pt_name}").GetXaxis().GetBinLowEdge(1) or r_phi_temp[0] > smirnov_file.Get(f"Smirnov_r_phi_{eta_name}_{pt_name}").GetXaxis().GetBinUpEdge(smirnov_file.Get(f"Smirnov_r_phi_{eta_name}_{pt_name}").GetNbinsX()) :
	#	   continue
	#  r_phi[0] = smirnov_file.Get(f"Smirnov_r_phi_{eta_name}_{pt_name}").Interpolate(r_phi_temp[0])
	#
	#  if w_stot_temp[0] < smirnov_file.Get(f"Smirnov_w_stot_{eta_name}_{pt_name}").GetXaxis().GetBinLowEdge(1) or w_stot_temp[0] > smirnov_file.Get(f"Smirnov_w_stot_{eta_name}_{pt_name}").GetXaxis().GetBinUpEdge(smirnov_file.Get(f"Smirnov_w_stot_{eta_name}_{pt_name}").GetNbinsX()) :
	#	   continue
	#  w_stot[0] = smirnov_file.Get(f"Smirnov_w_stot_{eta_name}_{pt_name}").Interpolate(w_stot_temp[0])
	#
	#  if fracs1_temp[0] < smirnov_file.Get(f"Smirnov_fracs1_{eta_name}_{pt_name}").GetXaxis().GetBinLowEdge(1) or fracs1_temp[0] > smirnov_file.Get(f"Smirnov_fracs1_{eta_name}_{pt_name}").GetXaxis().GetBinUpEdge(smirnov_file.Get(f"Smirnov_fracs1_{eta_name}_{pt_name}").GetNbinsX()) :
	#	   continue

	r_had0[0] = smirnov_file.Get(f"Smirnov_r_had0_{eta_name}_{pt_name}").Interpolate(r_had0_temp[0])
	r_had1[0] = smirnov_file.Get(f"Smirnov_r_had1_{eta_name}_{pt_name}").Interpolate(r_had1_temp[0])
	e_ratio[0] = smirnov_file.Get(f"Smirnov_e_ratio_{eta_name}_{pt_name}").Interpolate(e_ratio_temp[0])
	delta_E[0] = smirnov_file.Get(f"Smirnov_delta_E_{eta_name}_{pt_name}").Interpolate(delta_E_temp[0])
	w_eta2[0] = smirnov_file.Get(f"Smirnov_w_eta2_{eta_name}_{pt_name}").Interpolate(w_eta2_temp[0])
	w_eta1[0] = smirnov_file.Get(f"Smirnov_w_eta1_{eta_name}_{pt_name}").Interpolate(w_eta1_temp[0])
	r_eta[0] = smirnov_file.Get(f"Smirnov_r_eta_{eta_name}_{pt_name}").Interpolate(r_eta_temp[0])
	r_phi[0] = smirnov_file.Get(f"Smirnov_r_phi_{eta_name}_{pt_name}").Interpolate(r_phi_temp[0])
	w_stot[0] = smirnov_file.Get(f"Smirnov_w_stot_{eta_name}_{pt_name}").Interpolate(w_stot_temp[0])
	fracs1[0] = smirnov_file.Get(f"Smirnov_fracs1_{eta_name}_{pt_name}").Interpolate(fracs1_temp[0])

	variables = ["r_had0", "r_had1", "e_ratio", "delta_E", "w_eta2", "w_eta1", "r_eta", "r_phi", "w_stot", "fracs1"]
	list_low_edge = [smirnov_file.Get(f"Smirnov_{variable}_{eta_name}_{pt_name}").GetXaxis().GetBinLowEdge(1) for variable in variables]
	list_up_edge = [smirnov_file.Get(f"Smirnov_{variable}_{eta_name}_{pt_name}").GetXaxis().GetBinUpEdge(smirnov_file.Get(f"Smirnov_{variable}_{eta_name}_{pt_name}").GetNbinsX()) for variable in variables]

	list_temp =[r_had0_temp[0], r_had1_temp[0], e_ratio_temp[0], delta_E_temp[0], w_eta2_temp[0], w_eta1_temp[0], r_eta_temp[0], r_phi_temp[0], w_stot_temp[0], fracs1_temp[0]]
	list_final_value=[r_had0[0], r_had1[0], e_ratio[0], delta_E[0], w_eta2[0], w_eta1[0], r_eta[0], r_phi[0], w_stot[0], fracs1[0]]

	for i in range(len(list_temp)):
		#  if list_temp[2]==1.0:
			#  print(list_temp)
			#  print(list_final_value)
		if list_temp[i] <=list_low_edge[i] or list_temp[i] >= list_up_edge[i]:
			#  print("")
			#  print(list_temp[2])
			list_final_value[i] = list_temp[i]
	r_had0[0]  = list_final_value[0]
	r_had1[0]  = list_final_value[1]
	e_ratio[0]	= list_final_value[2]
	delta_E[0]	= list_final_value[3]
	w_eta2[0]  = list_final_value[4]
	w_eta1[0]  = list_final_value[5]
	r_eta[0]  = list_final_value[6]
	r_phi[0]  = list_final_value[7]
	w_stot[0]  = list_final_value[8]
	fracs1[0]  = list_final_value[9]

	output_tree.Fill()

output_file.Write()
output_file.Close()
data_file.Close()
print("it's over")
for i in range(len(num)):
	print(num[i])
print(output_file)





