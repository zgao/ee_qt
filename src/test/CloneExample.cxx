#include "TCanvas.h"
#include "TH1F.h"

void CloneExample() {
	TCanvas *c2 = new TCanvas("c2","first",800,600);

    // Create a histogram
    TH1F* originalHist = new TH1F("originalHist", "Original Histogram", 50, 0, 10);
    originalHist->FillRandom("gaus", 1000);
	originalHist->SetLineColor(kRed);
	c2->cd();
	originalHist->Draw();
	c2->SaveAs("first.png");

    // Clone the histogram
	TH1F* clonedHist = static_cast<TH1F*>(originalHist->Clone("clonedHist2"));

    // Modify the cloned histogram
    // clonedHist->SetTitle("Cloned Histogram");
    // clonedHist->SetFillColor(kBlue);

    // Create a canvas and draw both histograms
    TCanvas* canvas = new TCanvas("canvas", "Histograms", 800, 600);
    originalHist->Draw();
    clonedHist->Draw();

    // Save the canvas as an image
    canvas->SaveAs("histograms.png");

    // Clean up memory
    delete originalHist;
    delete clonedHist;
    delete canvas;
}

int main() {
    CloneExample();
    return 0;
}

