import ROOT

# Create a ROOT file to store the histogram
output_file = ROOT.TFile("histogram.root", "RECREATE")

# Create a histogram
hist = ROOT.TH1F("my_hist", "My Histogram", 100, 0, 10)

# Create a random number generator
random_generator = ROOT.TRandom3()

# Fill the histogram 1000 times with random values
for i in range(1000):
	value = random_generator.Uniform(0, 10)  # Generate a random value between 0 and 10
	hist.Fill(value,4)

# Save the histogram to the output file
hist.Write()

c1 = ROOT.TCanvas("c1","",800,600)
c1.cd()
hist.Draw()
c1.SaveAs("fill2.png")

# Close the output file
output_file.Close()

print("Histogram filling complete.")

