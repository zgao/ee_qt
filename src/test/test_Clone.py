import ROOT

# Create a TH1F histogram
original_hist = ROOT.TH1F("original_hist", "Original Histogram", 100, 0, 10)
for i in range(10000):
	original_hist.Fill(ROOT.gRandom.Gaus(5, 1))

# Clone the original histogram
cloned_hist = original_hist.Clone("cloned_hist")

# Print some information about the histograms
print("Original Histogram:")
original_hist.Print()
print("\nCloned Histogram:")
cloned_hist.Print()

# Close the ROOT application
ROOT.gApplication.Run()

