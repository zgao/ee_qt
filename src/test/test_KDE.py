import ROOT

# Create a ROOT histogram with some example data
data = [1.2, 1.5, 1.7, 2.0, 2.1]
hist = ROOT.TH1F("hist", "Example Histogram", 50, 0.0, 3.0)
for value in data:
    hist.Fill(value)

# KDE parameters (same as in previous example)
ktype = ROOT.TMVA.KDEKernel.kGauss
kiter = ROOT.TMVA.KDEKernel.kNormal
kborder = ROOT.TMVA.KDEKernel.kSampleMirror
fine_factor = 1.0
norm = True

# Create the PDF using the ROOT.TMVA.PDF constructor
pdf = ROOT.TMVA.PDF("my_pdf", hist, ktype, kiter, kborder, fine_factor, norm)

# Create a canvas and draw the PDF
canvas = ROOT.TCanvas("canvas", "PDF Plot", 800, 600)
pdf_hist = ROOT.TH1F("pdf_hist", "PDF", 50, 0.0, 3.0)

# Fill the PDF histogram with PDF values
for bin in range(pdf_hist.GetNbinsX()):
    x = pdf_hist.GetBinCenter(bin + 1)
    pdf_value = pdf.GetVal(x)
    pdf_hist.SetBinContent(bin + 1, pdf_value)

# Draw the PDF histogram
pdf_hist.Draw("HIST")

# Customize the plot
pdf_hist.SetLineColor(ROOT.kBlue)
pdf_hist.SetLineWidth(2)
pdf_hist.GetXaxis().SetTitle("X")
pdf_hist.GetYaxis().SetTitle("PDF Value")
canvas.SetGrid()

# Show the plot
#  canvas.Draw()
canvas.SaveAs("KDE.png")

