#!/bin/bash

# ./name /afs/cern.ch/user/z/zgao/eos/hist_7_9_interchange/zee/b/user.zgao.33986780._000107.hist-output.root output2.txt

# ./name /afs/cern.ch/user/z/zgao/eos/hist8_6_range_sum/gjet_photon/unconvb_fixedCutLoose/Py_mc20e_800663_p5536_Rel22_AB22.2.97_v13.root_10.root photon_con_kang.txt
# ./name /afs/cern.ch/user/z/zgao/eos/ee_qt/src/pho/con/1/Py_mc20e_800663_p5536_Rel22_AB22.2.97_v13.root_10.root photon_con_xjun.txt

# ./name /afs/cern.ch/user/z/zgao/eos/ee_qt/src/pho/unc/1/merge.root photon_con_xjun.txt
# ./name /afs/cern.ch/user/z/zgao/eos/hist8_6_range_sum/gjet_photon/convb_fixedCutLoose/merge.root  photon_con_kang.txt

# ./name /afs/cern.ch/user/z/zgao/eos/ee_qt/src/pho/con/1/merge.root photon_unc_xjun.txt
# ./name /afs/cern.ch/user/z/zgao/eos/hist8_6_range_sum/gjet_photon/unconvb_fixedCutLoose/merge.root photon_unc_kang.txt


for names in $(ls /afs/cern.ch/user/z/zgao/eos/ee_qt/src/pho/con/1/*)
do
	# echo $names
	name2=${names/\/afs\/cern.ch\/user\/z\/zgao\/eos\/ee_qt\/src\/pho\/con\/1\//}
	# echo $name2
	name3="${name2}_xjun.txt"
	./name $names many_files/$name3

	# name4="/afs/cern.ch/user/z/zgao/eos/hist8_6_range_sum/gjet_photon/unconvb_fixedCutLoose/${name2}"
	name4="/afs/cern.ch/user/z/zgao/eos/hist8_6_range_sum/gjet_photon/convb_fixedCutLoose/${name2}"
	name5="${name2}_kang.txt"
	./name $name4 many_files/$name5
	
	python3 arrange.py many_files/$name5 many_files/$name3 > many_files/arrange_${name2}.txt

	if grep -q "error" many_files/arrange_${name2}.txt; then
		echo  "$name2 is over,    error"
	else
		echo  "$name2 is over"
	fi
done


for names in $(ls /afs/cern.ch/user/z/zgao/eos/ee_qt/src/pho/unc/1/*)
do
	# echo $names
	name2=${names/\/afs\/cern.ch\/user\/z\/zgao\/eos\/ee_qt\/src\/pho\/unc\/1\//}
	# echo $name2
	name3="${name2}_xjun.txt"
	./name $names many_files/$name3

	name4="/afs/cern.ch/user/z/zgao/eos/hist8_6_range_sum/gjet_photon/unconvb_fixedCutLoose/${name2}"
	name5="${name2}_kang.txt"
	./name $name4 many_files/$name5
	
	python3 arrange.py many_files/$name5 many_files/$name3 > many_files/arrange_${name2}.txt

	if grep -q "error" many_files/arrange_${name2}.txt; then
		echo  "$name2 is over,    error"
	else
		echo  "$name2 is over"
	fi
done
