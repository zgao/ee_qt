#  list1 = ["Histogram Name:     singleph_r_had1_eta_0.00_0.60_pt_100_125           Entries: 33"]
#  list2 = list1[0].split()
#  print(list2)
#  print(list2[2])
#  list3 = list2[2].split("pt_")
#  print(list3)
#  print(list3[1])

import sys

twelve_pt_cut_name=['25<P_{t}<30','30<P_{t}<35','35<P_{t}<40','40<P_{t}<45','45<P_{t}<50','50<P_{t}<60','60<P_{t}<80','80<P_{t}<100','100<P_{t}<125','125<P_{t}<150','150<P_{t}<175','175<P_{t}<250']

twelve_pt_cut_name2=['25_30','30_35','35_40','40_45','45_50','50_60','60_80','80_100','100_125','125_150','150_175','175_250']
variables=['r_had1','r_had0','r_eta','r_phi','w_eta2','w_eta1','w_stot','fracs1','delta_E','e_ratio']

etabins = [0.00, 0.60, 0.80, 1.15, 1.52, 1.81, 2.01]

for name in range(len(variables)):
	#  list_all=[[] for i in range(len(twelve_pt_cut_name2))]
	list_all=[[0 for j in range(7)] for i in range(len(twelve_pt_cut_name2))]
	#  with open("photon_con_kang.txt") as file1:
	with open(sys.argv[1]) as file1:
		for line in  file1:
			for i in range(len(twelve_pt_cut_name2)):
					if twelve_pt_cut_name2[i] not in line:
						continue

						#  list_all[i].append(line.strip())
					line_temp = line.strip()
					#  print(line.strip())
					line_temp2 = line.strip().split("Entries:")
					line_temp3 = line.strip().split("eta_")
					line_temp4 = line_temp3[1].strip().split("_")
					#  print(line_temp4[0])
					#  exit()

					for j in range(7):
						if 'eta_'+line_temp4[0] not in line:
							continue

						list_all[i][j]=line_temp2[1]



	list_all2=[[0 for j in range(7)] for i in range(len(twelve_pt_cut_name))]
	#  with open("photon_con_xjun.txt") as file2:
	with open(sys.argv[2]) as file2:
		for line in  file2:
			for i in range(len(twelve_pt_cut_name)):
					if twelve_pt_cut_name[i] not in line:
						continue
						
					line_temp = line.strip()
					#  print(line.strip)
					line_temp2 = line.strip().split("Entries:")
					line_temp3 = line.strip().split("_")
					#  line_temp4 = line_temp3[1].strip().split("_")
					line_temp4 = line_temp3[-3].split("<|#")

					for j in range(7):
						if line_temp4[0] not in line:
							continue
						#  print(line_temp4[0])
						list_all2[i][j]=line_temp2[1]

	for i in range(12):
		for j in range(7):
			print("kang : ", list_all[i][j] , "xjun : ",list_all2[i][j])
			if list_all[i][j] != list_all2[i][j]:

				print(i,j," an error")
				#  exit()

	print(f"*_* {variables[name]} is over\n ")
