#include <fstream>
#include <iostream>
#include "TFile.h"
#include "TH1.h"
#include "TKey.h"
using namespace std;

std::string  names[10]={"r_had1","r_had0","r_eta","r_phi","w_eta2","w_eta1","w_stot","fracs1","delta_E","e_ratio"};

void PrintHistogramInfo(TFile* rootFile, std::ofstream & outputFile) {
    if (!rootFile || rootFile->IsZombie()) {
        std::cerr << "Error opening the ROOT file." << std::endl;
        return;
    }
	// cout<<rootFile->GetListOfKeys()<<endl;
	

    TIter next(rootFile->GetListOfKeys());
    TKey* key;

    while ((key = static_cast<TKey*>(next()))) {
        TObject* obj = key->ReadObj();
        if (obj->IsA() == TH1F::Class()) {
            TH1F* hist = static_cast<TH1F*>(obj);
			std::string histName = hist->GetName();
			
			for (int i=0;i<10;i++){

				if( histName.find(names[i]) != std::string::npos){
					outputFile << std::setw(20) << std::left<<"Histogram Name: " << std::setw(10) << histName;
					outputFile << std::setw(20)<<std::right << "Entries: " << hist->GetEntries() << std::endl;
				}
			}
        }
    }
}

int main(int argc, char** argv) {
    // Replace "your_file.root" with the actual path to your ROOT file.
	// std::string inputfilename =argv[1];
	// std::string outputfilename = argv[2];
    // TFile* rootFile = TFile::Open("/afs/cern.ch/user/z/zgao/eos/ee_qt/src/zee/1/user.zgao.33986780._000107.ANALYSIS.root");
    TFile* rootFile = TFile::Open(argv[1]);
	// cout << "name root File "<<rootFile<<endl;

    if (!rootFile || rootFile->IsZombie()) {
        std::cerr << "Error opening the ROOT file." << std::endl;
    }
	// std::ofstream outputFile("output.txt");
	std::ofstream outputFile(argv[2]);

    PrintHistogramInfo(rootFile, outputFile);

    rootFile->Close();
	outputFile.close();
    return 0;
}

