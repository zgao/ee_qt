#include <TH1F.h>
#include <TCanvas.h>

void test_reset() {
    // Create an original histogram
    TH1F* originalHist = new TH1F("originalHist", "Original Histogram", 100, 0, 100);
    originalHist->FillRandom("gaus", 10000);

    // Clone the original histogram
    TH1F* clonedHist = static_cast<TH1F*>(originalHist->Clone("clonedHist"));

    // Clear the content of the cloned histogram
    clonedHist->Reset();

    // Fill the cloned histogram with new data
    // clonedHist->FillRandom("gaus", 5000);

    // Create a canvas to visualize the histograms
    TCanvas* canvas = new TCanvas("canvas", "Histograms", 800, 600);

    // Draw the original histogram in blue
    originalHist->SetLineColor(kBlue);
    originalHist->Draw();

    // Draw the cloned histogram in red
    // clonedHist->SetLineColor(kRed);
    clonedHist->Draw("same");

    // Save the canvas as a PDF file
    canvas->Print("histograms.pdf");

    // Clean up memory
    delete originalHist;
    delete clonedHist;
    delete canvas;

    return 0;
}

