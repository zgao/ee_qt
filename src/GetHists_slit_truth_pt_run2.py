import numpy as np
from time import sleep
import ROOT
import argparse
import os
import math

def PrintProgressBar(index , total):
	if (index % 10000 ==0 ):
	#  if (index / 1 == index ):
		print_bar = " [";
		for bar in range(20):
			current_fraction = float(bar) / 20.0
			if (float(index) / float(total) > current_fraction):
				print_bar += "/"
			else:
				print_bar += "."

		print_bar +="]"
		progress = 100. * (float(index) / float(total))
		print(f"{print_bar}{progress: .2f}%\n", end="\r", flush=True )
		#  sleep(3)

#  for i in range(10):
	#  PrintProgressBar(i,10)

def read_file(input_file):
	try:
		with open(input_file, 'r') as file:
			#  content = file.read()
			#  content = file.readline().strip()
			#  lines = file.readline().splitlines()
			lines = file.readlines()
			#  print(lines)
			#  print(f" Contents of {input_file}:\n{lines}")

		#  chain = ROOT.TChain("PhotonCandidates")
		content = ''
		for line in lines:
			print(line)

			if not line:
				continue
			if "/*"  in line.strip() :
				continue
			content = line.strip()# it only contains one line
			print("---	 ",content)
			#  chain.Add(line)
		print(content,"123")
	except FileNotFoundError:
		print(f"Error: File '{input_file}' not found.")
	print(content)
	
	#  return chain
	return content



parser = argparse.ArgumentParser(description='')

parser.add_argument('-n', '--nominal', help='nominal without systematic uncertainties')
parser.add_argument('-in', '--input', dest="input_file", required=True, help='input file')
parser.add_argument('-v','--version',dest='versions',required=True, help='version ')
parser.add_argument('-c','--converted',dest='if_converted',required=True, help='con or unc ( con or unc) ')
parser.add_argument('-w','--cutflow',dest='histcutflow',required=True, help='hist_cutflow')
parser.add_argument('-conversion','--conversion',dest='conversion_uncertainty', help='systematic conversion uncertainty')

parser.add_argument('-frag','--fragmentation',dest='fragmentation_uncertainty',help=' systematic fragmentation uncertainty')

args = parser.parse_args()
if (args.if_converted not in ["con","unc"]):
	exit()
print(args.input_file)
filenames = read_file(args.input_file)
print("filenames  ",filenames)

output_file_name = filenames.rsplit('/',1)[-1]
print(output_file_name)

is16a = "mc16a" in	filenames 

#  print(is16a)

if (is16a):
	lumi = 36.207
else:
	lumi = 58.979

root_file = ROOT.TFile(filenames,"READ")
print("------------")

if not root_file or root_file.IsZombie():
	print("Error: Unable to open the file.")
else:
	print("File opened sucessfully.")

tree = root_file.Get("PhotonCandidates")
print(args.histcutflow)
hist_cut_file = ROOT.TFile(args.histcutflow,"READ")
hist_cut_flow = hist_cut_file.Get("histCutflow")
print(type(hist_cut_flow))
sumweight_temp = hist_cut_flow.GetBinContent(2)
print(sumweight_temp)

#  if __name__ == "__main__":
	#  parser = argparse.ArgumentParser(description="Read and print the contents of a file.")
	#  parser.add_argument("-in", dest="input_file",required=True,help="Path to the input file")


variables=['r_had1','r_had0','r_eta','r_phi','w_eta2','w_eta1','w_stot','fracs1','delta_E','e_ratio']
#  bin_numbers=[2000,4000,1000,2000,1000,1000,2000,1000,8000,1000]
bin_numbers=[100,200,500,500,500,500,200,500,500,500]
#  bin_numbers=[1000,2000,1000,1000,500,500,200,500,500,500]
#  bin_numbers=[1000,2000,1000,1000,500,500,200,500,10000,500]
#  bin_numbers=[1000,2000,1000,1000,500,500,200,500,500,500]
#  bin_numbers=[200,400,1000,1000,500,500,200,500,500,500]
labels=['Rhad1','Rhad0','R_{#eta}','R_{#phi}','W_{#eta2}','Ws3','Wtot','Fside','Delta_{E}','Eratio']
#  low_edge=[-0.05,-0.1,0.10,0.1,0.001,0.1,0.1,0.001,-100,0.]
low_edge=[-0.05,-0.1,0.2,0.2,0.,0.1,0,0,-500,0.]
#  high_edge=[0.2,0.3,1.5,1.2,0.02,1.0,20.0,1.4,7000,1.2]
#  high_edge=[0.05, 0.1, 1.2, 1.2, 0.02, 1.0, 6, 1, 3000, 1.0]
high_edge=[0.05, 0.1, 1.2, 1.2, 0.02, 1.0, 6, 1, 3000, 1.2]



#######################################################################
#################				#######################################
################# cut condition #######################################
#################				#######################################
#######################################################################
def debug_shower_shape(apple):
	if_e_ratio = ROOT.TTreeFormula("if_e_ratio","e_ratio >1", tree)
	if if_e_ratio.EvalInstanceLD() == 1 :
		return 1
	else:
		return 0


precut = ROOT.TTreeFormula("precut","f1>0.005 && FixedCutLoose>0.1",tree)

ifcon = ROOT.TTreeFormula("converted","convType > 0.1", tree)
#  ifcon = ROOT.TTreeFormula("converted","convType < 0.9", tree)
ifuncon = ROOT.TTreeFormula("unconverted","convType < 0.9", tree)
#  ifuncon = ROOT.TTreeFormula("unconverted","convType >0.1", tree)
#  if_converted=[0.1,0.9] #>0.1-> unconverted; <0.9->unconverted

four_eta_cut_name= ["0.00<|#eta|<0.60","0.60<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.37"]
#  four_eta_cut_select_temp=["TMath::Abs(primary_cluster_be2_eta)<0.6",
four_eta_cut_select_temp=["TMath::Abs(primary_cluster_be2_eta)<0.6",
					"TMath::Abs(primary_cluster_be2_eta)>0.6  && TMath::Abs(primary_cluster_be2_eta)<1.37",
					"TMath::Abs(primary_cluster_be2_eta)>1.52 && TMath::Abs(primary_cluster_be2_eta)<1.81",
					"TMath::Abs(primary_cluster_be2_eta)>1.81 && TMath::Abs(primary_cluster_be2_eta)<2.37"
					]
#  four_eta_cut_select=[ ROOT.TTreeFormula(f"four_eta_{i}",f"{four_eta_cut_select_temp[i]}",tree) for i in range(four_eta_cut_select_temp)]
four_eta_cut_select=[ ROOT.TTreeFormula(f"four_eta_{i}",four_eta_cut_select_temp[i],tree) for i in range(len(four_eta_cut_select_temp))] 

seven_eta_cut_name= ["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]

seven_eta_cut_select_temp=["TMath::Abs(primary_cluster_be2_eta)<0.6",
				"TMath::Abs(primary_cluster_be2_eta)>0.6  && TMath::Abs(primary_cluster_be2_eta)<0.80",
				"TMath::Abs(primary_cluster_be2_eta)>0.80 && TMath::Abs(primary_cluster_be2_eta)<1.15",
				"TMath::Abs(primary_cluster_be2_eta)>1.15 && TMath::Abs(primary_cluster_be2_eta)<1.37",
				"TMath::Abs(primary_cluster_be2_eta)>1.52 && TMath::Abs(primary_cluster_be2_eta)<1.81",
				"TMath::Abs(primary_cluster_be2_eta)>1.81 && TMath::Abs(primary_cluster_be2_eta)<2.01",
				"TMath::Abs(primary_cluster_be2_eta)>2.01 && TMath::Abs(primary_cluster_be2_eta)<2.37",
				]
#  seven_eta_cut_select=[ ROOT.TTreeFormula(f"seven_eta_{i}",f"{seven_eta_cut_select_temp[i]}",tree) for i in range(seven_eta_cut_select_temp)]
seven_eta_cut_select=[ ROOT.TTreeFormula(f"seven_eta_{i}",seven_eta_cut_select_temp[i],tree) for i in range(len(seven_eta_cut_select_temp))]

twelve_pt_cut_name=['25<P_{t}<30','30<P_{t}<35','35<P_{t}<40','40<P_{t}<45','45<P_{t}<50','50<P_{t}<60','60<P_{t}<80','80<P_{t}<100','100<P_{t}<125','125<P_{t}<150','150<P_{t}<175','175<P_{t}<250']

twelve_pt_cut_select_temp=["(pt>25)&&(pt<30)","(pt>30)&&(pt<35)","(pt>35)&&(pt<40)","(pt>40)&&(pt<45)","(pt>45)&&(pt<50)","(pt>50)&&(pt<60)","(pt>60)&&(pt<80)","(pt>80)&&(pt<100)","(pt>100)&&(pt<125)","(pt>125)&&(pt<150)","(pt>150)&&(pt<175)","(pt>175)&&(pt<250)"]

twelve_pt_cut_select=[ROOT.TTreeFormula(f"twelve_pt_{i}",twelve_pt_cut_select_temp[i],tree) for i in range(len(twelve_pt_cut_select_temp))]


hists_names = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]
hists = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]

for i in range(12):
	#  if i != 11:
		#  continue
	for j in range(7):
		#  if j!=2:
			#  continue
		for k in range(10):
			#  print(i,j,k)
			hists_names[i][j][k]= 'hist' + '_' + variables[k] + '_' + seven_eta_cut_name[j] + '_' + twelve_pt_cut_name[i] 
			#  print(len(variables),len(seven_eta_cut_name),len(twelve_pt_cut_name))
			#  print("pass ")
			hists[i][j][k] = ROOT.TH1F(hists_names[i][j][k],'',bin_numbers[k],low_edge[k],high_edge[k])


events = tree.GetEntries()
variables=['r_had1','r_had0','r_eta','r_phi','w_eta2','w_eta1','w_stot','fracs1','delta_E','e_ratio']


#  list_id=[800658,800659,800660,800661,800662,800663,800664,800665,800666,800667,800668,800669,800670,800671,800672,800673,800674,800675,800676,800677,800678,800679,800680,800681,800682,800683]
#  list_hard=[800658,800659,800660,800661,800662,800663,800664,800665,800666,800667,800668,800669,800670,800671]
#  list_frag=[800672,800673,800674,800675,800676,800677,800678,800679,800680,800681,800682,800683]

list_hard=[801663,801664,801665,801666,801667,801668,801669,801670,801671,801672,801673,801674,801675,801676]
list_frag=[801649,801650,801651,801652,801653,801654,801655,801656,801657,801658,801659,801660]


#  list_weight=[36753799,37715999,36744400,30704199,6983000,27416200,3481999.9,3491999.9,437999.99,439999.99,439999.99,437999.99,169999.99,169999.99,149000.00,579799.86,135319.45,43616.370,18943.559,18824.973,244.12635,17.316905,0.0155639,0.0008655,0.0003577,0.0001055]
list_truth_pt =[8,17,35,50,70,140,280,500,800,1000,1500,2000,2500,3000]

#  for j in range(len(list_hard)):
#	   MC_channel_number = list_hard[j]
#	   if MC_channel_number in list_hard:
#		   series = list_hard.index(MC_channel_number)
#		   if series < len(list_hard)-1:
#			   print("MC_channel_number",MC_channel_number,list_truth_pt[series],list_truth_pt[series+1])
#		   if series ==len(list_hard)-1:
#			   #  if truth_pt < list_truth_pt[series]:
#			   print("MC_channel_number",MC_channel_number,list_truth_pt[series])
#
#	   if MC_channel_number in list_frag:
#		   series = list_frag.index(MC_channel_number)
#		   if series < len(list_frag)-1:
#			   print("MC_channel_number",MC_channel_number,list_truth_pt[series],list_truth_pt[series+1])
#			   #  if truth_pt < list_truth_pt[series] or truth_pt > list_truth_pt[series+1]:
#				   #  continue
#		   if series ==len(list_frag)-1:
#			   #  if truth_pt < list_truth_pt[series]:
#			   print("MC_channel_number",MC_channel_number,list_truth_pt[series])
#
#  exit()


number=[0,0,0,0,0,0,0,0,0]
#  events =20
print(events)
for event in range(events):
	#  print(events)
	if(event%100000==0):
		print(event)

	PrintProgressBar(event,events)
	tree.GetEntry(event)

	MC_channel_number = tree.MC_channel_number
	truth_convType = tree.truth_convType
	truth_pt = tree.truth_pt
	#  puweight = tree.pileupWeight
	vtxweight = tree.vertexweight
	#  MC_event_weight = tree.MC_event_weight
	mcTotWeight = tree.mcTotWeight_1
	#  xSec = tree.xSec
	#  kFactor = tree.kFacrot
	pt = tree.pt
	eta = tree.primary_cluster_be2_eta
	#  ge = tree.GE
	#  sumweight = tree.SumWeights
	#  if MC_channel_number not in list_id:
		#  continue

	r_had1= tree.r_had1
	r_had0=	tree.r_had0
	r_eta= tree.r_eta
	r_phi= tree.r_phi
	w_eta2= tree.w_eta2
	w_eta1= tree.w_eta1
	w_stot= tree.w_stot
	fracs1= tree.fracs1
	delta_E= tree.delta_E
	e_ratio= tree.e_ratio
	if MC_channel_number in list_hard:
		series = list_hard.index(MC_channel_number)
		if series < len(list_hard)-1:
			if truth_pt < list_truth_pt[series] or truth_pt > list_truth_pt[series+1]:
				continue
		if series ==len(list_hard)-1:
			if truth_pt < list_truth_pt[series]:
				continue
		#  print("hard: i",i)
	if MC_channel_number in list_frag:
		series = list_frag.index(MC_channel_number)
		if series < len(list_frag)-1:
			if truth_pt < list_truth_pt[series] or truth_pt > list_truth_pt[series+1]:
				continue
		elif series ==len(list_frag)-1:
			if truth_pt < list_truth_pt[series]:
				continue
		#  print("frag: i",i)



	number[0]+=1
	if( precut.EvalInstanceLD() !=1):
		continue

	number[1]+=1
	if(args.if_converted == "con"):
		if(ifcon.EvalInstanceLD() !=1):
			continue
	elif(args.if_converted == "unc"):
		if( ifuncon.EvalInstanceLD() !=1):
			continue
	number[2]+=1


	for i in range(12):
		number[3]+=1
		if twelve_pt_cut_select[i].EvalInstanceLD() != 1 :continue
		number[4]+=1

		for j in range(7):
			number[5]+=1

			if seven_eta_cut_select[j].EvalInstanceLD() != 1 :continue
			number[6]+=1

			if debug_shower_shape(1):
				number[7]+=1
			

			weight = mcTotWeight * vtxweight
			#  weight = mcTotWeight

			if (args.fragmentation_uncertainty == "up"):
				if MC_channel_number in list_frag:
					weight = weight * 1.5
			if (args.fragmentation_uncertainty == "down"):
				if MC_channel_number in list_frag:
					weight = weight * 0.5
			if (args.conversion_uncertainty == "up"):
				if truth_convType >0:
					weight = weight *1.1

			if (args.conversion_uncertainty == "down"):
				if truth_convType >0:
					weight = weight *0.9


					
			number[8]+=1

			hists[i][j][0].Fill(r_had1,weight)

			hists[i][j][1].Fill(r_had0,weight)

			hists[i][j][2].Fill(r_eta,weight)

			hists[i][j][3].Fill(r_phi,weight)

			hists[i][j][4].Fill(w_eta2,weight)

			hists[i][j][5].Fill(w_eta1,weight)

			hists[i][j][6].Fill(w_stot,weight)

			hists[i][j][7].Fill(fracs1,weight)

			hists[i][j][8].Fill(delta_E,weight)

			hists[i][j][9].Fill(e_ratio,weight)




output_directory = f"/afs/cern.ch/user/z/zgao/eos/ee_qt/src/pho/{args.if_converted}/{args.versions}"

os.makedirs(output_directory, exist_ok=True)

output_file_path = f"/afs/cern.ch/user/z/zgao/eos/ee_qt/src/pho/{args.if_converted}/{args.versions}/{output_file_name}"
output_file = ROOT.TFile(output_file_path, "recreate")

for i in range(12):
	#  if i!=11:
		#  continue
	for j in range(7):
		#  if j!=2:
			#  continue
		for k in range(10):
			output_file.WriteTObject(hists[i][j][k])

output_file.Close()
number_dict ={"total":number[0],
		"precut":number[1],
		"con or uncon":number[2],
		"pt loop": number[3],
		"pt cut": number[4],
		"eta loop": number[5],
		"eta cut": number[6],
		"e_ratio >1": number[7],
		"rest": number[8]}
for key,value in number_dict.items():
	print(key," : ", value)
print(f"Save file as {output_file_path}")
