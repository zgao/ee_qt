import ROOT

# Create a new ROOT file
out_file = ROOT.TFile("out_put_file.root", "RECREATE")

# Create a new TTree
out_tree = ROOT.TTree("tree_name", "new")

# Define variables for branches
var1 = ROOT.vector('double')()
var2 = ROOT.vector('double')()

# Create branches in the tree
out_tree.Branch("branch1", var1)
out_tree.Branch("branch2", var2)

# Assign values to the variables
for name in range(10):
	var1_val = 3.14 + name
	var2_val = 2.13 + name

	# Fill the vectors (branches) with values
	var1.push_back(var1_val)
	var2.push_back(var2_val)

	# Fill the branches in the tree
	out_tree.Fill()

# Write the tree to the file
out_tree.Write()

# Close the output file
out_file.Close()

