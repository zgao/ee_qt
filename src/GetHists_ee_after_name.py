import numpy as np
from time import sleep
import ROOT
import argparse
import os

def PrintProgressBar(index , total):
	if (index % 10000 ==0 ):
	#  if (index / 1 == index ):
		print_bar = " [";
		for bar in range(20):
			current_fraction = float(bar) / 20.0
			if (float(index) / float(total) > current_fraction):
				print_bar += "/"
			else:
				print_bar += "."

		print_bar +="]"
		progress = 100. * (float(index) / float(total))
		print(f"{print_bar}{progress: .2f}%\n", end="\r", flush=True )
		#  sleep(3)

#  for i in range(10):
	#  PrintProgressBar(i,10)

def read_file(input_file):
	try:
		with open(input_file, 'r') as file:
			#  content = file.read()
			#  content = file.readline().strip()
			#  lines = file.readline().splitlines()
			lines = file.readlines()
			#  print(lines)
			#  print(f" Contents of {input_file}:\n{lines}")

		#  chain = ROOT.TChain("PhotonCandidates")
		content = ''
		for line in lines:
			if "/*" in line.strip() :
				continue
			content = line.strip() #it only contains one line
			#  chain.Add(line)
	except FileNotFoundError:
		print(f"Error: File '{input_file}' not found.")
	
	#  return chain
	return content



parser = argparse.ArgumentParser(description='')

parser.add_argument('-n', '--nominal', help='nominal without systematic uncertainties')
parser.add_argument('-in', '--input', dest="input_file", required=True, help='input file')
parser.add_argument('-v','--version',dest='versions',required=True, help='version ')
#  parser.add_argument('-c','--converted',dest='if_converted',required=True, help='con or uncon ( con or unc) ')
parser.add_argument('-w','--hist',dest='hist',required=True,help='hist used to correct weight')
parser.add_argument('-o','--output',dest='output',required=True,help='location hist saved')

args = parser.parse_args()

#  print(args.input_file)
filenames = read_file(args.input_file)
print("filenames  ",filenames)

output_file_name = filenames.rsplit('/',1)[-1]
print(output_file_name)

is16a = "mc16a" in	filenames 

#  print(is16a)

if (is16a):
	lumi = 36.207
else:
	#  lumi = 139
	lumi = 58.979

root_file = ROOT.TFile(filenames,"READ")
print("------------")

if not root_file or root_file.IsZombie():
	print("Error: Unable to open the file.")
else:
	print("File opened sucessfully.")

tree = root_file.Get("ZeeCandidates")

#  if __name__ == "__main__":
	#  parser = argparse.ArgumentParser(description="Read and print the contents of a file.")
	#  parser.add_argument("-in", dest="input_file",required=True,help="Path to the input file")


variables=['r_had1','r_had0','r_eta','r_phi','w_eta2','w_eta1','w_stot','fracs1','delta_E','e_ratio']
#  bin_numbers=[2000,4000,1000,2000,1000,1000,2000,1000,8000,1000]
bin_numbers=[100,200,500,500,500,500,200,500,500,500]
labels=['Rhad1','Rhad0','R_{#eta}','R_{#phi}','W_{#eta2}','Ws3','Wtot','Fside','Delta_{E}','Eratio']
low_edge=[-0.05,-0.1,0.2,0.2,0.,0.1,0,0,-500,0.]
#  high_edge=[0.2,0.3,1.5,1.2,0.02,1.0,20.0,1.4,7000,1.2]
high_edge=[0.05, 0.1, 1.2, 1.2, 0.02, 1.0, 6, 1, 3000, 1.0]



#######################################################################
#################				#######################################
################# cut condition #######################################
#################				#######################################
#######################################################################

def debug_shower_shape(apple):
	if_e_ratio = ROOT.TTreeFormula("if_e_ratio","e_ratio >1", tree)
	if if_e_ratio.EvalInstanceLD() >1 :
		return 1
	else:
		return 0






#  precut = ROOT.TTreeFormula("precut","f1>0.005 && pass__TightLLH>0",tree)
precut = ROOT.TTreeFormula("precut","f1>0.005 && pass__denominator>0",tree)
#  precut = ROOT.TTreeFormula("precut","f1>0.005 ",tree)

#  ifuncon = ROOT.TTreeFormula("unconverted","convType > 0.1", tree)
#  ifcon = ROOT.TTreeFormula("converted","convType < 0.9", tree)
#  if_converted=[0.1,0.9] #>0.1-> unconverted; <0.9->unconverted

four_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.37"]
#  four_eta_cut_select_temp=["TMath::Abs(primary_cluster_be2_eta)<0.6",
four_eta_cut_select_temp=["primary_cluster_be2_abseta<0.6",
					"primary_cluster_be2_abseta>0.6  && primary_cluster_be2_abseta<1.37",
					"primary_cluster_be2_abseta>1.52 && primary_cluster_be2_abseta<1.81",
					"primary_cluster_be2_abseta>1.81 && primary_cluster_be2_abseta<2.37"
					]
#  four_eta_cut_select=[ ROOT.TTreeFormula(f"four_eta_{i}",f"{four_eta_cut_select_temp[i]}",tree) for i in range(four_eta_cut_select_temp)]
four_eta_cut_select=[ ROOT.TTreeFormula(f"four_eta_{i}",four_eta_cut_select_temp[i],tree) for i in range(len(four_eta_cut_select_temp))] 

seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]

#  seven_eta_cut_select_temp=["(TMath::Abs(primary_cluster_be2_eta)<0.6)",
seven_eta_cut_select_temp=["primary_cluster_be2_abseta<0.6",
				#  "(TMath::Abs(primary_cluster_be2_eta)>0.6)&&(TMath::Abs(primary_cluster_be2_eta<0.80))",
				"primary_cluster_be2_abseta>0.6 && primary_cluster_be2_abseta<0.80",
				"primary_cluster_be2_abseta>0.80 && primary_cluster_be2_abseta<1.15",
				"primary_cluster_be2_abseta>1.15 && primary_cluster_be2_abseta<1.37",
				"primary_cluster_be2_abseta>1.52 && primary_cluster_be2_abseta<1.81",
				"primary_cluster_be2_abseta>1.81 && primary_cluster_be2_abseta<2.01",
				"primary_cluster_be2_abseta>2.01 && primary_cluster_be2_abseta<2.37",
				]
#  seven_eta_cut_select=[ ROOT.TTreeFormula(f"seven_eta_{i}",f"{seven_eta_cut_select_temp[i]}",tree) for i in range(seven_eta_cut_select_temp)]
seven_eta_cut_select=[ ROOT.TTreeFormula(f"seven_eta_{i}",seven_eta_cut_select_temp[i],tree) for i in range(len(seven_eta_cut_select_temp))]

twelve_pt_cut_name=['25<P_{t}<30','30<P_{t}<35','35<P_{t}<40','40<P_{t}<45','45<P_{t}<50','50<P_{t}<60','60<P_{t}<80','80<P_{t}<100','100<P_{t}<125','125<P_{t}<150','150<P_{t}<175','175<P_{t}<250']

twelve_pt_cut_select_temp=["(pt>25)&&(pt<30)","(pt>30)&&(pt<35)","(pt>35)&&(pt<40)","(pt>40)&&(pt<45)","(pt>45)&&(pt<50)","(pt>50)&&(pt<60)","(pt>60)&&(pt<80)","(pt>80)&&(pt<100)","(pt>100)&&(pt<125)","(pt>125)&&(pt<150)","(pt>150)&&(pt<175)","(pt>175)&&(pt<250)"]

twelve_pt_cut_select=[ROOT.TTreeFormula(f"twelve_pt_{i}",twelve_pt_cut_select_temp[i],tree) for i in range(len(twelve_pt_cut_select_temp))]


hists_names = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]
hists = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]

for i in range(12):
	for j in range(7):
		for k in range(10):
			#  print(i,j,k)
			hists_names[i][j][k]= 'hist' + '_' + variables[k] + '_' + seven_eta_cut_name[j] + '_' + twelve_pt_cut_name[i] 
			#  print(len(variables),len(seven_eta_cut_name),len(twelve_pt_cut_name))
			#  print("pass ")
			hists[i][j][k] = ROOT.TH1F(hists_names[i][j][k],hists_names[i][j][k],bin_numbers[k],low_edge[k],high_edge[k])


events = tree.GetEntries()
variables=['r_had1','r_had0','r_eta','r_phi','w_eta2','w_eta1','w_stot','fracs1','delta_E','e_ratio']


hist_weight_file_name = read_file(args.hist)
hist_weight_file = ROOT.TFile(hist_weight_file_name,"READ")

number=[0,0,0,0,0,0,0,0]
#  events=10
#  print(1.0==1)
for event in range(events):
	#  print(events)

	PrintProgressBar(event,events)
	tree.GetEntry(event)

	#  MC_channel_number = tree.MC_channel_number
	puweight = tree.puweight
	#  vtxweight = tree.vertexweight
	MC_event_weight = tree.MC_event_weight
	xSec = tree.xSec
	#  kFactor = tree.kFacrot
	pt = tree.pt
	eta =tree.primary_cluster_be2_eta
	#  ge = tree.GE

	r_had1= tree.r_had1
	r_had0=	tree.r_had0
	r_eta= tree.r_eta
	r_phi= tree.r_phi
	w_eta2= tree.w_eta2
	w_eta1= tree.w_eta1
	w_stot= tree.w_stot
	fracs1= tree.fracs1
	delta_E= tree.delta_E
	e_ratio= tree.e_ratio
	MC_channel_number = tree.MC_channel_number

	#  sumweight = hist_weight_file.Get(f"CutBookkeeper_{MC_channel_number}_1_NOSYS").GetBinContent(3)
	#  if( hist_weight_file.Get(f"CutBookkeeper_{MC_channel_number}_1_NOSYS").GetBinContent(1)!=0):
	#      BxAOD = hist_weight_file.Get(f"CutBookkeeper_{MC_channel_number}_1_NOSYS").GetBinContent(1)
	#      BDxAOD = hist_weight_file.Get(f"CutBookkeeper_{MC_channel_number}_1_NOSYS").GetBinContent(2)
	#      DxAOD = sumweight
	#      bookwei =BxAOD * DxAOD /BDxAOD ;
	#  else:
	#      DxAOD =sumweight
	#      bookwei =DxAOD
	#  bookwei = sumweight
	sumweight = hist_weight_file.Get(f"CutBookkeeper_{MC_channel_number}_1_NOSYS").GetBinContent(2)
	bookwei = sumweight
	
	number[0]+=1
	if( precut.EvalInstanceLD() !=1):
		continue
	number[1]+=1

	for i in range(12):
		number[2]+=1
		if twelve_pt_cut_select[i].EvalInstanceLD() != 1 :continue
		number[3]+=1
		#  if i ==0:
		#  print(pt,i,eta)

		#  print(twelve_pt_cut_select[i].EvalInstanceLD(),"  -------")

		for j in range(7):
			number[4]+=1
			#  if i!=0 :continue
			#  result = [seven_eta_cut_select[k].EvalInstanceLD() for k in range(7)]
			#  print(result)
			#  print(eta)
			#  exit()
			if seven_eta_cut_select[j].EvalInstanceLD() != 1 :continue
			number[5]+=1
			#  print(j, eta)
			
			#  if i == 0:
				#  number[3]+=1
				#  print(j,eta)

			if debug_shower_shape(1):
				number[6]+=1

			number[7]+=1
			#  weight = 0
			weight =MC_event_weight *puweight *xSec *lumi
			#  sumweight = hist_weight_file.Get(f"CutBookkeeper_{MC_channel_number}_1_NOSYS").GetBinContent(2)
			bookwei = sumweight
			weight =weight / bookwei
			#  weight =MC_event_weight

			#  print(weight," ",MC_event_weight)
			#  print(weight," ",sumweight)
			
			

			hists[i][j][0].Fill(r_had1,weight)

			hists[i][j][1].Fill(r_had0,weight)

			hists[i][j][2].Fill(r_eta,weight)

			hists[i][j][3].Fill(r_phi,weight)

			hists[i][j][4].Fill(w_eta2,weight)

			hists[i][j][5].Fill(w_eta1,weight)

			hists[i][j][6].Fill(w_stot,weight)

			hists[i][j][7].Fill(fracs1,weight)

			hists[i][j][8].Fill(delta_E,weight)

			hists[i][j][9].Fill(e_ratio,weight)
				#  if i==1 and j==1 :
				#      c1= ROOT.TCanvas("c1","",800,600)
				#      c1.cd()
				#      hists[i][j][9].Draw()
				#      c1.SaveAs("222.png")


output_directory = f"/afs/cern.ch/user/z/zgao/eos/ee_qt/src/zee/{args.versions}/{args.output}"

os.makedirs(output_directory, exist_ok=True)

#  output_file_path = f"/afs/cern.ch/user/z/zgao/eos/ee_qt/src/zee/{args.versions}/{output_file_name}"
output_file_path = f"/afs/cern.ch/user/z/zgao/eos/ee_qt/src/zee/{args.versions}/{args.output}/{output_file_name}"
output_file = ROOT.TFile(output_file_path, "recreate")

for i in range(12):
	for j in range(7):
		for k in range(10):
			output_file.WriteTObject(hists[i][j][k])

output_file.Close()
number_dict ={"total":number[0],
		"precut":number[1],
		"pt loop": number[2],
		"pt cut": number[3],
		"eta loop": number[4],
		"eta cut": number[5],
		"e_ratio >1": number[6],
		"rest": number[7]}
for key,value in number_dict.items():
	print(key," : ",value)
print(f"Save file as {output_file_path}")
