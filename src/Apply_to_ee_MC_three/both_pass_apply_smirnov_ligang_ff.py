import math
import ROOT
from array import array
import os
import argparse

def PrintProgressBar(index , total):
	if (index % 100000 ==0 ):
	#  if (index / 1 == index ):
		print_bar = " [";
		for bar in range(20):
			current_fraction = float(bar) / 20.0
			if (float(index) / float(total) > current_fraction):
				print_bar += "/"
			else:
				print_bar += "."

		print_bar +="]"
		progress = 100. * (float(index) / float(total))
		print(f"{print_bar}{progress: .2f}%\n", end="\r", flush=True )


variables=['r_had1','r_had0','r_eta','r_phi','w_eta2','w_eta1','w_stot','fracs1','delta_E','e_ratio']
list_eta = ["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]
list_pt =  ['25<P_{t}<30','30<P_{t}<35','35<P_{t}<40','40<P_{t}<45','45<P_{t}<50','50<P_{t}<60','60<P_{t}<80','80<P_{t}<100','100<P_{t}<125','125<P_{t}<150','150<P_{t}<175','175<P_{t}<250']

list_mus = [[[0 for i in range(len(list_pt))] for j in range(len(list_eta))] for k in range(len(variables))]
list_sigmas = [[[0 for i in range(len(list_pt))] for j in range(len(list_eta))] for k in range(len(variables))]

def get_sigma_mu_ff(index):
	
	with open(f"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/ff_ligangs/local_test/fit_information_2_yue/{variables[index]}_temp.txt") as file1:
		lines = file1.readlines()
	
		for i,line in enumerate(lines):
			#  print(line)
			if "The_fit_is_valid" in line:
				list_name = lines[i+1].split()
				#  print(list_name[0])
	
				list_sigma = lines[i-5].split()
				#  print("sigma: ",list_sigma[1])
				list_mu = lines[i-6].split()
				#  print("mu: ",list_mu[1])
			   
				name_split = list_name[0].split("_")
				pt_range = name_split[-2] + '_' + name_split[-1]
				eta_range = name_split[-3]
				#  print(pt_range)
				#  print(eta_range)
			   
	
			
				if name_split[2] != name_split[-3]:
					name_temp = 0
					name_temp = "_".join(name_split[2:-3])
				else:
					name_temp = name_split[2]
				#  print(name_temp)
			   
				index_variable = variables.index(name_temp)
				#  print(variables.index(name_temp))
				index_eta = list_eta.index(eta_range)
				#  print(list_eta.index(eta_range))
				index_pt = list_pt.index(pt_range)
				#  print(list_pt.index(pt_range))
			   
				list_mus[index_variable][index_eta][index_pt] = list_mu[1]
				list_sigmas[index_variable][index_eta][index_pt] = list_sigma[1]
for i in range(len(variables)):
	get_sigma_mu_ff(i)


parser = argparse.ArgumentParser(description='')

parser.add_argument('-n', '--nominal', help=' nominal without systematic uncertainties')
parser.add_argument('-in','--input',dest='input_file', required=True, help='input file to get Smirnov')
parser.add_argument('-out','--output',dest='output_file', required=True, help='output file allplied Smirnov')
parser.add_argument('-v', '--version',dest='versions', required=True, help='version')
parser.add_argument('-c', '--converted',dest='if_converted',required=True,help='con or uncon( con or unc )')
parser.add_argument('-s', '--smirnov',dest='smirnov',required=True,help='smirnov transformation')

args = parser.parse_args()
inputfile = args.input_file
outputfile = args.output_file

#  data_file = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/con1_fudge/user.zgao.tp-example_zmass-data18_13TeV-EGAM1-grp18_v01_p5535_20230716_071833_ANALYSIS.root/user.zgao.34090425._000112.ANALYSIS.root","READ")
data_file = ROOT.TFile(inputfile,"READ")
tree = data_file.Get("Electrons_All")


branch_names = [branch.GetName() for branch in tree.GetListOfBranches()]
#  smirnov_file = ROOT.TFile("smirnov_transformation.root","READ")
smirnov_file = ROOT.TFile(args.smirnov,"READ")


output_file = ROOT.TFile(outputfile,"RECREATE")
output_tree = ROOT.TTree("ZeeCandidates","ZeeCandidates")



puweight = array('f',[0])
MC_event_weight = array('d',[0])
xSec = array('f',[0])
MC_channel_number = array('i',[0])


eta = array('d',[0])
phi = array ('d',[0])
pt = array ('d',[0])
primary_cluster_be2_eta = array ('d',[0])
primary_cluster_be2_abseta = array ('d',[0])
r_had0 = array ('d',[0])
r_had1 = array ('d',[0])
e_ratio = array ('d',[0])
delta_E = array ('d',[0])
w_eta2 = array ('d',[0])
w_eta1 = array ('d',[0])
r_phi = array ('d',[0])
r_eta = array ('d',[0])
w_stot = array ('d',[0])
f1 = array ('d',[0])
fracs1 = array ('d',[0])
f3 = array ('d',[0])
ptcone20 = array ('d',[0])
topoetcone20 = array ('d',[0])
#  pass__TightLLH = array ('d',[0])
pass__denominator = array ('i',[0])
E277 = array ('d',[0])
#  pt_cluster = array ('d',[0])
#  eta_cluster = array ('d',[0])
mee = array ('d',[0])


output_tree.Branch("puweight",puweight, "puweight/F")
output_tree.Branch("MC_event_weight",MC_event_weight, "MC_event_weight/D")
output_tree.Branch("xSec",xSec, "xSec/F")
output_tree.Branch("MC_channel_number",MC_channel_number, "MC_channel_number/I")



output_tree.Branch("eta",eta, "eta/D")
output_tree.Branch("phi",phi, "phi/D")
output_tree.Branch("pt",pt, "pt/D")
output_tree.Branch("primary_cluster_be2_eta",primary_cluster_be2_eta, "primary_cluster_be2_eta/D")
output_tree.Branch("primary_cluster_be2_abseta",primary_cluster_be2_abseta, "primary_cluster_be2_abseta/D")
output_tree.Branch("r_had0",r_had0, "r_had0/D")
output_tree.Branch("r_had1",r_had1, "r_had1/D")
output_tree.Branch("e_ratio",e_ratio, "e_ratio/D")
output_tree.Branch("delta_E",delta_E, "delta_E/D")
output_tree.Branch("w_eta2",w_eta2, "w_eta2/D")
output_tree.Branch("w_eta1",w_eta1, "w_eta1/D")
output_tree.Branch("r_phi",r_phi, "r_phi/D")
output_tree.Branch("r_eta",r_eta, "r_eta/D")
output_tree.Branch("w_stot",w_stot, "w_stot/D")
output_tree.Branch("f1",f1, "f1/D")
output_tree.Branch("fracs1",fracs1, "fracs1/D")
output_tree.Branch("f3",f3, "f3/D")
output_tree.Branch("ptcone20",ptcone20, "ptcone20/D")
output_tree.Branch("topoetcone20",topoetcone20, "topoetcone20/D")
#  output_tree.Branch("pass__TightLLH",pass__TightLLH, "pass__TightLLH/D")
output_tree.Branch("pass__denominator",pass__denominator, "pass__denominator/I")
output_tree.Branch("E277",E277, "E277/D")
#  output_tree.Branch("pt_cluster",pt_cluster, "pt_cluster/F")
#  output_tree.Branch("eta_cluster",eta_cluster, "eta_cluster/F")
output_tree.Branch("mee",mee, "mee/D")

puweight_temp =array('f',[0])
MC_event_weight_temp =array('d',[0])
xSec_temp =array('f',[0])
MC_channel_number_temp =array('i',[0])

eta_temp=ROOT.vector('double')()
phi_temp = ROOT.vector('double')()
pt_temp = ROOT.vector('double')()
primary_cluster_be2_eta_temp = ROOT.vector('double')()
primary_cluster_be2_abseta_temp = ROOT.vector('double')()
r_had0_temp = ROOT.vector('double')()
r_had1_temp = ROOT.vector('double')()
e_ratio_temp = ROOT.vector('double')()
delta_E_temp = ROOT.vector('double')()
w_eta2_temp = ROOT.vector('double')()
w_eta1_temp = ROOT.vector('double')()
r_phi_temp = ROOT.vector('double')()
r_eta_temp = ROOT.vector('double')()
w_stot_temp = ROOT.vector('double')()
f1_temp = ROOT.vector('double')()
fracs1_temp = ROOT.vector('double')()
f3_temp = ROOT.vector('double')()
ptcone20_temp = ROOT.vector('double')()
topoetcone20_temp = ROOT.vector('double')()
pass__TightLLH_temp = ROOT.vector('int')()
pass__denominator_temp = ROOT.vector('int')()
pass__tag_temp= ROOT.vector('int')()
E277_temp = ROOT.vector('int')()
#  eta_cluster_temp = ROOT.vector('double')()
mee_temp = ROOT.vector('double')()
e_temp = ROOT.vector('double')()
actual_mu_temp = ROOT.vector('double')()
average_mu_temp = ROOT.vector('double')()



tree.SetBranchAddress("pileupWeight__NOSYS",puweight_temp)
tree.SetBranchAddress("mc_event_weight",MC_event_weight_temp)
tree.SetBranchAddress("interaction_per_crossing_actual",xSec_temp)
tree.SetBranchAddress("mc_channel_number",MC_channel_number_temp)

tree.SetBranchAddress("eta__NOSYS",eta_temp)
tree.SetBranchAddress("phi__NOSYS",phi_temp)
tree.SetBranchAddress("pt__NOSYS",pt_temp)
tree.SetBranchAddress("primary_cluster_be2_eta__NOSYS",primary_cluster_be2_eta_temp)
tree.SetBranchAddress("primary_cluster_be2_abseta__NOSYS",primary_cluster_be2_abseta_temp)
#  tree.SetBranchAddress("r_had__NOSYS",r_had_temp)
tree.SetBranchAddress("r_had0__NOSYS",r_had0_temp)
tree.SetBranchAddress("r_had1__NOSYS",r_had1_temp)
tree.SetBranchAddress("e_ratio__NOSYS",e_ratio_temp)
tree.SetBranchAddress("delta_E__NOSYS",delta_E_temp)
tree.SetBranchAddress("w_eta2__NOSYS",w_eta2_temp)
tree.SetBranchAddress("w_eta1__NOSYS",w_eta1_temp)
tree.SetBranchAddress("r_eta__NOSYS",r_eta_temp)
tree.SetBranchAddress("r_phi__NOSYS",r_phi_temp)
tree.SetBranchAddress("w_stot__NOSYS",w_stot_temp)
tree.SetBranchAddress("f1__NOSYS",f1_temp)
tree.SetBranchAddress("fracs1__NOSYS",fracs1_temp)
tree.SetBranchAddress("f3__NOSYS",f3_temp)
tree.SetBranchAddress("ptcone20__NOSYS",ptcone20_temp)
tree.SetBranchAddress("topoetcone20__NOSYS",topoetcone20_temp)
tree.SetBranchAddress("pass__TightLLH__NOSYS",pass__TightLLH_temp)
tree.SetBranchAddress("pass__denominator__NOSYS",pass__denominator_temp)
tree.SetBranchAddress("E277__NOSYS",E277_temp)
tree.SetBranchAddress("e__NOSYS",e_temp)
tree.SetBranchAddress("pass__tag__NOSYS",pass__tag_temp)
#  tree.SetBranchAddress("actual_mu__NOSYS",actual_mu_temp)
#  tree.SetBranchAddress("average_mu__NOSYS",average_mu_temp)

list_eta = [ 0, 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37]
list_pt = [25,30,35,40,45,50,60,80,100,125,150,175,250]



#  for branch_name in branch_names:
	#  print(branch_name)
print(tree.GetEntries())
for entry in range(tree.GetEntries()):
#  for entry in range(10000):
	tree.GetEntry(entry)
	#  if entry%100000==0:
		#  print(entry)
	PrintProgressBar(entry,tree.GetEntries())

	if eta_temp.size()<2:
		continue
		#  print(eta.at(0))

	itag =-1
	iprobe =-1

	if not (pass__denominator_temp.at(1) ==1 and pass__denominator_temp.at(0) ==1):
		continue
		#  itag = 1
		#  iprobe = 0
	#  elif(pass__tag_temp.at(0) ==1 and pass__TightLLH_temp.at(1) ==1):
		#  itag =0
		#  iprobe =1
	#  else:
		#  continue
	for time in range(2):
		if time == 0:
			iprobe =0
		elif time ==1:
			iprobe =1
		else:
			continue

		if (pt_temp.at(iprobe) <25 or pt_temp.at(iprobe) > 250):
			continue
		if (math.fabs(primary_cluster_be2_eta_temp.at(iprobe)) >1.37 and math.fabs(primary_cluster_be2_eta_temp.at(iprobe)) < 1.52):
			continue
		if math.fabs(primary_cluster_be2_eta_temp.at(iprobe)) >2.37:
			continue
		if (ptcone20_temp.at(iprobe) >0.05*pt_temp.at(iprobe) or topoetcone20_temp.at(iprobe)> 0.065*pt_temp.at(iprobe)):
			continue

		particle1 = ROOT.TLorentzVector()
		particle2 = ROOT.TLorentzVector()
		particle = ROOT.TLorentzVector()

		particle1.SetPtEtaPhiE(pt_temp.at(0),
							eta_temp.at(0),
							phi_temp.at(0),
							e_temp.at(0))
		particle2.SetPtEtaPhiE(pt_temp.at(1),
							eta_temp.at(1),
							phi_temp.at(1),
							e_temp.at(1))
		particle = particle1+particle2
		mzee = particle.M()

		if(mzee<80 or mzee >100):
			continue
		
		#  for i in range(len(list_eta)-1):

		if math.fabs(eta_temp.at(iprobe))<0.6: 
			eta_name ="0.00<|#eta|<0.60"
			index_eta = 0
		elif math.fabs(eta_temp.at(iprobe))>0.6 and math.fabs(eta_temp.at(iprobe))< 0.8 :
			eta_name ="0.60<|#eta|<0.80"
			index_eta = 1
		elif math.fabs(eta_temp.at(iprobe))>0.8 and math.fabs(eta_temp.at(iprobe))< 1.15 :
			eta_name ="0.80<|#eta|<1.15"
			index_eta = 2
		elif math.fabs(eta_temp.at(iprobe))>1.15 and math.fabs(eta_temp.at(iprobe))< 1.37 :
			eta_name ="1.15<|#eta|<1.37"
			index_eta = 3
		elif math.fabs(eta_temp.at(iprobe))>1.52 and math.fabs(eta_temp.at(iprobe))< 1.81 :
			eta_name ="1.52<|#eta|<1.81"
			index_eta = 4
		elif math.fabs(eta_temp.at(iprobe))>1.81 and math.fabs(eta_temp.at(iprobe))< 2.01 :
			eta_name ="1.81<|#eta|<2.01"
			index_eta = 5
		elif math.fabs(eta_temp.at(iprobe))>2.01 and math.fabs(eta_temp.at(iprobe))< 2.37 :
			eta_name ="2.01<|#eta|<2.37"
			index_eta = 6
		else:
			continue

		if pt_temp.at(iprobe)<30:
			pt_name ="25<P_{t}<30"
			index_pt = 0
		elif pt_temp.at(iprobe)> 30 and pt_temp.at(iprobe)<35:
			pt_name ="30<P_{t}<35"
			index_pt = 1
		elif pt_temp.at(iprobe)> 35 and pt_temp.at(iprobe)<40:
			pt_name ="35<P_{t}<40"
			index_pt = 2
		elif pt_temp.at(iprobe)> 40 and pt_temp.at(iprobe)<45:
			pt_name ="40<P_{t}<45"
			index_pt = 3
		elif pt_temp.at(iprobe)> 45 and pt_temp.at(iprobe)<50:
			pt_name ="45<P_{t}<50"
			index_pt = 4
		elif pt_temp.at(iprobe)> 50 and pt_temp.at(iprobe)<60:
			pt_name ="50<P_{t}<60"
			index_pt = 5
		elif pt_temp.at(iprobe)> 60 and pt_temp.at(iprobe)<80:
			pt_name ="60<P_{t}<80"
			index_pt = 6
		elif pt_temp.at(iprobe)> 80 and pt_temp.at(iprobe)<100:
			pt_name ="80<P_{t}<100"
			index_pt = 7
		elif pt_temp.at(iprobe)> 100 and pt_temp.at(iprobe)<125:
			pt_name ="100<P_{t}<125"
			index_pt = 8
		elif pt_temp.at(iprobe)> 125 and pt_temp.at(iprobe)<150:
			pt_name ="125<P_{t}<150"
			index_pt = 9
		elif pt_temp.at(iprobe)> 150 and pt_temp.at(iprobe)<175:
			pt_name ="150<P_{t}<175"
			index_pt = 10
		elif pt_temp.at(iprobe)> 175 and pt_temp.at(iprobe)<250:
			pt_name ="175<P_{t}<250"
			index_pt = 11
		else:
			continue
		
		#  if not (math.fabs(eta_temp.at(iprobe))>2.01 and math.fabs(eta_temp.at(iprobe))< 2.37):
			#  continue
		#  if not (pt_temp.at(iprobe)> 30 and pt_temp.at(iprobe)<35):
			#  continue

		#  for time in range(2):
		#	   if time == 0:
		#		   iprobe =0
		#	   elif time ==1:
		#		   iprobe =1
		#	   else:
		#		   continue

		eta[0] = eta_temp.at(iprobe)
		phi[0] = phi_temp.at(iprobe)
		primary_cluster_be2_eta[0] = primary_cluster_be2_eta_temp.at(iprobe)
		primary_cluster_be2_abseta[0] = primary_cluster_be2_abseta_temp.at(iprobe)
		ptcone20[0] = ptcone20_temp.at(iprobe)
		topoetcone20[0] = topoetcone20_temp.at(iprobe)
		E277[0] = E277_temp.at(iprobe)
		pt[0] = pt_temp.at(iprobe)
		mee[0] =mzee
		f3[0] = f3_temp.at(iprobe)
		f1[0] = f1_temp.at(iprobe)

		#  print(eta[0],"",phi[0])
		#  exit()

		puweight[0] = puweight_temp[0]
		MC_event_weight[0]=MC_event_weight_temp[0]
		xSec[0] = xSec_temp[0]
		#  print(puweight[0]," ",xSec[0])
		MC_channel_number[0]= MC_channel_number_temp[0]
		pass__denominator[0] = pass__denominator_temp.at(iprobe)

		#  print(f"Smirnov_r_had1_{eta_name}_{pt_name}")


		r_had1_final_temp = ( r_had1_temp.at(iprobe) + float(list_mus[0][index_eta][index_pt] )) /float(list_sigmas[0][index_eta][index_pt])
		#  print(r_had1_temp.at(iprobe), float(list_mus[0][index_eta][index_pt] ),float(list_sigmas[0][index_eta][index_pt]),r_had1_final_temp,index_eta, index_pt)
		#  r_had1[0] = smirnov_file.Get(f"Smirnov_r_had1_{eta_name}_{pt_name}").Interpolate(r_had1_temp.at(iprobe))
		r_had1[0] = smirnov_file.Get(f"Smirnov_r_had1_{eta_name}_{pt_name}").Interpolate(r_had1_final_temp)
		#  print(r_had1[0])



		r_had0_final_temp = ( r_had0_temp.at(iprobe) + float(list_mus[1][index_eta][index_pt] )) /float(list_sigmas[1][index_eta][index_pt])
		#  r_had0[0] = smirnov_file.Get(f"Smirnov_r_had0_{eta_name}_{pt_name}").Interpolate(r_had0_temp.at(iprobe))
		r_had0[0] = smirnov_file.Get(f"Smirnov_r_had0_{eta_name}_{pt_name}").Interpolate(r_had0_final_temp)

		r_eta_final_temp = ( r_eta_temp.at(iprobe) + float(list_mus[2][index_eta][index_pt] )) /float(list_sigmas[2][index_eta][index_pt])
		#  r_eta[0] = smirnov_file.Get(f"Smirnov_r_eta_{eta_name}_{pt_name}").Interpolate(r_eta_temp.at(iprobe))
		r_eta[0] = smirnov_file.Get(f"Smirnov_r_eta_{eta_name}_{pt_name}").Interpolate(r_eta_final_temp)

		r_phi_final_temp = ( r_phi_temp.at(iprobe) + float(list_mus[3][index_eta][index_pt] )) /float(list_sigmas[3][index_eta][index_pt])
		#  r_phi[0] = smirnov_file.Get(f"Smirnov_r_phi_{eta_name}_{pt_name}").Interpolate(r_phi_temp.at(iprobe))
		r_phi[0] = smirnov_file.Get(f"Smirnov_r_phi_{eta_name}_{pt_name}").Interpolate(r_phi_temp.at(iprobe))


		w_eta2_final_temp = ( w_eta2_temp.at(iprobe) + float(list_mus[4][index_eta][index_pt] )) /float(list_sigmas[4][index_eta][index_pt])
		#  w_eta2[0] = smirnov_file.Get(f"Smirnov_w_eta2_{eta_name}_{pt_name}").Interpolate(w_eta2_temp.at(iprobe))
		w_eta2[0] = smirnov_file.Get(f"Smirnov_w_eta2_{eta_name}_{pt_name}").Interpolate(w_eta2_final_temp)

		w_eta1_final_temp = ( w_eta1_temp.at(iprobe) + float(list_mus[5][index_eta][index_pt] )) /float(list_sigmas[5][index_eta][index_pt])
		#  w_eta1[0] = smirnov_file.Get(f"Smirnov_w_eta1_{eta_name}_{pt_name}").Interpolate(w_eta1_temp.at(iprobe))
		w_eta1[0] = smirnov_file.Get(f"Smirnov_w_eta1_{eta_name}_{pt_name}").Interpolate(w_eta1_final_temp)

		w_stot_final_temp = ( w_stot_temp.at(iprobe) + float(list_mus[6][index_eta][index_pt] )) /float(list_sigmas[6][index_eta][index_pt])
		#  w_stot[0] = smirnov_file.Get(f"Smirnov_w_stot_{eta_name}_{pt_name}").Interpolate(w_stot_temp.at(iprobe))
		w_stot[0] = smirnov_file.Get(f"Smirnov_w_stot_{eta_name}_{pt_name}").Interpolate(w_stot_final_temp)

		fracs1_final_temp = ( fracs1_temp.at(iprobe) + float(list_mus[7][index_eta][index_pt] )) /float(list_sigmas[7][index_eta][index_pt])
		#  fracs1[0] = smirnov_file.Get(f"Smirnov_fracs1_{eta_name}_{pt_name}").Interpolate(fracs1_temp.at(iprobe))
		fracs1[0] = smirnov_file.Get(f"Smirnov_fracs1_{eta_name}_{pt_name}").Interpolate(fracs1_final_temp)

		delta_E_final_temp = ( delta_E_temp.at(iprobe) + float(list_mus[8][index_eta][index_pt] )) /float(list_sigmas[8][index_eta][index_pt])
		#  delta_E[0] = smirnov_file.Get(f"Smirnov_delta_E_{eta_name}_{pt_name}").Interpolate(delta_E_temp.at(iprobe))
		delta_E[0] = smirnov_file.Get(f"Smirnov_delta_E_{eta_name}_{pt_name}").Interpolate(delta_E_final_temp)

		e_ratio_final_temp = ( e_ratio_temp.at(iprobe) + float(list_mus[9][index_eta][index_pt] )) /float(list_sigmas[9][index_eta][index_pt])
		#  e_ratio[0] = smirnov_file.Get(f"Smirnov_e_ratio_{eta_name}_{pt_name}").Interpolate(e_ratio_temp.at(iprobe))
		e_ratio[0] = smirnov_file.Get(f"Smirnov_e_ratio_{eta_name}_{pt_name}").Interpolate(e_ratio_final_temp)
		#  exit()
		output_tree.Fill()

output_file.Write()
output_file.Close()
data_file.Close()
print("it's over")
print(output_file)





