import ROOT
import numpy as np
from array import array

file1 = ROOT.TFile("tree.root", 'recreate')
tree = ROOT.TTree("tree_name","tree title")


px = array('f',[1.5])
tree.Branch("px",px, "px/F")

tree.Fill()

file1.Write()
file1.Close()
