#!/bin/bash

# python3 Smirnov.py -in smirnovs_txt/con/smirnov.txt -v 2 -c con -o photon_photon

# python3 Smirnov.py -in smirnovs_txt/unc/smirnov.txt -v 2 -c unc -o photon_photon
#
# # python3 Smirnov.py -in smirnovs_txt/con/smirnov_ee_ee.txt -v 2 -c con -o ee_ee
#

# python3 Smirnov.py -in smirnovs_txt/con/smirnov_ee_ee_both_pass.txt -v 2 -c con -o ee_ee/tag_probe_both_pass_probe
# # python3 Smirnov.py -in smirnovs_txt/con/smirnov_ee_ee_probe_probe.txt -v 2 -c con -o ee_ee/probe_probe
# python3 Smirnov.py -in smirnovs_txt/con/smirnov_ee_ee_tag_probe.txt -v 1 -c con -o ee_ee/tag_probe


# python3 Smirnov.py -in smirnovs_txt/con/smirnov_tag_probe.txt -v 2 -c con -o tag_probe
# python3 Smirnov.py -in smirnovs_txt/unc/smirnov_tag_probe.txt -v 2 -c unc -o tag_probe


# python3 Smirnov.py -in smirnovs_txt/con/smirnov_both_pass.txt -v 2 -c con -o tag_probe_both_pass_probe
# python3 Smirnov.py -in smirnovs_txt/unc/smirnov_both_pass.txt -v 2 -c unc -o tag_probe_both_pass_probe


# python3 Smirnov.py -in smirnovs_txt/con/smirnov_probe_probe.txt -v 2 -c con -o probe_probe
# python3 Smirnov.py -in smirnovs_txt/unc/smirnov_probe_probe.txt -v 2 -c unc -o probe_probe



# python3 Smirnov.py -in smirnovs_txt/con/smirnov_both_pass_r2.txt -v 2 -c con -o tag_probe_both_pass_probe_2
# python3 Smirnov.py -in smirnovs_txt/unc/smirnov_both_pass_r2.txt -v 2 -c unc -o tag_probe_both_pass_probe_2

#-----------------------------------------

# python3 Smirnov.py -in smirnovs_txt/unc/smirnov_both_pass_change_bin.txt -v 2 -c unc -o tag_probe_both_pass_probe_change_bin
# python3 Smirnov.py -in smirnovs_txt/con/smirnov_both_pass_change_bin.txt -v 2 -c con -o tag_probe_both_pass_probe_change_bin



#-------------------------------------------

# python3 Smirnov.py -in smirnovs_txt/con/change_range/smirnov_tag_probe_change.txt -v 2 -c con -o tag_probe/change_range
# python3 Smirnov.py -in smirnovs_txt/unc/change_range/smirnov_tag_probe_change.txt -v 2 -c unc -o tag_probe/change_range
#
#
# python3 Smirnov.py -in smirnovs_txt/con/change_range/smirnov_both_pass_change.txt -v 2 -c con -o tag_probe_both_pass_probe/change_range
# python3 Smirnov.py -in smirnovs_txt/unc/change_range/smirnov_both_pass_change.txt -v 2 -c unc -o tag_probe_both_pass_probe/change_range
#
#
# python3 Smirnov.py -in smirnovs_txt/con/change_range/smirnov_probe_probe_change.txt -v 2 -c con -o probe_probe/change_range
# python3 Smirnov.py -in smirnovs_txt/unc/change_range/smirnov_probe_probe_change.txt -v 2 -c unc -o probe_probe/change_range

#--------------------------------------------

# python3 Smirnov.py -in smirnovs_txt/con/change_range/final/smirnov_both_pass_change.txt -v 2 -c con -o final
# python3 Smirnov.py -in smirnovs_txt/unc/change_range/final/smirnov_both_pass_change.txt -v 2 -c unc -o final
#---------------------------------------
# python3 Smirnov.py -in smirnovs_txt/con/Run3/smirnov_txt.txt -v 2 -c con -o Run3
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/smirnov_txt.txt -v 2 -c unc -o Run3

#-------------------------------------------

# python3 Smirnov.py -in smirnovs_txt/con/Run3_ff_ligang/smirnov_txt.txt -v 2 -c con -o Run3_ff_ligang
# python3 Smirnov.py -in smirnovs_txt/unc/Run3_ff_ligang/smirnov_txt.txt -v 2 -c unc -o Run3_ff_ligang

#---------------------------------
# python3 Smirnov.py -in smirnovs_txt/con/Run3/ff_with_pileweight/smirnov_txt.txt -v 2 -c con -o Run3/ff_with_pileweight
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/ff_with_pileweight/smirnov_txt.txt -v 2 -c unc -o Run3/ff_with_pileweight
#---------------------------------
# python3 Smirnov.py -in smirnovs_txt/con/Run3/ff_1_2/smirnov_txt.txt -v 1 -c con -o Run3/ff_1_2
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/ff_1_2/smirnov_txt.txt -v 2 -c unc -o Run3/ff_1_2

# python3 Smirnov.py -in smirnovs_txt/con/Run3/ff_1_2_bin/smirnov_txt.txt -v 1 -c con -o Run3/ff_1_2_bin
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/ff_1_2_bin/smirnov_txt.txt -v 2 -c unc -o Run3/ff_1_2_bin

# python3 Smirnov.py -in smirnovs_txt/con/Run3/ff_1_2_bin_final/smirnov_txt.txt -v 1 -c con -o Run3/ff_1_2_bin_final
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/ff_1_2_bin_final/smirnov_txt.txt -v 2 -c unc -o Run3/ff_1_2_bin_final

# python3 Smirnov.py -in smirnovs_txt/con/Run3/ff_1_2_not/smirnov_txt.txt -v 1 -c con -o Run3/ff_1_2_not
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/ff_1_2_not/smirnov_txt.txt -v 2 -c unc -o Run3/ff_1_2_not

#-----------------------------nominal ---------------
# python3 Smirnov.py -in smirnovs_txt/con/Run3/nominal/smirnov_txt.txt -v 1 -c con -o Run3/nominal
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/nominal/smirnov_txt.txt -v 2 -c unc -o Run3/nominal

#--------------------------------------------------------
# python3 Smirnov.py -in smirnovs_txt/con/Run3/frag_down/smirnov_txt.txt -v 1 -c con -o Run3/frag_down
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/frag_down/smirnov_txt.txt -v 2 -c unc -o Run3/frag_down
# # #
# python3 Smirnov.py -in smirnovs_txt/con/Run3/frag_up/smirnov_txt.txt -v 1 -c con -o Run3/frag_up
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/frag_up/smirnov_txt.txt -v 2 -c unc -o Run3/frag_up




# python3 Smirnov.py -in smirnovs_txt/con/Run3/Run3_photon_add_add/smirnov_txt.txt -v 1 -c con -o Run3/Run3_photon_add_add/truth_pt
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/Run3_photon_add_add/smirnov_txt.txt -v 2 -c unc -o Run3/Run3_photon_add_add/truth_pt
# #
# python3 Smirnov.py -in smirnovs_txt/con/Run3/Run3_photon_add_substract/smirnov_txt.txt -v 1 -c con -o Run3/Run3_photon_add_substract/truth_pt
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/Run3_photon_add_substract/smirnov_txt.txt -v 2 -c unc -o Run3/Run3_photon_add_substract/truth_pt

# # #
# python3 Smirnov.py -in smirnovs_txt/con/Run3/Run3_photon_substract_substract/smirnov_txt.txt -v 1 -c con -o Run3/Run3_photon_substract_substract/truth_pt
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/Run3_photon_substract_substract/smirnov_txt.txt -v 2 -c unc -o Run3/Run3_photon_substract_substract/truth_pt
# # # #
# python3 Smirnov.py -in smirnovs_txt/con/Run3/Run3_photon_substract_add/smirnov_txt.txt -v 1 -c con -o Run3/Run3_photon_substract_add/truth_pt
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/Run3_photon_substract_add/smirnov_txt.txt -v 2 -c unc -o Run3/Run3_photon_substract_add/truth_pt




# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_had1_con_add_add_smirnov_txt.txt -v 1 -c con -o Run3/four_group/add_add/r_had1_r_had0
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_had1_unc_add_add_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/add_add/r_had1_r_had0
#
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_eta_con_add_add_smirnov_txt.txt -v 1 -c con -o Run3/four_group/add_add/r_eta_w_eta2
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_eta_unc_add_add_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/add_add/r_eta_w_eta2
#
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_phi_con_add_add_smirnov_txt.txt -v 1 -c con -o Run3/four_group/add_add/r_phi
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_phi_unc_add_add_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/add_add/r_phi
#
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/fracs1_con_add_add_smirnov_txt.txt -v 1 -c con -o Run3/four_group/add_add/w_s3_Fside_wstot
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/fracs1_unc_add_add_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/add_add/w_s3_Fside_wstot

# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_had1_con_add_substract_smirnov_txt.txt -v 1 -c con -o Run3/four_group/add_substract/r_had1_r_had0
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_had1_unc_add_substract_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/add_substract/r_had1_r_had0
#
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_eta_con_add_substract_smirnov_txt.txt -v 1 -c con -o Run3/four_group/add_substract/r_eta_w_eta2
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_eta_unc_add_substract_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/add_substract/r_eta_w_eta2
#
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_phi_con_add_substract_smirnov_txt.txt -v 1 -c con -o Run3/four_group/add_substract/r_phi
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_phi_unc_add_substract_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/add_substract/r_phi
#
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/fracs1_con_add_substract_smirnov_txt.txt -v 1 -c con -o Run3/four_group/add_substract/w_s3_Fside_wstot
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/fracs1_unc_add_substract_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/add_substract/w_s3_Fside_wstot
#
#
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_had1_con_substract_add_smirnov_txt.txt -v 1 -c con -o Run3/four_group/substract_add/r_had1_r_had0
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_had1_unc_substract_add_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/substract_add/r_had1_r_had0
#
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_eta_con_substract_add_smirnov_txt.txt -v 1 -c con -o Run3/four_group/substract_add/r_eta_w_eta2
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_eta_unc_substract_add_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/substract_add/r_eta_w_eta2
#
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_phi_con_substract_add_smirnov_txt.txt -v 1 -c con -o Run3/four_group/substract_add/r_phi
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_phi_unc_substract_add_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/substract_add/r_phi
#
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/fracs1_con_substract_add_smirnov_txt.txt -v 1 -c con -o Run3/four_group/substract_add/w_s3_Fside_wstot
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/fracs1_unc_substract_add_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/substract_add/w_s3_Fside_wstot
#
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_had1_con_substract_substract_smirnov_txt.txt -v 1 -c con -o Run3/four_group/substract_substract/r_had1_r_had0
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_had1_unc_substract_substract_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/substract_substract/r_had1_r_had0

# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_eta_con_substract_substract_smirnov_txt.txt -v 1 -c con -o Run3/four_group/substract_substract/r_eta_w_eta2
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_eta_unc_substract_substract_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/substract_substract/r_eta_w_eta2

# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_phi_con_substract_substract_smirnov_txt.txt -v 1 -c con -o Run3/four_group/substract_substract/r_phi
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/r_phi_unc_substract_substract_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/substract_substract/r_phi

# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/fracs1_con_substract_substract_smirnov_txt.txt -v 1 -c con -o Run3/four_group/substract_substract/w_s3_Fside_wstot
# python3 Smirnov.py -in four_fudge_factor/smirnocs_txt/fracs1_unc_substract_substract_smirnov_txt.txt -v 2 -c unc -o Run3/four_group/substract_substract/w_s3_Fside_wstot
#------------------------------ statistic mc ---------------------
# for i in {17..30}
# for i in {41..60}
# do
#     python3 Smirnov_statistic.py -in smirnovs_txt/con/Run3/Run3_photon/smirnov_txt.txt -v 1 -c con -o Run3/Run3_photon_statistic -t ${i}
#     python3 Smirnov_statistic.py -in smirnovs_txt/unc/Run3/Run3_photon/smirnov_txt.txt -v 2 -c unc -o Run3/Run3_photon_statistic -t ${i}
# done

# for i in {31..33}
# for i in {34..36}
# for i in {61..65}
# for i in {66..70}
# for i in {71..75}
# for i in {76..77}
# do
#     python3 Smirnov_statistic.py -in smirnovs_txt/con/Run3/Run3_photon/smirnov_txt.txt -v 1 -c con -o Run3/Run3_photon_statistic -t ${i} &
#     python3 Smirnov_statistic.py -in smirnovs_txt/unc/Run3/Run3_photon/smirnov_txt.txt -v 2 -c unc -o Run3/Run3_photon_statistic -t ${i} &
# done
#
# wait
# echo "76-77 are done"
#
# for i in {78..79}
# do
#     python3 Smirnov_statistic.py -in smirnovs_txt/con/Run3/Run3_photon/smirnov_txt.txt -v 1 -c con -o Run3/Run3_photon_statistic -t ${i} &
#     python3 Smirnov_statistic.py -in smirnovs_txt/unc/Run3/Run3_photon/smirnov_txt.txt -v 2 -c unc -o Run3/Run3_photon_statistic -t ${i} &
# done

# python3 Smirnov_statistic.py -in smirnovs_txt/con/Run3/Run3_photon/smirnov_txt.txt -v 1 -c con -o Run3/Run3_photon_statistic/truth -t 100

for j in {0..20}
# for j in {0..1}
do
	num1=$((j*5+95))
	# num1=$((j*5))
	num2=$((num1+5))
	echo $num1 $num2

	for ((i=num1; i<num2; i++))
	do
		python3 Smirnov_statistic.py -in smirnovs_txt/con/Run3/Run3_photon/smirnov_txt.txt -v 1 -c con -o Run3/Run3_photon_statistic/truth_new_conversion -t ${i} &
		python3 Smirnov_statistic.py -in smirnovs_txt/unc/Run3/Run3_photon/smirnov_txt.txt -v 2 -c unc -o Run3/Run3_photon_statistic/truth_new_conversion -t ${i} &
		echo $i
	done
	wait
done
#
#---------------------------conversion ----------------------

# # python3 Smirnov.py -in smirnovs_txt/unc/Run3/Run3_conversion_up/smirnov_txt.txt -v 2 -c unc -o Run3/Run3_conversion_up

# python3 Smirnov.py -in smirnovs_txt/con/Run3/Run3_conversion_up/smirnov_txt.txt -v 1 -c con -o Run3/truth_pt/conversion_up
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/Run3_conversion_up/smirnov_txt.txt -v 2 -c unc -o Run3/truth_pt/conversion_up

# python3 Smirnov.py -in smirnovs_txt/con/Run3/Run3_conversion_down/smirnov_txt.txt -v 1 -c con -o Run3/truth_pt/conversion_down
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/Run3_conversion_down/smirnov_txt.txt -v 2 -c unc -o Run3/truth_pt/conversion_down

#----------------------just has hard no fragmentation -----------
# python3 Smirnov.py -in smirnovs_txt/con/Run3/just_hard/smirnov_txt.txt -v 1 -c con -o Run3/just_hard
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/just_hard/smirnov_txt.txt -v 2 -c unc -o Run3/just_hard


#-------------truth_pt------------------
# python3 Smirnov.py -in smirnovs_txt/con/Run3/truth_pt/smirnov_txt.txt -v 1 -c con -o Run3/truth_pt
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/truth_pt/smirnov_txt.txt -v 2 -c unc -o Run3/truth_pt

#-------------truth_pt      new_pt------------------
# python3 Smirnov.py -in smirnovs_txt/con/Run3/truth_pt/new_pt/smirnov_txt.txt -v 1 -c con -o Run3/truth_pt/new_pt
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/truth_pt/new_pt/smirnov_txt.txt -v 2 -c unc -o Run3/truth_pt/new_pt
#
#-----------truth_pt new_conversion-----------
# python3 Smirnov.py -in smirnovs_txt/con/Run3/truth_pt/new_conversion/smirnov_txt.txt -v 1 -c con -o Run3/truth_pt/new_conversion
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/truth_pt/new_conversion/smirnov_txt.txt -v 2 -c unc -o Run3/truth_pt/new_conversion

# python3 Smirnov.py -in smirnovs_txt/con/Run3/truth_pt/just_match/smirnov_txt.txt -v 1 -c con -o Run3/truth_pt/just_match
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/truth_pt/just_match/smirnov_txt.txt -v 2 -c unc -o Run3/truth_pt/just_match
#------------truth_pt new_conversion fudge factor------------------
# for i in fracs1 r_eta r_had1 r_phi
# do
#     for ii in con unc
#     do
#         for iii in add_add add_substract substract_add substract_substract
#         do
#             python3 Smirnov.py -in four_fudge_factor/new_conversion/smirnocs_txt/${i}_${ii}_${iii}_smirnov_txt.txt -v 1 -c ${ii} -o Run3/four_group/${iii}/${i} &
#         done
#     done
# done
#--------------truth_pt new_covnersion conversion_up_down--------

# python3 Smirnov.py -in smirnovs_txt/con/Run3/Run3_conversion_up/smirnov_txt.txt -v 1 -c con -o Run3/truth_pt/conversion_up/new_conversion
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/Run3_conversion_up/smirnov_txt.txt -v 2 -c unc -o Run3/truth_pt/conversion_up/new_conversion
# #
# python3 Smirnov.py -in smirnovs_txt/con/Run3/Run3_conversion_down/smirnov_txt.txt -v 1 -c con -o Run3/truth_pt/conversion_down/new_conversion
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/Run3_conversion_down/smirnov_txt.txt -v 2 -c unc -o Run3/truth_pt/conversion_down/new_conversion
# #
# #
# python3 Smirnov.py -in smirnovs_txt/con/Run3/truth_pt/fragmentation/smirnov_txt_down.txt -v 1 -c con -o Run3/truth_pt/frag_down/new_conversion
# python3 Smirnov.py -in smirnovs_txt/con/Run3/truth_pt/fragmentation/smirnov_txt_up.txt -v 1 -c con -o Run3/truth_pt/frag_up/new_conversion
# #
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/truth_pt/fragmentation/smirnov_txt_down.txt -v 2 -c unc -o Run3/truth_pt/frag_down/new_conversion
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/truth_pt/fragmentation/smirnov_txt_up.txt -v 2 -c unc -o Run3/truth_pt/frag_up/new_conversion

#---------------run2-run3 --- frag-frag-----
# python3 Smirnov.py -in smirnovs_txt/con/Run3/2_3_hard_frag/3_2_frag.txt -v 1 -c con -o Run3/frag_frag
# python3 Smirnov.py -in smirnovs_txt/con/Run3/2_3_hard_frag/3_2_hard.txt -v 1 -c con -o Run3/hard_hard
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/2_3_hard_frag/3_2_frag.txt -v 2 -c unc -o Run3/frag_frag
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/2_3_hard_frag/3_2_hard.txt -v 2 -c unc -o Run3/hard_hard

# python3 Smirnov.py -in smirnovs_txt/con/Run3/2_3_hard_frag/3_2_frag_truth_pt.txt -v 1 -c con -o Run3/frag_frag
# python3 Smirnov.py -in smirnovs_txt/con/Run3/2_3_hard_frag/3_2_hard_truth_pt.txt -v 1 -c con -o Run3/hard_hard
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/2_3_hard_frag/3_2_frag_truth.txt -v 2 -c unc -o Run3/frag_frag
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/2_3_hard_frag/3_2_hard_truth.txt -v 2 -c unc -o Run3/hard_hard

#-----------------fragmentation truth_pt----------
# python3 Smirnov.py -in smirnovs_txt/con/Run3/truth_pt/fragmentation/smirnov_txt_down.txt -v 1 -c con -o Run3/truth_pt/frag_down
# python3 Smirnov.py -in smirnovs_txt/con/Run3/truth_pt/fragmentation/smirnov_txt_up.txt -v 1 -c con -o Run3/truth_pt/frag_up
#
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/truth_pt/fragmentation/smirnov_txt_down.txt -v 2 -c unc -o Run3/truth_pt/frag_down
# python3 Smirnov.py -in smirnovs_txt/unc/Run3/truth_pt/fragmentation/smirnov_txt_up.txt -v 2 -c unc -o Run3/truth_pt/frag_up
