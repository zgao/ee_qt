import ROOT

out_file = ROOT.TFile("out_put_file.root","RECREATE")
out_tree = ROOT.TTree("tree_name","new")

#  var1 = ROOT.Float()
var1 = ROOT.Double()
#  var2 = ROOT.Float()
var2 = ROOT.Double()

out_tree.Branch("branch1", var1,"branch1/D")
out_tree.Branch("branch2", var2,"branch2/D")


var1 = 3.14
var2 = 2.13
out_tree.Fill()

out_file.Write()
out_file.Close()
