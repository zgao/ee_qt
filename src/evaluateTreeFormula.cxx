#include <TFile.h>
#include <TTree.h>
#include <TTreeFormula.h>

void evaluateTreeFormula() {
    // Open a ROOT file and get the TTree from it
    TFile file("/afs/cern.ch/user/z/zgao/eos/con1_fudge/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230707_110900_ANALYSIS.root_7_9/user.zgao.33986780._000055.ANALYSIS.root");
    TTree* tree = dynamic_cast<TTree*>(file.Get("ZeeCandidates"));

    // Create a TTreeFormula to evaluate an expression involving the tree's branches
    TTreeFormula formula("my_formula", "sqrt(pt*pt + e*e)", tree);
    TTreeFormula formula2("my_formula2", "pt>1 && e<40", tree);

    // Loop over the entries in the TTree and evaluate the formula for each entry
    // for (Long64_t entry = 0; entry < tree->GetEntries(); ++entry) {
	int values[10]={};
    for (Long64_t entry = 0; entry < 10; ++entry) {
        tree->GetEntry(entry);
        double result = formula.EvalInstanceLD();
        int  result2  = formula2.EvalInstanceLD();
        // Do something with the evaluated result
		cout<<result<<endl;
		
		// values[entry]= result2;
		cout<<result2<<endl;
	}
	// cout<<values<<endl;
}

