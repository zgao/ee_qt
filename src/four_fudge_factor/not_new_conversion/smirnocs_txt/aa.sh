#!/bin/bash
# touch r_had1_con_add_add_smirnov_txt.txt
# touch r_had1_con_add_substract_smirnov_txt.txt
# touch r_had1_con_substract_add_smirnov_txt.txt
# touch r_had1_con_substract_substract_smirnov_txt.txt
# touch r_had1_unc_add_add_smirnov_txt.txt
# touch r_had1_unc_add_substract_smirnov_txt.txt
# touch r_had1_unc_substract_add_smirnov_txt.txt
# touch r_had1_unc_substract_substract_smirnov_txt.txt

cp r_had1_con_add_add_smirnov_txt.txt                  r_phi_con_add_add_smirnov_txt.txt
cp r_had1_con_add_substract_smirnov_txt.txt            r_phi_con_add_substract_smirnov_txt.txt
cp r_had1_con_substract_add_smirnov_txt.txt            r_phi_con_substract_add_smirnov_txt.txt
cp r_had1_con_substract_substract_smirnov_txt.txt      r_phi_con_substract_substract_smirnov_txt.txt
cp r_had1_unc_add_add_smirnov_txt.txt                  r_phi_unc_add_add_smirnov_txt.txt
cp r_had1_unc_add_substract_smirnov_txt.txt            r_phi_unc_add_substract_smirnov_txt.txt
cp r_had1_unc_substract_add_smirnov_txt.txt            r_phi_unc_substract_add_smirnov_txt.txt
cp r_had1_unc_substract_substract_smirnov_txt.txt      r_phi_unc_substract_substract_smirnov_txt.txt

cp r_had1_con_add_add_smirnov_txt.txt                  fracs1_con_add_add_smirnov_txt.txt
cp r_had1_con_add_substract_smirnov_txt.txt            fracs1_con_add_substract_smirnov_txt.txt
cp r_had1_con_substract_add_smirnov_txt.txt            fracs1_con_substract_add_smirnov_txt.txt
cp r_had1_con_substract_substract_smirnov_txt.txt      fracs1_con_substract_substract_smirnov_txt.txt
cp r_had1_unc_add_add_smirnov_txt.txt                  fracs1_unc_add_add_smirnov_txt.txt
cp r_had1_unc_add_substract_smirnov_txt.txt            fracs1_unc_add_substract_smirnov_txt.txt
cp r_had1_unc_substract_add_smirnov_txt.txt            fracs1_unc_substract_add_smirnov_txt.txt
cp r_had1_unc_substract_substract_smirnov_txt.txt      fracs1_unc_substract_substract_smirnov_txt.txt

cp r_had1_con_add_add_smirnov_txt.txt                  r_eta_con_add_add_smirnov_txt.txt
cp r_had1_con_add_substract_smirnov_txt.txt            r_eta_con_add_substract_smirnov_txt.txt
cp r_had1_con_substract_add_smirnov_txt.txt            r_eta_con_substract_add_smirnov_txt.txt
cp r_had1_con_substract_substract_smirnov_txt.txt      r_eta_con_substract_substract_smirnov_txt.txt
cp r_had1_unc_add_add_smirnov_txt.txt                  r_eta_unc_add_add_smirnov_txt.txt
cp r_had1_unc_add_substract_smirnov_txt.txt            r_eta_unc_add_substract_smirnov_txt.txt
cp r_had1_unc_substract_add_smirnov_txt.txt            r_eta_unc_substract_add_smirnov_txt.txt
cp r_had1_unc_substract_substract_smirnov_txt.txt      r_eta_unc_substract_substract_smirnov_txt.txt
