#!/bin/bash

root -l -q -b 'replaceHistograms.C("r_had1,r_had0", "merge_enrique.root", "add_add.root", "./add_add/output_r_had.root")'
root -l -q -b 'replaceHistograms.C("r_phi", "merge_enrique.root", "add_add.root", "./add_add/output_r_phi.root")'
root -l -q -b 'replaceHistograms.C("r_eta,w_eta2", "merge_enrique.root", "add_add.root", "./add_add/output_r_eta_w_eta2.root")'
root -l -q -b 'replaceHistograms.C("w_eta1,fracs1,w_stot", "merge_enrique.root", "add_add.root", "./add_add/output_w_eta1_fracs1_w_stot.root")'

root -l -q -b 'replaceHistograms.C("r_had1,r_had0", "merge_enrique.root", "add_substract.root", "./add_substract/output_r_had.root")'
root -l -q -b 'replaceHistograms.C("r_phi", "merge_enrique.root", "add_substract.root", "./add_substract/output_r_phi.root")'
root -l -q -b 'replaceHistograms.C("r_eta,w_eta2", "merge_enrique.root", "add_substract.root", "./add_substract/output_r_eta_w_eta2.root")'
root -l -q -b 'replaceHistograms.C("w_eta1,fracs1,w_stot", "merge_enrique.root", "add_substract.root", "./add_substract/output_w_eta1_fracs1_w_stot.root")'

root -l -q -b 'replaceHistograms.C("r_had1,r_had0", "merge_enrique.root", "substract_substract.root", "./substract_substract/output_r_had.root")'
root -l -q -b 'replaceHistograms.C("r_phi", "merge_enrique.root", "substract_substract.root", "./substract_substract/output_r_phi.root")'
root -l -q -b 'replaceHistograms.C("r_eta,w_eta2", "merge_enrique.root", "substract_substract.root", "./substract_substract/output_r_eta_w_eta2.root")'
root -l -q -b 'replaceHistograms.C("w_eta1,fracs1,w_stot", "merge_enrique.root", "substract_substract.root", "./substract_substract/output_w_eta1_fracs1_w_stot.root")'

root -l -q -b 'replaceHistograms.C("r_had1,r_had0", "merge_enrique.root", "substract_add.root", "./substract_add/output_r_had.root")'
root -l -q -b 'replaceHistograms.C("r_phi", "merge_enrique.root", "substract_add.root", "./substract_add/output_r_phi.root")'
root -l -q -b 'replaceHistograms.C("r_eta,w_eta2", "merge_enrique.root", "substract_add.root", "./substract_add/output_r_eta_w_eta2.root")'
root -l -q -b 'replaceHistograms.C("w_eta1,fracs1,w_stot", "merge_enrique.root", "substract_add.root", "./substract_add/output_w_eta1_fracs1_w_stot.root")'
