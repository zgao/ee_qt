#!/bin/bash

for i in fracs1 r_eta r_had1 r_phi
do
	# echo $i
	for ii in con unc
	do
		# echo ${i}_${ii}
		for iii in add_add add_substract substract_add substract_substract
		do
			echo /afs/cern.ch/user/z/zgao/eos/ee_qt/src/pho/${ii}/truth_pt/new_conversion/only_match_not_conversion/merge.root > smirnocs_txt/${i}_${ii}_${iii}_smirnov_txt.txt
			echo /afs/cern.ch/user/z/zgao/eos/ee_qt/src/four_fudge_factor/new_conversion/${iii}/output_${i}.root >> smirnocs_txt/${i}_${ii}_${iii}_smirnov_txt.txt
			echo ${i}_${ii}_${iii}_smirnov_txt.txt
		done
	done

done
