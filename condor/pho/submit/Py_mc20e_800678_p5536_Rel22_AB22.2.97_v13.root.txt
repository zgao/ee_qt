executable              = Py_mc20e_800678_p5536_Rel22_AB22.2.97_v13.root.sh
universe                = vanilla
log                     = /afs/cern.ch/user/z/zgao/condor/xjun/photon/con/Py_mc20e_800678_p5536_Rel22_AB22.2.97_v13.root.log
output                  = /afs/cern.ch/user/z/zgao/condor/xjun/photon/con/Py_mc20e_800678_p5536_Rel22_AB22.2.97_v13.root.out
error                   = /afs/cern.ch/user/z/zgao/condor/xjun/photon/con/Py_mc20e_800678_p5536_Rel22_AB22.2.97_v13.root.err
+JobFlavour             = "tomorrow"
+MaxRuntime             = 86400
should_transfer_files   = Yes
when_to_transfer_output = ON_EXIT
queue
