executable              = Py_mc20e_800661_p5536_Rel22_AB22.2.97_v13.root_un.sh
universe                = vanilla
log                     = /afs/cern.ch/user/z/zgao/uncdor/xjun/photon/unc/Py_mc20e_800661_p5536_Rel22_AB22.2.97_v13.root_un.log
output                  = /afs/cern.ch/user/z/zgao/uncdor/xjun/photon/unc/Py_mc20e_800661_p5536_Rel22_AB22.2.97_v13.root_un.out
error                   = /afs/cern.ch/user/z/zgao/uncdor/xjun/photon/unc/Py_mc20e_800661_p5536_Rel22_AB22.2.97_v13.root_un.err
+JobFlavour             = "tomorrow"
+MaxRuntime             = 86400
should_transfer_files   = Yes
when_to_transfer_output = ON_EXIT
queue
