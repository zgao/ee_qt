#!/bin/bash

# for filename in $(ls /afs/cern.ch/user/z/zgao/eos/con1_fudge/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230525_121005_hist/*)
# for filename in $(ls /afs/cern.ch/user/z/zgao/eos/con1_fudge/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230702_183533_hist/*)
for filename in $(ls /afs/cern.ch/user/z/zgao/eos/con1_fudge/photon_13/*)
do
	# name2=${filename/\/eos\/atlas\/atlascerngroupdisk\/perf-egamma\/InclusivePhotons\/mc20_gammajet_v05\//}
	# name2=${filename/\/afs\/cern.ch\/user\/z\/zgao\/eos\/con1_fudge\/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230525_121005_ANALYSIS.root\//}
	# name2=${filename/\/afs\/cern.ch\/user\/z\/zgao\/eos\/con1_fudge\/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230525_121005_hist\//}
	# name2=${filename/\/afs\/cern.ch\/user\/z\/zgao\/eos\/con1_fudge\/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230702_183533_hist\//}
	name2=${filename/\/afs\/cern.ch\/user\/z\/zgao\/eos\/con1_fudge\/photon_13\//}
	echo $name2
	# count=1
	content="executable              = ${name2}.sh
universe                = vanilla
log                     = /afs/cern.ch/user/z/zgao/condor/debug/photon/con/${name2}.log
output                  = /afs/cern.ch/user/z/zgao/condor/debug/photon/con/${name2}.out
error                   = /afs/cern.ch/user/z/zgao/condor/debug/photon/con/${name2}.err
+JobFlavour             = \"tomorrow\"
+MaxRuntime             = 86400
should_transfer_files   = Yes
when_to_transfer_output = ON_EXIT
queue" 
	echo "$content" > "submit/${name2}.txt"


	content="executable              = ${name2}_un.sh
universe                = vanilla
log                     = /afs/cern.ch/user/z/zgao/condor/debug/photon/con/${name2}_un.log
output                  = /afs/cern.ch/user/z/zgao/condor/debug/photon/con/${name2}_un.out
error                   = /afs/cern.ch/user/z/zgao/condor/debug/photon/con/${name2}_un.err
+JobFlavour             = \"tomorrow\"
+MaxRuntime             = 86400
should_transfer_files   = Yes
when_to_transfer_output = ON_EXIT
queue" 
	echo "$content" > "submit/${name2}_un.txt"




	# count=$((count+1))

	# echo "$content"
	# echo -e "$content" > "submit/${filename}.txt"
	# echo -e "$content"
	# content="12 \n3"
	# name="${filename}.txt"
	# echo -e "$content" > $name
	# echo -e "$content" > "submit/${name2}.txt"
	# content="a=/afs/cern.ch/user/z/zgao/eos/condor/submit/ele.out\n123"
	# echo -e "$content"
	

done

