#!/bin/bash
# for name in $(ls /afs/cern.ch/user/z/zgao/eos/con1_fudge/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230702_183533_hist/*)
for name in $(ls /afs/cern.ch/user/z/zgao/eos/con1_fudge/photon_13/*)
do
	# echo $name
	name2=${name/\/afs\/cern.ch\/user\/z\/zgao\/eos\/con1_fudge\/photon_13\//}

	filename="${name2}.sh"
	filename1="${name2}_un.sh"

	filename2=${name2/.root/.root.txt}
	filename3=${name2/.root/.root_un.txt}

	echo "#!/bin/bash" > "submit/$filename"
	echo "cp /afs/cern.ch/user/z/zgao/from_kang/test_pho.tar ." >> "submit/$filename"
	echo "tar -xvf test_pho.tar"  >> "submit/$filename"
	echo "cd ElectronExtrapolationToolsPhon " >> "submit/$filename"
	echo "source iminuit_2_18.sh" >> "submit/$filename"
	echo "cd baseTools" >> "submit/$filename"
	echo "make CreateHists" >> "submit/$filename"
	echo "cd run_egamma" >> "submit/$filename"
	echo "cp run_txt/$filename2 toberun.txt" >> "submit/$filename"


	echo "#!/bin/bash" > "submit/$filename1"
	echo "cp /afs/cern.ch/user/z/zgao/from_kang/test_pho.tar ." >> "submit/$filename1"
	echo "tar -xvf test_pho.tar"  >> "submit/$filename1"
	echo "cd ElectronExtrapolationToolsPhon " >> "submit/$filename1"
	echo "source iminuit_2_18.sh" >> "submit/$filename1"
	echo "cd baseTools" >> "submit/$filename1"
	echo "make CreateHists" >> "submit/$filename1"
	echo "cd run_egamma" >> "submit/$filename1"
	echo "cp run_txt/$filename3 toberun.txt" >> "submit/$filename1"

	echo "./../bin/CreateHists FILLHIST gjet_photon convb_fixedCutLoose     \"reset_sample,isprecut\" nom $name2" >> "submit/$filename"
	echo "./../bin/CreateHists FILLHIST gjet_photon unconvb_fixedCutLoose     \"reset_sample,isprecut\" nom $name2" >> "submit/$filename1"

	chmod +x "submit/$filename"
	chmod +x "submit/$filename1"

	# please note that for photon, the only difference between converted photon and unconverted photon is their conv type,
		# so in here the filename2 and filenam3 can be the same.

done
 
echo "#!/bin/bash" > "submit/submit_all.sh"
for i in {58..83}
do
	echo "condor_submit Py_mc20e_8006${i}_p5536_Rel22_AB22.2.97_v13.root.txt " >> "submit/submit_all.sh"
	echo "condor_submit Py_mc20e_8006${i}_p5536_Rel22_AB22.2.97_v13.root_un.txt " >> "submit/submit_all.sh"
done
chmod +x "submit/submit_all.sh"
