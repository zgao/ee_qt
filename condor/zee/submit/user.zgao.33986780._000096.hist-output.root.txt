executable              = user.zgao.33986780._000096.hist-output.root.sh
universe                = vanilla
log                     = /afs/cern.ch/user/z/zgao/condor/xjun/ele/user.zgao.33986780._000096.hist-output.root.log
output                  = /afs/cern.ch/user/z/zgao/condor/xjun/ele/user.zgao.33986780._000096.hist-output.root.out
error                   = /afs/cern.ch/user/z/zgao/condor/xjun/ele/user.zgao.33986780._000096.hist-output.root.err
+JobFlavour             = "tomorrow"
+MaxRuntime             = 86400
should_transfer_files   = Yes
when_to_transfer_output = ON_EXIT
queue
