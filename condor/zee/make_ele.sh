#!/bin/bash
for name in $(ls /afs/cern.ch/user/z/zgao/eos/con1_fudge/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230707_110900_hist/*)
do
	# echo $name
	name2=${name/\/afs\/cern.ch\/user\/z\/zgao\/eos\/con1_fudge\/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230707_110900_hist\//}

	filename="${name2}.sh"
	filename2=${name2/hist-output.root/ANALYSIS.root.txt}
	filename3="${name2}.txt"
	echo "#!/bin/bash" > "submit/$filename"
	echo "python3 GetHists_ee.py -in MC_ee/$filename2 -v 1 -w ee_hist/$filename3" >> "submit/$filename"
	chmod +x "submit/$filename"



	echo "$name" >> "hist_ee/$filename3"
done

for name in $(ls /afs/cern.ch/user/z/zgao/eos/con1_fudge/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230707_110900_ANALYSIS.root_7_9/*)
do
	name2=${name/\/afs\/cern.ch\/user\/z\/zgao\/eos\/con1_fudge\/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230707_110900_ANALYSIS.root_7_9\//}
	filename="${name2}.txt"
	echo "$name" >> "MC_ee/$filename"
done
 
echo "#!/bin/bash" > "submit/submit_all.sh"
for i in {1..9}
do
	echo "condor_submit user.zgao.33986780._00000${i}.hist-output.root.txt " >> "submit/submit_all.sh"
done

for i in {10..99}
do
	echo "condor_submit user.zgao.33986780._0000${i}.hist-output.root.txt " >> "submit/submit_all.sh"
done
for i in {100..108}
do
	echo "condor_submit user.zgao.33986780._000${i}.hist-output.root.txt " >> "submit/submit_all.sh"
done
chmod +x "submit/submit_all.sh"
