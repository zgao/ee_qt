#!/bin/bash

for filename in $(ls /afs/cern.ch/user/z/zgao/eos/con1_fudge/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230707_110900_hist/*) 
do
	name2=${filename/\/afs\/cern.ch\/user\/z\/zgao\/eos\/con1_fudge\/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230707_110900_hist\//}
	echo $name2
	content="executable              = ${name2}.sh
universe                = vanilla
log                     = /afs/cern.ch/user/z/zgao/condor/xjun/ele/${name2}.log
output                  = /afs/cern.ch/user/z/zgao/condor/xjun/ele/${name2}.out
error                   = /afs/cern.ch/user/z/zgao/condor/xjun/ele/${name2}.err
+JobFlavour             = \"tomorrow\"
+MaxRuntime             = 86400
should_transfer_files   = Yes
when_to_transfer_output = ON_EXIT
queue" 
	echo "$content" > "submit/${name2}.txt"
	

done

