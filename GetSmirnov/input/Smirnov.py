import numpy as np
from time import sleep
import ROOT
import argparse
import os
import time

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()

start_time =time.time()


def PrintProgressBar(index , total):
	if (index % 100000 ==0 ):
	#  if (index / 1 == index ):
		print_bar = " [";
		for bar in range(20):
			current_fraction = float(bar) / 20.0
			if (float(index) / float(total) > current_fraction):
				print_bar += "/"
			else:
				print_bar += "."

		print_bar +="]"
		progress = 100. * (float(index) / float(total))
		print(f"{print_bar}{progress: .2f}%", end="\r", flush=True )
		#  sleep(3)

#  for i in range(10):
	#  PrintProgressBar(i,10)

def read_file(input_file):
	try:
		with open(input_file, 'r') as file:
			lines = file.readlines()
		content = []
		for line in lines:
			#  if "/*" in line.strip():
			print(line.strip(),"   ++++")
			line=line.strip()
			if not line.strip():
				continue
			if line.strip().startswith('#') or line.strip().startswith('//') or line.strip().startswith('/*'):
				continue
			content.append(line.strip()) #it only contains one line
			print(line.strip())
			print(content)
	except FileNotFoundError:
		print(f"Error: File '{input_file}] not found.")

	return content


def GetCDF(hist, CDF_name):
	#  hist.Print()
	cdf = hist.Clone(CDF_name)

	cdf.SetBinContent(0,0)
	
	for i in range(1, hist.GetNbinsX()+1):

		integral = hist.Integral(1, i)
		cdf.SetBinContent(i, integral)	
	#  print(hist.Integral()," CDF ")
	#  cdf.Scale(1/hist.Integral())
	return cdf

def CDFtoPDF(cdf1, cdf2, transfername):
	# cdf1 ---> cdf2

	h_transform = cdf1.Clone(transfername)
	h_transform.Reset()
	n1 = cdf1.GetNbinsX()
	n2 = cdf2.GetNbinsX()

	x1 = np.array([cdf1.GetBinCenter(i) for i in range(1, n1 + 1)])
	y1 = np.array([cdf1.GetBinContent(i) for i in range(1, n1 + 1)])
	x2 = np.array([cdf2.GetBinCenter(i) for i in range(1, n2 + 1)])
	y2 = np.array([cdf2.GetBinContent(i) for i in range(1, n2 + 1)])
	#  print(n1)
	#  print(n2)
	#  print(y1[n1-1]," y1 biggest ")
	#  print(y2[n2-1]," y2 biggest ")


	for i in range(n1):
		yt = y1[i]
		ismatch = False


		for j in range(1, n2):
			y2min = y2[j -1]
			y2max = y2[j]

			if yt ==y2min:
				h_transform.SetBinContent(i + 1, x2[j -1])
				ismatch = True

			elif y2min < yt < y2max:
				x2min =x2[j - 1]
				x2max =x2[j]
				m = (y2max- y2min) / (x2max -x2min)
				q = y2min -m *x2min
				xi = (yt - q) / m
				h_transform.SetBinContent(i + 1, xi)
				ismatch = True

			elif yt == y2max:
				h_transform.SetBinContent(i +1, x2[j])
				ismatch = True

		y2min_all = y2[0]
		y2max = y2[n2-1]

		if yt < y2min_all:
			h_transform.SetBinContent(i+1,x2[0])
			print("yt < y2min_all "," yt ",yt,"y2min_all ",y2min_all," i ",i)
		elif yt > y2max:
			print("yt > y2max    ---->      "," yt ",yt," y2max ",y2max," i ",i)
			h_transform.SetBinContent(i+1,x2[i])
			#  h_transform.SetBinContent(i+1,0.1)
		#  if i== 479:
			#  print(y1[479])
			#  print(y1[480])
			#  h_transform.SetBinContent(i+1,x2[j])




		#  if not ismatch and x1[i] > x2[int(n2 /2) ]:
		#  #      h_transform.SetBinContent(i + 1, x1[i])  #y>ymax
		#  #      print("y>ymax ",i,j)
		#      h_transform.SetBinContent(i + 1,0.1)  #y>ymax
        #  #
		#  if not ismatch and x1[i] < x2[int(n2 /2) ]:
		#  #      h_transform.SetBinContent(i + 1, x2[0])  #y<ymin
		#  #      print("y<ymin ",i,j)
		#      h_transform.SetBinContent(i + 1, 0.2)  #y<ymin

	return h_transform

def Smirnov_to_applied(Smirnov_trans,primitive_hist ):
	applied_hist = primitive_hist.Clone("new name")
	
	for i in range(primitive_hist.GetNbinsX()):
		applied_hist.SetBinContent(i+1,0)
		applied_hist.SetBinError(i+1,0)

	for j in range(primitive_hist.GetNbinsX()):
		temp = primitive_hist.GetBinContent(j+1)
		primitive_hist.SetBinContent(j+1, abs(temp))

	abc = 0
	for k in range(50000):
		x = primitive_hist.GetRandom()

		if(primitive_hist.GetName().find("e_ratio") and x >1.0):
			print("bin :",primitive_hist.FindBin(x),"   ",x)
			abc+=1
			#  print(primitive_hist.GetNbinsX(),primitive_hist[0],primitive_hist[(primitive_hist.GetNbinsX())-1])
			#  print("e_ratio >1 ",x)
			#  continue;

		y= Smirnov_trans.Interpolate(x)
		applied_hist.Fill(y)
	print(abc)
	
	return applied_hist
	





parser = argparse.ArgumentParser(description='')

parser.add_argument('-n', '--nominal', help=' nominal without systematic uncertainties')
parser.add_argument('-in','--input',dest='input_file', required=True, help='input file to get Smirnov')
parser.add_argument('-v', '--version',dest='versions', required=True, help='version')
parser.add_argument('-c', '--converted',dest='if_converted',required=True,help='con or uncon( con or unc )')


args = parser.parse_args() 
#  inputfile = "/afs/cern.ch/user/z/zgao/eos/ee_qt/src/data_getSmirnov.txt"

inputfile = args.input_file
files = read_file(inputfile)
#  print(read_file(inputfile))

print(files[0],"  ==========")
chain_Pho = ROOT.TFile(files[0],"READ")


chain_Zee = ROOT.TFile(files[1],"READ")


variables=['r_had1','r_had0','r_eta','r_phi','w_eta2','w_eta1','w_stot','fracs1','delta_E','e_ratio']
#  bin_numbers=[2000,4000,1000,2000,1000,1000,1000,2000,1000,8000,1000]
bin_numbers=[100,200,500,500,500,500,200,500,500,500]
labels=['Rhad1','Rhad0','R_{#eta}','R_{#phi}','W_{#eta2}','Ws3','Wtot','Fside','Delta_{E}','Eratio']
#  low_edge=[-0.05,-0.1,0.10,0.1,0.001,0.1,0.1,0.001,-100,0.]
low_edge=[-0.05,-0.1,0.2,0.2,0.,0.1,0,0,-500,0.]
#  high_edge=[0.2,0.3,1.5,1.2,0.02,1.0,20.0,1.4,7000,1.2]
high_edge=[0.05,0.1,1.2,1.2,0.02,1.0,6,1,3000,1.0]	

four_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.37"]

seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]

twelve_pt_cut_name=['25<P_{t}<30','30<P_{t}<35','35<P_{t}<40','40<P_{t}<45','45<P_{t}<50','50<P_{t}<60','60<P_{t}<80','80<P_{t}<100','100<P_{t}<125','125<P_{t}<150','150<P_{t}<175','175<P_{t}<250']

hists_pho = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]
hists_zee = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]
cdfs_pho = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]
cdfs_zee = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]
cdfs_pho_name = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]
cdfs_zee_name = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]
transforms =[[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]
transforms_name =[[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]
hist_applied = [[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]

legends= [[[[ 0 for i in range(10)] for j in range(7)] for k in range(12)]for l in range(4)]

for i in range(12):
#  for i in range(1):
#  for i in range(1,2):
	for j in range(7):
	#  for j in range(1):
	#  for j in range(6,7):
		for k in range(10):
			#  print(i,j,k)
			#  print(f"hist_{variables[k]}_{seven_eta_cut_name[j]}_{twelve_pt_cut_name[i]}")
			#  print(f"hist_{variables[0]}_{seven_eta_cut_name[0]}_{twelve_pt_cut_name[1]}")
			#  hist_r_had0_0.00<|#eta|<0.60_25<P_{t}<30
			#  hists_pho[0][0][0] =chain_Pho.Get("hist_r_had0_0.00<|#eta|<0.60_25<P_{t}<30")
			#  hists_pho[0][0][0] =chain_Pho.Get("hist_r_had1_0.00<|#eta|<0.60_30<P_{t}<35")
			#  print(hists_pho[0][0][0].GetName())
			#  exit()

			hists_pho[i][j][k] = chain_Pho.Get(f"hist_{variables[k]}_{seven_eta_cut_name[j]}_{twelve_pt_cut_name[i]}")
			hists_pho[i][j][k].SetLineColor(ROOT.kRed)
			hists_pho[i][j][k].SetMarkerColor(ROOT.kRed)
			hists_pho[i][j][k].SetMarkerSize(0.01)
			hists_zee[i][j][k] = chain_Zee.Get(f"hist_{variables[k]}_{seven_eta_cut_name[j]}_{twelve_pt_cut_name[i]}")
			hists_zee[i][j][k].SetLineColor(ROOT.kBlack)
			hists_zee[i][j][k].SetMarkerColor(ROOT.kBlack)
			hists_zee[i][j][k].SetMarkerSize(0.01)

			cdfs_pho_name[i][j][k] = f"cdf_pho_{variables[k]}_{seven_eta_cut_name[j]}_{twelve_pt_cut_name[i]}"

			cdfs_zee_name[i][j][k] = f"cdf_zee_{variables[k]}_{seven_eta_cut_name[j]}_{twelve_pt_cut_name[i]}"

			#  print(hists_pho[i][j][k].Integral())
			#  print(hists_pho[i][j][k])
			#  print(hists_pho[i][j][k].Integral())

			#  hists_pho[i][j][k].Print()

			hists_pho[i][j][k].Scale(1/hists_pho[i][j][k].Integral())
			cdfs_pho[i][j][k] = GetCDF(hists_pho[i][j][k],cdfs_pho_name[i][j][k])
			cdfs_pho[i][j][k].SetLineColor(ROOT.kRed)
			cdfs_pho[i][j][k].SetMarkerColor(ROOT.kRed)
			cdfs_pho[i][j][k].SetMarkerSize(0.01)

			#  print(i,j,k,hists_zee[i][j][k].GetName())
			hists_zee[i][j][k].Scale(1/hists_zee[i][j][k].Integral())
			cdfs_zee[i][j][k] = GetCDF(hists_zee[i][j][k],cdfs_zee_name[i][j][k])
			cdfs_zee[i][j][k].SetLineColor(ROOT.kBlack)
			cdfs_zee[i][j][k].SetMarkerColor(ROOT.kBlack)
			cdfs_zee[i][j][k].SetMarkerSize(0.01)

			transforms_name[i][j][k] = f"Smirnov_{variables[k]}_{seven_eta_cut_name[j]}_{twelve_pt_cut_name[i]}"
			transforms[i][j][k] = CDFtoPDF(cdfs_zee[i][j][k], cdfs_pho[i][j][k], transforms_name[i][j][k])

			hist_applied[i][j][k] = Smirnov_to_applied(transforms[i][j][k],hists_zee[i][j][k])
	

c1=ROOT.TCanvas("c1","",800,600)
c1.Print("pictures.pdf"+"[")
#  pdf = ROOT.TPDF("pictures.pdf",2)
#  c1.Print("pictures.pdf"+"[")

for i in range(12):
#  for i in range(1):
#  for i in range(1,2):
	for j in range(7):
	#  for j in range(6,7):
		for k in range(10):
			#  print(i,j,k)
			#  print(hists_pho[i][j][k].GetName())
			c1.Clear()
			c1.Divide(2,2)
			c1.cd(1)
			hists_pho[i][j][k].Draw()
			hists_pho[i][j][k].SetTitle(f"{seven_eta_cut_name[j]}_{twelve_pt_cut_name[i]}")
			hists_pho[i][j][k].GetXaxis().SetTitle(variables[k])
			hists_pho[i][j][k].GetYaxis().SetTitle("A.U.")
			hists_zee[i][j][k].Draw("same")

			legends[0][i][j][k] = ROOT.TLegend(0.7,0.9,0.7,0.9)
			legends[0][i][j][k].AddEntry(hists_pho[i][j][k]," photon_pdf ","l")
			legends[0][i][j][k].AddEntry(hists_zee[i][j][k]," electron_pdf ","l")
			legends[0][i][j][k].Draw("same")

			c1.cd(2)
			cdfs_pho[i][j][k].GetXaxis().SetTitle(variables[k])
			cdfs_pho[i][j][k].GetYaxis().SetTitle("CDF")
			#  cdfs_pho[i][j][k].SetTitle(f"{seven_eta_cut_name[j]}_{twelve_pt_cut_name[i]}")
			cdfs_pho[i][j][k].Draw("h")
			cdfs_zee[i][j][k].Draw("h,same")
			
			legends[1][i][j][k]= ROOT.TLegend(0.7,0.9,0.7,0.9)
			legends[1][i][j][k].AddEntry(cdfs_pho[i][j][k]," photon_cdf ","l")
			legends[1][i][j][k].AddEntry(cdfs_zee[i][j][k]," electron_cdf ","l")
			legends[1][i][j][k].Draw()


			c1.cd(3)
			transforms[i][j][k].Draw("h")
			transforms[i][j][k].GetXaxis().SetTitle(variables[k])

			#  legends[2][i][j][k]= ROOT.TLegend(0.7,0.9,0.7,0.9)
			#  legends[2][i][j][k].AddEntry(transforms[i][j][k]," Smirnov_trans ","l")
			#  legends[2][i][j][k].Draw("same")

			c1.cd(4)
			hist_applied[i][j][k].Draw()
			hist_applied[i][j][k].GetXaxis().SetTitle(variables[k])

			#  legends[3][i][j][k]= ROOT.TLegend(0.7,0.9,0.7,0.9)
			#  legends[3][i][j][k].AddEntry(hist_applied[i][j][k]," electron transformed ","l")
			#  legends[3][i][j][k].Draw("same")

			c1.Update()

			#  pdf.NewPage()
			c1.Print("pictures.pdf")

c1.Print("pictures.pdf"+"]")
#  pdf.Close()
#  print("pdf is saved")

smirnov_file = ROOT.TFile("smirnov_transformation.root","RECREATE")

for i in range(12):
#  for i in range(1):
#  for i in range(1,2):
	for j in range(7):
	#  for j in range(1):
	#  for j in range(6,7):
		for k in range(10):
			transforms[i][j][k].Write()
smirnov_file.Close()
print("smirnov_transformation.root has benn created!")




end_time= time.time()
elapsed_time =end_time-start_time
print(f"Script execution time: {elapsed_time:.2f} seconds")
