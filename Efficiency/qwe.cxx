#include <iostream>
#include <string>

int qwe() {
    std::string line = "CutHadLeakage_photonsConverted: 0.0300 ; 0.0300 ; 0.0354 ; 0.0280 ; 9999 ; 0.0463 ; 0.0500 ; 0.0450 ; 9999 ; # 0 - 15 GeV #";

    // Find the position of the last semicolon
    size_t lastSemicolonPos = line.rfind(';');
    if (lastSemicolonPos != std::string::npos) {
        // Extract the substring before the last semicolon
        std::string newLine = line.substr(0, lastSemicolonPos);

        // Update the original line with the modified substring
        line = newLine;

        std::cout << "Modified line: " << line << std::endl;
    } else {
        std::cout << "No semicolon found in the line." << std::endl;
    }

    return 0;
}

