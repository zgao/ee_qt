import numpy as np
import math
import ROOT
import datetime
current_time = datetime.datetime.now()
print("Current time:", current_time)

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()

with open('dataconvIDEF_all_final.txt', 'r') as file:
	lines = file.readlines()
labels = [] 

ratios = []
errors = []

for i,line in enumerate(lines):
	values = line.strip().split('\t')

	if i%13==0:
		labels.append(line.strip())
	elif len(values)>=2:
		first_value = float(values[0])
		second_value = float(values[1])
		if second_value==0:
			ratio=0
			error=0
		else:
			ratio = first_value / second_value
			error = math.sqrt(ratio*(1-ratio)/second_value)
		ratios.append(ratio)
		errors.append(error)


with open('/afs/cern.ch/user/z/zgao/from_kang/ntuple_code/ID_efficiency/pho/MCconvIDEF_all.txt', 'r') as file2:
	lines2 = file2.readlines()
labels2 = []

ratios2 = []
errors2 = []

for i,line2 in enumerate(lines2):
	values2 = line2.strip().split('\t')

	if i%13==0:
		labels2.append(line2.strip())
	elif len(values2)>=2:
		first_value = float(values2[0])
		second_value = float(values2[1])
		if second_value==0:
			ratio2=0
			error2=0
		else:
			ratio2 = first_value / second_value
			error2 = math.sqrt(ratio2*(1-ratio2)/second_value)
		ratios2.append(ratio2)
		errors2.append(error2)


ROOT.gStyle.SetOptStat(0)
list_bin=[25,30,  35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250]
list_graph_x=[27.5,32.5,37.5,42.5,47.5,55,70,90,112.5,137.5,162.5,212.5]
canvas=ROOT.TCanvas("canvas","",800,600)
canvas.Print("converted_11_5.pdf"+"[")


histogram = ROOT.TH1F("histogram","efficiency",12,np.array(list_bin,np.float32))
histogram2 = ROOT.TH1F("histogram2","efficiency",12,np.array(list_bin,np.float32))
histogram3 = ROOT.TH1F("histogram3","ratio",12,np.array(list_bin,np.float32))

for j in range(7):
	for i in range(histogram.GetNbinsX()):
		histogram.SetBinContent(i+1,ratios[int(i+j*12)])
		histogram.SetBinError(i+1,errors[int(i+j*12)])

		histogram2.SetBinContent(i+1,ratios2[int(i+j*12)])
		histogram2.SetBinError(i+1,errors2[int(i+j*12)])

		histogram3.SetBinContent(i+1,histogram.GetBinContent(i+1)/histogram2.GetBinContent(i+1))
		histogram3.SetBinError(i+1,histogram3.GetBinContent(i+1)*math.sqrt((histogram.GetBinError(i+1)/histogram.GetBinContent(i+1))**2+(histogram2.GetBinError(i+1)/histogram2.GetBinContent(i+1))**2))

	canvas.cd()

	pad_top = ROOT.TPad("pad_top", "", 0,0.32,1,1)
	pad_top.Draw()

	pad_bot = ROOT.TPad("pad_bot", "", 0, 0.03,1, 0.29)
	pad_bot.Draw()

	pad_top.cd()
	pad_top.SetTickx(1)
	pad_top.SetTicky(1)
	pad_top.SetLeftMargin(0.12)
	pad_top.SetRightMargin(0.05)
	pad_top.SetTopMargin(0.05)
	pad_top.SetRightMargin(0.03)
	pad_top.SetBottomMargin(0.02)
	pad_top.SetFrameBorderMode(0)
	pad_top.SetFrameBorderMode(0)
	pad_top.SetFillColor(0)
	pad_top.SetBorderMode(0)
	pad_top.SetBorderSize(2)

	#  histogram.GetXaxis().SetTitle("pt[Gev]")
	histogram.GetYaxis().SetRangeUser(0.6,1)
	histogram.GetYaxis().SetTitle("ID efficiency")
	histogram.SetLineColor(ROOT.kBlack)
	#  histogram.SetMarkerSize(0)
	histogram.GetXaxis().SetTitleSize(0)
	histogram.GetXaxis().SetLabelSize(0)
	histogram.GetYaxis().SetTitleOffset(1.0)
	histogram.GetYaxis().CenterTitle()

	histogram2.SetLineColor(ROOT.kBlue)
	histogram2.SetMarkerSize(0)

	histogram.Draw("h")
	histogram2.Draw("h,same")
	#  histogram2.Draw("h,same")
	legend=ROOT.TLegend(0.5,0.6,0.7,0.7)
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	legend.SetTextSize(0.05)
	legend.AddEntry(histogram,"electron data","L")
	legend.AddEntry(histogram2,"MC photon","L")
	legend.Draw("same")

	pt = ROOT.TPaveText(0.5, 0.4, 0.7, 0.55, "ndc")
	pt.SetTextSize(0.05)
	pt.SetBorderSize(0)
	pt.SetFillStyle(0)
	pt.SetTextAlign(13)
	pt.SetTextFont(42)
	pt.AddText(f"converted,{labels[int(j%7)]}")
	pt.Draw("same")

	pad_bot.cd()
	pad_bot.SetTickx(1)
	pad_bot.SetTicky(1)
	pad_bot.SetLeftMargin(0.12)
	pad_bot.SetRightMargin(0.05)
	pad_bot.SetTopMargin(0.05)
	pad_bot.SetRightMargin(0.03)
	pad_bot.SetBottomMargin(0.3)
	pad_bot.SetFrameBorderMode(0)
	pad_bot.SetFrameBorderMode(0)
	pad_bot.SetFillColor(0)
	pad_bot.SetBorderMode(0)
	pad_bot.SetBorderSize(2)

	histogram3.Draw()
	histogram3.GetYaxis().SetNdivisions(202)
	histogram3.GetYaxis().SetRangeUser(0.95,1.05)
	#  histogram3.GetXaxis().SetTitle("pt[Gev]")

	histogram3.GetXaxis().SetTitle("P_{T} [GeV]")
	histogram3.GetYaxis().SetTitle("Scale Factor")
	histogram3.GetYaxis().CenterTitle()
	histogram3.GetXaxis().SetTitleSize(0.17)
	histogram3.GetXaxis().SetTitleOffset(0.9)
	histogram3.GetXaxis().SetLabelSize(0.12)
	histogram3.GetXaxis().SetMoreLogLabels()
	histogram3.GetXaxis().SetNoExponent()
	histogram3.GetYaxis().SetTitleSize(0.12)
	histogram3.GetYaxis().SetTitleOffset(0.4)
	histogram3.GetYaxis().SetLabelSize(0.12)

	line=ROOT.TLine(25,1,250,1)
	line.SetLineWidth(1)
	line.SetLineStyle(2)
	line.SetLineColor(ROOT.kBlue)
	line.Draw()
	canvas.Print("converted_11_5.pdf")
canvas.Print("converted_11_5.pdf"+"]")
