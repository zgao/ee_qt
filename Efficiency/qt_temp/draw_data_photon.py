import numpy as np
import math
import ROOT
import sys
import argparse

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()


ROOT.gStyle.SetOptStat(0)


parser = argparse.ArgumentParser(description='')
parser.add_argument('-conpdf', '--conpdf', dest="conpdf_file", required=True, help='con pdf file')
parser.add_argument('-uncpdf', '--uncpdf', dest="uncpdf_file", required=True, help='unc pdf file')
parser.add_argument('-conroot', '--conroot', dest="conroot_file", required=True, help='con root file')
parser.add_argument('-uncroot', '--uncroot', dest="uncroot_file", required=True, help='unc root file')
args = parser.parse_args()


list_bin=[25,30,  35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250]
list_graph_x=[27.5,32.5,37.5,42.5,47.5,55,70,90,112.5,137.5,162.5,212.5]
list_campaign=["23a","23d"]
seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]
#  seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.37"]



for campaign in list_campaign:
	file1 = ROOT.TFile(f"Efficiency/con_{campaign}.root","READ")
	file3 = ROOT.TFile(f"Efficiency/pho_con_mc_{campaign}.root","READ")
	file5 = ROOT.TFile(f"Efficiency/zee_con_mc_{campaign}.root","READ")

	file2 = ROOT.TFile(f"Efficiency/unc_{campaign}.root","READ")
	file4 = ROOT.TFile(f"Efficiency/pho_unc_mc_{campaign}.root","READ")
	file6 = ROOT.TFile(f"Efficiency/zee_unc_mc_{campaign}.root","READ")



	key_list = file1.GetListOfKeys()

	pdf_name_con = args.conpdf_file + f"_{campaign}.pdf"
	pdf_name_unc = args.uncpdf_file + f"_{campaign}.pdf"
	root_name_con = args.conroot_file + f"_{campaign}.root"
	root_name_unc = args.uncroot_file + f"_{campaign}.root"

	file7 = ROOT.TFile(args.conroot_file,"READ")
	file8 = ROOT.TFile(args.uncroot_file,"READ")
	#  print(args.conroot_file)
	#  print("123")


	canvas=ROOT.TCanvas("canvas","",800,600)
	canvas.Print(pdf_name_con+"[")

	canvas2=ROOT.TCanvas("canvas2","",800,600)
	canvas2.Print(pdf_name_unc+"[")

	for i in range(7):
	#  for i in range(4):
		hist1 = file1.Get(f"histogram;{int(i+1)}")
		hist2 = file2.Get(f"histogram;{int(i+1)}")
		hist3 = file3.Get(f"histogram;{int(i+1)}")
		hist4 = file4.Get(f"histogram;{int(i+1)}")
		hist5 = file5.Get(f"histogram;{int(i+1)}")
		hist6 = file6.Get(f"histogram;{int(i+1)}")

		#  hist7 = file7.Get(f"histogram_{campaign}_con;{int(i+1)}")
		#  hist8 = file8.Get(f"histogram_{campaign}_unc;{int(i+1)}")
		#  print(f"histogram_{campaign}_con;{int(i+1)}")
		#  print("123")
		#  for bins in range(hist1.GetNbinsX()):
		#	   hist1.SetBinError(bins+1, hist7.GetBinContent(bins+1))
		#	   hist4.SetBinError(bins+1, hist8.GetBinContent(bins+1))

		canvas.cd()

		pad_top = ROOT.TPad("pad_top", "", 0, 0.37, 1, 1)
		pad_top.Draw()

		pad_bot = ROOT.TPad("pad_bot", "", 0, 0, 1, 0.36)
		pad_bot.Draw()

		pad_top.cd()
		pad_top.SetFillColor(0)
		pad_top.SetBorderMode(0)
		pad_top.SetBorderSize(2)
		#  pad_top.SetLogx()
		pad_top.SetTickx(1)
		pad_top.SetTicky(1)
		pad_top.SetLeftMargin(0.12)
		pad_top.SetRightMargin(0.05)
		pad_top.SetTopMargin(0.05)
		pad_top.SetRightMargin(0.03)
		pad_top.SetBottomMargin(0.01)
		pad_top.SetFrameBorderMode(0)
		pad_top.SetFrameBorderMode(0)

		hist5.Draw("h,e")
		hist5.SetLineColor(ROOT.kRed)
		hist5.SetMarkerColor(ROOT.kRed)
		hist5.SetMarkerSize(0)
		hist5.GetYaxis().SetTitleOffset(1)
		hist5.GetYaxis().SetTitle("efficiency")
		hist1.Draw("h,same")
		hist1.SetLineColor(ROOT.kBlack)
		hist1.SetMarkerColor(ROOT.kBlack)
		hist1.SetMarkerSize(0)
		#  hist1.GetYaxis().SetTitleOffset(1)
		hist3.Draw("h,same")
		hist3.SetLineColor(ROOT.kBlue)
		hist3.SetMarkerColor(ROOT.kBlue)
		hist3.SetMarkerSize(0)
		legend = ROOT.TLegend(0.6,0.5,0.9,0.7)
		legend.AddEntry(hist3," photon mc ","l")
		legend.AddEntry(hist1," zee data(smirnoved)","l")
		legend.AddEntry(hist5," zee mc(smirnoved)","l")
		legend.SetFillStyle(0)
		legend.SetBorderSize(0)
		legend.SetTextSize(0.05)
		legend.Draw("same")
		#  text1 = ROOT.TPaveText(0.6,0.6,0.9,0.7)
		text1 = ROOT.TPaveText(150,0.6,200,0.7)
		text1.AddText(f"{campaign}")
		text1.AddText("converted")
		text1.AddText(seven_eta_cut_name[i])
		text1.SetFillStyle(0)
		text1.SetBorderSize(0)
		text1.SetTextSize(0.06)
		text1.Draw("same")

		ratio1 = [0 for j in range(hist1.GetNbinsX())]
		ratio1_errors = [0 for j in range(hist1.GetNbinsX())]
		ratio_x = [0 for j in range(hist1.GetNbinsX())]
		ratio_x_errors = [0 for j in range(hist1.GetNbinsX())]
		for j in range(hist1.GetNbinsX()):
			ratio_x[j] = hist1.GetBinCenter(j+1)
			if hist3.GetBinContent(j+1)!=0:
				ratio1[j] = hist1.GetBinContent(j+1)/hist3.GetBinContent(j+1)
				ratio1_errors[j] = hist1.GetBinContent(j+1)/hist3.GetBinContent(j+1) * math.sqrt( (hist1.GetBinError(j+1)/hist1.GetBinContent(j+1))**2 + (hist3.GetBinError(j+1)/hist3.GetBinContent(j+1))**2)
			else:
				ratio1[j] = 0

		graph1 = ROOT.TGraph(hist1.GetNbinsX(), np.array(ratio_x,np.float32), np.array(ratio1,np.float32))
		graph1 = ROOT.TGraphErrors(hist1.GetNbinsX(), np.array(ratio_x, dtype=np.float32), np.array(ratio1, dtype=np.float32),np.array(ratio_x_errors, dtype=np.float32), np.array(ratio1_errors, dtype=np.float32))

		pad_bot.cd()
		pad_bot.SetFillColor(0)
		pad_bot.SetBorderMode(0)
		pad_bot.SetBorderSize(2)
		#  pad_bot.SetLogx()
		pad_bot.SetTickx(1)
		pad_bot.SetTicky(1)
		pad_bot.SetLeftMargin(0.12)
		pad_bot.SetRightMargin(0.05)
		pad_bot.SetTopMargin(0.)
		pad_bot.SetRightMargin(0.03)
		pad_bot.SetBottomMargin(0.26)
		pad_bot.SetFrameBorderMode(0)
		pad_bot.SetFrameBorderMode(0)

		h = ROOT.TH2F("h", "h", hist1.GetNbinsX(), hist1.GetXaxis().GetBinLowEdge(1),hist1.GetXaxis().GetBinUpEdge(hist1.GetNbinsX()), 2, 0.9, 1.1)
		h.SetTitle("")
		h.GetXaxis().SetTitle("pt [Gev]")
		h.GetYaxis().SetTitle("SF")
		h.GetYaxis().CenterTitle()
		h.GetXaxis().SetTitleSize(0.10)
		h.GetXaxis().SetTitleOffset(1.1)
		h.GetXaxis().SetLabelSize(0.08)
		h.GetXaxis().SetMoreLogLabels()
		h.GetXaxis().SetNoExponent()
		h.GetYaxis().SetTitleSize(0.10)
		h.GetYaxis().SetTitleOffset(0.5)
		h.GetYaxis().SetLabelSize(0.09)
		#  h.GetYaxis().SetNdivisions(203)
		h.Draw()

		graph1.Draw("same,P")

		graph1.SetLineColor(ROOT.kBlack)
		graph1.SetLineWidth(1)
		line = ROOT.TLine(hist1.GetXaxis().GetBinLowEdge(1),1,hist1.GetXaxis().GetBinUpEdge(hist1.GetNbinsX()),1)
		line.SetLineWidth(1)
		line.SetLineStyle(2)
		line.Draw("same")
		line.SetLineColor(ROOT.kRed)



		#  canvas.Print("converted_all.pdf")
		canvas.Print(pdf_name_con)

		canvas2.cd()
		pad_top = ROOT.TPad("pad_top", "", 0, 0.37, 1, 1)
		pad_top.Draw()

		pad_bot = ROOT.TPad("pad_bot", "", 0, 0, 1, 0.36)
		pad_bot.Draw()

		pad_top.cd()
		pad_top.SetFillColor(0)
		pad_top.SetBorderMode(0)
		pad_top.SetBorderSize(2)
		#  pad_top.SetLogx()
		pad_top.SetTickx(1)
		pad_top.SetTicky(1)
		pad_top.SetLeftMargin(0.12)
		pad_top.SetRightMargin(0.05)
		pad_top.SetTopMargin(0.05)
		pad_top.SetRightMargin(0.03)
		pad_top.SetBottomMargin(0.01)
		pad_top.SetFrameBorderMode(0)
		pad_top.SetFrameBorderMode(0)

		hist6.Draw("h,e")
		hist6.SetLineColor(ROOT.kRed)
		hist6.SetMarkerColor(ROOT.kRed)
		hist6.SetMarkerSize(0)
		hist6.GetYaxis().SetTitleOffset(1)
		hist6.GetYaxis().SetTitle("efficiency")
		hist2.Draw("h,same")
		hist2.SetLineColor(ROOT.kBlack)
		hist2.SetMarkerColor(ROOT.kBlack)
		hist2.SetMarkerSize(0)
		hist2.GetYaxis().SetTitleOffset(1)
		hist4.Draw("h,same")
		hist4.SetLineColor(ROOT.kBlue)
		hist4.SetMarkerColor(ROOT.kBlue)
		hist4.SetMarkerSize(0)
		legend2 = ROOT.TLegend(0.6,0.5,0.9,0.7)
		legend2.AddEntry(hist4," photon MC","l")
		legend2.AddEntry(hist2," zee data (smirnoved)","l")
		legend2.AddEntry(hist6," zee mc (smirnoved)","l")
		legend2.SetFillStyle(0)
		legend2.SetBorderSize(0)
		legend2.Draw("same")
		legend2.SetTextSize(0.05)

		text2 = ROOT.TPaveText(150,0.6,200,0.7)
		text2.AddText(f"{campaign}")
		text2.AddText("unconverted")
		text2.AddText(seven_eta_cut_name[i])
		text2.SetFillStyle(0)
		text2.SetBorderSize(0)
		text2.SetTextSize(0.06)
		text2.Draw("same")



		ratio2 = [0 for j in range(hist2.GetNbinsX())]
		ratio2_errors = [0 for j in range(hist2.GetNbinsX())]
		for j in range(hist2.GetNbinsX()):
			ratio_x[j] = hist2.GetBinCenter(j+1)
			if hist4.GetBinContent(j+1)!=0:
				ratio2[j] = hist2.GetBinContent(j+1)/hist4.GetBinContent(j+1)
				ratio2_errors[j] = hist2.GetBinContent(j+1)/hist4.GetBinContent(j+1) * math.sqrt( (hist2.GetBinError(j+1)/hist2.GetBinContent(j+1))**2 + (hist4.GetBinError(j+1)/hist4.GetBinContent(j+1))**2)
			else:
				ratio2[j] = 0
		graph2 = ROOT.TGraphErrors(hist2.GetNbinsX(), np.array(ratio_x, dtype=np.float32), np.array(ratio2, dtype=np.float32),np.array(ratio_x_errors, dtype=np.float32), np.array(ratio2_errors, dtype=np.float32))

		#  graph2 = ROOT.TGraph(hist2.GetNbinsX(), np.array(ratio_x,np.float32), np.array(ratio2,np.float32))

		pad_bot.cd()
		pad_bot.SetFillColor(0)
		pad_bot.SetBorderMode(0)
		pad_bot.SetBorderSize(2)
		#  pad_bot.SetLogx()
		pad_bot.SetTickx(1)
		pad_bot.SetTicky(1)
		pad_bot.SetLeftMargin(0.12)
		pad_bot.SetRightMargin(0.05)
		pad_bot.SetTopMargin(0.)
		pad_bot.SetRightMargin(0.03)
		pad_bot.SetBottomMargin(0.26)
		pad_bot.SetFrameBorderMode(0)
		pad_bot.SetFrameBorderMode(0)

		h = ROOT.TH2F("h", "h", hist2.GetNbinsX(), hist2.GetXaxis().GetBinLowEdge(1),hist2.GetXaxis().GetBinUpEdge(hist2.GetNbinsX()), 2, 0.9, 1.1)
		h.SetTitle("")
		h.GetXaxis().SetTitle("pt [Gev]")
		h.GetYaxis().SetTitle("SF")
		h.GetYaxis().CenterTitle()
		h.GetXaxis().SetTitleSize(0.10)
		h.GetXaxis().SetTitleOffset(1.1)
		h.GetXaxis().SetLabelSize(0.08)
		h.GetXaxis().SetMoreLogLabels()
		h.GetXaxis().SetNoExponent()
		h.GetYaxis().SetTitleSize(0.10)
		h.GetYaxis().SetTitleOffset(0.5)
		h.GetYaxis().SetLabelSize(0.09)
		#  h.GetYaxis().SetNdivisions(203)
		h.Draw()
		graph2.Draw("same,P")
		graph2.SetLineColor(ROOT.kBlack)
		graph2.SetLineWidth(1)
		#  graph2.SetMarkerStyle(ROOT.kFullTriangleUp)
		graph2.SetMarkerStyle(20)

		line = ROOT.TLine(hist2.GetXaxis().GetBinLowEdge(1),1,hist2.GetXaxis().GetBinUpEdge(hist2.GetNbinsX()),1)
		line.SetLineWidth(1)
		line.SetLineStyle(2)
		line.Draw("same")
		line.SetLineColor(ROOT.kRed)

		canvas2.Print(pdf_name_unc)

	canvas.Print(pdf_name_con+"]")
	canvas2.Print(pdf_name_unc+"]")
