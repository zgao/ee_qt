#!/bin/bash

list_type=("con" "unc")
list_campaign=("23a" "23d")
list_mass_range=("80_100" "70_110")
list_mass_range_2=("75_105" "60_120")
list_ff_1=("add_add" "add_sub" "sub_add" "sub_sub")
list_ff_2=("fracs1" "r_eta" "r_had1" "r_phi")

# path="/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/final_txt/"
path="./final_txt/"

# background  closure_test	conversion	four_fudge_factor  fragmentation  nominal_nominal  statistic
##########
#background substract ----------------
###########

for types in "${list_type[@]}"
do
	for campaign in "${list_campaign[@]}"
	do
		if [ "$campaign" == "23a" ]; then
			datatype="data22"
		elif [ "$campaign" == "23d" ]; then
			datatype="data23"
		fi

		# #get nominal ID efficiecny
		p calculate_sys.py	${path}nominal_nominal/test_${types}_${datatype}_80_100.txt ${path}nominal_nominal/test_${types}_${datatype}_70_110.txt ./${types}/${campaign}/nominal_nominal/ID_nominal_${campaign}_${types}.txt "substract"
		# break

		# background systematic
		p calculate_sys.py	${path}background/test_${types}_mc${campaign}75_105.txt ${path}background/test_${types}_mc${campaign}60_120.txt ./${types}/${campaign}/background/ID_mc${campaign}_75.txt "substract"
		p calculate_sys.py	${path}background/test_${types}_mc${campaign}80_100.txt ${path}background/test_${types}_mc${campaign}70_110.txt ./${types}/${campaign}/background/ID_mc${campaign}_80.txt "substract"
		p calculate_sys.py	./${types}/${campaign}/background/ID_mc${campaign}_75.txt ./${types}/${campaign}/background/ID_mc${campaign}_80.txt ./${types}/${campaign}/background/systematic_bkg_${campaign}_${types}.txt

		#conversion
		p calculate_sys.py	${path}conversion_down/test_${types}_${datatype}_80_100.txt ${path}conversion_down/test_${types}_${datatype}_70_110.txt ./${types}/${campaign}/conversion/ID_${datatype}_80_conversion_down_${types}.txt "substract"
		p calculate_sys.py	${path}conversion_up/test_${types}_${datatype}_80_100.txt ${path}conversion_up/test_${types}_${datatype}_70_110.txt ./${types}/${campaign}/conversion/ID_${datatype}_80_conversion_up_${types}.txt "substract"
		p calculate_sys.py ./${types}/${campaign}/conversion/ID_${datatype}_80_conversion_down_${types}.txt ./${types}/${campaign}/conversion/ID_${datatype}_80_conversion_up_${types}.txt ./${types}/${campaign}/conversion/systematic_conversion_${campaign}_${types}.txt


		#fragmentation
		p calculate_sys.py	${path}fragmentation_down/test_${types}_${datatype}_80_100.txt ${path}fragmentation_down/test_${types}_${datatype}_70_110.txt ./${types}/${campaign}/fragmentation/ID_${datatype}_80_fragmentation_down_${types}.txt "substract"
		p calculate_sys.py	${path}fragmentation_up/test_${types}_${datatype}_80_100.txt ${path}fragmentation_up/test_${types}_${datatype}_70_110.txt ./${types}/${campaign}/fragmentation/ID_${datatype}_80_fragmentation_up_${types}.txt "substract"
		p calculate_sys.py ./${types}/${campaign}/fragmentation/ID_${datatype}_80_fragmentation_down_${types}.txt ./${types}/${campaign}/fragmentation/ID_${datatype}_80_fragmentation_up_${types}.txt ./${types}/${campaign}/fragmentation/systematic_fragmentation_${campaign}_${types}.txt


		#fudge factor
		for kind in "${list_ff_1[@]}"
		do
			for variable in "${list_ff_2[@]}"
			do
				p calculate_sys.py	${path}four_fudge_factor/test_${types}_${datatype}_${kind}_${variable}_80_100.txt ${path}four_fudge_factor/test_${types}_${datatype}_${kind}_${variable}_70_110.txt ./${types}/${campaign}/four_fudge_factor/ID_${datatype}_${kind}_${variable}_${types}.txt "substract"
				p calculate_sys.py ./${types}/${campaign}/four_fudge_factor/ID_${datatype}_${kind}_${variable}_${types}.txt ./${types}/${campaign}/nominal_nominal/ID_nominal_${campaign}_${types}.txt ./${types}/${campaign}/four_fudge_factor/systematic_${kind}_${variable}_${campaign}_${types}.txt
			done
		done

		#statistic
		for time in {1..250}
		do
			p calculate_sys.py ${path}statistic/test_${types}_${datatype}_${time}_80_100.txt ${path}statistic/test_${types}_${datatype}_${time}_70_110.txt ./${types}/${campaign}/statistic/ID_statistic_${time}_${campaign}_${types}.txt "substract"
		done

		#closure test
		p calculate_sys.py ${path}closure_test/test_${types}_mc${campaign}_80_100.txt ./${types}/${campaign}/nominal_nominal/ID_nominal_${campaign}_${types}.txt ./${types}/${campaign}/closure_test/ID_closure_test_${campaign}_${types}.txt
		# break

	done
done


