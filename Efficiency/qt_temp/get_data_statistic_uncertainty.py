import numpy as np
import math
import ROOT
import sys
import argparse
 
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()
 
 
parser = argparse.ArgumentParser(description='')
parser.add_argument('-in', '--input', dest="input_file", required=True, help='input file')
parser.add_argument('-c', '--con', dest="con_or_unc", required=True, help='con ot unc file')
parser.add_argument('-pdf', '--pdf', dest="pdf_file", required=True, help='pdf file')
parser.add_argument('-root', '--root', dest="root_file", required=True, help='root file')
 
args = parser.parse_args()
 
labels= ["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]

line_number = 0
entries_f0 = [[[0 for k in range(4)] for i in range(12) ] for j in range(7)]
with open(args.input_file,"r") as f0:
	for line in f0:
		line = line.strip()
		line = line.split()
		line_number +=1
		if (line_number-1) %13 ==0:
			continue
		#  print(line[0],line[1])
		eta_number = int((line_number -1) / 13)
		pt_number = line_number - eta_number* 13 -2
		entries_f0[eta_number][pt_number][0] = float(line[0])
		entries_f0[eta_number][pt_number][1] = float(line[1])
		#  print(line)
		if len(entries_f0[eta_number][pt_number])>2:
			entries_f0[eta_number][pt_number][2] = float(line[2])
			entries_f0[eta_number][pt_number][3] = float(line[3])
errors = [[0 for j in range(12)] for i in range(7)]
for i in range(7):
	for j in range(12):
		ratio = entries_f0[i][j][0] / entries_f0[i][j][1]
		errors[i][j] = math.sqrt(ratio *(1-ratio)/entries_f0[i][j][1])


ROOT.gStyle.SetOptStat(0)
list_bin=[25,30,  35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250]
list_graph_x=[27.5,32.5,37.5,42.5,47.5,55,70,90,112.5,137.5,162.5,212.5]

canvas=ROOT.TCanvas("canvas","",800,600)
canvas.Print(args.pdf_file+"[")
output_root = ROOT.TFile(args.root_file,"RECREATE")
histogram = ROOT.TH1F("histogram","bkg_errors",12,np.array(list_bin,np.float32))

for j in range(7):
	for i in range(histogram.GetNbinsX()):
		histogram.SetBinContent(i+1,errors[j][i])
	canvas.cd()

	histogram.GetXaxis().SetTitle("pt [Gev]")
	histogram.GetYaxis().SetRangeUser(0.,0.05)
	histogram.GetYaxis().SetTitle("statistic uncertainty")
	#  histogram.GetYaxis().SetTitleOffset(0.1)
	histogram.SetLineColor(ROOT.kBlack)
	histogram.SetMarkerSize(0)
	#  histogram.GetXaxis().SetTitleSize(0)
	#  histogram.GetXaxis().SetLabelSize(0)
	histogram.GetYaxis().SetTitleOffset(1.3)
	histogram.GetYaxis().CenterTitle()

	histogram.Draw("h")

	#  legend=ROOT.TLegend(0.5,0.6,0.7,0.7)
	#  legend.SetFillStyle(0)
	#  legend.SetBorderSize(0)
	#  legend.SetTextSize(0.05)
	#  legend.AddEntry(histogram,"fuge factor","L")
	#  legend.Draw("same")

	pt = ROOT.TPaveText(0.5, 0.4, 0.7, 0.55, "ndc")
	pt.SetTextSize(0.05)
	pt.SetBorderSize(0)
	pt.SetFillStyle(0)
	pt.SetTextAlign(13)
	pt.SetTextFont(42)
	pt.AddText(f"{args.con_or_unc},{labels[int(j%7)]}")
	pt.Draw("same")

	histogram.Write()


	canvas.Print(args.pdf_file)
canvas.Print(args.pdf_file+"]")
output_root.Close()
