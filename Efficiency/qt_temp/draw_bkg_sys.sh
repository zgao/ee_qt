#!/bin/bash
list_types=("con" "unc")
list_campaigns=("23a" "23d")
for types in "${list_types[@]}"
do
	for campaign in "${list_campaigns[@]}"
	do
		# p draw_bkg_sys.py -in statistic/systematic_statistic_${campaign}_${types}.txt -c ${types} -pdf ./all_uncertainties/statistic_${campaign}_${types}.pdf -root ./all_uncertainties/statistic_${campaign}_${types}.root
		# p draw_bkg_sys.py -in fudgefactor/systematic_fudgefactor_${campaign}_${types}.txt -c ${types} -pdf ./all_uncertainties/fudgefactor_${campaign}_${types}.pdf -root ./all_uncertainties/fudgefactor_${campaign}_${types}.root
		# p draw_bkg_sys.py -in ${types}/${campaign}/background/systematic_bkg_${campaign}_${types}.txt -c ${types} -pdf ./all_uncertainties/background_${campaign}_${types}.pdf -root ./all_uncertainties/background_${campaign}_${types}.root
		# p draw_bkg_sys.py -in ${types}/${campaign}/closure_test/ID_closure_test_${campaign}_${types}.txt -c ${types} -pdf ./all_uncertainties/closure_test_${campaign}_${types}.pdf -root ./all_uncertainties/closure_test_${campaign}_${types}.root
		# p draw_bkg_sys.py -in ${types}/${campaign}/conversion/systematic_conversion_${campaign}_${types}.txt -c ${types} -pdf ./all_uncertainties/conversion_${campaign}_${types}.pdf -root ./all_uncertainties/conversion_${campaign}_${types}.root
		# p draw_bkg_sys.py -in ${types}/${campaign}/fragmentation/systematic_fragmentation_${campaign}_${types}.txt -c ${types} -pdf ./all_uncertainties/fragmentation_${campaign}_${types}.pdf -root ./all_uncertainties/fragmentation_${campaign}_${types}.root

		# p get_data_statistic_uncertainty.py -in ${types}/${campaign}/nominal_nominal/ID_nominal_${campaign}_${types}.txt -c ${types} -pdf ./all_uncertainties/data_statistic_uncertainty_${campaign}_${types}.pdf -root ./all_uncertainties/data_statistic_uncertainty_${campaign}_${types}.root
		# p draw_ID_efficiecny.py -in ${types}/${campaign}/nominal_nominal/ID_nominal_${campaign}_${types}.txt -c ${types} -pdf Efficiency/${types}_${campaign}.pdf -root Efficiency/${types}_${campaign}.root
		# p draw_ID_efficiecny.py -in photon/test_${types}_pho_mc${campaign}.txt -c ${types} -pdf Efficiency/pho_${types}_mc_${campaign}.pdf -root Efficiency/pho_${types}_mc_${campaign}.root
		# p draw_ID_efficiecny.py -in final_txt/closure_test/test_${types}_mc${campaign}_80_100.txt -c ${types} -pdf Efficiency/zee_${types}_mc_${campaign}.pdf -root Efficiency/zee_${types}_mc_${campaign}.root
		p draw_data_photon.py -conpdf Efficiency/final_con_ -uncpdf Efficiency/final_unc_ -conroot all_uncertainties/all_uncertainty_con_${campaign}.root -uncroot all_uncertainties/all_uncertainty_unc_${campaign}.root
	done
done

