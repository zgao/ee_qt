import sys
eta_names_f1 = ["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]

print(len(sys.argv))
file1 = sys.argv[1]
file2 = sys.argv[2]
file3 = sys.argv[3]
judge_name ="no"
if len(sys.argv) >= 5:
	judge_name = sys.argv[4]
print("sys.argv[3]: ",sys.argv[3])
#  print("sys.argv[4]: ",sys.argv[4])

line_number = 0
entries_f0 = [[[0 for k in range(4)] for i in range(12) ] for j in range(7)]
with open(file1,"r") as f0:
	for line in f0:
		line = line.strip()
		line = line.split()
		line_number +=1
		if (line_number-1) %13 ==0:
			continue
		#  print(line[0],line[1])
		eta_number = int((line_number -1) / 13)
		pt_number = line_number - eta_number* 13 -2
		entries_f0[eta_number][pt_number][0] = float(line[0])
		entries_f0[eta_number][pt_number][1] = float(line[1])
		#  print(line)
		if len(entries_f0[eta_number][pt_number])>2:
			entries_f0[eta_number][pt_number][2] = float(line[2])
			entries_f0[eta_number][pt_number][3] = float(line[3])
			#  exit()


line_number = 0
entries_f1 = [[[0 for k in range(4)] for i in range(12) ] for j in range(7)]
with open(file2,"r") as f0:
	for line in f0:
		line = line.strip()
		line = line.split()
		line_number +=1
		if (line_number-1) %13 ==0:
			continue
		#  print(line[0],line[1])
		eta_number = int((line_number -1) / 13)
		pt_number = line_number - eta_number* 13 -2
		entries_f1[eta_number][pt_number][0] = float(line[0])
		entries_f1[eta_number][pt_number][1] = float(line[1])
		#  print(line)
		if len(entries_f1[eta_number][pt_number])>2:
			entries_f1[eta_number][pt_number][2] = float(line[2])
			entries_f1[eta_number][pt_number][3] = float(line[3])
			#  exit()



if judge_name!="no" :
	print("Now the nominal method is used to substract the background to get the true ID efficiency for the selected mass range ID efficiency\n")
	print("f0  : 80_100 ,f1 : 70_110 ____ or ____ f0: 75_105 , f1: 60_120")

	with open(file3,"w") as f3:
		for i in range(len(eta_names_f1)):
			f3.write(f"{eta_names_f1[i]}\n")
		
			for j in range(12):
				#  print(entries_f0[i][j][0],entries_f0[i][j][1],entries_f0[i][j][2],entries_f0[i][j][3])
				#  print(entries_f1[i][j][0],entries_f1[i][j][1],entries_f1[i][j][2],entries_f1[i][j][3])
				f3.write(f"{abs(entries_f0[i][j][0] - abs(entries_f1[i][j][0] - entries_f0[i][j][0]))}\t\t{abs(entries_f0[i][j][1] - (abs(entries_f0[i][j][1]- entries_f1[i][j][1])))}\t\t{abs(entries_f0[i][j][2]-abs(entries_f0[i][j][2]-entries_f1[i][j][2]))}\t\t{abs(entries_f0[i][j][3]-abs(entries_f0[i][j][3]-entries_f1[i][j][3]))}\n")
		
	f3.close()
	print(f"{file3} systematic uncertainty is calculated")
else:
	print("Now use to get the systematic uncertainty after substracted the background, the difference of the two ID efficiecny,such as 'converison' 'frag'.....'")
	with open(file3,"w") as f3:
		for i in range(len(eta_names_f1)):
			f3.write(f"{eta_names_f1[i]}\n")
		
			for j in range(12):
				#  f3.write(f"{abs(entries_f0[i][j][0] - abs(entries_f1[i][j][0] - entries_f0[i][j][0])) / abs(entries_f0[i][j][1] - (abs(entries_f0[i][j][1]- entries_f1[i][j][1])))}\n")
				f3.write(f"{abs(abs(entries_f0[i][j][0]/entries_f0[i][j][1]) - abs(entries_f1[i][j][0]/entries_f1[i][j][1]))}\n")
				#  f3.write(f"{abs(entries_f1[i][j][0] / entries_f1[i][j][1] - entries_f0[i][j][0] / entries_f0[i][j][1])}\n")
		
	f3.close()
	print(f"{file3} systematic uncertainty is calculated")
