import numpy as np
import sys
import argparse
import math
import ROOT
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()
 
 
ROOT.gStyle.SetOptStat(0)

parser = argparse.ArgumentParser(description='')
parser.add_argument('-conpdf', '--conpdf', dest="conpdf_file", required=True, help='con pdf file')
parser.add_argument('-uncpdf', '--uncpdf', dest="uncpdf_file", required=True, help='unc pdf file')
parser.add_argument('-conroot', '--conroot', dest="conroot_file", required=True, help='con root file')
parser.add_argument('-uncroot', '--uncroot', dest="uncroot_file", required=True, help='unc root file')
args = parser.parse_args()

list_bin=[25,30,  35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250]
list_graph_x=[27.5,32.5,37.5,42.5,47.5,55,70,90,112.5,137.5,162.5,212.5]

seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]
list_campaign=["23a","23d"]
list_type=["con","unc"]
path="/afs/cern.ch/user/z/zgao/qt_temp/qt_temp/all_uncertainties/"
for campaign in list_campaign:
	pdf_name_con = args.conpdf_file + f"_{campaign}.pdf"
	pdf_name_unc = args.uncpdf_file + f"_{campaign}.pdf"
	root_name_con = args.conroot_file + f"_{campaign}.root"
	root_name_unc = args.uncroot_file + f"_{campaign}.root"
	outfile_con = ROOT.TFile(root_name_con,"RECREATE")
	outfile_unc = ROOT.TFile(root_name_unc,"RECREATE")

	#background uncertainty 
	directory1 = f'{path}/background_{campaign}_con.root'
	directory2 = f'{path}/background_{campaign}_unc.root'


	#mc_stat uncertainty
	directory3 = f'{path}/statistic_{campaign}_con.root'
	directory4 = f'{path}/statistic_{campaign}_unc.root'

	#conversion uncertainty
	directory5 = f'{path}/conversion_{campaign}_con.root'
	directory6 = f'{path}/conversion_{campaign}_unc.root'

	#fudge factor
	directory7 = f'{path}/fudgefactor_{campaign}_con.root'
	directory8 = f'{path}/fudgefactor_{campaign}_unc.root'


	#fragmentation 
	directory9 = f'{path}/fragmentation_{campaign}_con.root'
	directory10 = f'{path}/fragmentation_{campaign}_unc.root'

	#closure test
	directory11 = f'{path}/closure_test_{campaign}_con.root'
	directory12 = f'{path}/closure_test_{campaign}_unc.root'

	#  #statistic uncertainty
	directory13 = f'{path}/data_statistic_uncertainty_{campaign}_con.root'
	directory14 = f'{path}/data_statistic_uncertainty_{campaign}_unc.root'

	f1= ROOT.TFile(directory1,"READ")
	f2= ROOT.TFile(directory2,"READ")
	f3= ROOT.TFile(directory3,"READ")
	f4= ROOT.TFile(directory4,"READ")
	f5= ROOT.TFile(directory5,"READ")
	f6= ROOT.TFile(directory6,"READ")
	f7= ROOT.TFile(directory7,"READ")
	f8= ROOT.TFile(directory8,"READ")
	f9= ROOT.TFile(directory9,"READ")
	f10= ROOT.TFile(directory10,"READ")
	f11= ROOT.TFile(directory11,"READ")
	f12= ROOT.TFile(directory12,"READ")
	f13= ROOT.TFile(directory13,"READ")
	f14= ROOT.TFile(directory14,"READ")

	key_list = f1.GetListOfKeys()

	canvas=ROOT.TCanvas("canvas","",800,600)
	canvas.Print(pdf_name_con+"[")

	canvas2=ROOT.TCanvas("canvas2","",800,600)
	canvas2.Print(pdf_name_unc+"[")

	def final_errors(hist):
		list1 = []
		for i in range(hist.GetNbinsX()):
			list1.append(hist.GetBinContent(i+1))
		 
		return list1

	for i in range(7):
		histogram_con = ROOT.TH1F(f"histogram_{campaign}_con",f"histogram_{campaign}_con",12,np.array(list_bin,np.float32))
		histogram_unc = ROOT.TH1F(f"histogram_{campaign}_unc",f"histogram_{campaign}_unc",12,np.array(list_bin,np.float32))
		hist1 = f1.Get(f"histogram;{int(i+1)}")
		hist2 = f2.Get(f"histogram;{int(i+1)}")
		hist3 = f3.Get(f"histogram;{int(i+1)}")
		hist4 = f4.Get(f"histogram;{int(i+1)}")
		hist5 = f5.Get(f"histogram;{int(i+1)}")
		hist6 = f6.Get(f"histogram;{int(i+1)}")
		hist7 = f7.Get(f"histogram;{int(i+1)}")
		hist8 = f8.Get(f"histogram;{int(i+1)}")
		hist9 = f9.Get(f"histogram;{int(i+1)}")
		hist10 = f10.Get(f"histogram;{int(i+1)}")
		hist11 = f11.Get(f"histogram;{int(i+1)}")
		hist12 = f12.Get(f"histogram;{int(i+1)}")
		hist13 = f13.Get(f"histogram;{int(i+1)}")
		hist14 = f14.Get(f"histogram;{int(i+1)}")

		error1 = final_errors(hist1)
		#  print(error1)
		error2 = final_errors(hist2)
		error3 = final_errors(hist3)
		error4 = final_errors(hist4)
		error5 = final_errors(hist5)
		error6 = final_errors(hist6)
		error7 = final_errors(hist7)
		error8 = final_errors(hist8)
		error9 = final_errors(hist9)
		error10 = final_errors(hist10)
		error11 = final_errors(hist11)
		error12 = final_errors(hist12)
		error13 = final_errors(hist13)
		error14 = final_errors(hist14)

		final_error_con = [error1[j]**2 + error3[j]**2 + error5[j]**2 + error7[j]**2 + error9[j]**2 + error11[j]**2 + error13[j]**2 for j in range(len(error1))]
		#  for j in range(12):
			#  print(error1[j],error3[j],error5[j],error7[j],error9[j],error11[j],error13[j])
			#  print(math.sqrt(final_error_con[j]))
		final_error_unc = [error2[j]**2 + error4[j]**2 + error6[j]**2 + error8[j]**2 + error10[j]**2 + error12[j]**2 + error14[j]**2 for j in range(len(error1))]
		for bins in range(12):
			histogram_con.SetBinContent(bins+1,final_error_con[bins])
			histogram_unc.SetBinContent(bins+1,final_error_unc[bins])
		outfile_con.cd()
		histogram_con.Write()
		outfile_unc.cd()
		histogram_unc.Write()

		con_hist = hist1.Clone("con")
		for j in range(con_hist.GetNbinsX()):
			con_hist.SetBinContent(j+1,0)
		unc_hist = hist2.Clone("unc")
		for j in range(unc_hist.GetNbinsX()):
			unc_hist.SetBinContent(j+1,0)

		for j in range(len(error1)):
			con_hist.SetBinContent(j+1, math.sqrt(final_error_con[j]))
			#  print("123: ",math.sqrt(final_error_con[j]),con_hist.GetBinContent(j+1))
			unc_hist.SetBinContent(j+1, math.sqrt(final_error_unc[j]))
		

		canvas.cd()
		#  canvas.Clear()
		hist1.Draw("h")
		hist1.GetYaxis().SetRangeUser(0,0.1)
		hist1.SetLineColor(ROOT.kViolet)
		hist1.SetMarkerSize(0)
		hist3.Draw("h,same")
		hist3.SetLineColor(ROOT.kBlue)
		hist3.GetYaxis().SetRangeUser(0,0.1)
		hist5.Draw("h,same")
		hist5.SetLineColor(7)
		hist5.GetYaxis().SetRangeUser(0,0.1)
		hist7.Draw("h,same")
		hist7.SetLineColor(ROOT.kGreen)
		hist7.GetYaxis().SetRangeUser(0,0.1)
		hist9.Draw("h,same")
		hist9.GetYaxis().SetRangeUser(0,0.1)
		hist9.SetLineColor(ROOT.kRed)
		hist11.Draw("h,same")
		hist11.SetLineColor(ROOT.kMagenta)
		hist11.GetYaxis().SetRangeUser(0,0.1)
		hist13.Draw("h,same")
		hist13.SetLineColor(ROOT.kYellow)
		hist13.GetYaxis().SetRangeUser(0,0.1)
		con_hist.Draw("same")
		#  for j in range(12):
		#	   print(con_hist.GetBinContent(j+1))
		con_hist.SetLineColor(ROOT.kBlack)
		con_hist.GetYaxis().SetRangeUser(0,0.1)

		legend = ROOT.TLegend(0.6,0.5,0.9,0.7)
		legend.AddEntry(hist1," background ","l")
		legend.AddEntry(hist3," mc_stat ","l")
		legend.AddEntry(hist5," conversion ","l")
		legend.AddEntry(hist7," fudge factor","l")
		legend.AddEntry(hist9," fragmentation","l")
		legend.AddEntry(hist11," closure test","l")
		legend.AddEntry(hist13," statistic","l")
		legend.AddEntry(con_hist," total","l")
		legend.SetFillStyle(0)
		legend.SetBorderSize(0)
		legend.Draw("same")
		text1 = ROOT.TPaveText(50,0.06,100,0.12)
		text1.AddText("converted")
		text1.AddText(f"{campaign}")
		text1.AddText(seven_eta_cut_name[i])
		text1.SetFillStyle(0)
		text1.SetBorderSize(0)
		text1.SetTextSize(0.03)
		text1.Draw("same")
		canvas.Update()
		canvas.Print(pdf_name_con)
		#  canvas.Print(pdf_name_con)

		canvas2.cd()
		hist2.Draw("h")
		hist2.GetYaxis().SetRangeUser(0,0.15)
		hist2.SetLineColor(ROOT.kViolet)
		hist4.Draw("h,same")
		hist4.SetLineColor(ROOT.kBlue)
		hist4.GetYaxis().SetRangeUser(0,0.15)
		hist6.Draw("h,same")
		hist6.SetLineColor(7)
		hist6.GetYaxis().SetRangeUser(0,0.15)
		hist8.Draw("h,same")
		hist8.SetLineColor(ROOT.kGreen)
		hist8.GetYaxis().SetRangeUser(0,0.15)
		hist10.Draw("h,same")
		hist10.SetLineColor(ROOT.kRed)
		hist10.GetYaxis().SetRangeUser(0,0.15)
		hist12.Draw("h,same")
		hist12.SetLineColor(ROOT.kMagenta)
		hist12.GetYaxis().SetRangeUser(0,0.15)
		hist14.Draw("h,same")
		hist14.SetLineColor(ROOT.kYellow)
		hist14.GetYaxis().SetRangeUser(0,0.15)
		unc_hist.Draw("h,same")
		unc_hist.SetLineColor(ROOT.kBlack)
		unc_hist.GetYaxis().SetRangeUser(0,0.15)

		legend = ROOT.TLegend(0.6,0.5,0.9,0.7)
		legend.AddEntry(hist2," background ","l")
		legend.AddEntry(hist4," mc_stat ","l")
		legend.AddEntry(hist6," conversion ","l")
		legend.AddEntry(hist8," fudge factor","l")
		legend.AddEntry(hist10," fragmentation","l")
		legend.AddEntry(hist12," closure test","l")
		legend.AddEntry(hist14," statistic","l")
		legend.AddEntry(unc_hist," total","l")
		legend.SetFillStyle(0)
		legend.SetBorderSize(0)
		legend.Draw("same")
		text1 = ROOT.TPaveText(50,0.06,100,0.12)
		text1.AddText("unconverted")
		text1.AddText(f"{campaign}")
		text1.AddText(seven_eta_cut_name[i])
		text1.SetFillStyle(0)
		text1.SetBorderSize(0)
		text1.SetTextSize(0.03)
		text1.Draw("same")
		canvas2.Print(pdf_name_unc)
		print(i)

	canvas.Print(pdf_name_con+"]")
	canvas2.Print(pdf_name_unc+"]")
	outfile_con.Close()
	outfile_unc.Close()
