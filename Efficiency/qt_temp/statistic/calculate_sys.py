import math
import sys
eta_names_f1 = ["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]


judge_name ="no"

#  list_1=["add_add","add_sub","sub_add","sub_sub"]
#  list_2=["fracs1","r_eta","r_phi","r_had1"]

list_types=["con","unc"]
list_campaign=["23a","23d"]
list_datas=["data22","data23"]
for campaign in range(2):
	for types in range(2):
		entries_f3 = [[0 for i in range(12) ] for j in range(7)]

		entries_f0 = [[[0 for k in range(4)] for i in range(12) ] for j in range(7)]
		entries_f1 = [[[0 for l in range(250) ] for i in range(12) ] for j in range(7)]
		entries_mean = [[0 for i in range(12) ] for j in range(7)]
		entries_final = [[0 for i in range(12) ] for j in range(7)]
		for time in range(250):
			file1 = f"/afs/cern.ch/user/z/zgao/qt_temp/qt_temp/{list_types[types]}/{list_campaign[campaign]}/statistic/ID_statistic_{time+1}_{list_campaign[campaign]}_{list_types[types]}.txt"
			line_number = 0
			with open(file1,"r") as f0:
				for line in f0:
					line = line.strip()
					line = line.split()
					line_number +=1
					if (line_number-1) %13 ==0:
						continue
					#  print(line[0],line[1])
					#  print(line_number)
					eta_number = int((line_number -1) / 13)
					pt_number = line_number - eta_number* 13 -2
					#  print(file1)
					#  print(line)
					#  print(line_number)
					#  print("pt_number ",pt_number)
					#  print("eta_number ",eta_number)
					entries_f0[eta_number][pt_number][0] = float(line[0])
					entries_f0[eta_number][pt_number][1] = float(line[1])
					#  print(line)
					#  exit()
					if len(entries_f0[eta_number][pt_number])>2:
						entries_f0[eta_number][pt_number][2] = float(line[2])
						entries_f0[eta_number][pt_number][3] = float(line[3])
			#  print("time ",time)
			#  exit()
			for i in range(len(eta_names_f1)):
				for j in range(12):
					#  print("i ",i," j ",j)
					entries_f1[i][j][time] = entries_f0[i][j][0] / entries_f0[i][j][1]
					entries_mean[i][j] += entries_f0[i][j][0] / entries_f0[i][j][1]/250
					#  entries_mean[i][j][time] += (entries_f0[i][j][0] / entries_f0[i][j][1])/250
		for time in range(250):
			for i in range(len(eta_names_f1)):
				for j in range(12):
					entries_final[i][j] = (entries_f1[i][j][time] - entries_mean[i][j])**2





		file3=f"./systematic_statistic_{list_campaign[campaign]}_{list_types[types]}.txt"
		with open(file3,"w") as f3:
			for i in range(len(eta_names_f1)):
				f3.write(f"{eta_names_f1[i]}\n")
			
				for j in range(12):
					#  f3.write(f"{abs(abs(entries_f0[i][j][0]/entries_f0[i][j][1]) - abs(entries_f1[i][j][0]/entries_f1[i][j][1]))}\n")
					f3.write(f"{math.sqrt(entries_final[i][j]/250)}\n")
			
		f3.close()
		print(f"{file3} systematic uncertainty is calculated")
