import re
import ROOT
from ROOT import gDirectory
from ROOT import TFile
#  from ROOT import TH1D
#  import ctypes
#  from pdb import set_trace

with open('PhotonIsEMTightSelectorCutDefs.conf','r') as file1:
	   lines = file1.readlines()

converted_lines = []
unconverted_lines = []



start_line=39
for i,line in enumerate(lines):
	   if i >=start_line:
		   if "photonsConverted" in line :
			   converted_lines.append(line)
		   elif "photonsNonConverted" in line:
			   unconverted_lines.append(line)

#  categories = {
#         "0-15 GeV": [],
#         "15-20 GeV": [],
#         "20-25 GeV": [],
#         "25-30 GeV": [],
#         "30-35 GeV": [],
#         "35-40 GeV": [],
#         "40-45 GeV": [],
#         "45-50 GeV": [],
#         "50-60 GeV": [],
#         "60-80 GeV": [],
#         "80-inf GeV": []
#  }

old_name_con = ["CutHadLeakage_photonsConverted" ,"Reta37_photonsConverted" ,"Rphi33_photonsConverted" ,"weta2_photonsConverted" ,"deltae_photonsConverted" ,"DEmaxs1_photonsConverted" ,"wtot_photonsConverted" ,"fracm_photonsConverted" ,"w1_photonsConverted"]
old_name_unc = ["CutHadLeakage_photonsNonConverted" ,"Reta37_photonsNonConverted" ,"Rphi33_photonsNonConverted" ,"weta2_photonsNonConverted" ,"deltae_photonsNonConverted" ,"DEmaxs1_photonsNonConverted" ,"wtot_photonsNonConverted" ,"fracm_photonsNonConverted" ,"w1_photonsNonConverted"]

new_name = ["rhad1 or rhad0 " ,"r_eta" ,"r_phi" ,"w_eta2" ,"delta_E" ,"e_ratio" ,"w_stot" ,"fracs1" ,"w_eta1"]
converted_file=open("converted_lines.txt","w")
for i,line in enumerate(converted_lines):
	   line = line.strip()
	   split_line=line.split(";")
	   new_line = ";".join(split_line[:-1])
	   #  print("i= ",i)
	   if ((i-10)%11==0  ) :
		   new_line += "; 9999"

	   new_line=new_line.replace(':',';')
	   new_line=new_line.replace('+','')
	   converted_file.write(new_line +"\n")
converted_file.close()
file1.close()

with open("converted_lines.txt","r") as file2:
	   lines2=file2.readlines()

#name--eta-pt = 9-9-11
values=[[[0 for k in range(11)] for j in range(9)] for i in range(9)]
name_number=[0 for i in range(len(old_name_con))]
for cutname,line in enumerate(lines2):
	   line=line.strip()
	   #  print(line)
	   #  print(line.strip())
	   split_line=line.split(";")
	   #  print(split_line)
	   #  for name in range(len(old_name_con)):
		   #  print(name)
	   #  if old_name_con[name] in split_line[0]:
	   if split_line[0]  in old_name_con:
		   index=old_name_con.index(split_line[0])
		   name_number[index]+=1
		   #  print(name_number[name])
	   for column in range(1,10):
		   values[index][int(column-1)][int(name_number[index]-1)]=split_line[column]
print(values[0][7][0])

file2.close()



file_input=ROOT.TFile("~/eos/con1_fudge/apply_ST/converted.root","READ")
#  tree=file_input.Get("ZeeCandidates")
mychain=gDirectory.Get("ZeeCandidates")
entries=mychain.GetEntriesFast()

#      #  print(branch.GetAddress())
#      #  branch_address.append(branch.GetAddress())
#      branch_name=branch.GetName()
#      branch_address= ROOT.AddressOf(branch)
#      tree.SetBranchAddress(branch_name,branch_address)
#  print(branch_list)
#  print(branch_address)
#  print(id(branch_list[0]))
#  print(branch_name)


 #  branch_list= tree.GetListOfBranches()
#  branch_names=[]
#  for branch in branch_list:
	#  print(branch)
	#  branch_name=branch.GetName()
#  list1=['pt']
#  #  list1[0]=getattr(tree,f"{list1[0]}")
#  pt=getattr(tree,"pt")
#  print(type(pt))
#  print(pt)
#  print(type(list1[0]))
#  print(list1[0])
#  pt=ROOT.RooFit.DouvleVar()
#  eta=ROOT.RooFit.DOuvleVar()
#
#  tree.SetBranchAddress("pt",pt.GetAddress())
#  tree.SetBranchAddress("eta",eta.Address())
total=0
numerator=0
# for entry in range(10**5):
for jentry in range(entries):
	ientry=mychain.LoadTree(jentry)
	if ientry<0:
		break
	
	nb=mychain.GetEntry(jentry)
	if nb<=0:
		continue
	if(jentry%100000==0):
		print(jentry)
		#  break
	eta=mychain.eta
	phi=mychain.phi
	pt=mychain.pt
	primary_cluster_be2_eta=mychain.primary_cluster_be2_eta
	r_had0=mychain.r_had0
	r_had1=mychain.r_had1
	e_ratio=mychain.e_ratio
	delta_E=mychain.delta_E
	w_eta2=mychain.w_eta2
	w_eta1=mychain.w_eta1
	r_eta=mychain.r_eta
	r_phi=mychain.r_phi
	w_stot=mychain.w_stot
	f1=mychain.f1
	fracs1=mychain.fracs1
	f3=mychain.f3
	ptcone20=mychain.ptcone20
	topoetcone20=mychain.topoetcone20
	E277=mychain.E277
	#  print(pt)
	# tree.GetEntry(entry)

	#  list1=['pt']
	#  list1[0]=getattr(tree,list1[0])
	#  print(list1[0])

	#  eta=getattr(entry,"eta")
	#  phi=getattr(entry,"phi")
	#  pt=getattr(entry,"pt")
	#  primary_cluster_be2_eta=getattr(entry,"primary_cluster_be2_eta")
	#  primary_cluster_be2_abseta=getattr(entry,"primary_cluster_be2_abseta")
	#  r_had0=getattr(entry,"r_had0")
	#  r_had1=getattr(entry,"r_had1")
	#  e_ratio=getattr(entry,"e_ratio")
	#  delta_E=getattr(entry,"delta_E")
	#  w_eta2=getattr(entry,"w_eta2")
	#  w_eta1=getattr(entry,"w_eta1")
	#  r_eta=getattr(entry,"r_eta")
	#  r_phi=getattr(entry,"r_phi")
	#  w_stot=getattr(entry,"w_stot")
	#  f1=getattr(entry,"f1")
	#  fracs1=getattr(entry,"fracs1")
	#  f3=getattr(entry,"f3")
	#  ptcone20=getattr(entry,"ptcone20")
	#  topoetcone20=getattr(entry,"topoetcone20")
	#  E277=getattr(entry,"E277")


	#  for branch in branch_list:
	#      branch_names.append(branch.GetName())
	#  for branch_name in branch_names:
	#      print(branch_name)
	#      branch=tree.GetBranch(branch_name)
	#      branch_address=ROOT.AddressOf(branch)
	#      setattr(tree,branch_name,branch_address)
	#  pt=getattr(tree,"pt")
	#  primary_cluster_be2_eta=getattr(tree,"primary_cluster_be2_eta")



	#  if(itera==10**5):
	#      break
	#      print(itera)
	pt_bin1 = (pt > 25 and	pt < 30)


	#  pt_bin1 = (list1[0] > 25 and	list1[0] < 30)
	#  print(fracs1)
	#  pt_bin2 = (pt > 30 and	pt < 35)
	#  pt_bin3 = (pt > 35 and	pt < 40)
	#  pt_bin4 = (pt > 40 and	pt < 45)
	#  pt_bin5 = (pt > 45 and	pt < 50)
	#  pt_bin6 = (pt > 50 and	pt < 60)
	#  pt_bin7 = (pt > 60 and	pt < 80)
	#  pt_bin8 = (pt > 80 and	pt < 100)
	#  pt_bin9 = (pt > 100 and  pt < 125)
	#  pt_bin10 = (pt > 125 and  pt < 150)
	#  pt_bin11 = (pt > 150 and  pt < 175)
	#  pt_bin12 = (pt > 175 and  pt < 250)

	eta_bin1 = (abs(primary_cluster_be2_eta) > 0 and abs(primary_cluster_be2_eta) < 0.6 )
	#  eta_bin2 = (fabs(primary_cluster_be2_eta) > 0.6 and fabs(primary_cluster_be2_eta) < 0.8 )
	#  eta_bin3 = (fabs(primary_cluster_be2_eta) > 0.8 and fabs(primary_cluster_be2_eta) < 1.15 )
	#  eta_bin4 = (fabs(primary_cluster_be2_eta) > 1.15 and fabs(primary_cluster_be2_eta) < 1.37 )
	#  eta_bin5 = (fabs(primary_cluster_be2_eta) > 1.52 and fabs(primary_cluster_be2_eta) < 1.81 )
	#  eta_bin6 = (fabs(primary_cluster_be2_eta) > 1.81 and fabs(primary_cluster_be2_eta) < 2.01 )
	#  eta_bin7 = (fabs(primary_cluster_be2_eta) > 2.01 and fabs(primary_cluster_be2_eta) < 2.37 )

	#  if (abs(primary_cluster_be2_eta) > 1.37 and abs(primary_cluster_be2_eta) < 1.52 ):
		#  continue
	if(pt_bin1 and eta_bin1):
	#  if(pt_bin1 ):
		total+=1
		if((r_had1<0.0260)and(r_eta>0.919)and(r_phi>0.57)and(w_eta2<0.0117)and(delta_E<198)and(e_ratio>0.866)and(w_stot<3.02)and(fracs1<0.361)and(w_eta1<0.692)):
			numerator+=1
print(numerator)
print(total)
print(numerator/total)

