#!/bin/bash

# python3 ID_con.py -t data -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_data_new/converted.root -o converted_data.pdf
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc.pdf
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_rahd1.pdf
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_w_eta2.pdf
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_w_stot.pdf
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_fracs1.pdf


# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_r_ha1.pdf -v 0
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_r_eta.pdf -v 1
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_r_phi.pdf -v 2
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_w_eta2.pdf -v 3
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_delta_E.pdf -v 4
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_e_ratio.pdf -v 5
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_w_stot.pdf -v 6
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_fracs1.pdf -v 7 &
# python3 ID_con.py -t mc -in /Users/xiangjun/EE/efficiency/xjun/smirnoved_eemc_new/converted.root -o converted_mc_no_w_eta1.pdf -v 8 &



python3 ID_con.py -t mc -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/converted.root -o converted_mc_no_w_eta1.pdf -v 8 

