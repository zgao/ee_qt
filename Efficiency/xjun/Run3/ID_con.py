import re
import ROOT
from ROOT import gDirectory,TFile,gROOT
#  from ROOT import TFile
import math
import numpy as np

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()

#  with open('/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/PhotonIsEMTightSelectorCutDefs.conf','r') as file1:
with open('/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/xjun/Run3/mc20_v22_tightID_menu_smoothed_29_06_2023-1.conf','r') as file1:
	lines = file1.readlines()

converted_lines = []
unconverted_lines = []



start_line=36
for i,line in enumerate(lines):
	if i >=start_line:
		if "photonsConverted" in line :
			converted_lines.append(line)
		elif "photonsNonConverted" in line:
			unconverted_lines.append(line)


old_name_con = ["CutHadLeakage_photonsConverted" ,"Reta37_photonsConverted" ,"Rphi33_photonsConverted" ,"weta2_photonsConverted" ,"deltae_photonsConverted" ,"DEmaxs1_photonsConverted" ,"wtot_photonsConverted" ,"fracm_photonsConverted" ,"w1_photonsConverted"]
old_name_unc = ["CutHadLeakage_photonsNonConverted" ,"Reta37_photonsNonConverted" ,"Rphi33_photonsNonConverted" ,"weta2_photonsNonConverted" ,"deltae_photonsNonConverted" ,"DEmaxs1_photonsNonConverted" ,"wtot_photonsNonConverted" ,"fracm_photonsNonConverted" ,"w1_photonsNonConverted"]

new_name = ["rhad1 or rhad0 " ,"r_eta" ,"r_phi" ,"w_eta2" ,"delta_E" ,"e_ratio" ,"w_stot" ,"fracs1" ,"w_eta1"]

con_uncon_files=[converted_lines,unconverted_lines]
file_names=["converted_lines.txt","unconverted_lines.txt"]

for name in range(2):
	#  converted_file=open("converted_lines.txt","w")
	con_uncon_file=open(file_names[name],"w")
	for i,line in enumerate(con_uncon_files[name]):
		line = line.strip()
		split_line=line.split(";")
		new_line = ";".join(split_line[:-1])
		if ((i-11)%12==0  ) :
			new_line += "; 9999"
		new_line=new_line.replace(':',';')
		new_line=new_line.replace('+','')
		con_uncon_file.write(new_line +"\n")
	con_uncon_file.close()
file1.close()
#  exit(0)
#  exit(0)

with open("converted_lines.txt","r") as file2:
	lines2=file2.readlines()

#name--eta-pt = 9-9-11
values=[[[0 for k in range(12)] for j in range(9)] for i in range(9)]
name_number=[0 for i in range(len(old_name_con))]
for cutname,line in enumerate(lines2):
	   line=line.strip()
	   split_line=line.split(";")
	   if split_line[0]  in old_name_con:
		   index=old_name_con.index(split_line[0])
		   name_number[index]+=1
	   for column in range(1,10):
		   values[index][int(column-1)][int(name_number[index]-1)]=float(split_line[column])
#  print(values[0][7][0])
#  print(values[0][7])
#  print(values[3][6])

#  values=values.
file2.close()

#  exit(0)



trees = ROOT.TChain("ZeeCandidates")
trees.Add("/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/use*.root")
#  file_input=ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/con1_fudge/apply_ST/converted.root","READ")
#  mychain=file_input.Get("ZeeCandidates")
#mychain=gDirectory.Get("ZeeCandidates")
#  entries=mychain.GetEntriesFast()
entries = trees.GetEntries()
print('entries = ', entries)
#  if entries > 100000:
	#  entries = 100000
numerator=[[0 for i in range(9)]for j in range(12)]
total=[[0 for i in range(9)]for j in range(12)]
ratios=[[0 for i in range(9)]for j in range(12)]
stat_err=[[0 for i in range(9)] for j in range(12)]

#  exit(0)

for jentry in range(entries):
	#  print(values[1][7])
	#ientry=mychain.LoadTree(jentry)
	trees.GetEntry(jentry)
	if jentry % 10000 == 0:
		print('jentry =', jentry)
	eta=trees.eta
	phi=trees.phi
	pt=trees.pt
	primary_cluster_be2_eta=trees.primary_cluster_be2_eta
	r_had0=trees.r_had0
	r_had1=trees.r_had1
	e_ratio=trees.e_ratio
	delta_E=trees.delta_E
	w_eta2=trees.w_eta2
	w_eta1=trees.w_eta1
	r_eta=trees.r_eta
	r_phi=trees.r_phi
	w_stot=trees.w_stot
	f1=trees.f1
	fracs1=trees.fracs1
	f3=trees.f3
	ptcone20=trees.ptcone20
	topoetcone20=trees.topoetcone20
	E277=trees.E277


	pt_bin1 = (pt > 25 and	pt < 30)
	pt_bin2 = (pt > 30 and	pt < 35)
	pt_bin3 = (pt > 35 and	pt < 40)
	pt_bin4 = (pt > 40 and	pt < 45)
	pt_bin5 = (pt > 45 and	pt < 50)
	pt_bin6 = (pt > 50 and	pt < 60)
	pt_bin7 = (pt > 60 and	pt < 80)
	pt_bin8 = (pt > 80 and	pt < 100)
	pt_bin9 = (pt > 100 and  pt < 125)
	pt_bin10 = (pt > 125 and  pt < 150)
	pt_bin11 = (pt > 150 and  pt < 175)
	pt_bin12 = (pt > 175 and  pt < 250)

	eta_bin1 = (abs(primary_cluster_be2_eta) > 0 and abs(primary_cluster_be2_eta) < 0.6 )
	eta_bin2 = (abs(primary_cluster_be2_eta) > 0.6 and abs(primary_cluster_be2_eta) < 0.8 )
	eta_bin3 = (abs(primary_cluster_be2_eta) > 0.8 and abs(primary_cluster_be2_eta) < 1.15 )
	eta_bin4 = (abs(primary_cluster_be2_eta) > 1.15 and abs(primary_cluster_be2_eta) < 1.37 )
	eta_bin5 = (abs(primary_cluster_be2_eta) > 1.52 and abs(primary_cluster_be2_eta) < 1.81 )
	eta_bin6 = (abs(primary_cluster_be2_eta) > 1.81 and abs(primary_cluster_be2_eta) < 2.01 )
	eta_bin7 = (abs(primary_cluster_be2_eta) > 2.01 and abs(primary_cluster_be2_eta) < 2.37 )
	
	pt_bins=[pt_bin1,pt_bin2,pt_bin3,pt_bin4,pt_bin5,pt_bin6,pt_bin7,pt_bin8,pt_bin9,pt_bin10,pt_bin11,pt_bin12]
	#  print(pt_bins)
	eta_bins=[eta_bin1,eta_bin2,eta_bin3,eta_bin4,False,eta_bin5,eta_bin6,eta_bin7,False]
	for pt_bin in range(len(pt_bins)):
		if not pt_bins[pt_bin]:
			continue
		for eta_bin in range(len(eta_bins)):
			if not eta_bins[eta_bin]:
				continue
			total[pt_bin][eta_bin]+=1
			if(eta_bin==2 or eta_bin==3):
				r_had1=r_had0
			if(pt_bin<=7):
				if((r_had1<values[0][eta_bin][int(pt_bin+4)])and(r_eta>values[1][eta_bin][int(pt_bin+4)])and(r_phi>values[2][eta_bin][int(pt_bin+4)])and(w_eta2<values[3][eta_bin][int(pt_bin+4)])and(delta_E<values[4][eta_bin][int(pt_bin+4)])and(e_ratio>values[5][eta_bin][int(pt_bin+4)])and(w_stot<values[6][eta_bin][int(pt_bin+4)])and(fracs1<values[7][eta_bin][int(pt_bin+4)])and(w_eta1<values[8][eta_bin][int(pt_bin+4)])):

					numerator[pt_bin][eta_bin]+=1
			else:
				if((r_had1<values[0][eta_bin][11])and(r_eta>values[1][eta_bin][11])and(r_phi>values[2][eta_bin][11])and(w_eta2<values[3][eta_bin][11])and(delta_E<values[4][eta_bin][11])and(e_ratio>values[5][eta_bin][11])and(w_stot<values[6][eta_bin][11])and(fracs1<values[7][eta_bin][11])and(w_eta1<values[8][eta_bin][11])):
					numerator[pt_bin][eta_bin]+=1



for eta_bin in range(len(eta_bins)):
	for pt_bin in range(len(pt_bins)):
		if total[pt_bin][eta_bin]==0:
			ratios[pt_bin][eta_bin]=0
			stat_err[pt_bin][eta_bin]=1e6
		else:
			ratios[pt_bin][eta_bin]=numerator[pt_bin][eta_bin]/total[pt_bin][eta_bin]
			stat_err[pt_bin][eta_bin]=math.sqrt(ratios[pt_bin][eta_bin]*(1-ratios[pt_bin][eta_bin])/total[pt_bin][eta_bin])
		print(ratios[pt_bin][eta_bin],f"{pt_bin},{eta_bin}")
		print(numerator[pt_bin][eta_bin],total[pt_bin][eta_bin])
		print(stat_err[pt_bin][eta_bin])

exit(0)


#  gROOT.SetStyle("ATLAS");
list_bin=[25,30,  35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250]
list_graph_x=[27.5,32.5,37.5,42.5,47.5,55,70,90,112.5,137.5,162.5,212.5]
list_error_x=[2.5,2.5,2.5,2.5,2.5,5,10,12.5,12.5,37.5]
list_eta=["|#eta|<=0.6","0.6<=|#eta|<=0.8","0.8<=|#eta|<=1.15","1.15<=|#eta|<=1.37","1.37<=|#eta|<=1.52","1.52<=|#eta|<=1.81","1.81<=|#eta|<=2.01","2.01<=|#eta|<=2.37","2.37<=|#eta|<=2.47"]


canvas=ROOT.TCanvas("canvas","",800,600)
canvas.cd()


canvas.Print("converted.pdf"+"[")

for eta in range(len(list_eta)):
	#  if eta==3 or eta ==7 :
		#  continue
	

	histogram = ROOT.TH1F("histogram","efficiency",12,np.array(list_bin,np.float32))

	for i in range(histogram.GetNbinsX()):
		histogram.SetBinContent(i+1,ratios[i][eta])
		histogram.SetBinError(i+1,stat_err[i][eta],)

	histogram.GetXaxis().SetTitle("pt[Gev]")
	histogram.GetYaxis().SetTitle("ID efficiency")
	#  histogram.SetMarkerstyle(20)
	#  histogram.
	histogram.Draw("h")

	legend=ROOT.TLegend(0.5,0.6,0.7,0.7)
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	legend.SetTextSize(0.03)
	legend.AddEntry(histogram,"EE->photon","L")
	legend.Draw()

	pt = ROOT.TPaveText(0.5, 0.5, 0.9, 0.7, "ndc")
	pt.SetTextSize(0.035)
	pt.SetBorderSize(0)
	pt.SetFillStyle(0)
	pt.SetTextAlign(13)
	pt.SetTextFont(42)
	pt.AddText(f"converted,{list_eta[eta]}")
	#  pt.AddText(list_eta[eta])
	pt.Draw()

	canvas.Print("converted.pdf")

canvas.Print("converted.pdf"+"]")

	#  canvas.SaveAs("123.png")
