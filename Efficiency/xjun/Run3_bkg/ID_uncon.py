import re
import ROOT
from ROOT import gDirectory,TFile,gROOT
#  from ROOT import TFile
import math
import numpy as np
import argparse

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()

with open('/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/PhotonIsEMTightSelectorCutDefs.conf','r') as file1:
	lines = file1.readlines()

converted_lines = []
unconverted_lines = []



start_line=39
for i,line in enumerate(lines):
	if i >=start_line:
		if "photonsConverted" in line :
			converted_lines.append(line)
		elif "photonsNonConverted" in line:
			unconverted_lines.append(line)


old_name_con = ["CutHadLeakage_photonsConverted" ,"Reta37_photonsConverted" ,"Rphi33_photonsConverted" ,"weta2_photonsConverted" ,"deltae_photonsConverted" ,"DEmaxs1_photonsConverted" ,"wtot_photonsConverted" ,"fracm_photonsConverted" ,"w1_photonsConverted"]
old_name_unc = ["CutHadLeakage_photonsNonConverted" ,"Reta37_photonsNonConverted" ,"Rphi33_photonsNonConverted" ,"weta2_photonsNonConverted" ,"deltae_photonsNonConverted" ,"DEmaxs1_photonsNonConverted" ,"wtot_photonsNonConverted" ,"fracm_photonsNonConverted" ,"w1_photonsNonConverted"]

new_name = ["rhad1 or rhad0 " ,"r_eta" ,"r_phi" ,"w_eta2" ,"delta_E" ,"e_ratio" ,"w_stot" ,"fracs1" ,"w_eta1"]

con_uncon_files=[converted_lines,unconverted_lines]
file_names=["converted_lines.txt","unconverted_lines.txt"]

#  for name in range(2):
#      #  converted_file=open("converted_lines.txt","w")
#      con_uncon_file=open(file_names[name],"w")
#      for i,line in enumerate(con_uncon_files[name]):
#          line = line.strip()
#          split_line=line.split(";")
#          new_line = ";".join(split_line[:-1])
#          if ((i-10)%11==0  ) :
#              new_line += "; 9999"
#          new_line=new_line.replace(':',';')
#          new_line=new_line.replace('+','')
#          con_uncon_file.write(new_line +"\n")
#      con_uncon_file.close()
#  file1.close()

with open("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/xjun/Run3_bkg/unconverted_lines.txt","r") as file2:
	lines2=file2.readlines()

#name--eta-pt = 9-9-11
values=[[[0 for k in range(11)] for j in range(9)] for i in range(9)]
name_number=[0 for i in range(len(old_name_unc))]
for cutname,line in enumerate(lines2):
	line=line.strip()
	split_line=line.split(";")
	if split_line[0]  in old_name_unc:
		#  print(split_line[0])
		index=old_name_unc.index(split_line[0])
		name_number[index]+=1
		for column in range(1,10):
			values[index][int(column-1)][int(name_number[index]-1)]=float(split_line[column])
#  print(values[0][7][0])
#  print(values[0][7])

#  values=values.
file2.close()

parser = argparse.ArgumentParser(description='')																							   
				 
parser.add_argument('-in', '--input', dest="input_file", required=True, help='input file')
#  parser.add_argument('-t', '--type', dest="type", required=True, help='data or mc')
parser.add_argument('-oAL','--outputAL',dest='outputAL',required=True,help='location hist for not ID cut saved')
parser.add_argument('-oID','--outputID',dest='outputID',required=True,help='location hist for ID cut saved')
#  parser.add_argument('-v','--variable',dest='variable',required=True,help=' cut single variable')
				 
args = parser.parse_args()

#  file_input=ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/con1_fudge/apply_ST/unconverted.root","READ")
file_input=ROOT.TFile(args.input_file,"READ")

mychain=file_input.Get("ZeeCandidates")
#  mychain=gDirectory.Get("ZeeCandidates")
entries=mychain.GetEntriesFast()
print('entries =', entries)
#  if entries > 100000:
	#  entries = 100000

numerator=[[0 for i in range(9)]for j in range(12)]
total=[[0 for i in range(9)]for j in range(12)]
ratios=[[0 for i in range(9)]for j in range(12)]
stat_err=[[0 for i in range(9)]for j in range(12)]


seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]
twelve_pt_cut_name=['25<P_{t}<30','30<P_{t}<35','35<P_{t}<40','40<P_{t}<45','45<P_{t}<50','50<P_{t}<60','60<P_{t}<80','80<P_{t}<100','100<P_{t}<125','125<P_{t}<150','150<P_{t}<175','175<P_{t}<250']
				 
hists_names_AL =[[0 for i in range(12)] for j in range(7)]
hists_names_ID =[[0 for i in range(12)] for j in range(7)]
hists_AL =[[0 for i in range(12)] for j in range(7)]
hists_ID =[[0 for i in range(12)] for j in range(7)]
				 
for i in range(7):
	for j in range(12):
		hists_names_AL[i][j] = 'AL_hist' +'_' +seven_eta_cut_name[i] +'_' + twelve_pt_cut_name[j]
		hists_names_ID[i][j] = 'ID_hist' +'_' +seven_eta_cut_name[i] +'_' + twelve_pt_cut_name[j]
		hists_AL[i][j] = ROOT.TH1F(hists_names_AL[i][j], hists_names_AL[i][j],100,50,130)
		hists_ID[i][j] = ROOT.TH1F(hists_names_ID[i][j], hists_names_ID[i][j],100,50,130)

#  entries =1000
for jentry in range(entries):
	#  print(values[1][7])
	#  ientry=mychain.LoadTree(jentry)
	mychain.GetEntry(jentry)
	if jentry % 10000 == 0:
		print('jentry =', jentry)

	mee = mychain.mee
	eta=mychain.eta
	phi=mychain.phi
	pt=mychain.pt
	primary_cluster_be2_eta=mychain.primary_cluster_be2_eta
	r_had0=mychain.r_had0
	r_had1=mychain.r_had1
	e_ratio=mychain.e_ratio
	delta_E=mychain.delta_E
	w_eta2=mychain.w_eta2
	w_eta1=mychain.w_eta1
	r_eta=mychain.r_eta
	r_phi=mychain.r_phi
	w_stot=mychain.w_stot
	f1=mychain.f1
	fracs1=mychain.fracs1
	f3=mychain.f3
	ptcone20=mychain.ptcone20
	topoetcone20=mychain.topoetcone20
	#  E277=mychain.E277


	pt_bin1 = (pt > 25 and	pt < 30)
	pt_bin2 = (pt > 30 and	pt < 35)
	pt_bin3 = (pt > 35 and	pt < 40)
	pt_bin4 = (pt > 40 and	pt < 45)
	pt_bin5 = (pt > 45 and	pt < 50)
	pt_bin6 = (pt > 50 and	pt < 60)
	pt_bin7 = (pt > 60 and	pt < 80)
	pt_bin8 = (pt > 80 and	pt < 100)
	pt_bin9 = (pt > 100 and  pt < 125)
	pt_bin10 = (pt > 125 and  pt < 150)
	pt_bin11 = (pt > 150 and  pt < 175)
	pt_bin12 = (pt > 175 and  pt < 250)

	eta_bin1 = (abs(primary_cluster_be2_eta) > 0 and abs(primary_cluster_be2_eta) < 0.6 )
	eta_bin2 = (abs(primary_cluster_be2_eta) > 0.6 and abs(primary_cluster_be2_eta) < 0.8 )
	eta_bin3 = (abs(primary_cluster_be2_eta) > 0.8 and abs(primary_cluster_be2_eta) < 1.15 )
	eta_bin4 = (abs(primary_cluster_be2_eta) > 1.15 and abs(primary_cluster_be2_eta) < 1.37 )
	eta_bin5 = (abs(primary_cluster_be2_eta) > 1.52 and abs(primary_cluster_be2_eta) < 1.81 )
	eta_bin6 = (abs(primary_cluster_be2_eta) > 1.81 and abs(primary_cluster_be2_eta) < 2.01 )
	eta_bin7 = (abs(primary_cluster_be2_eta) > 2.01 and abs(primary_cluster_be2_eta) < 2.37 )
	
	pt_bins=[pt_bin1,pt_bin2,pt_bin3,pt_bin4,pt_bin5,pt_bin6,pt_bin7,pt_bin8,pt_bin9,pt_bin10,pt_bin11,pt_bin12]
	#  print(pt_bins)
	eta_bins=[eta_bin1,eta_bin2,eta_bin3,eta_bin4,False,eta_bin5,eta_bin6,eta_bin7,False]
	for pt_bin in range(len(pt_bins)):
		if not pt_bins[pt_bin]:
			continue
		for eta_bin in range(len(eta_bins)):
			if not eta_bins[eta_bin]:
				continue

			eta_bin_series = 0
			if eta_bin >=5:
				eta_bin_series = eta_bin -1
			else:
				eta_bin_series = eta_bin


			total[pt_bin][eta_bin]+=1
			if(eta_bin==2 or eta_bin==3):
				r_had1=r_had0
			if(pt_bin<=7):

				hists_AL[eta_bin_series][pt_bin].Fill(mee)

				if((r_had1<values[0][eta_bin][int(pt_bin+3)])and(r_eta>values[1][eta_bin][int(pt_bin+3)])and(r_phi>values[2][eta_bin][int(pt_bin+3)])and(w_eta2<values[3][eta_bin][int(pt_bin+3)])and(delta_E<values[4][eta_bin][int(pt_bin+3)])and(e_ratio>values[5][eta_bin][int(pt_bin+3)])and(w_stot<values[6][eta_bin][int(pt_bin+3)])and(fracs1<values[7][eta_bin][int(pt_bin+3)])and(w_eta1<values[8][eta_bin][int(pt_bin+3)])):
					numerator[pt_bin][eta_bin]+=1
					hists_ID[eta_bin_series][pt_bin].Fill(mee)
			else:
				hists_AL[eta_bin_series][pt_bin].Fill(mee)

				if((r_had1<values[0][eta_bin][10])and(r_eta>values[1][eta_bin][10])and(r_phi>values[2][eta_bin][10])and(w_eta2<values[3][eta_bin][10])and(delta_E<values[4][eta_bin][10])and(e_ratio>values[5][eta_bin][10])and(w_stot<values[6][eta_bin][10])and(fracs1<values[7][eta_bin][10])and(w_eta1<values[8][eta_bin][10])):



					numerator[pt_bin][eta_bin]+=1
					hists_ID[eta_bin_series][pt_bin].Fill(mee)



#  for eta_bin in range(len(eta_bins)):
#	   for pt_bin in range(len(pt_bins)):
#		   if total[pt_bin][eta_bin]==0:
#			   ratios[pt_bin][eta_bin]=0
#			   stat_err[pt_bin][eta_bin]=1e6
#		   else:
#			   ratios[pt_bin][eta_bin]=numerator[pt_bin][eta_bin]/total[pt_bin][eta_bin]
#			   stat_err[pt_bin][eta_bin]=math.sqrt(ratios[pt_bin][eta_bin]*(1-ratios[pt_bin][eta_bin])/total[pt_bin][eta_bin])
#		   print(ratios[pt_bin][eta_bin],f"{pt_bin},{eta_bin}")
#		   print(numerator[pt_bin][eta_bin],total[pt_bin][eta_bin])
#		   print(stat_err[pt_bin][eta_bin])
#
#  #  gROOT.SetStyle("ATLAS");
#  list_bin=[25,30,  35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250]
#  list_graph_x=[27.5,32.5,37.5,42.5,47.5,55,70,90,112.5,137.5,162.5,212.5]
#  list_eta=["|#eta|<=0.6","0.6<=|#eta|<=0.8","0.8<=|#eta|<=1.15","1.15<=|#eta|<=1.37","1.37<=|#eta|<=1.52","1.52<=|#eta|<=1.81","1.81<=|#eta|<=2.01","2.01<=|#eta|<=2.37","2.37<=|#eta|<=2.47"]
#
#
#  canvas=ROOT.TCanvas("canvas","",800,600)
#  canvas.cd()
#
#
#  canvas.Print("unconverted.pdf"+"[")

out_hist_AL = ROOT.TFile(args.outputAL,"recreate")
out_hist_ID = ROOT.TFile(args.outputID,"recreate")
                 
for i in range(7):
    for j in range(12):
                 
        out_hist_AL.WriteTObject(hists_AL[i][j])
        out_hist_ID.WriteTObject(hists_ID[i][j])
                 
out_hist_AL.Close()
out_hist_ID.Close()
print(f" out_hist_AL is saved as {args.outputAL}")
print(f" out_hist_ID is saved as {args.outputID}")

#  for eta in range(len(list_eta)):
#      #  if eta==3 or eta ==7 :
#          #  continue
#
#          #  graph_bin=ROOT.TGraphErrors(len(list_graph_x),np.array(list_graph_x),np.array(sf_for_draw_sys_eff),np.ar    ray(err_for_x),np.array(sf_for_draw_sys_err))
#
#      histogram = ROOT.TH1F("histogram","efficiency",12,np.array(list_bin,np.float32))
#
#      for i in range(histogram.GetNbinsX()):
#          histogram.SetBinContent(i+1,ratios[i][eta])
#          histogram.SetBinError(i+1,stat_err[i][eta])
#
#      histogram.GetXaxis().SetTitle("pt[Gev]")
#      histogram.GetYaxis().SetTitle("ID efficiency")
#      #  histogram.SetMarkerstyle(20)
#      #  histogram.
#      histogram.Draw("h")
#
#      legend=ROOT.TLegend(0.5,0.6,0.7,0.7)
#      legend.SetFillStyle(0)
#      legend.SetBorderSize(0)
#      legend.SetTextSize(0.03)
#      legend.AddEntry(histogram,"EE->photon","L")
#      legend.Draw()
#
#      pt = ROOT.TPaveText(0.5, 0.5, 0.9, 0.7, "ndc")
#      pt.SetTextSize(0.035)
#      pt.SetBorderSize(0)
#      pt.SetFillStyle(0)
#      pt.SetTextAlign(13)
#      pt.SetTextFont(42)
#      pt.AddText(f"unconverted,{list_eta[eta]}")
#      #  pt.AddText(list_eta[eta])
#      pt.Draw()
#
#      canvas.Print("unconverted.pdf")
#
#  canvas.Print("unconverted.pdf"+"]")
#
#      #  canvas.SaveAs("123.png")
