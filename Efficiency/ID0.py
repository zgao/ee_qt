import re
import ROOT
from ROOT import gDirectory
from ROOT import TFile

with open('PhotonIsEMTightSelectorCutDefs.conf','r') as file1:
	   lines = file1.readlines()

converted_lines = []
unconverted_lines = []



start_line=39
for i,line in enumerate(lines):
	   if i >=start_line:
		   if "photonsConverted" in line :
			   converted_lines.append(line)
		   elif "photonsNonConverted" in line:
			   unconverted_lines.append(line)


old_name_con = ["CutHadLeakage_photonsConverted" ,"Reta37_photonsConverted" ,"Rphi33_photonsConverted" ,"weta2_photonsConverted" ,"deltae_photonsConverted" ,"DEmaxs1_photonsConverted" ,"wtot_photonsConverted" ,"fracm_photonsConverted" ,"w1_photonsConverted"]
old_name_unc = ["CutHadLeakage_photonsNonConverted" ,"Reta37_photonsNonConverted" ,"Rphi33_photonsNonConverted" ,"weta2_photonsNonConverted" ,"deltae_photonsNonConverted" ,"DEmaxs1_photonsNonConverted" ,"wtot_photonsNonConverted" ,"fracm_photonsNonConverted" ,"w1_photonsNonConverted"]

new_name = ["rhad1 or rhad0 " ,"r_eta" ,"r_phi" ,"w_eta2" ,"delta_E" ,"e_ratio" ,"w_stot" ,"fracs1" ,"w_eta1"]
converted_file=open("converted_lines.txt","w")
for i,line in enumerate(converted_lines):
	   line = line.strip()
	   split_line=line.split(";")
	   new_line = ";".join(split_line[:-1])
	   if ((i-10)%11==0  ) :
		   new_line += "; 9999"

	   new_line=new_line.replace(':',';')
	   new_line=new_line.replace('+','')
	   converted_file.write(new_line +"\n")
converted_file.close()
file1.close()

with open("converted_lines.txt","r") as file2:
	   lines2=file2.readlines()

#name--eta-pt = 9-9-11
values=[[[0 for k in range(11)] for j in range(9)] for i in range(9)]
name_number=[0 for i in range(len(old_name_con))]
for cutname,line in enumerate(lines2):
	   line=line.strip()
	   split_line=line.split(";")
	   if split_line[0]  in old_name_con:
		   index=old_name_con.index(split_line[0])
		   name_number[index]+=1
	   for column in range(1,10):
		   values[index][int(column-1)][int(name_number[index]-1)]=float(split_line[column])
print(values[0][7][0])
print(values[0][7])

#  values=values.
file2.close()


file_input=ROOT.TFile("~/eos/con1_fudge/apply_ST/converted.root","READ")
#  tree=file_input.Get("ZeeCandidates")
mychain=gDirectory.Get("ZeeCandidates")
entries=mychain.GetEntriesFast()

numerator=[[0 for i in range(9)]for j in range(12)]
total=[[0 for i in range(9)]for j in range(12)]
ratios=[[0 for i in range(9)]for j in range(12)]

for jentry in range(entries):
	#  print(values[1][7])
	ientry=mychain.LoadTree(jentry)
	if ientry<0:
		break
	#  if ientry>1e6:
		#  break
	
	nb=mychain.GetEntry(jentry)
	if nb<=0:
		continue
	if(jentry%100000==0):
		print(jentry)
	eta=mychain.eta
	phi=mychain.phi
	pt=mychain.pt
	primary_cluster_be2_eta=mychain.primary_cluster_be2_eta
	r_had0=mychain.r_had0
	r_had1=mychain.r_had1
	e_ratio=mychain.e_ratio
	delta_E=mychain.delta_E
	w_eta2=mychain.w_eta2
	w_eta1=mychain.w_eta1
	r_eta=mychain.r_eta
	r_phi=mychain.r_phi
	w_stot=mychain.w_stot
	f1=mychain.f1
	fracs1=mychain.fracs1
	f3=mychain.f3
	ptcone20=mychain.ptcone20
	topoetcone20=mychain.topoetcone20
	E277=mychain.E277


	pt_bin1 = (pt > 25 and	pt < 30)
	pt_bin2 = (pt > 30 and	pt < 35)
	pt_bin3 = (pt > 35 and	pt < 40)
	pt_bin4 = (pt > 40 and	pt < 45)
	pt_bin5 = (pt > 45 and	pt < 50)
	pt_bin6 = (pt > 50 and	pt < 60)
	pt_bin7 = (pt > 60 and	pt < 80)
	pt_bin8 = (pt > 80 and	pt < 100)
	pt_bin9 = (pt > 100 and  pt < 125)
	pt_bin10 = (pt > 125 and  pt < 150)
	pt_bin11 = (pt > 150 and  pt < 175)
	pt_bin12 = (pt > 175 and  pt < 250)

	eta_bin1 = (abs(primary_cluster_be2_eta) > 0 and abs(primary_cluster_be2_eta) < 0.6 )
	eta_bin2 = (abs(primary_cluster_be2_eta) > 0.6 and abs(primary_cluster_be2_eta) < 0.8 )
	eta_bin3 = (abs(primary_cluster_be2_eta) > 0.8 and abs(primary_cluster_be2_eta) < 1.15 )
	eta_bin4 = (abs(primary_cluster_be2_eta) > 1.15 and abs(primary_cluster_be2_eta) < 1.37 )
	eta_bin5 = (abs(primary_cluster_be2_eta) > 1.52 and abs(primary_cluster_be2_eta) < 1.81 )
	eta_bin6 = (abs(primary_cluster_be2_eta) > 1.81 and abs(primary_cluster_be2_eta) < 2.01 )
	eta_bin7 = (abs(primary_cluster_be2_eta) > 2.01 and abs(primary_cluster_be2_eta) < 2.37 )
	
	pt_bins=[pt_bin1,pt_bin2,pt_bin3,pt_bin4,pt_bin5,pt_bin6,pt_bin7,pt_bin8,pt_bin9,pt_bin10,pt_bin11,pt_bin12]
	#  print(pt_bins)
	eta_bins=[eta_bin1,eta_bin2,eta_bin3,eta_bin4,False,eta_bin5,eta_bin6,eta_bin7,False]
	for pt_bin in range(len(pt_bins)):
		#  if pt_bin!=0 :
		#      continue
		for eta_bin in range(len(eta_bins)):
			#  if eta_bin !=0:
			#      continue
			#  if eta_bin ==4 or eta_bin ==8 :
				#  continue
			if(pt_bins[pt_bin] and eta_bins[eta_bin]):

				total[pt_bin][eta_bin]+=1
				#  if(pt_bin>=7):pt_bin=7
				if(eta_bin==2 or eta_bin==3):
					r_had1=r_had0
				if(pt_bin<=7):
					if((r_had1<values[0][eta_bin][int(pt_bin+3)])and(r_eta>values[1][eta_bin][int(pt_bin+3)])and(r_phi>values[2][eta_bin][int(pt_bin+3)])and(w_eta2<values[3][eta_bin][int(pt_bin+3)])and(delta_E<values[4][eta_bin][int(pt_bin+3)])and(e_ratio>values[5][eta_bin][int(pt_bin+3)])and(w_stot<values[6][eta_bin][int(pt_bin+3)])and(fracs1<values[7][eta_bin][int(pt_bin+3)])and(w_eta1<values[8][eta_bin][int(pt_bin+3)])):
				#  numerator+=1
						numerator[pt_bin][eta_bin]+=1
				if(pt_bin>7):
					if((r_had1<values[0][eta_bin][10])and(r_eta>values[1][eta_bin][10])and(r_phi>values[2][eta_bin][10])and(w_eta2<values[3][eta_bin][10])and(delta_E<values[4][eta_bin][10])and(e_ratio>values[5][eta_bin][10])and(w_stot<values[6][eta_bin][10])and(fracs1<values[7][eta_bin][10])and(w_eta1<values[8][eta_bin][10])):
						numerator[pt_bin][eta_bin]+=1
			#  ratio[pt=numerator/total



for eta_bin in range(len(eta_bins)):
	for pt_bin in range(len(pt_bins)):
		if total[pt_bin][eta_bin]==0:
			ratios[pt_bin][eta_bin]=0
		else:
			ratios[pt_bin][eta_bin]=numerator[pt_bin][eta_bin]/total[pt_bin][eta_bin]
		print(ratios[pt_bin][eta_bin],f"{pt_bin} {eta_bin}")
		print(numerator[pt_bin][eta_bin])
		print(total[pt_bin][eta_bin])

			
					
     #  if(pt_bin1 and eta_bin1):
	 #     total+=1
	 #     if((r_had1<0.0260)and(r_eta>0.919)and(r_phi>0.57)and(w_eta2<0.0117)and(delta_E<198)and(e_ratio>0.866)and(w_stot<3.02)and(fracs1<0.361)and(w_eta1<0.692)):
	 #         numerator+=1
#  print(numerator)
#  print(total)
#  print(numerator/total)

