#!/bin/bash
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/75_80_con.txt -c converted -pdf bkg_systematic_uncertainty/75_80_con.pdf -root bkg_systematic_uncertainty/75_80_con.root
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/75_80_unc.txt -c unconverted -pdf bkg_systematic_uncertainty/75_80_unc.pdf -root bkg_systematic_uncertainty/75_80_unc.root


# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/frag_down/80_100_con_frag_down_uncertainty.txt -c converted -pdf bkg_systematic_uncertainty/frag_down/80_100_con_frag_down.pdf -root bkg_systematic_uncertainty/frag_down/80_100_con_frag_down.root
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/frag_down/80_100_unc_frag_down_uncertainty.txt -c unconverted -pdf bkg_systematic_uncertainty/frag_down/80_100_unc_frag_down.pdf -root bkg_systematic_uncertainty/frag_down/80_100_unc_frag_down.root
#
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/frag_up/80_100_con_frag_up_uncertainty.txt  -c converted -pdf bkg_systematic_uncertainty/frag_up/80_100_con_frag_up.pdf -root bkg_systematic_uncertainty/frag_up/80_100_con_frag_up.root
# python3 draw_bkg_sys.py -in bkg_systematic_uncertainty/frag_up/80_100_unc_frag_up_uncertainty.txt  -c unconverted -pdf bkg_systematic_uncertainty/frag_up/80_100_unc_frag_up.pdf -root bkg_systematic_uncertainty/frag_up/80_100_unc_frag_up.root

# python3 draw_bkg_sys.py -in truth_pt_subtract_con.txt -c converted   -pdf converted.pdf -root converted.root
# python3 draw_bkg_sys.py -in truth_pt_subtract_unc.txt -c unconverted -pdf unconverted.pdf -root unconverted.root

python3 draw_bkg_sys.py -in converted_final_uncertainty_for_background.txt -c converted   -pdf converted_bkg_sys.pdf -root converted_bkg_sys.root
python3 draw_bkg_sys.py -in unconverted_final_uncertainty_for_background.txt -c unconverted -pdf unconverted_bkg_sys.pdf -root unconverted_bkg_sys.root
