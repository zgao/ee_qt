import ROOT
with open("dataconvIDEF_all_final.txt","r") as file:
	lines = file.readlines()
labels = []
ratios = []
errors = []
first_value =[]
second_value =[]
for i, line in enumerate(lines):

	values =line.split()
	#  if 1%13 ==0:
		#  labels.append(line.strip())
	#  if "eta" in values:
	if i %13 ==0:
		continue
	if len(values)>=2:
		first_value.append(values[0])
		second_value.append(values[1])


#  print(len(first_value))
final_first_value = [0 for i in range(12*4)]
for i in range(12):
	#  print(first_value[i])
	final_first_value[i]=float(first_value[i])
	final_first_value[int(i+12)] = float(first_value[int(12+i)]) + float(first_value[int(12*2+i)]) + float(first_value[int(12*3+i)])
	final_first_value[int(i+12*2)] = float(first_value[int(12*4+i)])
	final_first_value[int(i+12*3)] = float(first_value[int(12*5+i)]) + float(first_value[int(12*6+i)])

final_second_value = [0 for i in range(12*4)]
for i in range(12):

	final_second_value[i]=float(second_value[i])
	final_second_value[int(i+12)] = float(second_value[int(12+i)]) + float(second_value[int(12*2+i)]) + float(second_value[int(12*3+i)])
	final_second_value[int(i+12*2)] = float(second_value[int(12*4+i)])
	final_second_value[int(i+12*3)] = float(second_value[int(12*5+i)]) + float(second_value[int(12*6+i)])

print(final_second_value)

eta_names =["0.00<|#eta|<0.60","0.60<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.37"]

with open("new_dataconvIDEF_all_final.txt","w") as file2:
	for i in range(4):
		file2.write(f"{eta_names[i]}\n")
		for j in range(12):
			file2.write(f"{final_first_value[int(i*12 +j)]}\t")
			file2.write(f"{final_second_value[int(i*12 +j)]}\n")
file2.close()
file.close()
				


