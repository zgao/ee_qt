import numpy as np
import math
import ROOT
import sys
import argparse

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()


ROOT.gStyle.SetOptStat(0)


parser = argparse.ArgumentParser(description='')
parser.add_argument('-conpdf', '--conpdf', dest="conpdf_file", required=True, help='con pdf file')
parser.add_argument('-uncpdf', '--uncpdf', dest="uncpdf_file", required=True, help='unc pdf file')
parser.add_argument('-conroot', '--conroot', dest="conroot_file", required=True, help='con root file')
parser.add_argument('-uncroot', '--uncroot', dest="uncroot_file", required=True, help='unc root file')
args = parser.parse_args()


list_bin=[25,30,  35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250]
list_graph_x=[27.5,32.5,37.5,42.5,47.5,55,70,90,112.5,137.5,162.5,212.5]
seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]
#  seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.37"]


#  file1 = ROOT.TFile("data_electron/con/four/converted.root","READ")
#  file1 = ROOT.TFile("data_electron/con/four/nov_21/converted.root","READ")
#  file2 = ROOT.TFile("data_electron/unc/four/unconverted.root","READ")
#  file2 = ROOT.TFile("data_electron/unc/four/nov_21/unconverted.root","READ")

#  file3 = ROOT.TFile("zee_with_weight/four/both_pass/weight_weight_converted.root","READ")
#  file3 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/MC_smirnoved/con/converted.root","READ")
#  file3 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2_final/con/converted.root","READ")
file3 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted.root","READ")
#  file5 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2/con/converted.root","READ")
file5 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/data/con/final_new/converted.root","READ")
#  file5 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2_not/con/converted.root","READ")
#  file5 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/ligang_MC_smirnoved/con/converted.root","READ")
#  file5 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_pileweight/con/converted.root","READ")
#  file4 = ROOT.TFile("zee_with_weight/four/both_pass/weight_weight_unconverted.root","READ")
#  file4 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/MC_smirnoved/unc/unconverted.root","READ")
#  file4 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2_final/unc/unconverted.root","READ")
file4 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted.root","READ")
#  file6 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2/unc/unconverted.root","READ")
file6 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/data/unc/final_new/unconverted.root","READ")
#  file6 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2_not/unc/unconverted.root","READ")
#  file6 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/ligang_MC_smirnoved/unc/unconverted.root","READ")
#  file6 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_pileweight/unc/unconverted.root","READ")

file7 = ROOT.TFile(args.conroot_file,"READ")
file8 = ROOT.TFile(args.uncroot_file,"READ")
#  key_list = file1.GetListOfKeys()
key_list = file3.GetListOfKeys()

canvas=ROOT.TCanvas("canvas","",800,600)
#  canvas.Print("converted_all.pdf"+"[")
canvas.Print(args.conpdf_file+"[")

canvas2=ROOT.TCanvas("canvas2","",800,600)
canvas2.Print(args.uncpdf_file+"[")


file_bkg_substract_con = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/bkg_systematic_uncertainty/75_80_con.root","READ")
file_bkg_substract_unc = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/bkg_systematic_uncertainty/75_80_unc.root","READ")
def final_errors(hist):
	list1 = []
	for i in range(hist.GetNbinsX()):
		list1.append(hist.GetBinError(i+1))
	return list1

def final_errors_2(hist):
	list1 = []
	for i in range(hist.GetNbinsX()):
		list1.append(hist.GetBinContent(i+1))
	return list1
for i in range(7):
#  for i in range(4):
	#  hist1 = file1.Get(f"histogram;{int(i+1)}")
	#  hist2 = file2.Get(f"histogram;{int(i+1)}")
	hist3 = file3.Get(f"histogram;{int(i+1)}")
	hist4 = file4.Get(f"histogram;{int(i+1)}")
	hist5 = file5.Get(f"histogram;{int(i+1)}")
	hist6 = file6.Get(f"histogram;{int(i+1)}")
	hist7 = file7.Get(f"histogram;{int(i+1)}")
	hist8 = file8.Get(f"histogram;{int(i+1)}")
	hist_bkg_con = file_bkg_substract_con.Get(f"histogram;{int(i+1)}")
	hist_bkg_unc = file_bkg_substract_unc.Get(f"histogram;{int(i+1)}")

	stat_error_con = final_errors(hist5)
	stat_error_unc = final_errors(hist6)
	bkg_con = final_errors_2(hist_bkg_con)
	bkg_unc = final_errors_2(hist_bkg_unc)

	#  print(hist_bkg_con.GetNbinsX())
	print(stat_error_con,bkg_con)

	values7 =[ hist7.GetBinContent(j+1) for j in range(hist3.GetNbinsX())]
	values8 =[ hist8.GetBinContent(j+1) for j in range(hist4.GetNbinsX())]
	values5 =[ hist5.GetBinContent(j+1) for j in range(hist5.GetNbinsX())]
	values6 =[ hist6.GetBinContent(j+1) for j in range(hist6.GetNbinsX())]

	eratio_con = [ values5[j]/values7[j] for j in range(hist3.GetNbinsX())]
	eratio_unc = [ values6[j]/values8[j] for j in range(hist3.GetNbinsX())]

	sf_error_stat_con = [ eratio_con[j] * math.sqrt((hist5.GetBinError(j+1) / hist5.GetBinContent(j+1) )**2 + (hist7.GetBinError(j+1)/hist7.GetBinContent(j+1))**2) for j in range(hist3.GetNbinsX())]
	sf_error_bkg_con = [ eratio_con[j] * bkg_con[j]/hist5.GetBinContent(j+1) for j in range(hist3.GetNbinsX())]
	sf_error_con_total = [math.sqrt(sf_error_stat_con[j]**2 + sf_error_bkg_con[j]**2) for j in range(hist3.GetNbinsX())]
	
	sf_error_stat_unc = [ eratio_unc[j] * math.sqrt((hist6.GetBinError(j+1) / hist6.GetBinContent(j+1) )**2 + (hist8.GetBinError(j+1)/hist8.GetBinContent(j+1))**2) for j in range(hist4.GetNbinsX())]
	sf_error_bkg_unc = [ eratio_unc[j] * bkg_unc[j]/hist6.GetBinContent(j+1) for j in range(hist4.GetNbinsX())]
	sf_error_unc_total = [math.sqrt(sf_error_stat_unc[j]**2 + sf_error_bkg_unc[j]**2) for j in range(hist3.GetNbinsX())]

	err_for_x = [2.5, 2.5, 2.5, 2.5, 2.5, 5, 10, 10, 12.5, 12.5, 12.5, 37.5]
	list_graph_x =[27.5, 32.5, 37.5, 42.5, 47.5, 55, 70, 90, 112.5, 137.5, 162.5, 212.5]

	line = ROOT.TLine(25,1,250,1)
	line.SetLineWidth(1)
	line.SetLineStyle(2)
	

	graph_con = ROOT.TGraphErrors(12, np.array(list_graph_x), np.array(eratio_con), np.array(err_for_x), np.array(sf_error_con_total))
	graph_unc = ROOT.TGraphErrors(12, np.array(list_graph_x), np.array(eratio_unc), np.array(err_for_x), np.array(sf_error_unc_total))

	final_error_con = [math.sqrt(stat_error_con[j]**2 + bkg_con[j]**2) for j in range(hist_bkg_con.GetNbinsX())]
	print(final_error_con)
	final_error_unc = [math.sqrt(stat_error_unc[j]**2 + bkg_unc[j]**2) for j in range(hist_bkg_unc.GetNbinsX())]
	for j in range(hist_bkg_con.GetNbinsX()):
		hist5.SetBinError(j+1,final_error_con[j])
		hist6.SetBinError(j+1,final_error_unc[j])

	h_con = ROOT.TH2F("h_con","h_con",500,25,250,2,0.9,1.1)
	h_con.SetTitle("")
	h_con.GetXaxis().SetTitle("P_{T} [GeV]")
	h_con.GetYaxis().SetTitle("SF")
	h_con.GetYaxis().CenterTitle()
	h_con.GetXaxis().SetTitleSize(0.13)
	h_con.GetXaxis().SetLabelSize(0.1)
	h_con.GetYaxis().SetTitleSize(0.10)
	h_con.GetYaxis().SetTitleOffset(0.5)
	h_con.GetXaxis().SetTitleOffset(0.9)
	h_con.GetYaxis().SetLabelSize(0.1)
	h_con.GetYaxis().SetNdivisions(203)

	h_unc = ROOT.TH2F("h_unc","h_unc",500,25,250,2,0.9,1.1)
	h_unc.SetTitle("")
	h_unc.GetXaxis().SetTitle("P_{T} [GeV]")
	h_unc.GetYaxis().SetTitle("SF")
	h_unc.GetYaxis().CenterTitle()
	h_unc.GetXaxis().SetTitleSize(0.13)
	h_unc.GetXaxis().SetLabelSize(0.1)
	h_unc.GetYaxis().SetTitleSize(0.10)
	h_unc.GetYaxis().SetTitleOffset(0.5)
	h_unc.GetXaxis().SetTitleOffset(0.9)
	h_unc.GetYaxis().SetLabelSize(0.1)
	h_unc.GetYaxis().SetNdivisions(203)


	canvas.cd()


	pad_top_con = ROOT.TPad("pad_top_con", "", 0, 0.3, 1, 1)
	pad_top_con.Draw()

	pad_bot_con = ROOT.TPad("pad_bot_con", "", 0, 0, 1, 0.29)
	pad_bot_con.Draw()

	pad_top_con.cd()
	pad_top_con.SetFillColor(0)
	pad_top_con.SetBorderMode(0)
	pad_top_con.SetBorderSize(2)
	pad_top_con.SetTickx(1)
	pad_top_con.SetTicky(1)
	pad_top_con.SetLeftMargin(0.12)
	pad_top_con.SetRightMargin(0.05)
	pad_top_con.SetTopMargin(0.05)
	pad_top_con.SetRightMargin(0.03)
	pad_top_con.SetBottomMargin(0.01)
	pad_top_con.SetFrameBorderMode(0)
	pad_top_con.SetFrameBorderMode(0)



	hist3.Draw("h,e")
	hist3.GetYaxis().SetTitleOffset(1)
	hist3.SetLineColor(ROOT.kBlue)
	hist3.SetMarkerColor(ROOT.kBlue)
	hist5.Draw("h,same")
	hist5.SetLineColor(ROOT.kBlack)
	hist5.SetMarkerColor(ROOT.kBlack)
	hist7.Draw("h,same")
	hist7.SetLineColor(ROOT.kGreen)
	hist7.SetMarkerColor(ROOT.kGreen)
	hist7.SetMarkerSize(0)
	legend = ROOT.TLegend(0.6,0.5,0.9,0.7)
	legend.AddEntry(hist3," zee MC ","l")
	legend.AddEntry(hist5," zee data ","l")
	legend.AddEntry(hist7," photon MC ","l")
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	legend.Draw("same")
	text1 = ROOT.TPaveText(150,0.6,200,0.7)
	text1.AddText("converted")
	text1.AddText(seven_eta_cut_name[i])
	text1.SetFillStyle(0)
	text1.SetBorderSize(0)
	text1.SetTextSize(0.05)
	text1.Draw("same")

	pad_bot_con.cd()
	pad_bot_con.SetFillColor(0)
	pad_bot_con.SetBorderMode(0)
	pad_bot_con.SetBorderSize(2)
	#  pad_bot_con.SetLogx()
	pad_bot_con.SetTickx(1)
	pad_bot_con.SetTicky(1)
	pad_bot_con.SetLeftMargin(0.12)
	pad_bot_con.SetRightMargin(0.05)
	pad_bot_con.SetTopMargin(0.)
	pad_bot_con.SetRightMargin(0.03)
	pad_bot_con.SetBottomMargin(0.26)
	pad_bot_con.SetFrameBorderMode(0)
	pad_bot_con.SetFrameBorderMode(0)
	#  pad_bot_con.GetYaxis().SetRangeUser(0.9,1.1)
	#  graph_con.GetYaxis().SetRangeUser(0.95,1.05)
	h_con.Draw()
	line.Draw("same")
	#  h_con.GetXaxis().SetRangeUser(25,250)

	graph_con.Draw("P,same")



	canvas.Print(args.conpdf_file)

	canvas2.cd()
	pad_top_unc = ROOT.TPad("pad_top_unc", "", 0, 0.3, 1, 1)
	pad_top_unc.Draw()

	pad_bot_unc = ROOT.TPad("pad_bot_unc", "", 0, 0, 1, 0.29)
	pad_bot_unc.Draw()

	pad_top_unc.cd()
	pad_top_unc.SetFillColor(0)
	pad_top_unc.SetBorderMode(0)
	pad_top_unc.SetBorderSize(2)
	pad_top_unc.SetTickx(1)
	pad_top_unc.SetTicky(1)
	pad_top_unc.SetLeftMargin(0.12)
	pad_top_unc.SetRightMargin(0.05)
	pad_top_unc.SetTopMargin(0.05)
	pad_top_unc.SetRightMargin(0.03)
	pad_top_unc.SetBottomMargin(0.01)
	pad_top_unc.SetFrameBorderMode(0)
	pad_top_unc.SetFrameBorderMode(0)

	hist4.Draw("h,e")
	hist4.GetYaxis().SetTitleOffset(1)
	hist4.SetLineColor(ROOT.kBlue)
	hist4.SetMarkerColor(ROOT.kBlue)
	hist6.Draw("h,same")
	hist6.SetLineColor(ROOT.kBlack)
	hist6.SetMarkerColor(ROOT.kBlack)
	hist8.Draw("h,same")
	hist8.SetLineColor(ROOT.kGreen)
	hist8.SetMarkerColor(ROOT.kGreen)
	hist8.SetMarkerSize(0)
	legend2 = ROOT.TLegend(0.6,0.5,0.9,0.7)
	legend2.AddEntry(hist4," zee MC ","l")
	legend2.AddEntry(hist6," zee MC data ","l")
	legend2.AddEntry(hist8," photon MC ","l")
	legend2.SetFillStyle(0)
	legend2.SetBorderSize(0)
	legend2.Draw("same")
	text2 = ROOT.TPaveText(150,0.6,200,0.7)
	text2.AddText("unconverted")
	text2.AddText(seven_eta_cut_name[i])
	text2.SetFillStyle(0)
	text2.SetBorderSize(0)
	text2.SetTextSize(0.05)
	text2.Draw("same")

	pad_bot_unc.cd()
	pad_bot_unc.SetFillColor(0)
	pad_bot_unc.SetBorderMode(0)
	pad_bot_unc.SetBorderSize(2)
	#  pad_bot_unc.SetLogx()
	pad_bot_unc.SetTickx(1)
	pad_bot_unc.SetTicky(1)
	pad_bot_unc.SetLeftMargin(0.12)
	pad_bot_unc.SetRightMargin(0.05)
	pad_bot_unc.SetTopMargin(0.)
	pad_bot_unc.SetRightMargin(0.03)
	pad_bot_unc.SetBottomMargin(0.26)
	pad_bot_unc.SetFrameBorderMode(0)
	pad_bot_unc.SetFrameBorderMode(0)
	h_unc.Draw()
	line.Draw("same")
	graph_unc.Draw("P,same")
	#  graph_unc.GetYaxis().SetRangeUser(0.9,1.1)

	canvas2.Print(args.uncpdf_file)
canvas.Print(args.conpdf_file+"]")
canvas2.Print(args.uncpdf_file+"]")



