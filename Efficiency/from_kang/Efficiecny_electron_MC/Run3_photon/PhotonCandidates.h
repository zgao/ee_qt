//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jun  5 15:43:19 2024 by ROOT version 6.30/06
// from TTree PhotonCandidates/PhotonCandidates
// found on file: root://eoshome-z.cern.ch//eos/user/z/zgao/Run3_photon/rename/new_conversion/merge.root
//////////////////////////////////////////////////////////

#ifndef PhotonCandidates_h
#define PhotonCandidates_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class PhotonCandidates {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Bool_t          isTruthMatchedPhoton;
   Float_t         convRadius;
   Bool_t          photon_isTight;
   Bool_t          photon_isLoose;
   Int_t           truth_convType;
   Float_t         truth_pt;
   Double_t        mcTotWeight_1;
   Double_t        mcTotWeightNoPU_1;
   Double_t        mcTotWeightCheck_1;
   Double_t        mcTotWeightNoPU_PIDuse_1;
   Int_t           MC_channel_number;
   Double_t        MC_weight;
   Long_t          Event_number;
   Double_t        Lumi;
   Double_t        MC_event_weight;
   Float_t         pileupWeight;
   Float_t         vertexweight;
   Float_t         kFacrot;
   Float_t         xSec;
   Double_t        pt;
   Double_t        eta;
   Double_t        phi;
   Double_t        primary_cluster_be2_eta;
   Double_t        r_had0;
   Double_t        r_had1;
   Double_t        e_ratio;
   Double_t        e;
   Double_t        delta_E;
   Double_t        w_eta2;
   Double_t        w_eta1;
   Double_t        r_phi;
   Double_t        r_eta;
   Double_t        w_stot;
   Double_t        f1;
   Double_t        fracs1;
   Double_t        average_mu;
   Int_t           convType;
   Bool_t          FixedCutLoose;
   Bool_t          FixedCutTight;
   Float_t         E277;
   Float_t         ptcone20;
   Float_t         ptcone30;
   Float_t         ptcone40;
   Float_t         topoetcone20;
   Float_t         topoetcone30;
   Float_t         topoetcone40;
   Float_t         tetcone20;
   Float_t         tetcone30;
   Float_t         tetcone40;
   Double_t        XsecWeight;
   Double_t        SumWeights;
   Double_t        Yweight;
   Double_t        GE;

   // List of branches
   TBranch        *b_isTruthMatchedPhoton;   //!
   TBranch        *b_convRadius;   //!
   TBranch        *b_photon_isTight;   //!
   TBranch        *b_photon_isLoose;   //!
   TBranch        *b_truth_convType;   //!
   TBranch        *b_truth_pt;   //!
   TBranch        *b_mcTotWeight_1;   //!
   TBranch        *b_mcTotWeightNoPU_1;   //!
   TBranch        *b_mcTotWeightCheck_1;   //!
   TBranch        *b_mcTotWeightNoPU_PIDuse_1;   //!
   TBranch        *b_MC_channel_number;   //!
   TBranch        *b_MC_weight;   //!
   TBranch        *b_Event_number;   //!
   TBranch        *b_Lumi;   //!
   TBranch        *b_MC_event_weight;   //!
   TBranch        *b_pileupWeight;   //!
   TBranch        *b_vertexweight;   //!
   TBranch        *b_kFacrot;   //!
   TBranch        *b_xSec;   //!
   TBranch        *b_pt;   //!
   TBranch        *b_eta;   //!
   TBranch        *b_phi;   //!
   TBranch        *b_primary_cluster_be2_eta;   //!
   TBranch        *b_r_had0;   //!
   TBranch        *b_r_had1;   //!
   TBranch        *b_e_ratio;   //!
   TBranch        *b_e;   //!
   TBranch        *b_delta_E;   //!
   TBranch        *b_w_eta2;   //!
   TBranch        *b_w_eta1;   //!
   TBranch        *b_r_phi;   //!
   TBranch        *b_r_eta;   //!
   TBranch        *b_w_stot;   //!
   TBranch        *b_f1;   //!
   TBranch        *b_fracs1;   //!
   TBranch        *b_average_mu;   //!
   TBranch        *b_convType;   //!
   TBranch        *b_FixedCutLoose;   //!
   TBranch        *b_FixedCutTight;   //!
   TBranch        *b_E277;   //!
   TBranch        *b_ptcone20;   //!
   TBranch        *b_ptcone30;   //!
   TBranch        *b_ptcone40;   //!
   TBranch        *b_topoetcone20;   //!
   TBranch        *b_topoetcone30;   //!
   TBranch        *b_topoetcone40;   //!
   TBranch        *b_tetcone20;   //!
   TBranch        *b_tetcone30;   //!
   TBranch        *b_tetcone40;   //!
   TBranch        *b_XsecWeight;   //!
   TBranch        *b_SumWeights;   //!
   TBranch        *b_Yweight;   //!
   TBranch        *b_GE;   //!

   PhotonCandidates(TTree *tree=0);
   virtual ~PhotonCandidates();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef PhotonCandidates_cxx
PhotonCandidates::PhotonCandidates(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("root://eoshome-z.cern.ch//eos/user/z/zgao/Run3_photon/rename/new_conversion/merge.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("root://eoshome-z.cern.ch//eos/user/z/zgao/Run3_photon/rename/new_conversion/merge.root");
      }
      f->GetObject("PhotonCandidates",tree);

   }
   Init(tree);
}

PhotonCandidates::~PhotonCandidates()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t PhotonCandidates::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t PhotonCandidates::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void PhotonCandidates::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("isTruthMatchedPhoton", &isTruthMatchedPhoton, &b_isTruthMatchedPhoton);
   fChain->SetBranchAddress("convRadius", &convRadius, &b_convRadius);
   fChain->SetBranchAddress("photon_isTight", &photon_isTight, &b_photon_isTight);
   fChain->SetBranchAddress("photon_isLoose", &photon_isLoose, &b_photon_isLoose);
   fChain->SetBranchAddress("truth_convType", &truth_convType, &b_truth_convType);
   fChain->SetBranchAddress("truth_pt", &truth_pt, &b_truth_pt);
   fChain->SetBranchAddress("mcTotWeight_1", &mcTotWeight_1, &b_mcTotWeight_1);
   fChain->SetBranchAddress("mcTotWeightNoPU_1", &mcTotWeightNoPU_1, &b_mcTotWeightNoPU_1);
   fChain->SetBranchAddress("mcTotWeightCheck_1", &mcTotWeightCheck_1, &b_mcTotWeightCheck_1);
   fChain->SetBranchAddress("mcTotWeightNoPU_PIDuse_1", &mcTotWeightNoPU_PIDuse_1, &b_mcTotWeightNoPU_PIDuse_1);
   fChain->SetBranchAddress("MC_channel_number", &MC_channel_number, &b_MC_channel_number);
   fChain->SetBranchAddress("MC_weight", &MC_weight, &b_MC_weight);
   fChain->SetBranchAddress("Event_number", &Event_number, &b_Event_number);
   fChain->SetBranchAddress("Lumi", &Lumi, &b_Lumi);
   fChain->SetBranchAddress("MC_event_weight", &MC_event_weight, &b_MC_event_weight);
   fChain->SetBranchAddress("pileupWeight", &pileupWeight, &b_pileupWeight);
   fChain->SetBranchAddress("vertexweight", &vertexweight, &b_vertexweight);
   fChain->SetBranchAddress("kFacrot", &kFacrot, &b_kFacrot);
   fChain->SetBranchAddress("xSec", &xSec, &b_xSec);
   fChain->SetBranchAddress("pt", &pt, &b_pt);
   fChain->SetBranchAddress("eta", &eta, &b_eta);
   fChain->SetBranchAddress("phi", &phi, &b_phi);
   fChain->SetBranchAddress("primary_cluster_be2_eta", &primary_cluster_be2_eta, &b_primary_cluster_be2_eta);
   fChain->SetBranchAddress("r_had0", &r_had0, &b_r_had0);
   fChain->SetBranchAddress("r_had1", &r_had1, &b_r_had1);
   fChain->SetBranchAddress("e_ratio", &e_ratio, &b_e_ratio);
   fChain->SetBranchAddress("e", &e, &b_e);
   fChain->SetBranchAddress("delta_E", &delta_E, &b_delta_E);
   fChain->SetBranchAddress("w_eta2", &w_eta2, &b_w_eta2);
   fChain->SetBranchAddress("w_eta1", &w_eta1, &b_w_eta1);
   fChain->SetBranchAddress("r_phi", &r_phi, &b_r_phi);
   fChain->SetBranchAddress("r_eta", &r_eta, &b_r_eta);
   fChain->SetBranchAddress("w_stot", &w_stot, &b_w_stot);
   fChain->SetBranchAddress("f1", &f1, &b_f1);
   fChain->SetBranchAddress("fracs1", &fracs1, &b_fracs1);
   fChain->SetBranchAddress("average_mu", &average_mu, &b_average_mu);
   fChain->SetBranchAddress("convType", &convType, &b_convType);
   fChain->SetBranchAddress("FixedCutLoose", &FixedCutLoose, &b_FixedCutLoose);
   fChain->SetBranchAddress("FixedCutTight", &FixedCutTight, &b_FixedCutTight);
   fChain->SetBranchAddress("E277", &E277, &b_E277);
   fChain->SetBranchAddress("ptcone20", &ptcone20, &b_ptcone20);
   fChain->SetBranchAddress("ptcone30", &ptcone30, &b_ptcone30);
   fChain->SetBranchAddress("ptcone40", &ptcone40, &b_ptcone40);
   fChain->SetBranchAddress("topoetcone20", &topoetcone20, &b_topoetcone20);
   fChain->SetBranchAddress("topoetcone30", &topoetcone30, &b_topoetcone30);
   fChain->SetBranchAddress("topoetcone40", &topoetcone40, &b_topoetcone40);
   fChain->SetBranchAddress("tetcone20", &tetcone20, &b_tetcone20);
   fChain->SetBranchAddress("tetcone30", &tetcone30, &b_tetcone30);
   fChain->SetBranchAddress("tetcone40", &tetcone40, &b_tetcone40);
   fChain->SetBranchAddress("XsecWeight", &XsecWeight, &b_XsecWeight);
   fChain->SetBranchAddress("SumWeights", &SumWeights, &b_SumWeights);
   fChain->SetBranchAddress("Yweight", &Yweight, &b_Yweight);
   fChain->SetBranchAddress("GE", &GE, &b_GE);
   Notify();
}

Bool_t PhotonCandidates::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void PhotonCandidates::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t PhotonCandidates::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef PhotonCandidates_cxx
