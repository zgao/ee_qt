import numpy as np
import math
import ROOT
import sys
import argparse

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()


ROOT.gStyle.SetOptStat(0)


parser = argparse.ArgumentParser(description='')
parser.add_argument('-conpdf', '--conpdf', dest="conpdf_file", required=True, help='con pdf file')
parser.add_argument('-uncpdf', '--uncpdf', dest="uncpdf_file", required=True, help='unc pdf file')
parser.add_argument('-conroot', '--conroot', dest="conroot_file", required=True, help='con root file')
parser.add_argument('-uncroot', '--uncroot', dest="uncroot_file", required=True, help='unc root file')
args = parser.parse_args()


list_bin=[25,30,  35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250]
list_graph_x=[27.5,32.5,37.5,42.5,47.5,55,70,90,112.5,137.5,162.5,212.5]
seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]
#  seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.37"]


file3 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/new_conversion/converted_match_conversion.root","READ")
file5 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/data/con/final_new/truth_pt_new_pt/conversion_match_converted.root","READ")
file9 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/new_conversion/converted_match_just.root","READ")
file11 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/data/con/final_new/truth_pt_new_pt/just_match_converted.root","READ")
file13 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/new_conversion/photon_match_converted_just.root","READ")

file4 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/new_conversion/unconverted_match_conversion.root","READ")
file6 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/data/unc/final_new/truth_pt_new_pt/conversion_match_unconverted.root","READ")
file10 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/new_conversion/unconverted_match_just.root","READ")
file12 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/data/unc/final_new/truth_pt_new_pt/just_match_unconverted.root","READ")
file14 = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/new_conversion/photon_match_unconverted_just.root","READ")

file7 = ROOT.TFile(args.conroot_file,"READ")
file8 = ROOT.TFile(args.uncroot_file,"READ")
#  key_list = file1.GetListOfKeys()
key_list = file3.GetListOfKeys()

canvas=ROOT.TCanvas("canvas","",800,600)
#  canvas.Print("converted_all.pdf"+"[")
canvas.Print(args.conpdf_file+"[")

canvas2=ROOT.TCanvas("canvas2","",800,600)
canvas2.Print(args.uncpdf_file+"[")


#  file_bkg_substract_con = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/bkg_systematic_uncertainty/75_80_con.root","READ")
#  file_bkg_substract_unc = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/bkg_systematic_uncertainty/75_80_unc.root","READ")
#  file_bkg_substract_con = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/converted.root","READ")
#  file_bkg_substract_unc = ROOT.TFile("/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/unconverted.root","READ")
def final_errors(hist):
	list1 = []
	for i in range(hist.GetNbinsX()):
		list1.append(hist.GetBinError(i+1))
	return list1

def final_errors_2(hist):
	list1 = []
	for i in range(hist.GetNbinsX()):
		list1.append(hist.GetBinContent(i+1))
	return list1
for i in range(7):
#  for i in range(4):
	#  hist1 = file1.Get(f"histogram;{int(i+1)}")
	#  hist2 = file2.Get(f"histogram;{int(i+1)}")
	hist3 = file3.Get(f"histogram;{int(i+1)}")
	hist4 = file4.Get(f"histogram;{int(i+1)}")
	hist5 = file5.Get(f"histogram;{int(i+1)}")
	hist6 = file6.Get(f"histogram;{int(i+1)}")
	hist7 = file7.Get(f"histogram;{int(i+1)}")
	hist8 = file8.Get(f"histogram;{int(i+1)}")
	hist9 = file9.Get(f"histogram;{int(i+1)}")
	hist10 = file10.Get(f"histogram;{int(i+1)}")
	hist11 = file11.Get(f"histogram;{int(i+1)}")
	hist12 = file12.Get(f"histogram;{int(i+1)}")
	hist13 = file13.Get(f"histogram;{int(i+1)}")
	hist14 = file14.Get(f"histogram;{int(i+1)}")
	#  hist15 = file15.Get(f"histogram;{int(i+1)}")
	#  hist16 = file16.Get(f"histogram;{int(i+1)}")
	#  hist_bkg_con = file_bkg_substract_con.Get(f"histogram;{int(i+1)}")
	#  hist_bkg_unc = file_bkg_substract_unc.Get(f"histogram;{int(i+1)}")

	#  stat_error_con = final_errors(hist5)
	#  stat_error_unc = final_errors(hist6)
	#  bkg_con = final_errors_2(hist_bkg_con)
	#  bkg_unc = final_errors_2(hist_bkg_unc)

	#  print(hist_bkg_con.GetNbinsX())
	#  print(stat_error_con,bkg_con)
	
	#  final_error_con = [math.sqrt(stat_error_con[j]**2 + bkg_con[j]**2) for j in range(hist_bkg_con.GetNbinsX())]
	#  print(final_error_con)
	#  final_error_unc = [math.sqrt(stat_error_unc[j]**2 + bkg_unc[j]**2) for j in range(hist_bkg_unc.GetNbinsX())]
	#  for j in range(hist_bkg_con.GetNbinsX()):
	#      hist5.SetBinError(j+1,final_error_con[j])
	#      hist6.SetBinError(j+1,final_error_unc[j])

	canvas.cd()
	#  hist1.Draw("h,e")
	#  hist1.SetLineColor(ROOT.kRed)
	#  hist1.SetMarkerColor(ROOT.kRed)
	#  hist1.SetMarkerSize(0)
	hist3.Draw("h,e")
	hist3.SetLineColor(ROOT.kBlue)
	hist3.SetMarkerColor(ROOT.kBlue)
	hist5.Draw("h,same")
	hist5.SetLineColor(ROOT.kBlack)
	hist5.SetMarkerColor(ROOT.kBlack)
	hist7.Draw("h,same")
	hist7.SetLineColor(ROOT.kGreen)
	hist7.SetMarkerColor(ROOT.kGreen)
	#  hist7.SetMarkerSize(0)
	hist9.Draw("h,same")
	hist9.SetLineColor(ROOT.kRed)
	hist9.SetMarkerColor(ROOT.kRed)
	hist11.Draw("h,same")
	hist11.SetLineColor(ROOT.kMagenta)
	hist11.SetMarkerColor(ROOT.kMagenta)
	hist13.Draw("h,same")
	hist13.SetLineColor(ROOT.kYellow)
	hist13.SetMarkerColor(ROOT.kYellow)
	#  hist15.Draw("h,same")
	#  hist15.SetLineColor(ROOT.kViolet)
	#  hist15.SetMarkerColor(ROOT.kViolet)
	legend = ROOT.TLegend(0.6,0.5,0.9,0.7)
	#  legend.AddEntry(hist1," electron_data(both-pass) ","l")
	legend.AddEntry(hist3," new zee MC ","l")
	#  legend.AddEntry(hist5," ligang_ff ","l")
	#  legend.AddEntry(hist5," zee MC delta_E ","l")
	legend.AddEntry(hist5," new zee data ","l")
	#  legend.AddEntry(hist5," zee MC pile ","l")
	legend.AddEntry(hist7," new photon MC ","l")
	legend.AddEntry(hist9, " just zee  MC ","l")
	legend.AddEntry(hist11," just zee data ","l")
	legend.AddEntry(hist13," just pho MC","l")
	#  legend.AddEntry(hist15," frag_up data ","l")
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	legend.Draw("same")
	#  text1 = ROOT.TPaveText(0.6,0.6,0.9,0.7)
	text1 = ROOT.TPaveText(150,0.6,200,0.7)
	text1.AddText("converted")
	text1.AddText(seven_eta_cut_name[i])
	text1.SetFillStyle(0)
	text1.SetBorderSize(0)
	text1.SetTextSize(0.03)
	text1.Draw("same")


	#  canvas.Print("converted_all.pdf")
	canvas.Print(args.conpdf_file)

	canvas2.cd()
	#  hist2.Draw("h,e")
	#  hist2.SetLineColor(ROOT.kRed)
	#  hist2.SetMarkerColor(ROOT.kRed)
	#  hist2.SetMarkerSize(0)
	hist4.Draw("h,e")
	#  hist4.Draw("h,same")
	hist4.SetLineColor(ROOT.kBlue)
	hist4.SetMarkerColor(ROOT.kBlue)
	#  hist4.SetMarkerSize(0)
	hist6.Draw("h,same")
	#  hist6.SetLineColor(ROOT.kMagenta)
	#  hist6.SetMarkerColor(ROOT.kMagenta)
	hist6.SetLineColor(ROOT.kBlack)
	hist6.SetMarkerColor(ROOT.kBlack)
	#  hist6.SetMarkerSize(0)
	hist8.Draw("h,same")
	hist8.SetLineColor(ROOT.kGreen)
	hist8.SetMarkerColor(ROOT.kGreen)
	#  hist8.SetMarkerSize(0)
	hist10.Draw("h,same")
	hist10.SetLineColor(ROOT.kRed)
	hist10.SetMarkerColor(ROOT.kRed)
	hist12.Draw("h,same")
	hist12.SetLineColor(ROOT.kMagenta)
	hist12.SetMarkerColor(ROOT.kMagenta)
	hist14.Draw("h,same")
	hist14.SetLineColor(ROOT.kYellow)
	hist14.SetMarkerColor(ROOT.kYellow)
	#  hist16.Draw("h,same")
	#  hist16.SetLineColor(ROOT.kViolet)
	#  hist16.SetMarkerColor(ROOT.kViolet)
	legend2 = ROOT.TLegend(0.6,0.5,0.9,0.7)
	#  legend2.AddEntry(hist2," electron_data(both-pass) ","l")
	legend2.AddEntry(hist4," new zee MC ","l")
	#  legend2.AddEntry(hist6," ligang_ff ","l")
	#  legend2.AddEntry(hist6," zee MC pile ","l")
	#  legend2.AddEntry(hist6," zee MC delta_E ","l")
	legend2.AddEntry(hist6," new zee MC data ","l")
	legend2.AddEntry(hist8," new photon MC ","l")
	legend2.AddEntry(hist10," just zee MC ","l")
	legend2.AddEntry(hist12," just zee data ","l")
	legend2.AddEntry(hist14," just pho MC","l")
	#  legend2.AddEntry(hist16,"  frag_up data ","l")
	legend2.SetFillStyle(0)
	legend2.SetBorderSize(0)
	legend2.Draw("same")
	#  text2 = ROOT.TPaveText(0.6,0.4,0.9,0.6)
	text2 = ROOT.TPaveText(150,0.6,200,0.7)
	text2.AddText("unconverted")
	text2.AddText(seven_eta_cut_name[i])
	text2.SetFillStyle(0)
	text2.SetBorderSize(0)
	text2.SetTextSize(0.03)
	text2.Draw("same")
	#  canvas2.Print("unconverted_all.pdf")
	canvas2.Print(args.uncpdf_file)

#  canvas.Print("converted_all.pdf"+"]")
canvas.Print(args.conpdf_file+"]")
#  canvas2.Print("unconverted_all.pdf"+"]")
canvas2.Print(args.uncpdf_file+"]")



