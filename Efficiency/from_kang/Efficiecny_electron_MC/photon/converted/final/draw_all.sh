#!/bin/bash

# python3 draw_all.py -now_conroot ../../both_pass_probe/converted.root -now_uncroot ../../both_pass_probe/uconverted.root -w_conroot ./converted.root -w_uncroot ./uconverted.root -conpdf ./w_wconverted.pdf -uncpdf ./w_wunconverted.pdf
# python3 draw_all.py -now_conroot ../../../photon/converted/all_weight_1.root -now_uncroot ../../../photon/unconverted/all_weight_1.root -w_conroot ./converted.root -w_uncroot ../../unconverted/final/unconverted.root -conpdf ./w_wconverted.pdf -uncpdf ./w_wunconverted.pdf


python3 draw_all.py -now_conroot ../../../photon/converted/all_weight_1.root -now_uncroot ../../../photon/unconverted/all_weight_1.root -w_conroot ./converted.root -w_uncroot ../../unconverted/final/unconverted.root -conpdf ./w_wconverted.pdf -uncpdf ./w_wunconverted.pdf
