import os
import numpy as np

def parse_data(file_path):
	data =[[[0 for i in range(4)] for j in range(12)] for k in range(7)]

	with open(file_path,"r") as file:
		section = -1
		row = 0
		col = 0
		for line in file:
			line = line.strip()
			if "eta" in line:
				section +=1
				row =0
				col =0

			else:
				parts = line.split()
				if len(parts) >=2:
					#  print(parts[0],parts[1])
					val1, val2 ,val3,val4= float(parts[0]),float(parts[1]),float(parts[2]),float(parts[3])
					#  print(val1,val2)
					#  print("")
					if 0 <= section < 7 and row < 12 and col < 2:
						#  data[section][row][col] = [val1, val2]
						data[section][row][0] = val1
						data[section][row][1] = val2
						data[section][row][2] = val3
						data[section][row][3] = val4
						#  print(data[section][row][0])
						#  print(data[section][row][1])
						#  print(data[section][row][col])
						#  print(data[section][row][col][0])
						row += 1

						#  print("row ",row,"col ",col)
						if row == 12:
							row = 0
							col += 1
						#  print(data[section][row][col])
	
	return data

directory ='.'
files = os.listdir(directory)
tot_data=[]
for file_name in files:
	print("")
	print(file_name)
	if file_name.endswith('.root_txt'):
		file_path = os.path.join(directory, file_name)
		print(file_path)
		data = parse_data(file_path)
		tot_data.append(data)
		#  print('mydata ',len(tot_data))

		#  print(tot_data)

#  print(len(tot_data))
#  print(len(tot_data[0]))
#  print(len(tot_data[0][0]))
#  print(len(tot_data[0][0][0]))
#
#  print(tot_data[134][0][0])
#  print(tot_data[134][0][1])
#  print(tot_data[134][1][1])
#  print(tot_data[0][1][1])
#  print(tot_data[1][1][1])

final_data_temp=[[[0 for i in range(4)] for j in range(12)] for k in range(7)]
eta_name = ["0 < |eta| < 0.6","0.6 < |eta| < 0.8","0.8 < |eta| < 1.15","1.15 < |eta| < 1.37","1.52 < |eta| < 1.81","1.81 < |eta| < 2.01","2.01 < |eta| < 2.37"]


outfile_name="../final_w.txt"
with open(outfile_name,"w") as outfile:
	for i in range(7):
		for j in range(12):
			for k in range(4):
				for name in range(len(tot_data)):
					final_data_temp[i][j][k] += tot_data[name][i][j][k]

			#  print("i ",i,"j ",j)
			if j==0:
				print(eta_name[i])
				if i ==0:
					outfile.write(eta_name[i])
				else:
					outfile.write(f"\n{eta_name[i]}")
			print(final_data_temp[i][j][0]," ",final_data_temp[i][j][1]," ",final_data_temp[i][j][2]," ",final_data_temp[i][j][3])
			outfile.write(f"\n{final_data_temp[i][j][0]}	{final_data_temp[i][j][1]}  {final_data_temp[i][j][2]}  {final_data_temp[i][j][3]}")

print(f"{outfile_name} is created and written")
			#  print("")
