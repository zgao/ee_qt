# !/bin/bash
# python3 draw_con.py -in both_pass_probe/dataconvIDEF_all_final.txt -pdf both_pass_probe/converted.pdf -root both_pass_probe/converted.root
# python3 draw_con.py -in probe_probe/dataconvIDEF_all_final.txt -pdf probe_probe/converted.pdf -root probe_probe/converted.root
# python3 draw_con.py -in tag_probe/dataconvIDEF_all_final.txt -pdf tag_probe/converted.pdf -root tag_probe/converted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/pho/MCconvIDEF_all.txt -pdf photon/converted.pdf -root photon/converted.root
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/pho_no_fudge/MCconvIDEF_all_noFF.txt -pdf photon_noFF/converted.pdf -root photon_noFF/converted.root


# python3 draw_unc.py -in both_pass_probe/dataUnconvIDEF_all_final.txt -pdf both_pass_probe/uconverted.pdf -root both_pass_probe/uconverted.root
# python3 draw_unc.py -in probe_probe/dataUnconvIDEF_all_final.txt -pdf probe_probe/uconverted.pdf -root probe_probe/uconverted.root
# python3 draw_unc.py -in tag_probe/dataUnconvIDEF_all_final.txt -pdf tag_probe/uconverted.pdf -root tag_probe/uconverted.root

# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/pho/MCUnconvIDEF_all.txt -pdf photon/uconverted.pdf -root photon/uconverted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/pho_no_fudge/MCUnconvIDEF_all_noFF.txt -pdf photon_noFF/uconverted.pdf -root photon_noFF/uconverted.root



# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/pho/MCUnconvIDEF_all_test_with_weigh_cut_bigger_than_40.txt  -pdf photon/uconverted.pdf   -root photon/uconverted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/pho/MCUnconvIDEF_all_test_with_weight.txt -pdf photon/uconverted.pdf   -root photon/uconverted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/pho/MCUnconvIDEF_all_test.txt -pdf photon/uconverted.pdf   -root photon/uconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/pho/MCconvIDEF_all.txt -pdf photon/converted.pdf -root photon/converted.root
# python3 draw_unc.py -in both_pass_probe/dataUnconvIDEF_all_final_change_range.txt -pdf both_pass_probe/uconverted_change_range.pdf -root both_pass_probe/uconverted_change_range.root


# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/pho/MCconvIDEF_all.txt -pdf photon/converted_change_range.pdf -root photon/converted_change_range.root

#--------------------------------------------------------------

# python3 draw_con.py -in both_pass_probe/converted/final.txt -pdf zee_with_weight/both_pass/1_converted.pdf -root zee_with_weight/both_pass/1_converted.root
# python3 draw_con.py -in probe_probe/converted/final.txt -pdf zee_with_weight/probe_probe/1_converted.pdf -root zee_with_weight/probe_probe/1_converted.root
# python3 draw_con.py -in tag_probe/converted/final.txt -pdf zee_with_weight/tag_probe/1_converted.pdf -root zee_with_weight/tag_probe/1_converted.root
#
#
# python3 draw_unc.py -in both_pass_probe/unconverted/final.txt -pdf zee_with_weight/both_pass/uconverted.pdf -root zee_with_weight/both_pass/uconverted.root
# python3 draw_unc.py -in probe_probe/unconverted/final.txt -pdf zee_with_weight/probe_probe/uconverted.pdf -root zee_with_weight/probe_probe/uconverted.root
# python3 draw_unc.py -in tag_probe/unconverted/final.txt -pdf zee_with_weight/tag_probe/uconverted.pdf -root zee_with_weight/tag_probe/uconverted.root


# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/MCconvIDEF_all.txt -pdf photon/converted/final/converted.pdf -root photon/converted/final/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/MCUnconvIDEF_all_test.txt -pdf photon/unconverted/final/unconverted.pdf -root photon/unconverted/final/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/MCconvIDEF_all_weight_weight.txt -pdf photon/converted/final/weight_weight_converted.pdf -root photon/converted/final/weight_weight_converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/MCUnconvIDEF_all_test_weight_weight.txt -pdf photon/unconverted/final/weight_weight_unconverted.pdf -root photon/unconverted/final/weight_weight_unconverted.root


#-------------------------------------------------------------
# for name in $(ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/pho/converted/new/*.txt)
# do
#     echo $name
#     name2=${name/\/afs\/cern.ch\/user\/z\/zgao\/eos\/ee_qt\/Efficiency\/from_kang\/pho\/converted\/new\//}
#     name3=${name2/txt/pdf}
#     name4=${name2/txt/root}
#     echo $name3
#     echo $name4
#     # exit(0)
#     python3 draw_con.py -in $name -pdf photon/converted/$name3 -root photon/converted/$name4
# done
#
#
#
# for name in $(ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/pho/unconverted/new/*.txt)
# do
#     echo $name
#     name2=${name/\/afs\/cern.ch\/user\/z\/zgao\/eos\/ee_qt\/Efficiency\/from_kang\/pho\/unconverted\/new\//}
#     name3=${name2/txt/pdf}
#     name4=${name2/txt/root}
#     echo $name3
#     echo $name4
#     # exit(0)
#     python3 draw_unc.py -in $name -pdf photon/unconverted/$name3 -root photon/unconverted/$name4
# done


#--------------------------------------------------

#
# python3 draw_con.py -in both_pass_probe/converted/final_w.txt -pdf zee_with_weight/another_error_version/both_pass_change/converted.pdf -root zee_with_weight/another_error_version/both_pass_change/converted.root
# python3 draw_con.py -in probe_probe/converted/final_w.txt -pdf zee_with_weight/another_error_version/probe_probe_change/converted.pdf -root zee_with_weight/another_error_version/probe_probe_change/converted.root
# python3 draw_con.py -in tag_probe/converted/final_w.txt -pdf zee_with_weight/another_error_version/tag_probe_change/converted.pdf -root zee_with_weight/another_error_version/tag_probe_change/converted.root
#
#
# python3 draw_unc.py -in both_pass_probe/unconverted/final_w.txt -pdf zee_with_weight/another_error_version/both_pass_change/uconverted.pdf -root zee_with_weight/another_error_version/both_pass_change/uconverted.root
# python3 draw_unc.py -in probe_probe/unconverted/final_w.txt -pdf zee_with_weight/another_error_version/probe_probe_change/uconverted.pdf -root zee_with_weight/another_error_version/probe_probe_change/uconverted.root
# python3 draw_unc.py -in tag_probe/unconverted/final_w.txt -pdf zee_with_weight/another_error_version/tag_probe_change/uconverted.pdf -root zee_with_weight/another_error_version/tag_probe_change/uconverted.root

#---------------------------------------------------------------
# python3 draw_con_data.py -in data_electron/con/dataconvIDEF_all_final.txt -pdf data_electron/con/data_converted.pdf -root data_electron/con/converted.root
# python3 draw_unc_data.py -in data_electron/unc/dataUnconvIDEF_all_final.txt -pdf data_electron/unc/data_unconverted.pdf -root data_electron/unc/unconverted.root
#----------------------------------------------------------------

# python3 draw_con_data.py -in data_electron/con/new_dataconvIDEF_all_final.txt -pdf data_electron/con/four/data_converted.pdf -root data_electron/con/four/converted.root
# python3 draw_unc_data.py -in data_electron/unc/new_dataUnconvIDEF_all_final.txt -pdf data_electron/unc/four/data_unconverted.pdf -root data_electron/unc/four/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/new_MCconvIDEF_all_weight_weight.txt -pdf photon/converted/final/four/weight_weight_converted.pdf -root photon/converted/final/four/weight_weight_converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/new_MCUnconvIDEF_all_test_weight_weight.txt -pdf photon/unconverted/final/four/weight_weight_unconverted.pdf -root photon/unconverted/final/four/weight_weight_unconverted.root

#---------------------------------------------------------------

# python3 draw_con_data.py -in data_electron/con/nov_21/new_dataconvIDEF_all_final.txt -pdf data_electron/con/four/nov_21/data_converted.pdf -root data_electron/con/four/nov_21/converted.root
# python3 draw_unc_data.py -in data_electron/unc/nov_21/new_dataUnconvIDEF_all_final.txt -pdf data_electron/unc/four/nov_21/data_unconverted.pdf -root data_electron/unc/four/nov_21/unconverted.root


# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/nov_21/MCconvIDEF_all.txt -pdf photon/converted/final/four/nov_21/weight_weight_converted.pdf -root photon/converted/final/four/nov_21/weight_weight_converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/nov_21/MCUnconvIDEF_all_test.txt -pdf photon/unconverted/final/four/nov_21/weight_weight_unconverted.pdf -root photon/unconverted/final/four/nov_21/weight_weight_unconverted.root

#---------------------------------------------------------------

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/four/test_script.txt -pdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/zee_with_weight/four/both_pass/converted/converted.pdf -root /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/zee_with_weight/four/both_pass/converted/converted.root

#-------------------------------
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/nov_23/MCconvIDEF_all.txt -pdf photon/converted/final/four/nov_23/weight_weight_converted.pdf -root photon/converted/final/four/nov_23/weight_weight_converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/nov_23/MCUnconvIDEF_all_test.txt -pdf photon/unconverted/final/four/nov_23/weight_weight_unconverted.pdf -root photon/unconverted/final/four/nov_23/weight_weight_unconverted.root


# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/four/test_script.txt -pdf zee_with_weight/four/both_pass/weight_weight_converted.pdf -root zee_with_weight/four/both_pass/weight_weight_converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/four/test_script.txt  -pdf zee_with_weight/four/both_pass/weight_weight_unconverted.pdf -root zee_with_weight/four/both_pass/weight_weight_unconverted.root

#-------------------------------------

# python3 draw_con_data.py -in data_electron/con/new_dataconvIDEF_all_final.txt -pdf data_electron/con/four/nov_23/data_converted.pdf -root data_electron/con/four/nov_23/converted.root
# python3 draw_unc_data.py -in data_electron/unc/new_dataUnconvIDEF_all_final.txt -pdf data_electron/unc/four/nov_23/data_unconverted.pdf -root data_electron/unc/four/nov_23/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/nov_23/new_MCconvIDEF_all_weight_weight.txt -pdf photon/converted/final/four/nov_23/weight_weight_converted.pdf -root photon/converted/final/four/nov_23/weight_weight_converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/nov_23/new_MCUnconvIDEF_all_test_weight_weight.txt  -pdf photon/unconverted/final/four/nov_23/weight_weight_unconverted.pdf -root photon/unconverted/final/four/nov_23/weight_weight_unconverted.root

#-----------------------------------------
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/dataconvIDEF_all_final.txt -pdf Run3/data/con/new/converted.pdf -root Run3/data/con/new/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/dataUnconvIDEF_all_final.txt -pdf Run3/data/unc/new/unconverted.pdf -root Run3/data/unc/new/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3/final_w.txt -pdf Run3/MC_smirnoved/con/converted.pdf -root Run3/MC_smirnoved/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3/final_w.txt -pdf Run3/MC_smirnoved/unc/unconverted.pdf -root Run3/MC_smirnoved/unc/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3_ligang_ff/final_w.txt -pdf Run3/ligang_MC_smirnoved/con/converted.pdf -root Run3/ligang_MC_smirnoved/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3_ligang_ff/final_w.txt -pdf Run3/ligang_MC_smirnoved/unc/unconverted.pdf -root Run3/ligang_MC_smirnoved/unc/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3_pileweight/final_w.txt -pdf Run3/Run2_photon_pileweight/con/converted.pdf -root Run3/Run2_photon_pileweight/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3_pileweight/final_w.txt -pdf Run3/Run2_photon_pileweight/unc/unconverted.pdf -root Run3/Run2_photon_pileweight/unc/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3_1_2/final_w.txt -pdf Run3/Run2_photon_1_2/con/converted.pdf -root Run3/Run2_photon_1_2/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3_1_2/final_w.txt -pdf Run3/Run2_photon_1_2/unc/unconverted.pdf -root Run3/Run2_photon_1_2/unc/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3_1_2_not/final_w.txt -pdf Run3/Run2_photon_1_2_not/con/converted.pdf -root Run3/Run2_photon_1_2_not/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3_1_2_not/final_w.txt -pdf Run3/Run2_photon_1_2_not/unc/unconverted.pdf -root Run3/Run2_photon_1_2_not/unc/unconverted.root

# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/dataconvIDEF_all_final.txt  -pdf Run3/data/con/new/converted.pdf  -root  Run3/data/con/new/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/dataUnconvIDEF_all_final.txt  -pdf Run3/data/unc/new/unconverted.pdf  -root  Run3/data/unc/new/unconverted.root

#------------------------------------

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3_1_2_final/final_w.txt -pdf Run3/Run2_photon_1_2_final/con/converted.pdf -root Run3/Run2_photon_1_2_final/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3_1_2_final/final_w.txt -pdf Run3/Run2_photon_1_2_final/unc/unconverted.pdf -root Run3/Run2_photon_1_2_final/unc/unconverted.root

# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataconvIDEF_all_final.txt  -pdf Run3/data/con/final_new/converted.pdf  -root  Run3/data/con/final_new/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataUnconvIDEF_all_final.txt  -pdf Run3/data/unc/final_new/unconverted.pdf  -root  Run3/data/unc/final_new/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/nov_23/MCconvIDEF_all.txt -pdf Run3/Run2_photon/con/converted.pdf -root Run3/Run2_photon/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/nov_23/MCUnconvIDEF_all_test.txt -pdf Run3/Run2_photon/unc/unconverted.pdf -root Run3/Run2_photon/unc/unconverted.root

#--------------------------------
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/MCconvIDEF_all.txt -pdf Run3_photon/converted/converted.pdf -root Run3_photon/converted/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/MCUnconvIDEF_all_test.txt  -pdf Run3_photon/unconverted/unconverted.pdf -root Run3_photon/unconverted/unconverted.root


# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3_photon/final_w.txt -pdf Run3/Run3_photon/con/converted.pdf -root Run3/Run3_photon/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3_photon/final_w.txt  -pdf Run3/Run3_photon/unc/unconverted.pdf -root Run3/Run3_photon/unc/unconverted.root


# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataconvIDEF_all_final.txt  -pdf Run3/data/con/final_new/converted.pdf  -root  Run3/data/con/final_new/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/80_100/dataUnconvIDEF_all_final.txt  -pdf Run3/data/unc/final_new/unconverted.pdf  -root  Run3/data/unc/final_new/unconverted.root

# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/80_100_con_final.txt -pdf Run3/data/con/final_new/converted.pdf  -root  Run3/data/con/final_new/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/80_100_unc_final.txt  -pdf Run3/data/unc/final_new/unconverted.pdf  -root  Run3/data/unc/final_new/unconverted.root


# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/frag_down/final_w.txt  -pdf Run3/Run3_photon_frag_down/con/converted.pdf -root Run3/Run3_photon_frag_down/con/converted.root
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/frag_up/final_w.txt  -pdf Run3/Run3_photon_frag_up/con/converted.pdf -root Run3/Run3_photon_frag_up/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/frag_down/final_w.txt  -pdf Run3/Run3_photon_frag_down/unc/unconverted.pdf -root Run3/Run3_photon_frag_down/unc/unconverted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/frag_up/final_w.txt  -pdf Run3/Run3_photon_frag_up/unc/unconverted.pdf -root Run3/Run3_photon_frag_up/unc/unconverted.root
#
#
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/down_subtract_con.txt -pdf Run3/data/con/final_new/frag_down/converted.pdf -root Run3/data/con/final_new/frag_down/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/down_subtract_unc.txt -pdf Run3/data/unc/final_new/frag_down/unconverted.pdf -root Run3/data/unc/final_new/frag_down/unconverted.root
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/up_subtract_con.txt  -pdf Run3/data/con/final_new/frag_up/converted.pdf -root Run3/data/con/final_new/frag_up/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/up_subtract_unc.txt  -pdf Run3/data/unc/final_new/frag_up/unconverted.pdf -root Run3/data/unc/final_new/frag_up/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_down/80_100_con_frag_down.txt  -pdf Run3/data/con/final_new/frag_down/converted.pdf -root Run3/data/con/final_new/frag_down/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_down/80_100_unc_frag_down.txt  -pdf Run3/data/unc/final_new/frag_down/unconverted.pdf -root Run3/data/unc/final_new/frag_down/unconverted.root
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_up/80_100_con_frag_up.txt  -pdf Run3/data/con/final_new/frag_up/converted.pdf -root Run3/data/con/final_new/frag_up/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/frag_up/80_100_unc_frag_up.txt  -pdf Run3/data/unc/final_new/frag_up/unconverted.pdf -root Run3/data/unc/final_new/frag_up/unconverted.root


# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/add_add/final_w.txt  -pdf Run3/Run3_photon_add_add/con/converted.pdf -root Run3/Run3_photon_add_add/con/converted.root
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/add_substract/final_w.txt  -pdf Run3/Run3_photon_add_substract/con/converted.pdf -root Run3/Run3_photon_add_substract/con/converted.root
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/substract_substract/final_w.txt  -pdf Run3/Run3_photon_substract_substract/con/converted.pdf -root Run3/Run3_photon_substract_substract/con/converted.root
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/substract_add/final_w.txt  -pdf Run3/Run3_photon_substract_add/con/converted.pdf -root Run3/Run3_photon_substract_add/con/converted.root
#
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/add_add/final_w.txt  -pdf Run3/Run3_photon_add_add/unc/unconverted.pdf -root Run3/Run3_photon_add_add/unc/unconverted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/add_substract/final_w.txt  -pdf Run3/Run3_photon_add_substract/unc/unconverted.pdf -root Run3/Run3_photon_add_substract/unc/unconverted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/substract_substract/final_w.txt  -pdf Run3/Run3_photon_substract_substract/unc/unconverted.pdf -root Run3/Run3_photon_substract_substract/unc/unconverted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/substract_add/final_w.txt  -pdf Run3/Run3_photon_substract_add/unc/unconverted.pdf -root Run3/Run3_photon_substract_add/unc/unconverted.root

# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/add_add/dataconvIDEF_all_final.txt  -pdf Run3/data/con/final_new/add_add/converted.pdf  -root  Run3/data/con/final_new/add_add/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/add_add/dataUnconvIDEF_all_final.txt  -pdf Run3/data/unc/final_new/add_add/unconverted.pdf  -root  Run3/data/unc/final_new/add_add/unconverted.root
#
# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/add_substract/dataconvIDEF_all_final.txt  -pdf Run3/data/con/final_new/add_substract/converted.pdf  -root  Run3/data/con/final_new/add_substract/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/add_substract/dataUnconvIDEF_all_final.txt  -pdf Run3/data/unc/final_new/add_substract/unconverted.pdf  -root  Run3/data/unc/final_new/add_substract/unconverted.root
#
# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/substract_add/dataconvIDEF_all_final.txt  -pdf Run3/data/con/final_new/substract_add/converted.pdf  -root  Run3/data/con/final_new/substract_add/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/substract_add/dataUnconvIDEF_all_final.txt  -pdf Run3/data/unc/final_new/substract_add/unconverted.pdf  -root  Run3/data/unc/final_new/substract_add/unconverted.root
#
# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/substract_substract/dataconvIDEF_all_final.txt  -pdf Run3/data/con/final_new/substract_substract/converted.pdf  -root  Run3/data/con/final_new/substract_substract/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/substract_substract/dataUnconvIDEF_all_final.txt  -pdf Run3/data/unc/final_new/substract_substract/unconverted.pdf  -root  Run3/data/unc/final_new/substract_substract/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/frag_up/final_w.txt  -pdf Run3/Run3_photon_frag_up/con/converted.pdf -root Run3/Run3_photon_frag_up/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/frag_down/final_w.txt  -pdf Run3/Run3_photon_frag_down/unc/unconverted.pdf -root Run3/Run3_photon_frag_down/unc/unconverted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/frag_up/final_w.txt  -pdf Run3/Run3_photon_frag_up/unc/unconverted.pdf -root Run3/Run3_photon_frag_up/unc/unconverted.root
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/frag_down/final_w.txt  -pdf Run3/Run3_photon_frag_down/con/converted.pdf -root Run3/Run3_photon_frag_down/con/converted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/statistic/final_w.txt  -pdf Run3/Run3_photon_statistic/con/converted.pdf -root Run3/Run3_photon_statistic/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/statistic/final_w.txt  -pdf Run3/Run3_photon_statistic/unc/unconverted.pdf -root Run3/Run3_photon_statistic/unc/unconverted.root
#
# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/statistic/dataconvIDEF_all_final.txt  -pdf Run3/data/con/final_new/statistic/converted.pdf  -root  Run3/data/con/final_new/statistic/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/statistic/dataUnconvIDEF_all_final.txt  -pdf Run3/data/unc/final_new/statistic/unconverted.pdf  -root  Run3/data/unc/final_new/statistic/unconverted.root
#

#------------------ -------------------- nominal ---------------------------
#------------------ without background  subtract    ---------------
# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/nominal/con_get_con.txt -pdf Run3/data/con/final_new/nominal/converted.pdf    -root  Run3/data/con/final_new/nominal/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/nominal/unc_get_unc.txt -pdf Run3/data/unc/final_new/nominal/unconverted.pdf  -root  Run3/data/unc/final_new/nominal/unconverted.root
#------------------ with background subtract --------

# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/nominal_subtract_con.txt -pdf Run3/data/con/final_new/nominal/converted.pdf    -root  Run3/data/con/final_new/nominal/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/nominal_subtract_unc.txt -pdf Run3/data/unc/final_new/nominal/unconverted.pdf  -root  Run3/data/unc/final_new/nominal/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/nominal/final_w.txt  -pdf Run3/Run3_photon_nominal/con/converted.pdf -root Run3/Run3_photon_nominal/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/nominal/final_w.txt  -pdf Run3/Run3_photon_nominal/unc/unconverted.pdf -root Run3/Run3_photon_nominal/unc/unconverted.root

#----nomnal from 1_2_fianl not enrique , above is from enrique---------
# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/80_100_con_final.txt -pdf Run3/data/con/final_new/nominal/revise_converted.pdf    -root  Run3/data/con/final_new/nominal/revise_converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal/after_revise_uncertainty/80_100_unc_final.txt -pdf Run3/data/unc/final_new/nominal/revise_unconverted.pdf  -root  Run3/data/unc/final_new/nominal/revise_unconverted.root


#-------------------nominal background ----------
#-----------------------------------conversion MC and Data ------------------------
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/conversion_down/final_w.txt  -pdf Run3/conversion_down/con/converted.pdf -root Run3/conversion_down/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/conversion_down/final_w.txt  -pdf Run3/conversion_down/unc/unconverted.pdf -root Run3/conversion_down/unc/unconverted.root
#
# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/conversion_up/final_w.txt  -pdf Run3/conversion_up/con/converted.pdf -root Run3/conversion_up/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/conversion_up/final_w.txt  -pdf Run3/conversion_up/unc/unconverted.pdf -root Run3/conversion_up/unc/unconverted.root


# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/conversion/conversion_down_subtract_con.txt -pdf Run3/data/con/final_new/conversion_down/converted.pdf    -root  Run3/data/con/final_new/conversion_down/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/conversion/conversion_down_subtract_unc.txt -pdf Run3/data/unc/final_new/conversion_down/unconverted.pdf  -root  Run3/data/unc/final_new/conversion_down/unconverted.root

# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/conversion/conversion_up_subtract_con.txt -pdf Run3/data/con/final_new/conversion_up/converted.pdf    -root  Run3/data/con/final_new/conversion_up/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/conversion/conversion_up_subtract_unc.txt -pdf Run3/data/unc/final_new/conversion_up/unconverted.pdf  -root  Run3/data/unc/final_new/conversion_up/unconverted.root

#---------------------------------------just hard ----------------------------

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/just_hard/final_w.txt  -pdf Run3/just_hard/con/converted.pdf -root Run3/just_hard/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/just_hard/final_w.txt  -pdf Run3/just_hard/unc/unconverted.pdf -root Run3/just_hard/unc/unconverted.root

# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/just_hard/just_hard_subtract_con.txt -pdf Run3/data/con/final_new/just_hard/converted.pdf    -root  Run3/data/con/final_new/just_hard/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/just_hard/just_hard_subtract_unc.txt -pdf Run3/data/unc/final_new/just_hard/unconverted.pdf  -root  Run3/data/unc/final_new/just_hard/unconverted.root


# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/just_hard/MCconvIDEF_all_just_hard.txt -pdf Run3_photon/converted/just_hard/converted.pdf -root Run3_photon/converted/just_hard/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/just_hard/MCUnconvIDEF_all_just_hard.txt  -pdf Run3_photon/unconverted/just_hard/unconverted.pdf -root Run3_photon/unconverted/just_hard/unconverted.root

#------------------------------------------truth_pt------------------

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/truth_pt/final_w.txt  -pdf Run3/truth_pt/con/converted.pdf -root Run3/truth_pt/con/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/truth_pt/final_w.txt  -pdf Run3/truth_pt/unc/unconverted.pdf -root Run3/truth_pt/unc/unconverted.root

# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/truth_pt_subtract_con.txt -pdf Run3/data/con/final_new/truth_pt/converted.pdf    -root  Run3/data/con/final_new/truth_pt/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/truth_pt_subtract_unc.txt -pdf Run3/data/unc/final_new/truth_pt/unconverted.pdf  -root  Run3/data/unc/final_new/truth_pt/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/truth_pt/MCconvIDEF_all_truth_pt.txt -pdf Run3_photon/converted/truth_pt/converted.pdf -root Run3_photon/converted/truth_pt/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/truth_pt/MCUnconvIDEF_all_truth_pt.txt  -pdf Run3_photon/unconverted/truth_pt/unconverted.pdf -root Run3_photon/unconverted/truth_pt/unconverted.root

# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/down_subtract_con.txt -pdf Run3/data/con/final_new/truth_pt_frag_down/converted.pdf    -root  Run3/data/con/final_new/truth_pt_frag_down/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/down_subtract_unc.txt -pdf Run3/data/unc/final_new/truth_pt_frag_down/unconverted.pdf  -root  Run3/data/unc/final_new/truth_pt_frag_down/unconverted.root
# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/up_subtract_con.txt -pdf Run3/data/con/final_new/truth_pt_frag_up/converted.pdf    -root  Run3/data/con/final_new/truth_pt_frag_up/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/up_subtract_unc.txt -pdf Run3/data/unc/final_new/truth_pt_frag_up/unconverted.pdf  -root  Run3/data/unc/final_new/truth_pt_frag_up/unconverted.root

#----truth_pt_new_page---

# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/truth_pt_new_pt/80_100_sub_sub_con.txt -pdf Run3/data/con/final_new/truth_pt_new_pt/converted.pdf    -root  Run3/data/con/final_new/truth_pt_new_pt/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/data/new/truth_pt_new_pt/80_100_sub_sub_unc.txt -pdf Run3/data/unc/final_new/truth_pt_new_pt/unconverted.pdf  -root  Run3/data/unc/final_new/truth_pt_new_pt/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/truth_pt_new_pt/MCconvIDEF_all_truth_pt.txt -pdf Run3_photon/converted/truth_pt_new_pt/converted.pdf -root Run3_photon/converted/truth_pt_new_pt/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/truth_pt_new_pt/MCUnconvIDEF_all_truth_pt.txt  -pdf Run3_photon/unconverted/truth_pt_new_pt/unconverted.pdf -root Run3_photon/unconverted/truth_pt_new_pt/unconverted.root

#-------------------new conversion----------

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/truth_pt_subtract_con.txt -pdf Run3_photon/converted/new_conversion/converted.pdf -root Run3_photon/converted/new_conversion/converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/truth_pt_subtract_unc.txt -pdf Run3_photon/unconverted/new_conversion/unconverted.pdf -root Run3_photon/unconverted/new_conversion/unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/truth_pt_new_conversion/final_w.txt -pdf Run3_photon/converted/new_conversion/converted_match.pdf -root Run3_photon/converted/new_conversion/converted_match.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/truth_pt_new_conversion/final_w.txt -pdf Run3_photon/unconverted/new_conversion/unconverted_match.pdf -root Run3_photon/unconverted/new_conversion/unconverted_match.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/MCconvIDEF_all_truth_pt.txt -pdf Run3_photon/converted/new_conversion/photon_match_converted.pdf -root Run3_photon/converted/new_conversion/photon_match_converted.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/MCUnconvIDEF_all_truth_pt.txt -pdf Run3_photon/unconverted/new_conversion/photon_match_unconverted.pdf -root Run3_photon/unconverted/new_conversion/photon_match_unconverted.root

# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/truth_pt_subtract_con.txt -pdf Run3/data/con/final_new/truth_pt_new_pt/converted.pdf    -root  Run3/data/con/final_new/truth_pt_new_pt/converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/truth_pt_subtract_unc.txt -pdf Run3/data/unc/final_new/truth_pt_new_pt/unconverted.pdf  -root  Run3/data/unc/final_new/truth_pt_new_pt/unconverted.root

#--just_match------------

# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/just_match/truth_pt_subtract_con.txt -pdf Run3/data/con/final_new/truth_pt_new_pt/just_match_converted.pdf    -root  Run3/data/con/final_new/truth_pt_new_pt/just_match_converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/just_match/truth_pt_subtract_unc.txt -pdf Run3/data/unc/final_new/truth_pt_new_pt/just_match_unconverted.pdf  -root  Run3/data/unc/final_new/truth_pt_new_pt/just_match_unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/truth_pt_new_conversion/final_w.txt -pdf Run3_photon/converted/new_conversion/converted_match_just.pdf -root Run3_photon/converted/new_conversion/converted_match_just.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/truth_pt_new_conversion/final_w.txt -pdf Run3_photon/unconverted/new_conversion/unconverted_match_just.pdf -root Run3_photon/unconverted/new_conversion/unconverted_match_just.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/just_match/MCconvIDEF_all_truth_pt.txt -pdf Run3_photon/converted/new_conversion/photon_match_converted_just.pdf -root Run3_photon/converted/new_conversion/photon_match_converted_just.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/just_match/MCUnconvIDEF_all_truth_pt.txt -pdf Run3_photon/unconverted/new_conversion/photon_match_unconverted_just.pdf -root Run3_photon/unconverted/new_conversion/photon_match_unconverted_just.root

#------match_conversion-----
# python3 draw_con_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/truth_pt_subtract_con.txt -pdf Run3/data/con/final_new/truth_pt_new_pt/conversion_match_converted.pdf    -root  Run3/data/con/final_new/truth_pt_new_pt/conversion_match_converted.root
# python3 draw_unc_data.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/truth_pt_subtract_unc.txt -pdf Run3/data/unc/final_new/truth_pt_new_pt/conversion_match_unconverted.pdf  -root  Run3/data/unc/final_new/truth_pt_new_pt/conversion_match_unconverted.root

# python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/truth_pt_new_conversion/final_w.txt -pdf Run3_photon/converted/new_conversion/converted_match_conversion.pdf -root Run3_photon/converted/new_conversion/converted_match_conversion.root
# python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/truth_pt_new_conversion/final_w.txt -pdf Run3_photon/unconverted/new_conversion/unconverted_match_conversion.pdf -root Run3_photon/unconverted/new_conversion/unconverted_match_conversion.root

python3 draw_con.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/final/MCconvIDEF_all_truth_pt.txt -pdf Run3_photon/converted/new_conversion/photon_match_converted_conversion.pdf -root Run3_photon/converted/new_conversion/photon_match_converted_conversion.root
python3 draw_unc.py -in /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/final/MCUnconvIDEF_all_truth_pt.txt -pdf Run3_photon/unconverted/new_conversion/photon_match_unconverted_conversion.pdf -root Run3_photon/unconverted/new_conversion/photon_match_unconverted_conversion.root
