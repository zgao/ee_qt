import numpy as np
import math
import ROOT
import sys
import argparse

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()


ROOT.gStyle.SetOptStat(0)


parser = argparse.ArgumentParser(description='')
parser.add_argument('-conpdf', '--conpdf', dest="conpdf_file", required=True, help='con pdf file')
parser.add_argument('-uncpdf', '--uncpdf', dest="uncpdf_file", required=True, help='unc pdf file')
parser.add_argument('-now_conroot', '--now_conroot', dest="now_conroot_file", required=True, help='now con root file')
parser.add_argument('-now_uncroot', '--now_uncroot', dest="now_uncroot_file", required=True, help='now unc root file')
parser.add_argument('-w_conroot', '--w_conroot', dest="w_conroot_file", required=True, help='w con root file')
parser.add_argument('-w_uncroot', '--w_uncroot', dest="w_uncroot_file", required=True, help='w unc root file')
args = parser.parse_args()


list_bin=[25,30,  35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250]
list_graph_x=[27.5,32.5,37.5,42.5,47.5,55,70,90,112.5,137.5,162.5,212.5]
seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]


#  file1 = ROOT.TFile("tag_probe/converted.root","READ")
file1 = ROOT.TFile(args.now_conroot_file,"READ")
#  file2 = ROOT.TFile("tag_probe/uconverted.root","READ")
file2 = ROOT.TFile(args.now_uncroot_file,"READ")
#  file3 = ROOT.TFile("both_pass_probe/converted.root","READ")
file3 = ROOT.TFile(args.w_conroot_file,"READ")
#  file4 = ROOT.TFile("both_pass_probe/uconverted.root","READ")
file4 = ROOT.TFile(args.w_uncroot_file,"READ")

#  file7 = ROOT.TFile(args.conroot_file,"READ")
#  file8 = ROOT.TFile(args.uncroot_file,"READ")
key_list = file1.GetListOfKeys()

canvas=ROOT.TCanvas("canvas","",800,600)
canvas.Print(args.conpdf_file+"[")

canvas2=ROOT.TCanvas("canvas2","",800,600)
canvas2.Print(args.uncpdf_file+"[")

for i in range(7):
	hist1 = file1.Get(f"histogram;{int(i+1)}")
	hist2 = file2.Get(f"histogram;{int(i+1)}")
	hist3 = file3.Get(f"histogram;{int(i+1)}")
	hist4 = file4.Get(f"histogram;{int(i+1)}")
	#  hist5 = file5.Get(f"histogram;{int(i+1)}")
	#  hist6 = file6.Get(f"histogram;{int(i+1)}")
	#  hist7 = file7.Get(f"histogram;{int(i+1)}")
	#  hist8 = file8.Get(f"histogram;{int(i+1)}")
	canvas.cd()
	hist1.Draw("h,e")
	hist1.SetLineColor(ROOT.kRed)
	hist1.SetMarkerColor(ROOT.kRed)
	hist1.SetMarkerSize(0)
	hist3.Draw("h,same")
	hist3.SetLineColor(ROOT.kBlue)
	hist3.SetMarkerColor(ROOT.kBlue)
	hist3.SetMarkerSize(0)
	#  hist5.Draw("h,same")
	#  hist5.SetLineColor(ROOT.kBlack)
	#  hist5.SetMarkerColor(ROOT.kBlack)
	#  hist5.SetMarkerSize(0)
	#  hist7.Draw("h,same")
	#  hist7.SetLineColor(ROOT.kGreen)
	#  hist7.SetMarkerColor(ROOT.kGreen)
	#  hist7.SetMarkerSize(0)
	legend = ROOT.TLegend(0.6,0.5,0.9,0.7)
	legend.AddEntry(hist3," weight ","l")
	legend.AddEntry(hist1," noweight ","l")
	#  legend.AddEntry(hist7," photon MC ","l")
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	legend.Draw("same")
	#  text1 = ROOT.TPaveText(0.6,0.6,0.9,0.7)
	text1 = ROOT.TPaveText(150,0.6,200,0.7)
	text1.AddText("converted")
	text1.AddText(seven_eta_cut_name[i])
	text1.SetFillStyle(0)
	text1.SetBorderSize(0)
	text1.SetTextSize(0.03)
	text1.Draw("same")


	#  canvas.Print("converted_all.pdf")
	canvas.Print(args.conpdf_file)

	canvas2.cd()
	hist2.Draw("h,e")
	hist2.SetLineColor(ROOT.kRed)
	hist2.SetMarkerColor(ROOT.kRed)
	hist2.SetMarkerSize(0)
	hist4.Draw("h,same")
	hist4.SetLineColor(ROOT.kBlue)
	hist4.SetMarkerColor(ROOT.kBlue)
	hist4.SetMarkerSize(0)
	#  hist6.Draw("h,same")
	#  hist6.SetLineColor(ROOT.kBlack)
	#  hist6.SetMarkerColor(ROOT.kBlack)
	#  hist6.SetMarkerSize(0)
	#  hist8.Draw("h,same")
	#  hist8.SetLineColor(ROOT.kGreen)
	#  hist8.SetMarkerColor(ROOT.kGreen)
	#  hist8.SetMarkerSize(0)
	legend2 = ROOT.TLegend(0.6,0.5,0.9,0.7)
	legend2.AddEntry(hist4," weight ","l")
	legend2.AddEntry(hist2," noweight","l")
	#  legend2.AddEntry(hist6," probe_probe ","l")
	#  legend2.AddEntry(hist8," photon MC ","l")
	legend2.SetFillStyle(0)
	legend2.SetBorderSize(0)
	legend2.Draw("same")
	text2 = ROOT.TPaveText(150,0.6,200,0.7)
	text2.AddText("unconverted")
	text2.AddText(seven_eta_cut_name[i])
	text2.SetFillStyle(0)
	text2.SetBorderSize(0)
	text2.SetTextSize(0.03)
	text2.Draw("same")
	canvas2.Print(args.uncpdf_file)

canvas.Print(args.conpdf_file+"]")
canvas2.Print(args.uncpdf_file+"]")


