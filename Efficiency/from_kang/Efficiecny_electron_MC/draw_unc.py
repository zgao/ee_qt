import numpy as np
import ROOT
import sys 
import math
import argparse

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()


parser = argparse.ArgumentParser(description='')
parser.add_argument('-in', '--input', dest="input_file", required=True, help='input file')
#  parser.add_argument('-conf', '--conf', dest="conf_file", required=True, help='conf file')
parser.add_argument('-pdf', '--pdf', dest="pdf_file", required=True, help='pdf file')
parser.add_argument('-root', '--root', dest="root_file", required=True, help='root file')

args = parser.parse_args()

with open(args.input_file, 'r') as file:
	lines = file.readlines()
labels = [] 

ratios = []
errors = []
for i,line in enumerate(lines):
	#  values = line.strip().split('\t')
	values = line.split()

	if i%13==0:
		labels.append(line.strip())
	elif len(values)>2:
		first_value = float(values[0])
		second_value = float(values[1])
		third_value = float(values[2])
		fourth_value = float(values[3])
		if second_value==0:
			ratio=0
			error=0
		else:
			ratio = first_value / second_value
			#  error = math.sqrt(ratio*(1-ratio)/second_value)
			error = math.sqrt( ((second_value-first_value)/(second_value**2)*math.sqrt(third_value))**2 + ((first_value)/(second_value**2)*math.sqrt(fourth_value))**2 )
		ratios.append(ratio)
		errors.append(error)

	elif len(values)==2:
		first_value = float(values[0])
		second_value = float(values[1])
		#  third_value = float(values[2])
		#  fourth_value = float(values[3])
		if second_value==0:
			ratio=0
			error=0
		else:
			ratio = first_value / second_value
			error = math.sqrt(ratio*(1-ratio)/second_value)
			#  error = math.sqrt( ((second_value-first_value)/(second_value**2)*math.sqrt(third_value))**2 + ((first_value)/(second_value**2)*math.sqrt(fourth_value))**2 )
		ratios.append(ratio)
		errors.append(error)

#  with open('/afs/cern.ch/user/z/zgao/from_kang/ntuple_code/ID_efficiency/pho/MCUnconvIDEF_all.txt', 'r') as file2:
#  with open('/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/pho/MCUnconvIDEF_all_test_with_weigh_cut_bigger_than_40.txt', 'r') as file2:
#	   lines2 = file2.readlines()
#  labels2 = []
#
#  ratios2 = []
#  errors2 = []
#
#  for i,line2 in enumerate(lines2):
#	   values2 = line2.strip().split('\t')
#
#	   if i%13==0:
#		   labels2.append(line2.strip())
#	   elif len(values2)>=2:
#		   first_value = float(values2[0])
#		   second_value = float(values2[1])
#		   if second_value==0:
#			   ratio2=0
#			   error2=0
#		   else:
#			   ratio2 = first_value / second_value
#			   error2 = math.sqrt(ratio2*(1-ratio2)/second_value)
#		   ratios2.append(ratio2)
#		   errors2.append(error2)


ROOT.gStyle.SetOptStat(0)
list_bin=[25,30,  35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250]
list_graph_x=[27.5,32.5,37.5,42.5,47.5,55,70,90,112.5,137.5,162.5,212.5]

canvas=ROOT.TCanvas("canvas","",800,600)
canvas.Print(args.pdf_file+"[")

output_root = ROOT.TFile(args.root_file,"RECREATE")
histogram = ROOT.TH1F("histogram","efficiency",12,np.array(list_bin,np.float32))


for j in range(7):
#  for j in range(4):
	for i in range(histogram.GetNbinsX()):
		histogram.SetBinContent(i+1,ratios[int(i+j*12)])
		histogram.SetBinError(i+1,errors[int(i+j*12)])
		print(errors[int(i+j*12)])

	canvas.cd()

	histogram.GetXaxis().SetTitle("pt [Gev]")
	histogram.GetYaxis().SetTitle("ID efficiency")
	histogram.GetYaxis().SetRangeUser(0.6,1)
	histogram.SetLineColor(ROOT.kBlack)
	histogram.GetXaxis().SetTitleSize(0)
	histogram.GetXaxis().SetLabelSize(0)
	histogram.GetYaxis().SetTitleOffset(1.0)
	histogram.GetYaxis().CenterTitle()
	histogram.SetMarkerSize(0)

	histogram.Draw("h,e")

	legend=ROOT.TLegend(0.5,0.6,0.7,0.7)
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	legend.SetTextSize(0.05)
	legend.AddEntry(histogram,"electron MC","L")
	legend.Draw("same")

	pt = ROOT.TPaveText(0.5, 0.4, 0.7, 0.55, "ndc")
	pt.SetTextSize(0.05)
	pt.SetBorderSize(0)
	pt.SetFillStyle(0)
	pt.SetTextAlign(13)
	pt.SetTextFont(42)
	pt.AddText(f"unconverted,{labels[int(j%7)]}")
	#  pt.AddText(f"unconverted,{labels[int(j%4)]}")
	pt.Draw("same")

	histogram.Write()


	canvas.Print(args.pdf_file)
canvas.Print(args.pdf_file+"]")
output_root.Close()


canvas_err=ROOT.TCanvas("canvas","",800,600)
new_pdf_file = args.pdf_file.replace(".pdf", "_err.pdf")
canvas_err.Print(new_pdf_file+"[")

err_root_file =args.root_file.replace(".root","_err.root")
erroutput_root = ROOT.TFile(err_root_file,"RECREATE")
histogram2 = ROOT.TH1F("histogram2","err",12,np.array(list_bin,np.float32))
for j in range(7):
#  for j in range(4):
	for i in range(histogram2.GetNbinsX()):
		canvas_err.cd()

		histogram2.SetBinContent(i+1,errors[int(i+j*12)])
		#  histogram2.GetYaxis().SetRangeUser(0,0.01)
	histogram2.GetXaxis().SetTitle("pt [Gev]")
	histogram2.Draw("h")


	pt2 = ROOT.TPaveText(0.5, 0.4, 0.7, 0.55, "ndc")
	pt2.SetTextSize(0.05)
	pt2.SetBorderSize(0)
	pt2.SetFillStyle(0)
	pt2.SetTextAlign(13)
	pt2.SetTextFont(42)
	pt2.AddText(f"unconverted,{labels[int(j%7)]}")
	#  pt2.AddText(f"unconverted,{labels[int(j%4)]}")
	pt2.Draw("same")
	histogram2.Write()
	canvas_err.Print(new_pdf_file)
canvas_err.Print(new_pdf_file+"]")
erroutput_root.Close()


