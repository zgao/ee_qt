#/bin/bash
# for name in $(ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/*.root)
# do
#	  # echo $name
#	  name2=${name/\/afs\/cern.ch\/user\/z\/zgao\/eos\/ee_qt\/Efficiency\/from_kang\/Efficiecny_electron_MC\/photon\/converted\//}
#	  # echo $name2
#	  if [[ "$name2" == *err* ]]; then
#		  continue
#	  fi
#
#	  echo "Provessing file: $name2"
#	  name3=${name2/.root/_con.pdf}
#	  name4=${name2/.root/_unc.pdf}
#	  echo " pdf---------> $name3"
#	  echo " pdf---------> $name4"
#
#	  python3 draw_all.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/$name2 -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/$name2 -conpdf new/$name3 -uncpdf new/$name4
# done

# for name in $(ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/*.root)
# do
#	  # echo $name
#	  name2=${name/\/afs\/cern.ch\/user\/z\/zgao\/eos\/ee_qt\/Efficiency\/from_kang\/Efficiecny_electron_MC\/photon\/unconverted\//}
#	  # echo $name2
#	  if [[ "$name2" == *err* ]]; then
#		  continue
#	  fi
#
#	  echo "Provessing file: $name2"
# done
# python3 draw_all.py -conroot -uncroot -conpdf new/ -uncpdf new/

# python3 draw_all.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/unconverted.root -conpdf new/new_err_final_converted.pdf -uncpdf new/new_err_final_unconverted.pdf

# python3 draw_all.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/weight_weight_converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/weight_weight_unconverted.root -conpdf new/new_err_final_converted.pdf -uncpdf new/new_err_final_unconverted.pdf

# python3 draw_all.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/weight_weight_converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/weight_weight_unconverted.root -conpdf new/change_new_err_final_converted.pdf -uncpdf new/change_new_err_final_unconverted.pdf

#---------------------------------------------------------

# python3 draw_data_photon.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/weight_weight_converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/weight_weight_unconverted.root -conpdf data-photon/converted.pdf -uncpdf data-photon/unconverted.pdf
#-----------------------------------------------------------

# python3 draw_data_photon.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/four/weight_weight_converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/four/weight_weight_unconverted.root -conpdf data-photon/four_converted.pdf -uncpdf data-photon/four_unconverted.pdf

# python3 draw_data_photon.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/four/nov_21/weight_weight_converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/four/nov_21/weight_weight_unconverted.root -conpdf data-photon/nov_21/four_converted.pdf -uncpdf data-photon/nov_21/four_unconverted.pdf

# python3 draw_data_photon_eemc.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/four/nov_21/weight_weight_converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/four/nov_21/weight_weight_unconverted.root -conpdf data-photon/nov_23/four_converted.pdf -uncpdf data-photon/nov_23/four_unconverted.pdf

# python3 draw_data_photon.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/converted/final/four/nov_23/weight_weight_converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/photon/unconverted/final/four/nov_23/weight_weight_unconverted.root -conpdf data-photon/nov_23/four_converted.pdf -uncpdf data-photon/nov_23/four_unconverted.pdf


#--------------------------------------------------------------

# python3 draw_data_photon_eemc.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/con/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/unc/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Zee_MC_photon/con/converted.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Zee_MC_photon/unc/unconverted.pdf

# python3 draw_data_photon_eemc.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/con/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/unc/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Zee_MC_photon/con/converted_ligang.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Zee_MC_photon/unc/unconverted_ligang.pdf


#--------------------------------------------------------


# python3 draw_data_photon_eemc.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/con/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/unc/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_pileweight/con/converted_pileweight.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_pileweight/unc/unconverted_pileweight.pdf

# python3 draw_data_photon_eemc.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/con/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/unc/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2/con/converted_delta_E.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2/unc/unconverted_delta_E.pdf
# python3 draw_data_photon_eemc.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/con/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/unc/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2/con/converted_1_2.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2/unc/unconverted_1_2.pdf
# python3 draw_data_photon_eemc.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/con/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/unc/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2_not/con/converted_delta_E.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2_not/unc/unconverted_delta_E.pdf


# python3 draw_data_photon_eemc.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/con/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon/unc/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2_final/con/converted_1_2_final.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run2_photon_1_2_final/unc/unconverted_1_2_final.pdf
# python3 draw_data_photon_eemc.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all.pdf

# python3 draw_data_photon_eemc.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_bkg.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_bkg.pdf
# python3 draw_data_photon_eemc.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_bkg_contain_sf.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_bkg_contain_sf.pdf

#--------- fragmentation ---

# python3 draw_photon_eemc_frag_up_frag_down.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_frag_down_up.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_frag_down_up.pdf
# python3 draw_photon_eemc_frag_up_frag_down.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_frag_down_up_exchange_hard_no_data.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_frag_down_up_exchange_hard_no_data.pdf
# python3 draw_photon_eemc_frag_up_frag_down.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_frag_down_up_mc_data.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_frag_down_up_mc_data.pdf
# python3 draw_photon_eemc_frag_up_frag_down.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_frag_down_up_mc_data_4_24.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_frag_down_up_mc_data_4_24.pdf

#------------- fudge factor ------
# python3 draw_photon_eemc_fudge_factor_MC.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_fudge_factor_MC.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_fudge_factor_MC.pdf
# python3 draw_photon_eemc_fudge_factor_data.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_fudge_factor_data.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_fudge_factor_data.pdf

#---------mc  statistic ------

# python3 draw_photon_eemc_statistic.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_statistic.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_statistic.pdf

#-----------------conversion --------

# python3 draw_photon_eemc_conversion_data_MC.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_conversion_data_MC.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_conversion_data_MC.pdf

#---------just hard --------

# python3 draw_photon_eemc_just_hard.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_just_hard_data_MC.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_just_hard_data_MC.pdf

#---------enrique and 1_2_final -----------
# python3 draw_photon_eemc_enrique_and_1_2_data_MC.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_enrique_1_2_final.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_enrique_1_2_final.pdf

#------------truth_pt ------
# python3 draw_photon_eemc_truth_pt.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_truth_pt.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_truth_pt.pdf
# python3 draw_photon_eemc_truth_pt.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/truth_pt_new_pt/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/truth_pt_new_pt/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_truth_pt_new_pt.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_truth_pt_new_pt.pdf

# python3 draw_photon_eemc_frag_up_frag_down_truth_pt.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/truth_pt/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/truth_pt/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_truth_pt_fragmentation.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_truth_pt_fragmentation.pdf

# python3 draw_photon_eemc_frag_up_frag_down_truth_pt.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/truth_pt/converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/truth_pt/unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_truth_pt_fragmentation.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_truth_pt_fragmentation.pdf


#---------new_conversion---------

# python3 draw_photon_eemc_truth_pt.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/new_conversion/photon_match_converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/new_conversion/photon_match_unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_truth_pt_new_conversion_match.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_truth_pt_new_conversion_match.pdf
# python3 draw_photon_eemc_truth_pt.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/new_conversion/photon_match_converted.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/new_conversion/photon_match_unconverted.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_truth_pt_new_conversion_match_just.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_truth_pt_new_conversion_match_just.pdf
python3 draw_photon_eemc_truth_pt.py -conroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/converted/new_conversion/photon_match_converted_conversion.root -uncroot /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3_photon/unconverted/new_conversion/photon_match_unconverted_conversion.root -conpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/con/converted_all_truth_pt_new_conversion_match_conversion.pdf -uncpdf /afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/Run3/Run3_photon/unc/unconverted_all_truth_pt_new_conversion_match_conversion.pdf
