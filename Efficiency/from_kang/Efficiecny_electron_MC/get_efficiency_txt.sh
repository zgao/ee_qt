#!/bin/bash

# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/unconverted.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/dataUnconvIDEF_all_final.txt\"\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/user.zgao.34463569._000001.ANALYSIS.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/a_test.txt\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230823_042848_hist/user.zgao.34463569._000001.hist-output.root\"\)

# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/probe_probe/unc/unconverted.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/probe_probe/dataUnconvIDEF_all_final.txt\"\)

# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/tag_probe/unc/unconverted.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/tag_probe/dataUnconvIDEF_all_final.txt\"\)


# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/converted.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/dataconvIDEF_all_final.txt\"\)

# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/probe_probe/con/converted.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/probe_probe/dataconvIDEF_all_final.txt\"\)

# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/tag_probe/con/converted.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/tag_probe/dataconvIDEF_all_final.txt\"\)




# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/2/unc/unconverted.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/dataUnconvIDEF_all_final_2.txt\"\)

# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/unconverted.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/dataUnconvIDEF_all_final_change_range.txt\"\)

#----------------------------------------------------------------------------------------------------------------------------------

# # # for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/add_add/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/add_substract/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/substract_add/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/substract_substract/user*.root)

# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/frag_down/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/nominal/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/just_hard/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/conversion_down/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/conversion_up/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/frag_up/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/truth_pt/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/truth_pt_frag_down/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/truth_pt_frag_up/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/truth_pt_new_conversion/user*.root)
for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/truth_pt_new_conversion/just_match/user*.root)
# # for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/con/statistic/user*.root)
# # # # for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/probe_probe/con/user*.root)
# # # for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/tag_probe/con/user*.root)
do
# #     # if [[ $name == *000002* ]]; then
	# echo $name
#
	name2=$(basename $name)
	name3=${name2/.root/.root_txt}
	name4=${name2/.ANALYSIS.root/.hist-output.root}
	echo $name2
#     # r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3_ligang_ff/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/no_ff/user.zgao.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20231212_134708_hist/$name4\"\)
#     # r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3_1_2/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/user.zgao.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20231221_070359_hist/$name4\"\)
#     # r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3_1_2_final/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/user.zgao.tp-analyse-example_zmass-data22_13p6TeV-EGAM1-grp22_v01_p5538_20240308_174627_hist/$name3\"\)
#     # r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3_photon/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/user.zgao.tp-analyse-example_zmass-data22_13p6TeV-EGAM1-grp22_v01_p5538_20240308_174627_hist/$name3\"\)
#     # r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3_photon/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/user.zgao.tp-analyse-example_zmass-data22_13p6TeV-EGAM1-grp22_v01_p5538_20240308_174627_hist/$name3\"\)
#
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/add_add/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.4_14_add_add.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240414_132903_hist/$name3\"\)
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/add_substract/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.4_14_add_substract.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240414_133236_hist/$name3\"\)
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/substract_add/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.4_14_substract_add.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240414_133618_hist/$name3\"\)
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/substract_substract/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.4_14_substract_substract.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240414_133938_hist/$name3\"\)
#
#     # r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/statistic/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.substract_substract.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240322_035526_hist/$name3\"\)
#
#
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/nominal/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/both_pass_probe/nominal/user.zgao.4_14_nominal.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240414_132410_hist/$name3\"\)
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/just_hard/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/both_pass_probe/nominal/user.zgao.4_14_nominal.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240414_132410_hist/$name3\"\)
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/conversion_down/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/both_pass_probe/nominal/user.zgao.4_14_nominal.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240414_132410_hist/$name3\"\)
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/conversion_up/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/both_pass_probe/nominal/user.zgao.4_14_nominal.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240414_132410_hist/$name3\"\)
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/frag_down/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.add_add.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240322_033815_hist/$name3\"\)
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/frag_up/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.add_add.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240322_033815_hist/$name3\"\)
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/truth_pt/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.add_add.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240322_033815_hist/$name3\"\)
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/truth_frag_down/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.add_add.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240322_033815_hist/$name3\"\)
	# r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/truth_frag_up/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.add_add.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240322_033815_hist/$name3\"\)
	r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/truth_pt_new_conversion/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/23a_nominal/user.zgao.ff_final.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240527_145548_hist/$name3\"\)
#
#     # r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3_1_2_not/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/user.zgao.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20231221_070359_hist/$name4\"\)
# #     r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/converted/Run3/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/user.zgao.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20231221_070359_hist/$name4\"\)
# #     # r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/probe_probe/converted/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/probe_probe/user.zgao.tp-example_zmass_pro_pro-361106-EGAM1-e3601_s3681_r13145_p4940_20230914_101851_hist/$name4\"\)
# #     # r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/tag_probe/converted/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230823_042848_hist/$name4\"\)
# #     # fi
done

#=======================--------------------------=============***********************************$$$$$$$$$$$$$$$$$===================================

# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/user*.root)

# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/add_add/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/add_substract/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/substract_add/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/substract_substract/user*.root)

# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/statistic/user*.root)

# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/frag_down/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/frag_up/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/truth_pt/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/truth_pt_frag_down/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/truth_pt_frag_up/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/truth_pt_new_conversion/user*.root)
for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/truth_pt_new_conversion/just_match/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/nominal/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/just_hard/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/conversion_down/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/both_pass_probe/unc/conversion_up/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/probe_probe/unc/user*.root)
# for name in $( ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved_electron_MC/tag_probe/unc/user*.root)
do
# #     # if [[ $name == *000095* ]]; then
	# echo $name
	name2=$(basename $name)
	name3=${name2/.root/.root_txt}
	name4=${name2/.ANALYSIS.root/.hist-output.root}
	echo $name2
	# echo $name3
	# echo $name4
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/user.zgao.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20231221_070359_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3_ligang_ff/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/no_ff/user.zgao.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20231212_134708_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3_pileweight/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/user.zgao.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20231221_070359_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3_1_2/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/user.zgao.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20231221_070359_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3_1_2_final/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/user.zgao.tp-analyse-example_zmass-data22_13p6TeV-EGAM1-grp22_v01_p5538_20240308_174627_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3_photon/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/user.zgao.tp-analyse-example_zmass-data22_13p6TeV-EGAM1-grp22_v01_p5538_20240308_174627_hist/$name4\"\)

	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/add_add/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.add_add_4_11.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240411_160426_hist/$name3\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/add_substract/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.add_tract_4_11.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240411_161215_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/substract_add/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.subtract_add_4_11.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240411_162303_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/substract_substract/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.subtract_subtract_4_11.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240411_162846_hist/$name4\"\)

	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/statistic/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.substract_substract.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240322_035526_hist/$name4\"\)

	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/frag_down/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.add_add.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240322_033815_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/frag_up/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.add_add.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240322_033815_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/truth_pt/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.add_add.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240322_033815_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/truth_frag_down/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.add_add.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240322_033815_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/truth_frag_up/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/four_conditions/user.zgao.add_add.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240322_033815_hist/$name4\"\)
	r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/truth_pt_new_conversion/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/23a_nominal/user.zgao.ff_final.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240527_145548_hist/$name4\"\)

	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/nominal/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/both_pass_probe/nominal/user.zgao.4_14_nominal.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240414_132410_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/just_hard/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/both_pass_probe/nominal/user.zgao.4_14_nominal.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240414_132410_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/conversion_down/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/both_pass_probe/nominal/user.zgao.4_14_nominal.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240414_132410_hist/$name4\"\)
	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/conversion_up/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/both_pass_probe/nominal/user.zgao.4_14_nominal.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20240414_132410_hist/$name4\"\)

	# r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/Run3_1_2_not/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/Run3/user.zgao.tp-analyse-example_zmass-601189-EGAM1-e8514_s4111_r14622_p5660_20231221_070359_hist/$name4\"\)
#     r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/both_pass_probe/unconverted/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230823_042848_hist/$name4\"\)
#     # r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/probe_probe/unconverted/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/probe_probe/user.zgao.tp-example_zmass_pro_pro-361106-EGAM1-e3601_s3681_r13145_p4940_20230914_101851_hist/$name4\"\)
#     # r forID_uncon.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/Efficiecny_electron_MC/tag_probe/unconverted/txt_w/$name3\",\"/afs/cern.ch/user/z/zgao/eos/my_fudge_factor/user.zgao.tp-example_zmass-361106-EGAM1-e3601_s3681_r13145_p4940_20230823_042848_hist/$name4\"\)
#     # fi
done

#--------------------------------------------------------------------------------------------------------------

