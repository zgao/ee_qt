import numpy as np
import math
import ROOT
import sys
import argparse

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/z/zgao/eos/AtlasStyle/AtlasStyle.C")
from ROOT import SetAtlasStyle
SetAtlasStyle()


ROOT.gStyle.SetOptStat(0)
list_bin=[25,30,  35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250]
list_graph_x=[27.5,32.5,37.5,42.5,47.5,55,70,90,112.5,137.5,162.5,212.5]
seven_eta_cut_name=["0.00<|#eta|<0.60","0.60<|#eta|<0.80","0.80<|#eta|<1.15","1.15<|#eta|<1.37","1.52<|#eta|<1.81","1.81<|#eta|<2.01","2.01<|#eta|<2.37"]


file1 = ROOT.TFile("photon/converted.root","READ")
file2 = ROOT.TFile("photon/uconverted.root","READ")
file3 = ROOT.TFile("photon_noFF/converted.root","READ")
file4 = ROOT.TFile("photon_noFF/uconverted.root","READ")

file1_err = ROOT.TFile("photon/converted_err.root","READ")
file2_err = ROOT.TFile("photon/uconverted_err.root","READ")
file3_err = ROOT.TFile("photon_noFF/converted_err.root","READ")
file4_err = ROOT.TFile("photon_noFF/uconverted_err.root","READ")

key_list = file1.GetListOfKeys()

canvas=ROOT.TCanvas("canvas","",800,600)
canvas.Print("converted_pho_FF_noFF.pdf"+"[")

canvas2=ROOT.TCanvas("canvas2","",800,600)
canvas2.Print("unconverted_pho_FF_noFF.pdf"+"[")

canvas_err=ROOT.TCanvas("canvas_err","",800,600)
canvas_err.Print("converted_pho_FF_noFF_err.pdf"+"[")

canvas2_err=ROOT.TCanvas("canvas2_err","",800,600)
canvas2_err.Print("unconverted_pho_FF_noFF_err.pdf"+"[")

for i in range(7):
	hist1 = file1.Get(f"histogram;{int(i+1)}")
	hist2 = file2.Get(f"histogram;{int(i+1)}")
	hist3 = file3.Get(f"histogram;{int(i+1)}")
	hist4 = file4.Get(f"histogram;{int(i+1)}")


	canvas.cd()
	hist1.Draw("h,e")
	hist1.SetLineColor(ROOT.kRed)
	hist1.SetMarkerColor(ROOT.kRed)
	hist1.SetMarkerSize(0)
	hist3.Draw("h,same")
	hist3.SetLineColor(ROOT.kBlue)
	hist3.SetMarkerColor(ROOT.kBlue)
	hist3.SetMarkerSize(0)
	legend = ROOT.TLegend(0.6,0.5,0.9,0.7)
	legend.AddEntry(hist3," photon_MC_noFF ","l")
	legend.AddEntry(hist1," photon_MC_FF ","l")
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	legend.Draw("same")
	#  text1 = ROOT.TPaveText(0.6,0.6,0.9,0.7)
	text1 = ROOT.TPaveText(150,0.6,200,0.7)
	text1.AddText("converted")
	text1.AddText(seven_eta_cut_name[i])
	text1.SetFillStyle(0)
	text1.SetBorderSize(0)
	text1.SetTextSize(0.03)
	text1.Draw("same")


	canvas.Print("converted_pho_FF_noFF.pdf")

	canvas2.cd()
	hist2.Draw("h,e")
	hist2.SetLineColor(ROOT.kRed)
	hist2.SetMarkerColor(ROOT.kRed)
	hist2.SetMarkerSize(0)
	hist4.Draw("h,same")
	hist4.SetLineColor(ROOT.kBlue)
	hist4.SetMarkerColor(ROOT.kBlue)
	hist4.SetMarkerSize(0)
	legend2 = ROOT.TLegend(0.6,0.5,0.9,0.7)
	legend2.AddEntry(hist4," photon_MC_noFF ","l")
	legend2.AddEntry(hist2," photon_MC_FF ","l")
	legend2.SetFillStyle(0)
	legend2.SetBorderSize(0)
	legend2.Draw("same")
	#  text2 = ROOT.TPaveText(0.6,0.4,0.9,0.6)
	text2 = ROOT.TPaveText(150,0.6,200,0.7)
	text2.AddText("unconverted")
	text2.AddText(seven_eta_cut_name[i])
	text2.SetFillStyle(0)
	text2.SetBorderSize(0)
	text2.SetTextSize(0.03)
	text2.Draw("same")
	canvas2.Print("unconverted_pho_FF_noFF.pdf")

	#-------------------------------------------------------------------------- error-------------
	hist1_err = file1_err.Get(f"histogram2;{int(i+1)}")
	hist2_err = file2_err.Get(f"histogram2;{int(i+1)}")
	hist3_err = file3_err.Get(f"histogram2;{int(i+1)}")
	hist4_err = file4_err.Get(f"histogram2;{int(i+1)}")


	canvas_err.cd()
	hist1_err.Draw("h")
	hist1_err.SetLineColor(ROOT.kRed)
	hist1_err.SetMarkerColor(ROOT.kRed)
	hist1_err.SetMarkerSize(0)
	hist3_err.Draw("h,same")
	hist3_err.SetLineColor(ROOT.kBlue)
	hist3_err.SetMarkerColor(ROOT.kBlue)
	hist3_err.SetMarkerSize(0)
	legend_err = ROOT.TLegend(0.6,0.5,0.9,0.7)
	legend_err.AddEntry(hist3," photon_MC_noFF ","l")
	legend_err.AddEntry(hist1," photon_MC_FF ","l")
	legend_err.SetFillStyle(0)
	legend_err.SetBorderSize(0)
	legend_err.Draw("same")
	#  text1 = ROOT.TPaveText(0.6,0.6,0.9,0.7)
	text1_err = ROOT.TPaveText(150,0.6,200,0.7)
	text1_err.AddText("converted")
	text1_err.AddText(seven_eta_cut_name[i])
	text1_err.SetFillStyle(0)
	text1_err.SetBorderSize(0)
	text1_err.SetTextSize(0.03)
	text1_err.Draw("same")


	canvas_err.Print("converted_pho_FF_noFF_err.pdf")

	canvas2_err.cd()
	hist2_err.Draw("h")
	hist2_err.SetLineColor(ROOT.kRed)
	hist2_err.SetMarkerColor(ROOT.kRed)
	hist2_err.SetMarkerSize(0)
	hist4_err.Draw("h,same")
	hist4_err.SetLineColor(ROOT.kBlue)
	hist4_err.SetMarkerColor(ROOT.kBlue)
	hist4_err.SetMarkerSize(0)
	legend2_err = ROOT.TLegend(0.6,0.5,0.9,0.7)
	legend2_err.AddEntry(hist4," photon_MC_noFF ","l")
	legend2_err.AddEntry(hist2," photon_MC_FF ","l")
	legend2_err.SetFillStyle(0)
	legend2_err.SetBorderSize(0)
	legend2_err.Draw("same")
	#  text2 = ROOT.TPaveText(0.6,0.4,0.9,0.6)
	text2_err = ROOT.TPaveText(150,0.6,200,0.7)
	text2_err.AddText("unconverted")
	text2_err.AddText(seven_eta_cut_name[i])
	text2_err.SetFillStyle(0)
	text2_err.SetBorderSize(0)
	text2_err.SetTextSize(0.03)
	text2_err.Draw("same")
	canvas2_err.Print("unconverted_pho_FF_noFF_err.pdf")

canvas.Print("converted_pho_FF_noFF.pdf"+"]")
canvas2.Print("unconverted_pho_FF_noFF.pdf"+"]")


canvas_err.Print("converted_pho_FF_noFF_err.pdf"+"]")
canvas2_err.Print("unconverted_pho_FF_noFF_err.pdf"+"]")
