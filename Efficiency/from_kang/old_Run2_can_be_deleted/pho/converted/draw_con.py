import numpy as np
import ROOT


with open('MCconvIDEF_all.txt', 'r') as file:
	lines = file.readlines()
labels = [] 

ratios = []

for i,line in enumerate(lines):
	values = line.strip().split('\t')

	if i%13==0:
		labels.append(line.strip())
	elif len(values)>=2:
		first_value = float(values[0])
		second_value = float(values[1])
		ratio = first_value / second_value
		ratios.append(ratio)

#  print("strings:",labels)
#  for string in labels:
#      print(string)
#  print("Ratios: ",ratios)

ROOT.gStyle.SetOptStat(0)
list_bin=[30,  35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250,350]
list_graph_x=[32.5,37.5,42.5,47.5,55,70,90,112.5,137.5,162.5,212.5,300]

canvas=ROOT.TCanvas("canvas","",800,600)
canvas.Print("converted.pdf"+"[")

histogram = ROOT.TH1F("histogram","efficiency",12,np.array(list_bin,np.float32))


for j in range(7):
	for i in range(histogram.GetNbinsX()):
		histogram.SetBinContent(i+1,ratios[int(i+j*12)])

	histogram.GetXaxis().SetTitle("pt[Gev]")
	histogram.GetYaxis().SetTitle("ID efficiency")
	canvas.cd()
	histogram.Draw()
	legend=ROOT.TLegend(0.5,0.6,0.7,0.7)
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	legend.SetTextSize(0.03)
	legend.AddEntry(histogram,"MC photon","L")
	legend.Draw()

	pt = ROOT.TPaveText(0.5, 0.4, 0.7, 0.55, "ndc")
	pt.SetTextSize(0.035)
	pt.SetBorderSize(0)
	pt.SetFillStyle(0)
	pt.SetTextAlign(13)
	pt.SetTextFont(42)
	pt.AddText(f"converted,{labels[int(j%7)]}")
	pt.Draw()

	canvas.Print("converted.pdf")
canvas.Print("converted.pdf"+"]")
