//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Apr 18 12:02:45 2023 by ROOT version 6.24/06
// from TTree PhotonCandidates/PhotonCandidates
// found on file: data18nofconvtrans.root
//////////////////////////////////////////////////////////

#ifndef IDEFconv_h
#define IDEFconv_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class IDEFconv {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          Run_number;
   ULong64_t       Event_number;
   Double_t        eta;
   Double_t        phi;
   Double_t        pt;
   Double_t        charge;
   Double_t        primary_cluster_be2_eta;
   Double_t        primary_cluster_be2_abseta;
   Double_t        r_had0;
   Double_t        r_had1;
   Double_t        e_ratio;
   Double_t        delta_E;
   Double_t        w_eta2;
   Double_t        w_eta1;
   Double_t        r_eta;
   Double_t        r_phi;
   Double_t        w_stot;
   Double_t        f1;
   Double_t        fracs1;
   Double_t        f3;
   Double_t        ptcone20;
   Double_t        topoetcone20;

   Double_t        SumWeights;

   Int_t           pass__TightLLH;
   Int_t           pass__denominator;
   Double_t        actual_mu;
   Double_t        average_mu;
	Int_t           convType;
	Bool_t          FixedCutLoose;


   // List of branches
   TBranch        *b_Run_number;   //!
   TBranch        *b_Event_number;   //!
   TBranch        *b_eta;   //!
   TBranch        *b_phi;   //!
   TBranch        *b_pt;   //!
   TBranch        *b_charge;   //!
   TBranch        *b_primary_cluster_be2_eta;   //!
   TBranch        *b_primary_cluster_be2_abseta;   //!
   TBranch        *b_r_had0;   //!
   TBranch        *b_r_had1;   //!
   TBranch        *b_e_ratio;   //!
   TBranch        *b_delta_E;   //!
   TBranch        *b_w_eta2;   //!
   TBranch        *b_w_eta1;   //!
   TBranch        *b_r_eta;   //!
   TBranch        *b_r_phi;   //!
   TBranch        *b_w_stot;   //!
   TBranch        *b_f1;   //!
   TBranch        *b_fracs1;   //!
   TBranch        *b_f3;   //!
   TBranch        *b_ptcone20;   //!
   TBranch        *b_topoetcone20;   //!
   TBranch        *b_pass__TightLLH;   //!
   TBranch        *b_pass__denominator;   //!
   TBranch        *b_actual_mu;   //!
   TBranch        *b_average_mu;   //!
   TBranch        *b_convType;   //!
   TBranch        *b_FixedCutLoose;   //!
   TBranch        *b_SumWeights;   //!

   IDEFconv(TTree *tree=0);
   virtual ~IDEFconv();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef IDEFconv_cxx
IDEFconv::IDEFconv(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      // TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("data18nofconvtrans.root");
      // TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../apply_ST/convtranstest.root");
      // TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("~/eos/con1_fudge/photon/merge.root");
      // TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/afs/cern.ch/user/z/zgao/eos/con1_fudge/pho_3day/PyPt8_inf_mc20e_p5536_Rel22_AB22.2.97_v09_3.root");
      // TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/afs/cern.ch/user/z/zgao/eos/con1_fudge/photon_13_totWeight_fudged/merge.root");
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/afs/cern.ch/user/z/zgao/eos/con1_fudge/photon_13_totWeight/merge.root");
      if (!f || !f->IsOpen()) {
         // f = new TFile("data18nofconvtrans.root");
         // f = new TFile("../apply_ST/convtranstest.root");
         // f = new TFile("/afs/cern.ch/user/z/zgao/eos/con1_fudge/pho_3day/PyPt8_inf_mc20e_p5536_Rel22_AB22.2.97_v09_3.root");
         // f = new TFile("/afs/cern.ch/user/z/zgao/eos/con1_fudge/photon_13_totWeight_fudged/merge.root");
         f = new TFile("/afs/cern.ch/user/z/zgao/eos/con1_fudge/photon_13_totWeight/merge.root");
      }
	  f->GetObject("PhotonCandidates",tree);
	  // f->GetObject("ZeeCandidates",tree);
   }
   Init(tree);
}

IDEFconv::~IDEFconv()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t IDEFconv::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t IDEFconv::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void IDEFconv::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Run_number", &Run_number, &b_Run_number);

   fChain->SetBranchAddress("SumWeights", &SumWeights, &b_SumWeights);

   fChain->SetBranchAddress("Event_number", &Event_number, &b_Event_number);
   fChain->SetBranchAddress("eta", &eta, &b_eta);
   fChain->SetBranchAddress("phi", &phi, &b_phi);
   fChain->SetBranchAddress("pt", &pt, &b_pt);
   fChain->SetBranchAddress("charge", &charge, &b_charge);
   fChain->SetBranchAddress("primary_cluster_be2_eta", &primary_cluster_be2_eta, &b_primary_cluster_be2_eta);
   fChain->SetBranchAddress("primary_cluster_be2_abseta", &primary_cluster_be2_abseta, &b_primary_cluster_be2_abseta);
   fChain->SetBranchAddress("r_had0", &r_had0, &b_r_had0);
   fChain->SetBranchAddress("r_had1", &r_had1, &b_r_had1);
   fChain->SetBranchAddress("e_ratio", &e_ratio, &b_e_ratio);
   fChain->SetBranchAddress("delta_E", &delta_E, &b_delta_E);
   fChain->SetBranchAddress("w_eta2", &w_eta2, &b_w_eta2);
   fChain->SetBranchAddress("w_eta1", &w_eta1, &b_w_eta1);
   fChain->SetBranchAddress("r_eta", &r_eta, &b_r_eta);
   fChain->SetBranchAddress("r_phi", &r_phi, &b_r_phi);
   fChain->SetBranchAddress("w_stot", &w_stot, &b_w_stot);
   fChain->SetBranchAddress("f1", &f1, &b_f1);
   fChain->SetBranchAddress("fracs1", &fracs1, &b_fracs1);
   fChain->SetBranchAddress("f3", &f3, &b_f3);
   fChain->SetBranchAddress("ptcone20", &ptcone20, &b_ptcone20);
   fChain->SetBranchAddress("topoetcone20", &topoetcone20, &b_topoetcone20);
   fChain->SetBranchAddress("pass__TightLLH", &pass__TightLLH, &b_pass__TightLLH);
   fChain->SetBranchAddress("pass__denominator", &pass__denominator, &b_pass__denominator);
   fChain->SetBranchAddress("actual_mu", &actual_mu, &b_actual_mu);
   fChain->SetBranchAddress("average_mu", &average_mu, &b_average_mu);
   fChain->SetBranchAddress("convType", &convType, &b_convType);
   fChain->SetBranchAddress("FixedCutLoose", &FixedCutLoose, &b_FixedCutLoose);
   Notify();
}

Bool_t IDEFconv::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void IDEFconv::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t IDEFconv::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef IDEFconv_cxx
