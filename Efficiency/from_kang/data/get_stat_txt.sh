#!/bin/bash

# for name in $(ls /afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/statistic/smirnov_*.root)
# do
#     echo $name
#     # if [ "$(basename $name)" = "smirnov_transformation0.root" ]; then
#
#     name2=$(basename "$name")
#     name3=${name2/.root/.root_txt}
#     r forID.cxx\(\"$name\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/mc_stat/$name3\"\)
#     # fi
# done

# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/nominal_all_range.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/80_100_con.txt\"\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/nominal_all_range.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/80_100_unc.txt\"\)

# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/nominal_all_range.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/70_110_con.txt\"\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/nominal_all_range.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/70_110_unc.txt\"\)

# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/nominal_all_range.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/75_105_con.txt\"\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/nominal_all_range.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/75_105_unc.txt\"\)

# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/nominal_all_range.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/60_120_con.txt\"\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/nominal_all_range.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/background/mee_AL/src/nominal_all_range/60_120_unc.txt\"\)
#----------------------just hard---------------------

# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/just_hard_all_range.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/just_hard/80_100_con.txt\"\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/just_hard_all_range.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/just_hard/80_100_unc.txt\"\)

# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/just_hard_all_range.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/just_hard/70_110_con.txt\"\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/just_hard_all_range.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/just_hard/70_110_unc.txt\"\)

#--------------conversion down ---------------
#------------------to truth pt--
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_conversion_down/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/conversion/truth/new_conversion/80_100_down_con.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_conversion_down/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/conversion/truth/new_conversion/80_100_down_unc.txt\",1\)

# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_conversion_down/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/conversion/truth/new_conversion/70_110_down_con.txt\",0\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_conversion_down/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/conversion/truth/new_conversion/70_110_down_unc.txt\",0\)

#----------------conversion up ---------------
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_conversion_up/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/conversion/truth/new_conversion/80_100_up_con.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_conversion_up/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/conversion/truth/new_conversion/80_100_up_unc.txt\",1\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_conversion_up/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/conversion/truth/new_conversion/70_110_up_con.txt\",0\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_conversion_up/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/conversion/truth/new_conversion/70_110_up_unc.txt\",0\)
#
# #-----------------------frag down -------
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_frag_down/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/new_conversion/80_100_down_con.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_frag_down/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/new_conversion/80_100_down_unc.txt\",1\)
# #
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_frag_down/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/new_conversion/70_110_down_con.txt\",0\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_frag_down/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/new_conversion/70_110_down_unc.txt\",0\)

#-------------------frag up -------------

# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_frag_up/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/new_conversion/80_100_up_con.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_frag_up/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/new_conversion/80_100_up_unc.txt\",1\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_frag_up/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/new_conversion/70_110_up_con.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_frag_up/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fragmentation/new_conversion/70_110_up_unc.txt\",1\)

#---------------add_add ------------ .    to   truth_pt --------------
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/add_add/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/add_add/truth/80_100_add_add_con.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/add_add/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/add_add/truth/80_100_add_add_unc.txt\",1\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/add_add/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/add_add/truth/70_110_add_add_con.txt\",0\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/add_add/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/add_add/truth/70_110_add_add_unc.txt\",0\)
#
# ---------------add_sub----------
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/add_substract/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/add_sub/truth/80_100_add_sub_con.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/add_substract/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/add_sub/truth/80_100_add_sub_unc.txt\",1\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/add_substract/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/add_sub/truth/70_110_add_sub_con.txt\",0\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/add_substract/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/add_sub/truth/70_110_add_sub_unc.txt\",0\)
#
# ----------------sub_add-----------
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/substract_add/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/sub_add/truth/80_100_sub_add_con.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/substract_add/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/sub_add/truth/80_100_sub_add_unc.txt\",1\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/substract_add/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/sub_add/truth/70_110_sub_add_con.txt\",0\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/substract_add/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/sub_add/truth/70_110_sub_add_unc.txt\",0\)
#
# ---------------sub_sub-----------
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/substract_substract/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/sub_sub/truth/80_100_sub_sub_con.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/substract_substract/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/sub_sub/truth/80_100_sub_sub_unc.txt\",1\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/substract_substract/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/sub_sub/truth/70_110_sub_sub_con.txt\",0\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/substract_substract/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/fudgefactor/calculate_uncertainty/sub_sub/truth/70_110_sub_sub_unc.txt\",0\)

#---------------------------truth_pt --------------
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_new_pt/merge.root\",\"./new/truth_pt_new_pt/80_100_sub_sub_con.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_new_pt/merge.root\",\"./new/truth_pt_new_pt/80_100_sub_sub_unc.txt\",1\)

#--------------------------------------truth_pt new_conversion---------------
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_new_version/merge.root\",\"./new/80_100_con_new_conversion.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_new_conversion/merge.root\",\"./new/80_100_unc_new_conversion.txt\",1\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_new_version/merge_deleted.root\",\"./new/80_100_con_new_conversion_deleted.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_new_conversion/merge_deleted.root\",\"./new/80_100_unc_new_conversion_deleted.txt\",1\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_new_version/just_match/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/80_100_con.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_new_conversion/just_match/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/80_100_unc.txt\",1\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_new_version/just_match/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/70_110_con.txt\",0\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_new_conversion/just_match/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/70_110_unc.txt\",0\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_new_version/just_match/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/60_120_con.txt\",2\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_new_conversion/just_match/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/60_120_unc.txt\",2\)
# #
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_new_version/just_match/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/75_105_con.txt\",3\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_new_conversion/just_match/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/75_105_unc.txt\",3\)
#-just_match--

# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_new_version/just_match/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/just_match/80_100_con.txt\",1\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_new_conversion/just_match/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/just_match/80_100_unc.txt\",1\)
# #
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_new_version/just_match/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/just_match/70_110_con.txt\",0\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_new_conversion/just_match/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/new_conversion/just_match/70_110_unc.txt\",0\)

#------------------------------------truth_pt fragmen------------

# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_frag_down/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt_frag_down/80_100_frag_down_con.txt\"\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_frag_down/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt_frag_down/80_100_frag_down_unc.txt\"\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_frag_down/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt_frag_down/70_110_frag_down_con.txt\"\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_frag_down/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt_frag_down/70_110_frag_down_unc.txt\"\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_frag_up/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt_frag_up/80_100_frag_up_con.txt\"\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_frag_up/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt_frag_up/80_100_frag_up_unc.txt\"\)
#
# r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/truth_pt_frag_up/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt_frag_up/70_110_frag_up_con.txt\"\)
# r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt_frag_up/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt_frag_up/70_110_frag_up_unc.txt\"\)

# #---------------------------for converted --------------------------------------------
directory="/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/statistic/"

files=($directory/smirnov_transformation9*.root)

total_files=${#files[@]}

counter=0

while [ $counter -lt $total_files ]; do
	end_index=$((counter + 5))

	for ((i = $counter; i < $end_index && i < $total_files; i++)); do
		echo "Processing file: ${files[i]}"

		name2=$(basename "${files[i]}")

		name3=${name2/.root/.root_txt}

		echo "Executing command for ${files[i]} in background..."
		# echo ${files[i]}
		# echo $name2
		# echo $name3
		r forID.cxx\(\"${files[i]}\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/mc_stat/con/70_110/truth/$name3\",0\) &
		# r forID.cxx\(\"${files[i]}\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/mc_stat/con/80_100/truth/$name3\",1\) &
		((counter++))
	done
	wait  # Wait for all background processes to finish before starting the next group
	# sleep 5
done
# #
# # #----------------------------------for unconverted ----------------------------------------
# directory="/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/statistic/"
#
# files=($directory/smirnov_*.root)
#
# total_files=${#files[@]}
#
# counter=0
#
# while [ $counter -lt $total_files ]; do
#     end_index=$((counter + 5))
#
#     for ((i = $counter; i < $end_index && i < $total_files; i++)); do
#         echo "Processing file: ${files[i]}"
#
#         name2=$(basename "${files[i]}")
#
#         name3=${name2/.root/.root_txt}
#
#         echo "Executing command for ${files[i]} in background..."
#         r forID_uncon.cxx\(\"${files[i]}\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/mc_stat/unc/70_110/truth/$name3\",0\) &
#         r forID_uncon.cxx\(\"${files[i]}\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/systematics/mc_stat/unc/80_100/truth/$name3\",1\) &
#         ((counter++))
#     done
#     wait  # Wait for all background processes to finish before starting the next group
#     # sleep 5
# done
#
# for four_condition in add_add add_substract substract_add substract_substract; do
#     echo $four_condition
#     for four_kinds in r_eta r_had1 r_phi fracs1; do
#         echo $four_kinds
#
#         # r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/four_fudge_factor/add_add/r_eta_w_eta2/smirnoved.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/four_fudge_factor/75_105_sub_sub_con.txt\"\)
#         # r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/truth_pt/merge.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/truth_pt/75_105_sub_sub_unc.txt\"\)
#
#         r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/four_fudge_factor/${four_condition}/${four_kinds}/smirnoved.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/four_fudge_factor/${four_condition}_${four_kinds}_70_110_con.txt\",0\)
#         r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/four_fudge_factor/${four_condition}/${four_kinds}/smirnoved.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/four_fudge_factor/${four_condition}_${four_kinds}_70_110_unc.txt\",0\)
#
#         r forID.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/con/Run3/four_fudge_factor/${four_condition}/${four_kinds}/smirnoved.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/four_fudge_factor/${four_condition}_${four_kinds}_80_100_con.txt\",1\)
#         r forID_uncon.cxx\(\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Smirnoved/unc/Run3/four_fudge_factor/${four_condition}/${four_kinds}/smirnoved.root\",\"/afs/cern.ch/user/z/zgao/eos/ee_qt/Efficiency/from_kang/four_fudge_factor/${four_condition}_${four_kinds}_80_100_unc.txt\",1\)
#
#     done
# done
