#include <iostream>
#include <fstream>
#include <vector>
using namespace std;
void ID(){


    std::ifstream file1("PhotonIsEMTightSelectorCutDefs.conf");
    std::vector<std::string> lines;

    if (file1.is_open()) {
        std::string line;
        while (std::getline(file1, line)) {
            lines.push_back(line);
        }
        file1.close();
    }

    std::vector<std::string> converted_lines;
    std::vector<std::string> unconverted_lines;

    int start_line = 39;
    for (int i = start_line; i < lines.size(); i++) {
        std::string line = lines[i];
        if (line.find("photonsConverted") != std::string::npos) {
            converted_lines.push_back(line);
			// cout<<line<<endl;
        } else if (line.find("photonsNonConverted") != std::string::npos) {
            unconverted_lines.push_back(line);
        }
    }

    std::vector<std::string> old_name_con = {"CutHadLeakage_photonsConverted", "Reta37_photonsConverted", "Rphi33_photonsConverted", "weta2_photonsConverted", "deltae_photonsConverted", "DEmaxs1_photonsConverted", "wtot_photonsConverted", "fracm_photonsConverted", "w1_photonsConverted"};
    std::vector<std::string> old_name_unc = {"CutHadLeakage_photonsNonConverted", "Reta37_photonsNonConverted", "Rphi33_photonsNonConverted", "weta2_photonsNonConverted", "deltae_photonsNonConverted", "DEmaxs1_photonsNonConverted", "wtot_photonsNonConverted", "fracm_photonsNonConverted", "w1_photonsNonConverted"};
    std::vector<std::string> new_name = {"rhad1 or rhad0", "r_eta", "r_phi", "w_eta2", "delta_E", "e_ratio", "w_stot", "fracs1", "w_eta1"};

    std::ofstream converted_file("converted_lines.txt");
    for (int i = 0; i < converted_lines.size(); i++) {
        std::string line = converted_lines[i];
		// cout<<converted_lines[i]<<endl;
		size_t lastSemicolonPos = line.rfind(";");
	    if (lastSemicolonPos != std::string::npos) {
          // Extract the substring before the last semicolon
			std::string newLine = line.substr(0, lastSemicolonPos);
  
          // Update the original line with the modified substring
            line = newLine;
  
            // std::cout << "Modified line: " << line << std::endl;
        } else {
             std::cout << "No semicolon found in the line." << std::endl;
        }
        // line = line.substr(0, line.find('#'));
		// cout<<line<<endl;
        if ((i - 10) % 11 == 0) {
            line += "; 9999";
        }
		// cout<<line<<endl;
        std::replace(line.begin(), line.end(), ':', ';');
        std::replace(line.begin(), line.end(), '+', ' ');
        converted_file << line << std::endl;
    }
    converted_file.close();


  	//----------------------------------------------------Part 2--------------------------------//
	
    std::ifstream file2("converted_lines.txt");
    // if (!file2) {
    //     std::cerr << "Failed to open the file." << std::endl;
    //     return 1;
    // }

    std::vector<std::vector<std::vector<double>>> values(9, std::vector<std::vector<double>>(9, std::vector<double>(11, 0.0)));
    std::vector<int> name_number(old_name_con.size(), 0);

    std::string line;
    while (std::getline(file2, line)) {
        line.erase(line.find_last_not_of(" \t\r\n") + 1); // Remove trailing whitespaces
        std::vector<std::string> split_line;
        size_t pos = 0;
        std::string delimiter = ";";
		cout<<line<<"    -------"<<endl;
        while ((pos = line.find(delimiter)) != std::string::npos) {
            std::string token = line.substr(0, pos);
            split_line.push_back(token);
            line.erase(0, pos + delimiter.length());
        }
		cout<<line<<endl;

        split_line.push_back(line);

		// for(const auto & value:split_line){
		//     cout<<value<<endl;
		// }
		// int index=0;
        // if (std::find(old_name_con.begin(), old_name_con.end(), split_line[0]) != old_name_con.end()) {
		int index = std::distance(old_name_con.begin(), std::find(old_name_con.begin(), old_name_con.end(), split_line[0]));
		name_number[index]++;
			// }
		for (int column = 1; column < 10; column++) {
				cout<<index<<endl;
                values[index][int(column - 1)][int(name_number[index] - 1)] = std::stod(split_line[column]);
				// cout<<split_line[column]<<"  ******"<<endl;
            }
        }
    file2.close();

    // std::cout << values[0][7][0] << std::endl;
    // std::cout << values[0][7][1] << std::endl;
    // std::cout << values[0][7][2] << std::endl;
    // std::cout << values[0][7][3] << std::endl;
    // std::cout << values[0][7][4] << std::endl;
    // std::cout << values[0][7][5] << std::endl;
    // std::cout << values[0][7][6] << std::endl;
	
	std::vector<double>&subvector = values[0][7];
	for ( const auto &value: subvector){
		cout<<value<<endl;
	}







} 
