import re

with open('PhotonIsEMTightSelectorCutDefs.conf','r') as file:
	lines = file.readlines()

converted_lines = []
unconverted_lines = []



start_line=39
for i,line in enumerate(lines):
	if i >=start_line:
		if "photonsConverted" in line :
			converted_lines.append(line)
		elif "photonsNonConverted" in line:
			unconverted_lines.append(line)

categories = {
	"0-15 GeV": [],
	"15-20 GeV": [],
	"20-25 GeV": [],
	"25-30 GeV": [],
	"30-35 GeV": [],
	"35-40 GeV": [],
	"40-45 GeV": [],
	"45-50 GeV": [],
	"50-60 GeV": [],
	"60-80 GeV": [],
	"80-inf GeV": []
}

old_name_con = ["CutHadLeakage_photonsConverted" ,"Reta37_photonsConverted" ,"Rphi33_photonsConverted" ,"weta2_photonsConverted" ,"deltae_photonsConverted" ,"DEmaxs1_photonsConverted" ,"wtot_photonsConverted" ,"fracm_photonsConverted" ,"w1_photonsConverted"]
old_name_unc = ["CutHadLeakage_photonsNonConverted" ,"Reta37_photonsNonConverted" ,"Rphi33_photonsNonConverted" ,"weta2_photonsNonConverted" ,"deltae_photonsNonConverted" ,"DEmaxs1_photonsNonConverted" ,"wtot_photonsNonConverted" ,"fracm_photonsNonConverted" ,"w1_photonsNonConverted"]

new_name = ["rhad1 or rhad0 " ,"r_eta" ,"r_phi" ,"w_eta2" ,"delta_E" ,"e_ratio" ,"w_stot" ,"fracs1" ,"w_eta1"]
converted_file=open("converted_lines.txt","w")
for i,line in enumerate(converted_lines):
	line = line.strip()
	split_line=line.split(";")
	new_line = ";".join(split_line[:-1])
	#  print("i= ",i)
	if ((i-10)%11==0  ) :
		new_line += "; 9999"

	new_line=new_line.replace(':',';')		
	converted_file.write(new_line +"\n")
converted_file.close()

with open("converted_lines.txt","r") as file2:
	lines2=file2.readlines()

values=[[[0 for k in range(11)] for j in range(9)] for i in range(9)]
for cutname,line in enumerate(lines2):
	line=line.strip()
	split_line=line.split(";")
	
	for name in range(len(old_name_con)):
		name_number=[0 for i in range(11)]
		if old_name_con[name] in split_line[0]:
			name_number[name]+=1
			for column in range(1,10):
				values[name][int(column-1)][name_number[name]]=split_line[column]
print(values[0][1])
			#  index=old_name_con.index(old_name_con[0])
			#  print("index is  ",old_name_con[name])

	
