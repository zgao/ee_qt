import sys
import ROOT

import json
import argparse


def PrintProgressBar(index, total):
	if(index %10000==0):
		print_bar=" ["
		for bar in range(20):
			current_fraction = bar/20
			if (index/total) > current_fraction:
				print_bar += "/"
			else:
				print_bar += "."

		print_bar +="] "
		print(print_bar,f" {index/total*100}%")

number=100000000
for i in range(number):
	PrintProgressBar(i,number)
